const services = require('../../models/service.model');
const users = require('../../models/user.model');
const commission = require('../../models/commission.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    data.DateTime = new Date();
    data.Active = 1;
    const service = new services(data);
    
    services.findOne({'Slug': service.Slug}).exec((err, doc)=> {
        if(err) return err;
        else if(doc) {
            res.status(200).json({
                'status': false,
                'message': 'Slug Name already exist',
                'result': []
            })
        } else {
            service.save(function (err) {
                if (err) {
                    return next(err);
                }

                const condition = {
                    'Role': '5cb7fe5c6d2f510b5888660f'
                };
                users.find(condition)
                    .select(['_id','UserName'])
                    .exec(function(err, data){
                        if(err)
                        { 
                                    res.status(501).json({
                                        'status': true,
                                        'message': 'Error',
                                        'result': err
                                    });
                        }
                        else
                        {
                                
                                    console.log(data);
                                    for(i in data){
                                        console.log(data[i]._id);
                                            let commissiondata = {};
                                            commissiondata.ServiceID = service._id;
                                            commissiondata.VendorID = data[i]._id;
                                            commissiondata.CommissionType = service.CommissionType;
                                            commissiondata.Commission = service.Commission;
                                            commissiondata.Active = service.Active;
                                            const com = new commission(commissiondata);
                                            console.log(com);
                                            com.save(function (err) {
                                                if (err) {
                                                    return next(err);
                                                }
        
                                            });
                                    }
                        }
                    });


                res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    'result': service
                })
            })
        }
    })
}



// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    services.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    services.findById(id).populate(['Category' , 'Type' , 'Unit']).exec((err, data) => {
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
});
}

// getOneServicesBySlug
exports.getOneServicesBySlug = function(req, res, next){
    let slug = req.params.slug;
    services.findOne({'Slug': slug}).populate(['Category' , 'Type' , 'Unit']).exec((err, data) => {
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
});
}


// getOneByCategory
exports.getOneByCategory = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    services.find({'Category':id , 'Active':1 }, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data,
        'Table' : 'Services'
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    services.find(condition)
        .sort({_id: 'desc'})
        .populate(['Category' , 'Type' , 'Unit'])
        .exec(function(err, data){
            if(err) res.send(err);
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                    ret._id = row._id;
                    ret.Name = row.Name;
                    ret.Category = row.Category.Name;
                    ret.Type = row.Type.Name;
                    ret.Commission = row.Commission;
                    ret.CommissionType = row.CommissionType;
                    ret.Unit = row.Unit.Symbol;
                    ret.Price = row.Price;
                    return ret;
                })
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        })
}

// getAll
exports.getAllForCommission = function(req, res, next){
    const condition= {
        'Active': 1
    };
    services.find(condition)
        .select(['_id','Name' , 'Commission','CommissionType', 'Price' , 'Active'])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    services.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            console.log(id);
            const condition= {
                'ServiceID': id
            };
            commission.find(condition)
            .exec(function(err, data){
                if(err) res.send(err);
                for(i in data){
                    console.log(data[i]._id + ' Delete');
                    commission.findByIdAndUpdate(data[i]._id, {Active: 0}, (err, data) => {
                        if (err) {
                            return next(err);
                        }
                    });
                }
            })
            //commission.deleteMany({ 'ServiceID': id }, function (err) { console.log(err); });
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': data
        })
    }
});
}

