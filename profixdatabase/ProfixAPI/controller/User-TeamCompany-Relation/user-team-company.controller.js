const teamCompany = require('../../models/teams.model');
const utcr = require('../../models/user-teamcompany-relation.model');

// create
exports.create = function (req, res, next) {
    try{
        let data = req.body;
        utcr.find({'UserID': data.UserID , 'TeamID' : data.TeamID}).then((uctrdata)=>{

                if(uctrdata.length > 0){
                    utcr.updateOne({'TeamID': data.TeamID , 'UserID': data.UserID}, {'Active' : 1 , 'Modified': new Date()}, {new: true}).then((uctrsdata) => {
                        res.status(200).json({
                            'status': true,
                            'message': 'Successfully updated record',
                            'result': uctrsdata
                        })
                    }).catch(err=>{
                        next(err);
                    })

                } else{
                    data.Active = 1;
                    data.DateTime = new Date();
                    const utcs = new utcr(data);
                    utcs.save().then((data) =>{
                        res.status(200).json({
                            'status': true,
                            'message': 'Successfully created record',
                            'result': utcs
                        })
                    }).catch(err=>{
                        next(err);
                    })
                }
        }).catch(err=>{
            next(err);
        })
        
    }catch(err){
        next(err);
    }
}

// update
exports.update = function(req, res, next){
    try{
        let id = req.params._id;
        let UserID = req.body.UserID;

        utcr.update({'TeamID': id , 'UserID': UserID}, {'Active' : 0 , 'Modified': new Date()}, {new: true}).then((data) => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': utcr
            })
        }).catch(err=>{
            next(err);
        })
    }catch(err){
        next(err);
    }
}
