const menu = require('../../models/menu.model');
const menuPermission = require('../../models/menu_permissions.model');

// create
exports.create = function(req, res, next) {
    let data = new menu(req.body);
    data['Modified'] = new Date();
    data['Active'] = 1;
    data.save(function(err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': data
        })
    })
}

// update
exports.update = function(req, res, next) {
    let id = req.params._id;
    let allMenus = req.body;
    const result = updatePermission(allMenus, id);
    if (result == true) {
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated permission',
            'result': allMenus
        })
    } else {
        res.status(200).json({
            'status': false,
            'message': 'Something went wrong',
            'result': result
        })
    }

}

function updatePermission(array, id) {
    const allMenus = array;
    for (let row of allMenus) {
        menuPermission.findOne({ 'RoleID': id, 'MenuID': row.value }).exec(function(err, response) {
            if (err) return err;
            else if (response) {
                menuPermission.findByIdAndUpdate(response._id, { 'Permission': row.internalChecked, 'Modified': new Date() }).exec(function(err, updateData) {
                    if (err) return err;
                })
            } else if (row.internalChecked) {
                let array = {
                    RoleID: id,
                    MenuID: row.value,
                    Permission: row.internalChecked
                };
                const singleArray = new menuPermission(array);
                singleArray.save(function(err) {
                    if (err) return err;
                })
            }
            if (row.internalChildren) {
                updatePermission(row.internalChildren, id);
            }
        })
    }
    return true;
}

// getOne
exports.getOne = function(req, res, next) {
    let id = req.params._id;
    console.log(id)
    menu.findById(id, (err, data) => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
        .catch(exc => {
            console.log(exc)
            return handleError(exc);
        })
}

// getAll
exports.getAll = function(req, res, next) {
    var localArray = [];
    let RoleID = req.params.RoleID;
    menu.find({ 'Active': 1, 'ParentID': null })
        .sort({ _id: 'desc' })
        .exec(function(err, data) {
            if (err) res.send(err);
            else {
                new Promise(async resolve => {
                        await Promise.all(data.map((row) => {
                            return new Promise(async resolvenew => {
                                await menu.find({ 'ParentID': row._id })
                                    .sort({ _id: 'desc' })
                                    .exec(function(err, result) {
                                        if (err) res.send(err);
                                        else {
                                            let tempVal = false;
                                            new Promise(async resolveMenuPermission => {
                                                let checkedVal = false;
                                                await menuPermission.findOne({ 'RoleID': RoleID, 'MenuID': row._id })
                                                    .exec(function(err, menupermlocal) {
                                                        if (err) res.send(err);
                                                        else {
                                                            if (menupermlocal) {
                                                                checkedVal = menupermlocal.Permission;
                                                            }
                                                            resolveMenuPermission(checkedVal)
                                                        }
                                                    })
                                            }).then(async permissionResult => {
                                                let singleArray = {};
                                                singleArray.value = row._id;
                                                singleArray.text = row.name;
                                                singleArray.sequence = row.sequence;
                                                singleArray.checked = permissionResult;
                                                if (result.length != 0) {
                                                    singleArray.children = await getPermissionChild(result, RoleID);
                                                }
                                                localArray.push(singleArray);
                                                resolvenew()
                                            })
                                        }
                                    });
                            })
                        }))
                        resolve(localArray);
                    })
                    .then(result => {
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': result
                        })
                    })
            }
        })
}

function getPermissionChild(result, RoleID) {
    return new Promise(async resolveChildren => {
        let tempArray = [];
        await Promise.all(result.map((res) => {
            return new Promise(async resolveChildrenNew => {
                await menuPermission.findOne({ 'RoleID': RoleID, 'MenuID': res._id })
                    .exec(function(err, child) {
                        if (err) res.send(err);
                        else {
                            let childChecked = false;
                            if (child) {
                                childChecked = child.Permission;
                            }
                            let singleChildrenArray = {}
                            singleChildrenArray.value = res._id;
                            singleChildrenArray.text = res.name;
                            singleChildrenArray.checked = childChecked;
                            singleChildrenArray.sequence = res.sequence;
                            tempArray.push(singleChildrenArray);
                            resolveChildrenNew()
                        }
                    })
            })
        }))
        resolveChildren(tempArray);
    })
}

// getAllByRoles
exports.getAllMenuByRoles = function(req, res, next) {
    var localArray = [];
    let RoleID = req.params.RoleID;
    menu.find({ 'Active': 1, 'ParentID': null })
        .sort({ _id: 'desc' })
        .exec(function(err, data) {
            if (err) res.send(err);
            else {
                new Promise(async resolve => {
                        await Promise.all(data.map((row) => {
                            return new Promise(async resolvenew => {
                                await menu.find({ 'ParentID': row._id })
                                    .sort({ _id: 'desc' })
                                    .exec(function(err, result) {
                                        if (err) res.send(err);
                                        else {
                                            let tempVal = false;
                                            new Promise(async resolveMenuPermission => {
                                                let checkedVal = false;
                                                await menuPermission.findOne({ 'RoleID': RoleID, 'MenuID': row._id })
                                                    .exec(function(err, menupermlocal) {
                                                        if (err) res.send(err);
                                                        else {
                                                            if (menupermlocal) {
                                                                checkedVal = menupermlocal.Permission;
                                                            }
                                                            resolveMenuPermission(checkedVal)
                                                        }
                                                    })
                                            }).then(async permissionResult => {
                                                let singleArray = {};
                                                singleArray._id = row._id;
                                                singleArray.name = row.name;
                                                singleArray.url = row.url;
                                                singleArray.icon = row.icon;
                                                singleArray.title = row.title;
                                                singleArray.class = row.class;
                                                singleArray.sequence = row.sequence;
                                                if (result.length != 0) {
                                                    singleArray.children = await getMenuChild(result, RoleID);
                                                }
                                                if (permissionResult || (singleArray.children && singleArray.children.length != 0))
                                                    localArray.push(singleArray);
                                                resolvenew()
                                            })
                                        }
                                    });
                            })
                        }))
                        resolve(localArray);
                    })
                    .then(result => {
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': result
                        })
                    })
            }
        })
}

function getMenuChild(result, RoleID) {
    return new Promise(async resolveChildren => {
        let tempArray = [];
        await Promise.all(result.map((res) => {
            return new Promise(async resolveChildrenNew => {
                await menuPermission.findOne({ 'RoleID': RoleID, 'MenuID': res._id })
                    .exec(function(err, child) {
                        if (err) res.send(err);
                        else {
                            let childChecked = false;
                            if (child) {
                                childChecked = child.Permission;
                            }
                            let singleChildrenArray = {}
                            singleChildrenArray._id = res._id;
                            singleChildrenArray.name = res.name;
                            singleChildrenArray.url = res.url;
                            singleChildrenArray.icon = res.icon;
                            singleChildrenArray.title = res.title;
                            singleChildrenArray.class = res.class;
                            singleChildrenArray.sequence = res.sequence;
                            if (childChecked)
                                tempArray.push(singleChildrenArray);
                            resolveChildrenNew()
                        }
                    })
            })
        }))
        resolveChildren(tempArray);
    })
}


// getAllForBackup
exports.getAllForBackup = function(req, res, next) {
    var localArray = [];
    menu.find({ 'Active': 1, 'ParentID': null })
        .sort({ _id: 'desc' })
        .exec(function(err, data) {
            if (err) res.send(err);
            else {
                new Promise(async resolve => {
                        await Promise.all(data.map((row) => {
                            return new Promise(async resolvenew => {
                                await menu.find({ 'ParentID': row._id })
                                    .sort({ _id: 'desc' })
                                    .exec(function(err, result) {
                                        if (err) res.send(err);
                                        else {
                                            let singleArray = {};
                                            singleArray._id = row._id;
                                            singleArray.name = row.name;
                                            singleArray.url = row.url;
                                            singleArray.icon = row.icon;
                                            singleArray.title = row.title;
                                            singleArray.class = row.class;
                                            singleArray.sequence = row.sequence;
                                            if (result.length != 0)
                                                singleArray.children = result;
                                            localArray.push(singleArray);
                                            resolvenew()
                                        }
                                    });
                            })
                        }))
                        resolve(localArray);
                    })
                    .then(result => {
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': result
                        })
                    })
            }
        })
}

// delete
exports.delete = function(req, res, next) {
    let id = req.params._id;
    console.log(id);
    menu.findByIdAndRemove(id, (err, data) => {
        if (err) {
            return next(err);
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully deleted record',
                'result': data
            })
        }
    });
}