const roles = require('../../models/role.model');
const users = require('../../models/user.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    console.log(req.body);
    data.DateTime = new Date();
    data.Active = 1;
    console.log(data);

    const role = new roles(data);
    role.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created role',
            'result': role
        })
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    const HiddenRole = ['5cbd51bd56d3cb1e606a676e' , '5cb7fe746d2f510b58886610' , '5cb7fe5c6d2f510b5888660f' , '5cb7fe496d2f510b5888660e' , '5cb5b2bb7d799119b01c6226' , '5cb5b2b17d799119b01c6225'];

    if(HiddenRole.indexOf(id) > -1) {
        res.status(200).json({
            'status': true,
            'message': 'Record not updated',
            'result': data
        });
    }
    else{
        roles.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
            if(err) {
                return next(err);
            }
            else {
                res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data
            })
        }
        })
    }
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    roles.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getRelatedRecordOfCategory
exports.getRelatedRecordOfRole = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    users.find({'Role':id , 'Active':1 }, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}


// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1,
        '_id' : {
            $ne : '5d0a16fdb457473128b080be'
        }
    };
    roles.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    const HiddenRole = ['5cbd51bd56d3cb1e606a676e' , '5cb7fe746d2f510b58886610' , '5cb7fe5c6d2f510b5888660f'  , '5cb5b2bb7d799119b01c6226' , '5cb5b2b17d799119b01c6225'];

    if(HiddenRole.indexOf(id) > -1) {
        res.status(200).json({
            'status': true,
            'message': 'Record not updated',
            'result': []
        })
    }
    else
        {
            roles.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
                if(err) {
                    return next(err);
                }
                else{
                    res.status(200).json({
                    'status': true,
                    'message': 'Successfully deleted record',
                    'result': data
                })
            }
        })
            ;
        }
}

//Get Role ID by Key
exports.getRoleIDByKey = function(req, res, next){
    let key = req.params.key;
    const condition= {
        'Key' : key 
    };
    roles.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}