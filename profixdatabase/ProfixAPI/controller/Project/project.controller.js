const projects = require('../../models/project.model');
const OrderActivity = require('../../models/orderAcitivities.model');
const OrderAssignment = require('../../models/orderAssignment.model');
const OrderPayment = require('../../models/orderPayment.model');
const OrderMilestone = require('../../models/milestone.model');
const OrderReview = require('../../models/reviews.model')
const vendortransactions = require('../../models/vendorTransactions.model');
const setting = require('../../models/settings.model');
const User = require('../../models/user.model');

const email = require('../../routes/email-functions');

var dateFormat = require('dateformat');
const PaymentAmountNet = 5000;
//createOrderActivity
function createOrderActivity(data) {
    
    const activityData = new OrderActivity(data);
    activityData.save(function(err) {
        if(err){
            console.log(err);
            return false;
        }
        else return true;
    })
}

// create
exports.create = async function (req, res, next) {
    let data = req.body; 
    let oID = 0;   
    let CustomerEmail = data.Basic.CustomerEmail;
    
    oID = await new Promise((resolve,reject)=>{ projects.findOne().sort({_id: 'desc'}).exec(function(err, result) {                  
        if(err)
        return next(err);
        else{
            if(result)
            resolve(result.OrderID);
            else{
                resolve(0)  
            } 
        } })});      
        
        new Promise(async(resolve,reject)=>{
            await Promise.all(data.Services.map(async(service) => {       
                var order = {};
                order  = Object.assign(data.Basic , data.Payment);
                order.ServiceID = service._id;
                order.Services = {
                    ID : service._id,
                    Name : service.Name,
                    Width: service.Width,
                    Height : service.Height,
                    Price : service.Price,
                    Amount : service.Amount,
                    TotalAmount : service.TotalAmount,
                    GST : service.GST,
                    ScheduledServiceProvider : service.ScheduledServiceProvider,

                }
                order.Status = 'New';
                order.PaymentAmount  = service.TotalAmount;
                order.Comments  = service.Comments;
                order.ScheduledServiceProvider  = service.ScheduledServiceProvider;
                order.DateTime = new Date();
                order.Active = 1;
                let OID = 0;
                const cust = new projects(order);
                if(data.UserRole == 'Vendor'){
                    var vendorTransaction = {
                        VendorID: cust.CreatedBy,
                        OrderID: cust._id,
                        OrderAMT: cust.PaymentAmount,
                        DateTime: new Date(),
                        IsSettled : 0,
                        SettledAMT : 0.0
                    }
                    console.log("vendor")
                    const vtran = new vendortransactions(vendorTransaction);
                    vtran.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                    })
                }       
                cust.OrderID = ++oID;
               await new Promise((resolve2,reject2)=>{  cust.save().then(afterSave=> {                      
                        let activityData = {};
                        activityData.OrderID = cust._id;
                        activityData.UserID = cust.Customer._id;
                        activityData.Status = cust.Status;
                        activityData.Message = 'New Order Created';
                        activityData.DateTime = new Date();
                        activityData.Active = 1;
                        createOrderActivity(activityData);                         
                        
                        //console.log(cust,afterSave);
                        var orderdate = dateFormat(cust.DateTime, "dd-mm-yyyy");
                        var discountAmt = cust.DiscountAmount ? cust.DiscountAmount : 0;
                        var gstIs = (cust.PaymentAmount * cust.Services.GST)/100;
                        var Totalis = cust.PaymentAmount;
                        var replacementsValue = {
                            Ordernumber : cust.OrderID,              
                            Orderdate : orderdate,
                            TotalAmountIs : Totalis,
                            GST : gstIs,
                            discountAmt: discountAmt,
                            ServiceName : cust.Services.Name,
                            ServicePrice : cust.Services.TotalAmount
                        };
                
                        email(CustomerEmail,'Your Profix Order Receipt from '+orderdate,'admin-order-invoice',replacementsValue);

                        resolve2("");
                    })})    
                console.log("programme completed");           
            }))
            resolve("");
        }).then(data=>{
            console.log("Done");
            res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    'result': data
                }) 
        })

}
// create
exports.createProject = async function (req, res, next) {
    let data = req.body;

    let oID = 0;   
    oID = await new Promise((resolve,reject)=>{ projects.findOne().sort({_id: 'desc'}).exec(function(err, result) {                  
        if(err)
        return next(err);
        else{           
            if(result){
                resolve(result.OrderID);
            }
            else{
                resolve(0);
            }
        }    
          
    })});      
        
        new Promise(async(resolve,reject)=>{
            await Promise.all(data.Services.map(async(service) => {       
                var order = {};
                //order  = Object.assign(data.Basic , data.Payment);
                order.ProjectName = data.ProjectName;
                order.Customer = data.Customer._id;
                order.PaymentMode = data.PaymentMode,
                order.ServiceID = service._id;
                order.PaymentAmount= (service.Width * service.Width * service.Price) + ((service.Width * service.Width * service.Price) * (service.GST/100)) ;
                order.Locality= data.Locality;
                order.City= data.City;
                order.State = data.State;
                order.Address = data.Address;
                order.Services = {
                    ID : service._id,
                    Name : service.Name,
                    Width: service.Width,
                    Height : service.Height,
                    Price : service.Price,
                    Amount : service.Amount,
                    TotalAmount : service.TotalAmount,
                    GST : service.GST,
                    ScheduledServiceProvider : service.ScheduledServiceProvider,

                }
                order.ProjectImages = service.ProjectImages;
                order.Status = 'New';
                order.PaymentAmount  = data.PaymentAmount;
                order.Comments  = service.Comments;
                order.ScheduledServiceProvider  = service.ScheduledServiceProvider;
                order.DateTime = new Date();
                order.Active = 1;
                
                const cust = new projects(order);
                if(data.UserRole == 'Vendor'){
                    var vendorTransaction = {
                        VendorID: cust.CreatedBy,
                        OrderID: cust._id,
                        OrderAMT: cust.PaymentAmount,
                        DateTime: new Date(),
                        IsSettled : 0,
                        SettledAMT : 0.0
                    }
                    console.log("vendor")
                    const vtran = new vendortransactions(vendorTransaction);
                    vtran.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                    })
                }       
                cust.OrderID = ++oID;
               await new Promise((resolve2,reject2)=>{  cust.save().then(afterSave=> {                      
                        let activityData = {};
                        activityData.OrderID = cust._id;
                        activityData.UserID = cust.Customer._id;
                        activityData.Status = cust.Status;
                        activityData.Message = 'New Order Created';
                        activityData.DateTime = new Date();
                        activityData.Active = 1;
                        createOrderActivity(activityData);                         
                        
                        console.log(cust.OrderID,afterSave);
                        resolve2("");
                    })})    
                console.log("programme completed");           
            }))
            resolve("");
        }).then(data=>{
            console.log("asas");
            res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    'result': data,
                    'OID' : oID
                }) 
        })
}


exports.assignOrder = function (req, res, next) {
    let data = req.body;

    /*OrderAssignment.findOne({'OrderID' : data.OrderID , 'UserID': data.UserID , 'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data.length){
                    res.status(200).json({
                        'status': false,
                        'message': 'Already project assign',
                        'result': data
                    })
                }
                else{*/
                    OrderAssignment.updateMany({'OrderID' : data.OrderID} , {'Active': 0}).exec( function (updaterr , updateData) {
                        if (updaterr) {
                            return next(updaterr);
                        }
                        else {
                            var orderAssign = {
                                'OrderID' : data.OrderID,
                                'UserID': data.AssignUser,
                                'DateTime': new Date(),
                                'Active': 1
                            }
                            const orderAssignment = new OrderAssignment(orderAssign);

                            console.log(orderAssignment)
                            orderAssignment.save(function (err) {
                                if (err) {
                                    return next(err);
                                }
                            });

                            var orderActivity = {
                                'OrderID' : data.OrderID,
                                'UserID': data.AssignUser,
                                'Status': 'In-Route',
                                'Message': 'Order In-Route',
                                'DateTime': new Date(),
                                'Active': 1
                            }
                            const orderAct = new OrderActivity(orderActivity);
                            console.log(orderAct)
                            orderAct.save(function (err) {
                                if (err) {
                                    return next(err);
                                }
                            });

                            var UpdateOrder = {
                                'Status' : 'In-Route',
                                'AssignTo': data.AssignUser
                            }

                            projects.findByIdAndUpdate(data.OrderID, UpdateOrder, {new: true}, (err, data) => {
                                if(err) {
                                    return next(err);
                                }
                                else {
                                    res.status(200).json({
                                    'status': true,
                                    'message': 'Successfully updated record',
                                    'result': data
                                    })
                                }
                            });

                        }

                    })
/*
                }
            }
    })*/


}

exports.acceptOrder = function (req, res, next) {
    let Orderdata = req.body;

    OrderAssignment.findOne({'OrderID' : Orderdata.OrderID ,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    res.status(200).json({
                        'status': false,
                        'message': 'Already project assign',
                        'result': data
                    })
                }
                else{

                    var orderAssign = {
                        'OrderID' : Orderdata.OrderID,
                        'UserID': Orderdata.AssignUser,
                        'DateTime': new Date(),
                        'Active': 1
                    }
                    const orderAssignment = new OrderAssignment(orderAssign);

                    console.log(orderAssignment)
                    orderAssignment.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                    });

                    var orderActivity = {
                        'OrderID' : Orderdata.OrderID,
                        'UserID': Orderdata.AssignUser,
                        'Status': 'In-Route',
                        'Message': 'Order In-Route',
                        'DateTime': new Date(),
                        'Active': 1
                    }
                    createOrderActivity(orderActivity);

                    var orderActivityProcess = {
                        'OrderID' : Orderdata.OrderID,
                        'UserID': Orderdata.AssignUser,
                        'Status': 'In Process',
                        'Message': 'Order In Process',
                        'DateTime': new Date(),
                        'Active': 1
                    }
                    createOrderActivity(orderActivityProcess);


                    var UpdateOrder = {
                        'Status' : 'In Process',
                        'AssignTo': Orderdata.AssignUser
                    }

                    projects.findByIdAndUpdate(Orderdata.OrderID, UpdateOrder, {new: true}, (err, data) => {
                        if(err) {
                            return next(err);
                        }
                        else {
                            res.status(200).json({
                            'status': true,
                            'message': 'Successfully updated record',
                            'result': data
                            })
                        }
                    });
                }
            }
    })


}


// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    var user1  = Object.assign(data.Basic , data.Account);;
    data['Modified'] = new Date();
    projects.findByIdAndUpdate(id, user1, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    projects.findOne({'_id': id , 'Active' : 1})
        .sort({_id: 'desc'})
        .populate(['Customer','Locality','City','State' , 'ServiceID'] )
        .exec(function(err, data){
            if(err){
                res.send(err);
            }
            else{
            }
            OrderAssignment.findOne({'OrderID': id, 'Active' : 1})
                .select(['OrderID' , 'UserID._id' , 'UserID.FirstName','UserID.MiddleName','UserID.LastName'])
                .populate(['UserID'])
                .exec(function (ordererr , orderdata) {
                    if(err){
                        res.send(err);
                    }
                    else{
                        if(orderdata){
                            res.status(200).json({
                                'status': true,
                                'message': 'Success',
                                'result': data,
                                'OrderAssign': orderdata.UserID.FirstName + " "+orderdata.UserID.MiddleName +" " +orderdata.UserID.LastName ,
                                "UserID":orderdata.UserID._id,
                                'OrderAssignData': orderdata
                            })
                        }
                        else{
                            res.status(200).json({
                                'status': true,
                                'message': 'Success',
                                'result': data,
                                'OrderAssign': '',
                                "UserID":null
                            })
                        }

                    }

                })
        })

}

// getAll projects
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };

    condition.UserID = '5cc04ebe00445f04a07af20f';

    OrderAssignment.find(condition)
        .sort({_id: 'desc'})
        .populate([{path : 'OrderID', populate : [{path : 'Customer'},{path : 'Locality'} , {path : 'City'},{path : 'State'}]}, 'UserID' ,{path :  'MilestoneID', populate : [{path : 'Service'},{path : 'UserID'}]}])
        .exec(function(err, data){
            if(err) res.send(err);
            let sentData = [];
            let i = 1;

            res.status(200).json({
                'status': true,
                'message': 'Success',
                'data': data
            })
        })

}

// getAll projects
exports.getForProject = function(req, res, next){
    const condition= {
        'Status': 'New'
    };   

    projects.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
           
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'data': data
            })
        })

}

// getAll gg
exports.getAllOrderServiceProvicer = function(req, res, next){
    const condition= {
        'Active': 1
    };
    var SelectedAssignTo = req.params.SelectedAssignTo;
    var Role = req.params.Role;
    if (SelectedAssignTo != 0) {
        if( Role == 'Service Provider'){
            condition.UserID = SelectedAssignTo;
            OrderAssignment.find(condition)
                .sort({_id: 'desc'})
                .populate([{path : 'OrderID', populate : [{path : 'Customer'},{path : 'Locality'} , {path : 'City'},{path : 'State'}]}, 'UserID' ,{path :  'MilestoneID', populate : [{path : 'Service'},{path : 'UserID'}]}])
                .exec(function(err, data){
                    if(err) res.send(err);
                    let sentData = [];
                    let i = 1;
                    sentData = data.map(row => {
                        let ret = {};

                    ret.OrderID = row.OrderID._id;
                    ret.OrderAssignID = row._id;
                    ret._id = row.OrderID._id;
                    ret.name = row.OrderID.ProjectName+" #"+row.OrderID.OrderID+ " " +(row.MilestoneID ? row.MilestoneID.Title :'')+"<br><small class='text-muted'><i>Budget: &#x20b9;"+(row.MilestoneID ? row.MilestoneID.Amount : row.OrderID.PaymentAmount)+"<i></small>";

                    ret.contacts = "<div class='project-members'><a href='javascript:void(0)'><img src='assets/img/avatars/male.png' class='offline' alt='user'></a> </div> ";
                    if(row.MilestoneID){
                        if(row.MilestoneID.Status == 'New'){
                            ret.status = "<span class='label label-default'>"+row.MilestoneID.Status+"</span>";
                            ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'15'+"'><div class='progress-bar'></div></div></td>";
                        }
                        else{
                            ret.status = "<span class='label label-success'>"+row.MilestoneID.Status+"</span>";
                            ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'30'+"'><div class='progress-bar'></div></div></td>";
                        }
                    }
                    else{
                        if(row.OrderID.Status == 'New'){
                            ret.status = "<span class='label label-default'>"+row.OrderID.Status+"</span>";
                            ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'15'+"'><div class='progress-bar'></div></div></td>";
                        }
                        else{
                            ret.status = "<span class='label label-success'>"+row.OrderID.Status+"</span>";
                            ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'30'+"'><div class='progress-bar'></div></div></td>";
                        }
                    }


                    if(row.UserID){
                        ret.AssignTo = row.UserID.FirstName + " " + row.UserID.MiddleName + " " +row.UserID.LastName;
                    }
                    else{
                        ret.AssignTo = "--------";
                    }
                    ret.starts= row.OrderID.ScheduledServiceProvider;
                    ret.customer = row.OrderID.Customer.FirstName + " " + row.OrderID.Customer.MiddleName + " " +row.OrderID.Customer.LastName;
                    ret.tracker = "<span class='onoffswitch'><input type='checkbox' name='start_interval' class='onoffswitch-checkbox' id='st"+row._id+"'><label class='onoffswitch-label' for='st"+row._id+"'><span class='onoffswitch-inner' data-swchon-text='ON' data-swchoff-text='OFF'></span><span class='onoffswitch-switch'></span></label></span>";
                    ret.comments = row.OrderID.Comments ? row.OrderID.Comments : '';
                    ret.action = "<button class='btn btn-xs' (ngClick)='open("+row.OrderID._id+")'>Open case</button> <button class='btn btn-xs btn-danger pull-right' style='margin-left:5px'>Delete Record</button> <button class='btn btn-xs btn-success pull-right'>Save Changes</button> ";
                    return ret;
                });
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'data': sentData
                    })
                })
        }
        else{
            condition.CreatedBy =  SelectedAssignTo;
            projects.find(condition)
                .sort({_id: 'desc'})
                .populate(['Customer', 'AssignTo'])
                .exec(function(err, data){
                    if(err) res.send(err);
                    let sentData = [];
                    let i = 1;
                    sentData = data.map(row => {
                        let ret = {};

                    ret._id = row._id;
                    ret.name = row.ProjectName+" #"+row.OrderID+"<br><small class='text-muted'><i>Budget: &#x20b9;"+row.PaymentAmount+"<i></small>";

                    ret.contacts = "<div class='project-members'><a href='javascript:void(0)'><img src='assets/img/avatars/male.png' class='offline' alt='user'></a> </div> ";
                    if(row.Status == 'New'){
                        ret.status = "<span class='label label-default'>"+row.Status+"</span>";
                        ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'15'+"'><div class='progress-bar'></div></div></td>";
                    }
                    else{
                        ret.status = "<span class='label label-success'>"+row.Status+"</span>";
                        ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'30'+"'><div class='progress-bar'></div></div></td>";
                    }

                    if(row.AssignTo){
                        ret.AssignTo = row.AssignTo.FirstName + " " + row.AssignTo.MiddleName + " " +row.AssignTo.LastName;
                    }
                    else{
                        ret.AssignTo = "--------";
                    }
                    ret.starts= row.ScheduledServiceProvider;
                    ret.customer = row.Customer.FirstName + " " + row.Customer.MiddleName + " " +row.Customer.LastName;
                    ret.tracker = "<span class='onoffswitch'><input type='checkbox' name='start_interval' class='onoffswitch-checkbox' id='st"+row._id+"'><label class='onoffswitch-label' for='st"+row._id+"'><span class='onoffswitch-inner' data-swchon-text='ON' data-swchoff-text='OFF'></span><span class='onoffswitch-switch'></span></label></span>";
                    ret.comments = row.Comments ? row.Comments : '';
                    ret.action = "<button class='btn btn-xs' (ngClick)='open("+row._id+")'>Open case</button> <button class='btn btn-xs btn-danger pull-right' style='margin-left:5px'>Delete Record</button> <button class='btn btn-xs btn-success pull-right'>Save Changes</button> ";
                    return ret;
                });
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'data': sentData
                    })
                })
        }

    }
    else{
        //condition.Status =  'New';
        projects.find(condition)
            .sort({_id: 'desc'})
            .populate([ {path : 'Customer', select :{'FirstName':1 , 'MiddleName':1 , 'LastName':1}}, {path :'AssignTo', select :{'FirstName':1 , 'MiddleName':1 , 'LastName':1}}])
            .select(['ProjectName' , 'PaymentAmount' , 'Status' , 'OrderID' , 'ScheduledServiceProvider'])
            .exec(function(err, data){
                if(err) res.send(err);
            //     let sentData = [];
            //     let i = 1;
            //     sentData = data.map(row => {
            //         let ret = {};

            //     ret._id = row._id;
            //     ret.name = row.ProjectName+" #"+row.OrderID+"<br><small class='text-muted'><i>Budget: &#x20b9;"+row.PaymentAmount+"<i></small>";

               
            //     if(row.Status == 'New'){
            //         ret.status = "<span class='label label-default'>"+row.Status+"</span>";
                    
            //     }
            //     else{
            //         ret.status = "<span class='label label-success'>"+row.Status+"</span>";
                    
            //     }

            //     if(row.AssignTo){
            //         ret.AssignTo = row.AssignTo.FirstName + " " + row.AssignTo.MiddleName + " " +row.AssignTo.LastName;
            //     }
            //     else{
            //         ret.AssignTo = "--------";
            //     }
            //     ret.starts= row.ScheduledServiceProvider;
            //     ret.customer = row.Customer ? row.Customer.FirstName + " " + row.Customer.MiddleName + " " +row.Customer.LastName : 'test';
               
            //     ret.comments = row.Comments ? row.Comments : '';
            //     ret.action = "<button class='btn btn-xs' (ngClick)='open("+row._id+")'>Open case</button> <button class='btn btn-xs btn-danger pull-right' style='margin-left:5px'>Delete Record</button> <button class='btn btn-xs btn-success pull-right'>Save Changes</button> ";
            //     return ret;
            // });
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'data': data
                })
            })
    }


}

//getAdminCommission
exports.getAdminCommission = function(req, res, next){
    var AdminCommissionValue = 0;
    setting.findOne({'metaKey':'AdminCommission'}).exec( (settingerr , settingdata)=>{
        if(settingdata){
            AdminCommissionValue = settingdata.metaValue;
        }
        else{
            AdminCommissionValue = 10;   // if no data receive from api
        }
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': AdminCommissionValue
        })
    });
}


// getGSTValue  
exports.getGSTValue = function(req, res, next){
    var GSTValue = 0;
    setting.findOne({'metaKey':'GST'}).exec( (settingerr , settingdata)=>{
        if(settingdata){
            GSTValue = settingdata.metaValue;
        }
        else{
            GSTValue = 18;   // if no data receive from api
        }
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': GSTValue
        })
    });
}
// getAll
exports.getAllOrderPriceUnder5000 = function(req, res, next){
    let id = req.params.id;
    var UnderPaymentAmount = 0;
    setting.findOne({'metaKey' : 'SplitOrder'}).sort({_id: 'desc'}).exec( (settingerr , settingdata)=>{
        if(settingdata){
            UnderPaymentAmount = settingdata.metaValue;
        }
        else{
            UnderPaymentAmount = 11000;
        }
        User.findById(id).exec( (err , userdata)=>{
            var skill = userdata.Services;
            if(skill){
                var userSkill = skill.map( x=> {
                    return x.id;
                });
            }
            else{
                var userSkill = null;
            }
            const condition= {
                'Active': 1,
                'Status': 'New',
                'PaymentAmount' :{'$lte' : UnderPaymentAmount },
               // 'City' :userdata.City
            };
            if(userSkill){
                condition.ServiceID = {'$in' : userSkill};
            }
            projects.find(condition)
                .sort({_id: 'desc'})
                .populate(['Customer', 'AssignTo'])
                .exec().then(function(data){
                    
                    let sentData = [];
                    let i = 1;
                    if(data){
                        sentData = data.map(row => {
                            let ret = {};
            
                            ret._id = row._id;
                            ret.name = row.ProjectName+" #"+row.OrderID+"<br><small class='text-muted'><i>Budget: &#x20b9;"+row.PaymentAmount+"<i></small>";
            
                            ret.contacts = "<div class='project-members'><a href='javascript:void(0)'><img src='assets/img/avatars/male.png' class='offline' alt='user'></a> </div> ";
                            if(row.Status == 'New'){
                                ret.status = "<span class='label label-default'>"+row.Status+"</span>";
                                ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'15'+"'><div class='progress-bar'></div></div></td>";
                            }
                            else{
                                ret.status = "<span class='label label-success'>"+row.Status+"</span>";
                                ret.est = "<td><div class='progress progress-xs' data-progressbar-value='"+'30'+"'><div class='progress-bar'></div></div></td>";
                            }
            
                        if(row.AssignTo){
                            ret.AssignTo = row.AssignTo.FirstName + " " + row.AssignTo.MiddleName + " " +row.AssignTo.LastName;
                        }
                        else{
                            ret.AssignTo = "--------";
                        }
                            ret.starts= row.ScheduledServiceProvider;
                            ret.customer = row.Customer ?row.Customer.FirstName + " " + row.Customer.MiddleName + " " +row.Customer.LastName : '';
                            ret.tracker = "<span class='onoffswitch'><input type='checkbox' name='start_interval' class='onoffswitch-checkbox' id='st"+row._id+"'><label class='onoffswitch-label' for='st"+row._id+"'><span class='onoffswitch-inner' data-swchon-text='ON' data-swchoff-text='OFF'></span><span class='onoffswitch-switch'></span></label></span>";
                            ret.comments = row.Comments ? row.Comments : '';
                            ret.action = "<button class='btn btn-xs' (ngClick)='open("+row._id+")'>Open case</button> <button class='btn btn-xs btn-danger pull-right' style='margin-left:5px'>Delete Record</button> <button class='btn btn-xs btn-success pull-right'>Save Changes</button> ";
                            return ret;
                        });
                    }
                    
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'data': sentData
                    })
                }).catch(err=>{
                    console.log(err)
                    next(err);
                })
        })
       
    })
    

}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    projects.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': data
        })
    }
});
}

// submit Order by service provider
exports.submitOrderBySP = function (req, res, next) {
    let Orderdata = req.body;



    OrderAssignment.findOne({'OrderID' : Orderdata.OrderID ,'UserID': Orderdata.AssignUser,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){


                    if(Orderdata.Submit.Status == 'Close'){

                        var orderActivityReview = {
                            'OrderID' : Orderdata.OrderID,
                            'UserID': Orderdata.AssignUser,
                            'Status': 'Review',
                            'Message': "Order Review successfully",
                            'DateTime': new Date(),
                            'Active': 1
                        }
                        createOrderActivity(orderActivityReview);

                        var orderActivityCompleted = {
                            'OrderID' : Orderdata.OrderID,
                            'UserID': Orderdata.AssignUser,
                            'Status': Orderdata.Submit.Status,
                            'Message': Orderdata.Submit.Notes,
                            'DateTime': new Date(),
                            'Active': 1
                        }

                        createOrderActivity(orderActivityCompleted);
                        var UpdateOrderStatus = {
                            'Status' : Orderdata.Submit.Status
                        }

                        projects.findByIdAndUpdate(Orderdata.OrderID, UpdateOrderStatus, {new: true}, (err, data) => {
                            if(err) {
                                return next(err);
                            }
                            else {

                            }
                        });
                    }
                    else{
                        var orderActivity = {
                            'OrderID' : Orderdata.OrderID,
                            'UserID': Orderdata.AssignUser,
                            'Status': Orderdata.Submit.Status,
                            'Message': Orderdata.Submit.Notes,
                            'DateTime': new Date(),
                            'Active': 1
                        }

                        createOrderActivity(orderActivity);
                    }


                    var UpdateOrder = {
                        'Status' : Orderdata.Submit.Status,
                        'Notes': Orderdata.Submit.Notes
                    }

                    OrderAssignment.findByIdAndUpdate(data._id, UpdateOrder, {new: true}, (err, data) => {
                        if(err) {
                            return next(err);
                        }
                        else {
                            res.status(200).json({
                            'status': true,
                            'message': 'Successfully updated record',
                            'result': data
                        })
                    }
                });
                }
            }
        })
}

// getAllOrderActivitiesByOrderID
exports.getAllOrderActivitiesByOrderID = function(req, res, next){
    var OrderID = req.params.OrderID;
    const condition= {
        'Active': 1,
        'OrderID': OrderID
    };
    OrderActivity.find(condition).sort({'_id': -1}).exec((err, data) => {
        if(err) return err;
        else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// submit Order by service provider
exports.paymentCollect = function (req, res, next) {
    let Orderdata = req.body;

    OrderAssignment.findOne({'OrderID' : Orderdata.OrderID ,'UserID': Orderdata.AssignUser,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    var orderActivity = {
                        'OrderID' : Orderdata.OrderID,
                        'UserID': Orderdata.AssignUser,
                        'Status': data.Status,
                        'Message': 'Order Payment successfully paid',
                        'DateTime': new Date(),
                        'Active': 1
                    }
                    createOrderActivity(orderActivity);

                    var PaymentOrder = {
                        'OrderAssignID' : data._id,
                        'UserID': Orderdata.AssignUser,
                        'PaymentMode': Orderdata.Payment.PaymentMode,
                        'Notes': Orderdata.Payment.Notes,
                        'PaymentAmount': Orderdata.PaymentAmount,
                        'DateTime': new Date(),
                        'Active': 1
                    }

                    var payOrder = new OrderPayment(PaymentOrder);
                    payOrder.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                        else{
                            res.status(200).json({
                                'status': true,
                                'message': 'Order payment successfully',
                                'result': payOrder
                            })
                        }
                    });
                }
            }
        })
}

//  get Information Payment
exports.getInfoPayment = function(req, res, next){
    let OrderID = req.params.OrderID;
    let UserID = req.params.UserID;

    OrderAssignment.findOne({'OrderID' : OrderID ,'UserID': UserID,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    OrderPayment.find({'OrderAssignID' : data._id , 'UserID' : UserID }).exec((OrderPayerr , OrderPaydata)=>{
                        if(OrderPayerr) return next(OrderPayerr);
                        else{
                            res.status(200).json({
                                'status': true,
                                'message': 'Success',
                                'result': OrderPaydata
                            })
                        }
                    })
                }
            }
        })
}
exports.createMilestoneAndAssignOrder = function(req, res, next){
    let MilestoneData = req.body;

    console.log(MilestoneData.Milestone);

    const om = new OrderMilestone(MilestoneData.Milestone);
    om.DateTime = new Date();
    om.Active = 1;
    om.Status = 'In-Route';
    om.save(function (err) {
        if (err) {
            return next(err);
        }
    });

    var orderActivity = {
        'OrderID' : MilestoneData.OrderID,
        'UserID': MilestoneData.Milestone.UserID,
        'Status': 'In-Route',
        'Message': 'Milestone create and assigned',
        'DateTime': new Date(),
        'Active': 1
    }
    createOrderActivity(orderActivity);

    var orderAssign = {
        'OrderID'  :  MilestoneData.OrderID,
        'UserID' : MilestoneData.Milestone.UserID,
        'MilestoneID' : om._id,
        'Status' : 'In-Route',
        'Notes' : 'Milestone In-Route',
        'DateTime': new Date(),
        'Active': 1
    }

    const orderAssignment = new OrderAssignment(orderAssign);

    console.log(orderAssignment)
    orderAssignment.save(function (err) {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': om
            })
        }
    });
}

// delete Order Milestone
exports.deleteOrderMilestone = function(req, res, next){
    let id = req.params._id;

    OrderAssignment.findOne({'MilestoneID' : id}).exec((err , data) => {
        if (err) {
            console.log(err);
            return next(err);
        }
        else{
            var orderact = {
                'OrderID': data.OrderID,
                'UserID': data.UserID,
                'Status': data.Status,
                'Message': 'Milestone deleted',
                'DateTime': new Date(),
                'Active': 1
            }
            createOrderActivity(orderact);

        }
    });

    OrderMilestone.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            OrderAssignment.update({'MilestoneID' : id}, {Active: 0}, (oserr, osdata) => {
                    if (oserr) {
                        return next(oserr);
                    }
                    else{
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully deleted record',
                        'result': osdata
                    })
                }
            });
        }
    });
}



// send Review
exports.sendReview = function (req, res, next) {
    let Orderdata = req.body;

    OrderAssignment.findOne({'OrderID' : Orderdata.OrderID ,'UserID': Orderdata.AssignUser,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    var orderActivity = {
                        'OrderID' : Orderdata.OrderID,
                        'UserID': Orderdata.AssignUser,
                        'Status': data.Status,
                        'Message': 'Order review has been send successfully',
                        'DateTime': new Date(),
                        'Active': 1
                    }
                    createOrderActivity(orderActivity);

                    var ReviewOrder = {
                        'OrderAssignID' : data._id,
                        'UserID': Orderdata.AssignUser,
                        'Rating': Orderdata.Review.Rating,
                        'Review': Orderdata.Review.Review,
                        'DateTime': new Date(),
                        'Active': 1
                    }

                    var reviewOrder = new OrderReview(ReviewOrder);
                    reviewOrder.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                        else{
                            res.status(200).json({
                                'status': true,
                                'message': 'Order payment successfully',
                                'result': reviewOrder
                            })
                        }
                    });
                }
            }
        })
}


//  get Information Payment
exports.getInfoReview = function(req, res, next){
    let OrderID = req.params.OrderID;
    let UserID = req.params.UserID;

    OrderAssignment.findOne({'OrderID' : OrderID ,'UserID': UserID,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    OrderReview.find({'OrderAssignID' : data._id , 'UserID' : UserID }).exec((OrderPayerr , OrderPaydata)=>{
                        if(OrderPayerr) return next(OrderPayerr);
                else{
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': OrderPaydata
                        })
                    }
                })
                }
            }
        })
}


//  get All Review DataInformation Payment
exports.getReviews = function(req, res, next){
    let OrderID = req.params.OrderID;
    let UserID = req.params.UserID;

    OrderAssignment.findOne({'OrderID' : OrderID,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    OrderReview.find({'OrderAssignID' : data._id})
                        .populate(['CustomerID' , 'UserID'])
                        .exec((OrderPayerr , OrderPaydata)=>{
                        if(OrderPayerr) return next(OrderPayerr);
                else{
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': OrderPaydata
                        })
                    }
                })
                }
            }
        })
}

exports.reviewApproved = function (req, res, next) {

    let OrderAssignID = req.params.OrderAssignID;


    OrderReview.update({'OrderAssignID' : OrderAssignID}, {IsApproved: true}, (oserr, osdata) => {
        if (oserr) {
            console.log(oserr)
            return next(oserr);
        }
        else{
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': osdata
        })
    }
});
}

exports.disapproveReview = function (req, res, next) {

    let OrderAssignID = req.params.OrderAssignID;

    OrderReview.update({'OrderAssignID' : OrderAssignID}, {IsApproved: false}, (oserr, osdata) => {
        if (oserr) {
            return next(oserr);
        }
        else{
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': osdata
        })
    }
});
}