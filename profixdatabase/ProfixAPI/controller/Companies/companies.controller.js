var express = require('express');
var router = express.Router();
const Companies = require('../../models/companies.model');
const compAddress = require('../../models/companies-address.model')
const companyTeam = require('../../models/teams.model')
const fileUpload = require('./../../global');
const User = require('../../models/user.model');
const SettingsData = require('../../models/settings.model');
const btoa  =require('btoa');
const email = require('../../routes/email-functions');
const configData = require('../../routes/common-config');
const teams = require('../../models/teams.model');
const teammember = require('../../models/team-member.model');

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}

//create
exports.create = function (req, res, next) {
    let data = req.body;
     let Address = req.body.Address;

    var userData = {
        'Role' : '5d0a16fdb457473128b080be',
        'FirstName' : data.FirstName,
        'MiddleName' : data.MiddleName,
        'LastName' : data.LastName,
        'Email' : data.ManagerEmail,
        'UserName' : data.ManagerEmail,
        'Phone' : data.ManagerPhone,
        'Password' : btoa(data.Password),
        'DateTime' : new Date(),
        'Active' : 1,

    }

    const user = new User(userData);

    user.save().then(function (data) {

    }).catch((err)=>{
        next(err)
    })
    data.DateTime = new Date();
    data.Active = 1;
    data.Manager = user._id;

    const craetecomp = new Companies(data);



    craetecomp.save().then((data)=> {
        
        for(let add of Address){
            var createCO = {};
            createCO  = add;
            createCO.CompanyID = craetecomp._id;
            createCO.DateTime = new Date();
            createCO.Active = 1;
            const addData = new compAddress(createCO);
            addData.save().then(function (err) {
               
            }).catch((error)=>{
                next(error)
            })
        }   

        var replacementsValue = {
            useremail: data.ManagerEmail,                
            password : data.Password,
            loginlink : configData.AdminLoginLink
        };

        email(data.ManagerEmail,'New Company Created','company-template',replacementsValue);       
         

        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': craetecomp
        })
    }).catch((error)=>{
        next(error)
    })
}



// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    var user = {};
    if(data.Password != '' && data.ConfirmPassword != ''){
        data.Password = btoa(data.Password);
        user.FirstName = data.FirstName;
        user.MiddleName = data.MiddleName;
        user.LastName = data.LastName;
        user.Email = data.Email;
        user.Password = data.Password;
        user.Phone = data.Phone;
    }
    else{
        user.FirstName = data.FirstName;
        user.MiddleName = data.MiddleName;
        user.LastName = data.LastName;
        user.Email = data.ManagerEmail;
        user.Phone = data.ManagerPhone;
    }
    Companies.findById(id).exec().then((userdata)=>{
        User.findByIdAndUpdate(userdata.Manager , user).exec().then((updatedata)=>{

        }).catch((err)=>{
            next(err)
        })
    }).catch((err)=>{
        next(err)
    })

    

    data['Modified'] = new Date();
    Companies.findByIdAndUpdate(id, data, {new: true}).then((data) => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data,
        })   
    }).catch((err)=>{
        next(err)
    })
}

// getOne
// exports.getOne = function(req, res, next){
//     let id = req.params._id;
//     cities.findById(id, (err, data) =>{
//         res.status(200).json({
//         'status': true,
//         'message': 'Success',
//         'result': data
//     })
// })
// .catch(exc => {
//     return handleError(exc);
// })
// }

// getOneCompanyDetail
exports.getOneCompanyDetail = function(req, res, next){
    let id = req.params._id;
    Companies.findById(id).populate([{path : 'Manager' , select :{'FirstName' :1 , 'MiddleName' :1 , 'LastName' :1, 'Email' :1, 'Phone' :1}}]).exec().then((data) =>{
        companyTeam.find({'CompanyID' : id, 'Active' : 1}).populate([ {path : 'Projects' , select :{'ProjectName' :1}} , {path : 'TLID' , select :{'FirstName' :1 , 'MiddleName' :1 , 'LastName' :1}}]).exec().then((compTeamdata)=>{
            compAddress.find({'CompanyID' : id ,'Active' : 1}).exec().then((compAddressdata)=>{
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data,
                    'CompanyTeams' : compTeamdata,
                    'CompanyAddress' : compAddressdata
                })
            }).catch(compaddresserror => {
                next(compaddresserror)
            }) 
        }).catch(err => {
            next(err)
        })  
    })
    .catch(exc => {
        next(exc)
    })
    }

// getAll
exports.getAll = function(req, res, next){  
    Companies.find({'Active' : 1}).populate(['Manager']).exec().then(function(data){
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    }).catch(exc => {
        next(exc)
    })
}

//delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    Companies.findById(id).select(['Name' , 'Manager']).then((data)=>{
        var cname = data.Name;
        User.findByIdAndUpdate(data.Manager , {'Active' : 0}).then(data=>{

        }).catch(err=>{
            console.log(err)
            next(err)
        })
        User.updateMany({'CompanyID' : id} , {'Account_Block' : true , 'CompanyID' : null}).then(data=>{

        }).catch(err=>{
            console.log(err)
            next(err)
        })
        Companies.findByIdAndUpdate(id, {Active: 0 , 'Name' : 'Deleted-'+cname},(Cityerr, Citydata) => {
            teams.find({'CompanyID' : id}).then((teamData)=>{
                if(teamData){
                    for(let team of teamData){
                        User.findByIdAndUpdate(team.TLID , {'type' : 'SP'}).exec().then((data)=>{

                        }).catch(tlerr=>{
                            next(tlerr);
                        })
                        
                        var teamname = 'Deleted-'+team.TeamName;
                        teams.findByIdAndUpdate(team._id, {'TeamName' : teamname,'Active': 0}, (err, data) => {
                            if (err) {
                                return next(err);
                            }
                            else{        
                                teammember.find({'TeamID': team._id}).exec((err, TeamMemberdata) => {
                                    if(err) {
                                        return next(err);
                                    }
                                    else{
                                        teammember.updateMany({'TeamID': team._id} , {'Active' : 0}).exec().then((tmdata)=>{
                    
                                        }).then((tmerr)=>{
                                            next(tmerr)
                                        })
                                        for(let tm of TeamMemberdata){
                                            User.findByIdAndUpdate(tm.UserID , {'type' : 'SP'}).exec().then((data)=>{
                                                
                                            }).catch((membererr)=>{
                                                next(membererr);
                                            })
                                        }
                                          
                                    }
                                });          
                        }
                        });
                        
                    }
                }
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully deleted record',
                    'result': data
                })
            })
        });
    }).catch((err)=>{
        next(err)
    })
}

// ValidateCompanyName
exports.ValidateCompanyNameAndEmail = function(req, res, next){
    let validate = req.params.Type;
    let val = req.params.TypeValue;
    var condition = {
        'Active':1
    }
    if(validate == 'Email'){
        condition.Email = val;
    }
    else if(validate == 'Name'){
        condition.Name = val;
    }
    Companies.findOne(condition).exec().then((data) =>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
    .catch(exc => {
        next(exc);
    })
}

exports.addCompanyOfficeAddress = function(req, res, next){
    let id = req.params._id;
    var data = req.body;

    data.CompanyID = id;

    data.DateTime = new Date();
    data.Active = 1;

    const createCompanyAddress = new compAddress(data);

    createCompanyAddress.save(function (err) {
        if (err) {
            return next(err);
        }
        else{
            compAddress.find({'CompanyID' : id ,'Active' : 1}).exec().then((data)=>{
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data
                })
            }).catch((err)=>{
               return next(err);
            })
        }
    })
}


exports.updateCompanyOfficeAddress = function(req, res, next){
    try{
        let id = req.params._id;
        let CompanyID = req.params.CompanyID;
        var data = req.body;
        compAddress.findByIdAndUpdate(id, data, {new: true}).exec().then((data) => {
            compAddress.find({'CompanyID'  :CompanyID ,'Active' : 1}).exec().then((addressData)=>{
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully updated record',
                    'result': data,
                    'CompanyAddress' : addressData
                })
            })
        }).catch((error)=>{
            next(error)
        })
    }
    catch(err){
        next(err);
    }
    
    
}


exports.deleteCompanyOfficeAddress = function(req, res, next){
    try{
        let id = req.params._id;
        compAddress.findByIdAndUpdate(id, {'Active' : 0}, {new: true}).exec().then((data) => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data,
            })  
        }).catch((error)=>{
            next(error)
        })
    }
    catch(err){
        next(err);
    }
}

// getOne
exports.getCompanyUser = function(req, res, next){
    let id = req.params._id;
    User.find({'CompanyID':id , 'Active' : 1})
    .populate([{path : 'State',select: { 'Name':1} }, {path : 'City',select: { 'Name':1} } , {path : 'Role',select: { 'Name':1} }] )
    .select(['FirstName' , 'LastName' ,'MiddleName','ProfixID' ,'Email','Phone','EndDate' , 'type' , 'IsAvailable' , 'Account_Block'])
    .exec().then( (data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
    next(exc);
})
}


// getOne
exports.getCompanySPUser = function(req, res, next){
    let id = req.params._id;
    const now  = new Date();
    User.find({
        'CompanyID':id , 
        'type' : 'SP', 
        'Active' : 1,
        'EndDate': {'$gt': now},  //For not showing user whose membership date is expired.
        'Account_Block': false,
        'IsAvailable' : 'Y'
    })
    .sort({_id: 'desc'})
    .populate(['State' , 'City'])
    .exec().then( (data) =>{
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;
                ret.ProfixID = row.ProfixID;
                ret.Email = row.Email;
                ret.Phone = row.Phone;
                ret.FirstName = row.FirstName;
                ret.MiddleName = row.MiddleName;
                ret.LastName = row.LastName;
                ret.State = row.State.Name;
                ret.City = row.City.Name;
                ret.Avatar = row.Avatar;
                return ret;
            });
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
    })
.catch(exc => {
    next(exc);
})
}

