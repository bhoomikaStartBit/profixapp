const OrderReview = require('../../models/reviews.model')

//  get All Review DataInformation Payment
exports.getAllReviews = function(req, res, next){

    OrderReview.find()
        .populate(['CustomerID' , 'UserID', 'OrderAssignID'])
        .exec((OrderPayerr , OrderPaydata)=>{
    
            if(OrderPayerr) return next(OrderPayerr);
                else{
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'result': OrderPaydata
                    })
                }
        })
}

// getOneReview
exports.getOneReview = function(req, res, next){
    let id = req.params._id;
    OrderReview.findById(id).populate(['CustomerID' , 'UserID', 'OrderAssignID']).exec((err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
}

// update
exports.updateReview = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    OrderReview.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data
            })
        }
    });
}

// acceptRequest
exports.acceptReview = function(req, res, next){
    const id = req.params._id;
    OrderReview.findByIdAndUpdate(id, {'IsApproved': true, 'DateTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully approved review',
                'result': data
            })
        }
    })
}