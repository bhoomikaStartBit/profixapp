var express = require('express');
var router = express.Router();
const customers = require('../../models/customer.model');
const configData = require('../../routes/common-config');
const speakeasy = require('speakeasy');
var btoa = require('btoa');
const email = require('../../routes/email-functions');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    var user1  = Object.assign(data.Basic , data.Account);
    user1.UserName = user1.Email;
    user1.DateTime = new Date();
    user1.Active = 1;


    const cust = new customers(user1);
    cust.save().then((function (err) {
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': cust
        })
    })).catch((error)=>{
        next(error);
    })
}


// sendEmailVerification
exports.sendEmailVerification = function (req, res, next) {
    let data = req.body;
    let CustEmail = data.CustEmail;
    let Custid = data.CustId;
    
    const secret = speakeasy.generateSecret();
    data.Base32secret = secret.base32;
    data.Modified = new Date();
    
    customers.findByIdAndUpdate(Custid, data, {new: true}).exec().then((data) => {
        var replacementsValue = {
            emailverificationlink  : configData.CustomerEmailVerificationLink,
            custemail : CustEmail,
            basesec : data.Base32secret,                 
            custloginlink : configData.CustomerLoginLink
        };

        email(CustEmail,'Account Verification Request','account-verification-request',replacementsValue);

        res.status(200).json({
            'status': true,
            'message': 'Successfully sent email',
            'result': data
        })
       
    }).catch((error)=>{
        next(error);
    })
    
};

// createCustomer
exports.createCustomer = function (req, res, next) {
    let data = req.body.Basic;
    
    data.DateTime = new Date();
    data.Active = 1;
    data.UserName = data.Email;
    data.Password = btoa(data.Password);
    const now = new Date();
    const random = Math.floor(Math.random() * (9999999 - 100 + 1) + 100);
    //data.ProfixID = 'PFX-' + now.getFullYear() + '' + now.getMonth() + '' + random;
    const secret = speakeasy.generateSecret();
    data.Base32secret = secret.base32;
    data.TOTPVerified = 0;
    data.EmailVerified = 0;
    //console.log(secret.base32 + ' Secret');

    const totptoken = speakeasy.totp({
        secret: data.Base32secret,
        encoding: 'base32',
        step: 300
    });

    // const a = speakeasy.totp.verify({
    //     secret: data.Base32secret,
    //     encoding: 'base32',
    //     token: totptoken,
    //     step: 900
    // });
    // console.log(a + ' verify');

    //console.log(totptoken + ' token');

    data = new customers(data);
    
    data.save().then((result)=> {
            const Name = data.FirstName + ' ' + data.MiddleName + ' ' + data.LastName;
            const Email = data.Email;
            const Phone = data.Phone

            var replacementsValue = {
                emailverificationlink  : configData.CustomerEmailVerificationLink,
                email : Email,
                basesec : data.Base32secret,
                name : Name,
                phone : Phone,
                profixid : data.ProfixID,
                custloginlink : configData.CustomerLoginLink
            };
    
            email(data.Email,'Registration successfull','registration-template',replacementsValue);
            
            res.status(200).json({
                'status': true,
                'message': 'Account created Successfully. Please check your email for account activation.',
                'result': data
            })
        
    }).catch((error)=>{
        next(error);
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    if(data.Account.Password != ''){
        var user1  = Object.assign(data.Basic , data.Account);
        user1.Password = btoa(user1.Password);
    }
    else{
        var user1  = Object.assign(data.Basic);
    }

    user1.UserName = user1.Email;
    data['Modified'] = new Date();
    customers.findByIdAndUpdate(id, user1, {new: true}).then((data) => {
        
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
        
    }).catch((error)=>{
        next(error);
    })
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    customers.findById(id).then((data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
        })
    })
    .catch(exc => {
        next(exc);
    })
}

// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    var selectedReferenceBy = req.params.selectedReferenceBy;
    if (selectedReferenceBy != 0) {
        condition.ReferenceBy = selectedReferenceBy;
    }
    customers.find(condition)
        .sort({_id: 'desc'})
        .populate(['State' , 'City' , 'Locality' , 'ReferenceBy'] )
        .then((data)=>{
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;
                ret.ProfixID = row.ProfixID;
                ret.Email = row.Email;
                ret.FirstName = row.FirstName;
                ret.MiddleName = row.MiddleName;
                ret.LastName = row.LastName;
                ret.State = row.State ? row.State.Name : '';
                ret.City = row.City ? row.City.Name : '';
                ret.Locality = row.Locality ? row.Locality.Name : '';
                ret.ReferenceBy = row.ReferenceBy ? (row.ReferenceBy.FirstName + " " +row.ReferenceBy.MiddleName+ " " +row.ReferenceBy.LastName) : '' ; //
                return ret;
            });
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    customers.findByIdAndUpdate(id, {Active: 0}).then(( data) => {
        
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': data
        })
    
});
}

// updateProfilePassword
exports.updateProfilePassword = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    customers.findOne({'_id': id, 'Password': btoa(data.OldPassword)}).exec((err, CustomerData) => {
        if(err) {
            return err;
        }
        else if (!CustomerData){
            res.status(200).json({
                'status': false,
                'message': 'Invalid old password',
                'result': []
            })
        }else {
            const myData = {
                Password: btoa(data.NewPassword)
            }
            customers.findByIdAndUpdate(id, myData, {new: true}, (err, data) => {
                if(err) {
                    return err;
                }
                else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Password Updated Successfully',
                        'result': data,
                        'Avatar': data['Avatar']
                    })
                }
            });
        }
    })
}

exports.getByType = function(req, res, next){
    customers.find({})
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}

exports.checkUserName = function (req , res , next) {
    var name = req.params.name;
    customers.find({'UserName': name})
        .exec(function(err, data){
        if(err) res.send(err);
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
}
exports.checkEmail = function (req , res , next) {
    var email = req.params.Email;
    customers.find({'Email': email})
        .exec(function(err, data){
        if(err) res.send(err);
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
}

