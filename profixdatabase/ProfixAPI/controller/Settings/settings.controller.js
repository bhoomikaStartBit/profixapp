const settings = require('../../models/settings.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    console.log(req.body);

    const vtran = new settings(data);
    vtran.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': vtran
        })
    })
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    settings.findById(id).sort({_id : -1}).exec((err, data) =>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
}



//getOrderTime

exports.getOrderTime = function(req , res , next){
    var TimeValue = 12;
    console.log(TimeValue);
    var dateTim = new Date();

    settings.findOne({'metaKey': 'nextDayScheduleTime'}).exec((settingerr,settingdata)=>{

        if(settingdata){
            TimeValue = settingdata.metaValue;
        }
        else{
            TimeValue = 12; // if no data receive from api
        }
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': TimeValue,
            'CurrentDateTime' : dateTim.getHours()
        })
    });
}


//Get metaValue by metaKey

exports.getmetaValue = function(req , res , next){
    let metakey = req.params.metakey;

    settings.findOne({'metaKey': metakey}).exec((settingerr,settingdata)=>{
        let metaValue = settingdata.metaValue;

        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': metaValue
        })
    });
}


// getDateTime
exports.getDateTime = function(req, res, next){
    res.status(200).json({
    'status': true,
    'message': 'Success',
    'result': new Date()
   
})
}
// getAll
exports.getAll = function(req, res, next){
    settings.find()
        .sort({_id: 'desc'})
        .then((data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
        })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;

    for(var myKey in data) {
        //console.log("key:"+myKey+", value:"+data[myKey]);
        let ndata = {};
        ndata = {'metaKey':myKey, 'metaValue':data[myKey]};

        const query = {'metaKey':myKey};
        // Replace it with a new document
        const replacement = {'metaValue': data[myKey]};
        // Return the original document as it was before being replaced
        const options = { "returnNewDocument": true };

        settings.findOneAndUpdate(query, replacement, options)
        .then(replacedDocument => {
            if(replacedDocument) {
                //console.log('Successfully replaced the following document')
            } else {
               
                const vtran = new settings(ndata);
                vtran.save((result) =>{
                    //console.log(`NEw entry add: ${result}.`)
                }).catch(err=>{
                    next(err);
                })
            }
        }).catch(err=>{
            next(err);
        })
    }

    res.status(200).json({
        'status': true,
        'message': 'Successfully updated record',
        'result': data
    })

    
}


// update
exports.updateNew = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    settings.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data
            })
        }
    });
}