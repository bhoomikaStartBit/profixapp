const projects = require('../../models/project.model');
const OrderAssignment = require('../../models/orderAssignment.model');
const OrderReview = require('../../models/reviews.model')
const OrderActivity = require('../../models/orderAcitivities.model');


function createOrderActivity(data) {
    
    const activityData = new OrderActivity(data);
    activityData.save(function(err) {
        if(err){
            return false;
        }
        else return true;
    })
}

// getAll projects
exports.currentOrder = function(req, res, next){
    var CID = req.params.id;
    const condition= {
        'Active': 1
    };
    projects.find({'Customer' : CID ,'Status' : { $ne : 'Completed'}})
        .populate([{ path : 'Locality'  , select : {'Name' : 1}} , { path : 'City'  , select : {'Name' : 1}} , { path : 'State'  , select : {'Name' : 1}}])
        .sort({'_id': -1})
        .then((data) => {
        
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    
    }).catch((err)=>{
        next(err)
    })

}


// getAll projects
exports.pastOrder = function(req, res, next){
    var CID = req.params.id;
    const condition= {
        'Active': 1
    };
    projects.find({'Customer' : CID ,'Status' :  'Close'})
        .populate([{ path : 'Locality'  , select : {'Name' : 1}} , { path : 'City'  , select : {'Name' : 1}} , { path : 'State'  , select : {'Name' : 1}}])
        .then((err, data) => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            }) 
        }).catch((err)=>{
            next(err)
        })


}

// getAll projects
exports.geyOneOrderData = function(req, res, next){
    var OrderID = req.params.id;
    const condition= {
        'Active': 1
    };
    projects.find({'_id' : OrderID})
        .sort({_id: 'desc'})
        .populate(['Locality' , 'City' , 'State'])
        .then(function( data){
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch((err)=>{
            next(err);
        })
}


// send Review
exports.sendReview = function (req, res, next) {
    let Orderdata = req.body;

    OrderAssignment.findOne({'OrderID' : Orderdata.OrderID, 'Active': 1, 'Status': 'Close' })
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    var orderActivity = {
                        'OrderID' : Orderdata.OrderID,
                        'CustomerID': Orderdata.CurrentCustomer,
                        'Status': data.Status,
                        'Message': 'Order review has been send successfully',
                        'DateTime': new Date(),
                        'Active': 1
                    }
                    createOrderActivity(orderActivity);

                    var ReviewOrder = {
                        'OrderAssignID' : data._id,
                        'CustomerID': Orderdata.CurrentCustomer,
                        'Rating': Orderdata.Review.Rating,
                        'Review': Orderdata.Review.Review,
                        'DateTime': new Date(),
                        'Active': 1
                    }

                    var reviewOrder = new OrderReview(ReviewOrder);
                    reviewOrder.save().then(function(err) {
                        
                            res.status(200).json({
                                'status': true,
                                'message': 'Customer review successfully',
                                'result': reviewOrder
                            })
                        
                    }).catch((err)=>{
                        next(err);
                    })
                }
            }
        })
}


//  get Information Payment
exports.getInfoReview = function(req, res, next){
    let OrderID = req.params.OrderID;
    let UserID = req.params.UserID;

    OrderAssignment.findOne({'OrderID' : OrderID , 'Active': 1, 'Status': 'Close'})
        .then(function( data){
            if(data){
                OrderReview.find({'OrderAssignID' : data._id , 'CustomerID' : UserID }).then(( OrderPaydata)=>{
                    
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'result': OrderPaydata
                    })
                
                }).catch((err)=>{
                    next(err)
                })
            }
            else{
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': []
                }) 
            }
            
        }).catch((err)=>{
            next(err)
        })
}


//  get All Review DataInformation Payment
exports.getAllSPReviews = function(req, res, next){
    let OrderID = req.params.OrderID;

    OrderAssignment.findOne({'OrderID' : OrderID,'Active': 1})
        .exec(function(err, data){
            if (err) {
                return next(err);
            }
            else{
                if(data){
                    OrderReview.find({'OrderAssignID' : data._id, 'UserID' : data.UserID, 'IsApproved': true})
                        .populate(['CustomerID' , 'UserID'])
                        .then(( OrderPaydata)=>{
                        
                            res.status(200).json({
                                'status': true,
                                'message': 'Success',
                                'result': OrderPaydata
                            })
                    
                        }).catch((err)=>{
                            next(err)
                        })
                }
            }
        })
}