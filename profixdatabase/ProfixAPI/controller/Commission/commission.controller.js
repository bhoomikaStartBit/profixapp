const commission = require('../../models/commission.model');
const users = require('../../models/user.model');
const services = require('../../models/service.model');

// update
exports.create = function(req, res, next){
    let data = req.body;
    commission.insertMany(data).then(function (err) {
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': commission
        })
    }).catch((error)=>{
        next(error)
    })
}


exports.getAllVendors = function(req, res, next){
    const condition = {
        'Role': '5cb7fe5c6d2f510b5888660f',
        'Active': 1
    };
    users.find(condition)
        .select(['_id','UserName','FirstName','MiddleName','LastName','Email','Phone', 'Avatar'])
        .exec()
        .then(function(data){
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            }); 
        }).catch((error)=>{
            next(error)
        })
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    users.findById(id).exec().then((data) =>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    }).catch((error)=>{
        next(error)
    })
}


exports.getAll = function(req, res, next){
    const condition = {
        'Active': 1
    };
    commission.find(condition)
        .populate(['ServiceID'])
        .exec()
        .then(function(data){
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch((error)=>{
            next(error)
        })
}

exports.getByVendorID = function(req, res, next){
    
    const id = req.params._id;

    const condition = {
        'VendorID': id,
        'Active': 1
    };
    commission.find(condition)
        .populate(['ServiceID'])
        .exec()
        .then(function(data){
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch((error)=>{
            next(error)
        })
}

exports.updateCommissionServices = function(req, res, next){
    let id = req.params._id;
    let data1 = req.body[0];
    commission.findByIdAndUpdate(id, data1, {new: true}).then((data) => {  
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record 1',
            'result': data
        })
    }).catch((error)=>{
        next(error)
    })
}     

exports.addNewServiceCommission = function(req, res, next){
    let id = req.params._id;
    let data1 = req.body[0];

    services.find()
        .populate(['ServiceID'])
        .exec()
        .then(function(result){
            for (let row of result) {
            }
            res.status(200).json({
              'status': true,
              'message': 'Successfully updated record 1',
              'result': []
          })
        }).catch((error)=>{
            next(error)
        })
}     
