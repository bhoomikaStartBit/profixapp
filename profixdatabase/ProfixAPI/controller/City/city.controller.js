const cities = require('../../models/city.model');
const localities = require('../../models/locality.model');

// create
exports.create = function (req, res, next) {
    try{
        let data = req.body;
        data.DateTime = new Date();
        data.Active = 1;
    
        const cty = new cities(data);
        cty.save().then(
            res.status(200).json({
                'status': true,
                'message': 'Successfully created record',
                'result': cty
            })
            ).catch((err)=>{
            next(err)
        })
    }catch(e){
        next(e)
    }
    
}



// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    cities.findByIdAndUpdate(id, data, {new: true}).then((data) => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data,
        })
    }).catch((err)=>{
        next(err)
    })
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    cities.findById(id).then((data) =>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
    .catch(exc => {
        next(exc);
    })
}



// getRelatedRecordOfCity
exports.getRelatedRecordOfCity = function(req, res, next){
    let id = req.params._id;
    localities.find({'City':id , 'Active':1 }).then((data) =>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data,
            'Table': 'Localities'
        })
    })
    .catch(exc => {
        next(exc);
    })
}
// getOne
exports.getAllCitiesByState = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    if(!id || id == null || id == undefined){
        res.status(200).json({
            'status': false,
            'message': 'Id not found',
            'result': []
        })  
    }
    else{
        cities.find({'State' : id , 'Active': 1}).then(( data) =>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
        .catch(exc => {
            next(exc);
        })
    }
    
}

// getAll
exports.getAll = function(req, res, next){
    try{
        const condition= {
            'Active': 1
        };
        cities.find(condition)
            .sort({_id: 'desc'})
            .populate([{path : 'State',select: { 'Name':1} }])
            .select(['Name' , 'Latitude' , 'Longitude'])
            .then(( data) => {
                // let sentData = [];
                // sentData = data.map(row=>{
                //     let ret = {};
                //     ret._id = row._id;
                //     ret.Name = row.Name;
                //     ret.Latitude = row.Latitude;
                //     ret.Longitude = row.Longitude;
                //     ret.StateName = row.State.Name;
                //     return ret;
                // })
                return res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data
                })
            }).catch ((err)=>{
                    next(err);
            })
    }
    catch(err){
        next(err);
    }
    
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    cities.findByIdAndUpdate(id, {Active: 0}).then(( Citydata) => {
        localities.updateMany({'City': id}, {Active: 0}).then(( data) => {   
            res.status(200).json({
                'status': true,
                'message': 'Successfully deleted record',
                'result': Citydata 
            })
        }).catch ((err)=>{
            next(err);
        })
    }).catch ((error)=>{
        next(error);
    })
}

