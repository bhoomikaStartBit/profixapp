var express = require('express');
var router = express.Router();
const teams = require('../../models/teams.model');
const teammember = require('../../models/team-member.model');
const utcr = require('../../models/user-teamcompany-relation.model')
const users = require('../../models/user.model');
const email = require('../../routes/email-functions');
const OrderAssignment = require('../../models/orderAssignment.model');
const OrderTable = require('../../models/project.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    var user1  = data.Basic;
    user1.Accept = false;
    user1.DateTime = new Date();
    user1.Active = 1;
    user1.RequestTime = new Date();
    user1.RequestStatus = 1;

    const teams = new teams(user1);
    
    teams.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': teams
        })
    })
    
}

// create
exports.createBySA = function (req, res, next) {
    let data = req.body;
    var teamData  = req.body.Basic;  
    teamData.DateTime = new Date();
    teamData.Active = 1;
    let teamMemberExisting = [];
    if(teamData.Projects == null || teamData.Projects.trim() == ""){
        delete teamData.Projects; 
       // delete teamData.TeamName; 
    }
    else{
        var orderAssign = {
            'OrderID'  :  teamData.Projects,
            'UserID' : teamData.TLID,
            'Status' : 'In-Route',
            'Notes' : 'Order In-Route',
            'DateTime': new Date(),
            'Active': 1
        }
        const orderAssignment = new OrderAssignment(orderAssign);
        orderAssignment.save().then(function (data) {
            
        }).catch(err=>{next(err)});

        var UpdateOrder = {
            'Status' : 'In Process',
            'AssignTo': teamData.TLID
        }

        OrderTable.findByIdAndUpdate(teamData.Projects, UpdateOrder, {new: true}).then((data) => {

        }).catch(err=>{
            next(err);
        })
    }
          

    var UserID = teamData.TLID;
    const createTeamFormData = new teams(teamData);   
   
 teams.findOne({'TeamName': teamData.TeamName , 'Active' : 1}).exec((err, doc)=> { 
        
        if(err) return err;
        else if(doc) {            
           return res.status(200).json({
                'status': false,
                'message': 'Team Name already exist.',
                'result': []
            })
            
        } else {            
                users.findOneAndUpdate({'_id':UserID, 'Active' : 1,'type' : 'SP'}, {type: 'TL'}, (err, userdata) => {
                    console.log(userdata);
                    if(err) {
                        console.log("hello")
                        return next(err);
                    }
                    else if(!userdata){
                        return res.status(200).json({
                            'status': false,
                            'message': 'Team Leader already associated another team.',
                            'result': [],
                            'createStatus': true
                        })
                    }
                    else{ 
                        var replacementsValue = {
                            teamname: teamData.TeamName,    
                            tlname:data.Basic.FullName,   
                        };
                        OrderTable.findByIdAndUpdate(teamData.Projects, UpdateOrder, {new: true}).then((data) => {

                        }).catch(err=>{
                            next(err);
                        })
                        // email(data.Basic.Email,'Team Created','team-tl-template',replacementsValue);
                        createTeamFormData.save(async function (err) {
                            console.log("errrrrr" ,err);
                        if (err) {
                            users.findOneAndUpdate({'_id':UserID, 'Active' : 1,'type' : 'TL'}, {type: 'SP'}, (err, userdata) => {})
                            
                            return res.status(200).json({
                                'status': false,
                                'message': 'Team Name already exist.',
                                'result': [],
                                'createStatus': true
                            })
                            
                            // return next(err);
                        }   
                        else{  
                            
                        await Promise.all(teamData.TeamMembers.map(member=>{
                                return new Promise(resolve=>{
                                    users.findOneAndUpdate({'_id':member._id, 'type' : 'SP', 'Active' : 1}, {type: 'MSP'}).exec((err,findMemberdata)=>{
                                    console.log("first");
                                        if(!findMemberdata){
                                            teamMemberExisting.push(member.FirstName);
                                        }else{
                                            var createTeamMember = {
                                                TeamID : createTeamFormData._id,
                                                UserID : member._id,
                                                Active : 1,
                                                DateTime : new Date()
                                            }
                                            var addteammember = new teammember(createTeamMember);
                                            addteammember.save().then(function (adderr) {
                                                    
                                            }).catch(err=>{
                                                next(err);
                                            })                            
                            
                                            // var replacementsValue = {
                                            //     firstname: member.FirstName,    
                                            //     tlname:data.Basic.FullName,
                                            //     tlemail: data.Basic.Email,                                 
                                            //     teamname: teamData.TeamName,  
                                            // };
                                            //  email(member.Email,'Welcome to Team','team-member-template',replacementsValue);
                                        }

                                        resolve();
                                    })
                                })
                                }));
                                console.log("second");
                                console.log('teamMemberExisting :' , teamMemberExisting)
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully created record',
                                'result': teams,
                                'TeamMemberExisting' : teamMemberExisting
                            })
                                
                    }
                    });
                                                                
                    }  
                });
                
            } 
               
        })
    }    


exports.getAllServiceProvider = function(req, res, next){
    //console.log('get');
    const now = new Date();
    const condition= {
        'Active': 1,
        'type': 'SP',
       //'EndDate': {'$gt': now},
        //'Block': false
    };
    users.find(condition)
        .select(['_id','UserName','FirstName','MiddleName','LastName','Email','Phone', 'Avatar'])
        .exec(function(err, data){
            if(err)
            {
                res.status(501).json({
                    'status': true,
                    'message': 'Error',
                    'result': err
                });
            }
            else
            {
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data
                });
            }
        })
}

// getOne
exports.getOneServiceProvider = function(req, res, next){
    let id = req.params._id;
    users.findById(id).exec((err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    var user1  = Object.assign(data.Basic , data.Account);;
    data['Modified'] = new Date();
    teams.findByIdAndUpdate(id, user1, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    teams.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}


// getAll
exports.getDetail = function(req, res, next){
    const condition= {
        '_id': req.params._id
    };
var id = req.params._id;
teams.findById(id)
        .sort({_id: 'desc'})
        .populate(['Projects', 'TLID'] )
        .exec(function(err, data){
            if(err) res.send(err);
            else{
                utcr.find({'TeamID' : data._id , 'Active': 1})
                    .sort({_id: 'desc'})
                    .populate(['TeamID' , 'UserID'] )
                    .exec(function(err, utcrData){
                        if(err) res.send(err);
                        let sentData = [];
                        sentData = utcrData.map(row=>{
                            let ret = {};
                        ret._id = row.UserID._id;
                        ret.FirstName = row.UserID.FirstName;
                        ret.MiddleName = row.UserID.MiddleName;
                        ret.LastName = row.UserID.LastName;
                        ret.Email = row.UserID.Email;
                        return ret;
                    });
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': data,
                            'TeamMember':sentData
                        })
                    })
            }

        })
}


// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1,
        'CompanyID' : { $exists: false},

    };
    var selectedReferenceBy = req.params.selectedReferenceBy;
    if (selectedReferenceBy != 0) {
        condition.ReferenceBy = selectedReferenceBy;
    }
    teams.find(condition)
        .sort({_id: 'desc'})
        .populate([ { path :'TLID' , select : { 'FirstName' : 1, 'LastName' : 1 , 'MiddleName' : 1  }} , { path : 'Projects' , select : {'ProjectName' : 1}}] )
        .select(['TeamName'])
        .exec(function(err, data){  
            if(err) next(err);
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;                
                ret.TeamName = row.TeamName;         
                ret.TLID = row.TLID.FirstName + " " +row.TLID.MiddleName+ " " +row.TLID.LastName ;
                ret.Projects = row.Projects ? row.Projects.ProjectName: '---' ;
                ret.TeamLeaderID = row.TLID._id
                return ret;
            });
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        })
}


// delete
exports.delete = function(req, res, next){
    let id = req.params._id;  
    let teamleaderid = req.params.tl_id;
    users.findByIdAndUpdate(teamleaderid , {'type' : 'SP'}).exec().then((data)=>{

    }).catch(tlerr=>{
        next(tlerr);
    })
    teams.findById(id).select('TeamName').then((data)=>{
        var teamname = 'Deleted-'+data.TeamName;
        teams.findByIdAndUpdate(id, {'TeamName' : teamname,'Active': 0}, (err, data) => {
            if (err) {
                return next(err);
            }
            else{        
                teammember.find({'TeamID': id}).exec((err, TeamMemberdata) => {
                    if(err) {
                        return next(err);
                    }
                    else{
                        teammember.updateMany({'TeamID': id} , {'Active' : 0}).exec().then((tmdata)=>{
    
                        }).then((tmerr)=>{
                            next(tmerr)
                        })
                        for(let tm of TeamMemberdata){
                            users.findByIdAndUpdate(tm.UserID , {'type' : 'SP'}).exec().then((data)=>{
                                
                            }).catch((membererr)=>{
                                next(membererr);
                            })
                        }
                        res.status(200).json({
                            'status': true,
                            'message': 'Successfully deleted record',
                            'result': data
                        })  
                    }
                });          
        }
     });
    }).catch((err)=>{
        next(err);
    })
    
}


exports.checkMember = function(req, res, next){
    let id = req.params._id;
    utcr.find({'UserID' : id ,'Active':1})
        .sort({_id: 'desc'})
        .populate(['TeamID' , 'UserID'] )
        .exec(function(err, utcrData){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': utcrData
            })
        })
}

// getOneTeamCompanyByOwner
exports.getOneTeamCompanyByOwner = function(req, res, next){
    let id = req.params._id;
    teams.findOne({'TLID': id, Active: 1}).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// rejectRequest
exports.rejectRequest = function(req, res, next){
    const id = req.params._id; 
    teams.findByIdAndUpdate(id, {'RequestStatus': 2, 'DateTime': new Date(), 'Active': 0}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully deleted',
                'result': data
            })
        }
    })
}

// acceptRequest
exports.acceptRequest = function(req, res, next){
    const id = req.params._id;
    teams.findByIdAndUpdate(id, {'RequestStatus': 3, 'DateTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully accepted request',
                'result': data
            })
        }
    })
}


// requestForDisposal
exports.requestForDisposal = function(req, res, next){
    let id = req.params._id;
    res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
}

// getOne
exports.DuplicateTeamNamecheck = function(req, res, next){
    let name = req.params.name;
    teams.findOne({'TeamName' : name , 'Active' : 1}).exec((err, data) =>{
        
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
        
})
}