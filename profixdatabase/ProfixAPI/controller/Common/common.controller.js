const creditlimits =  require('../../models/credit_limit')
const creditdays =  require('../../models/credit_days')
const language =  require('../../models/languages.model')
const bloodgroup =  require('../../models/blood_groups.model')
const companyType = require('../../models/company_type.model')

// getOne
exports.getCreditLimits = function(req, res, next){
    try{
        creditlimits.find({'Active' : 1}).sort({'Order': 1}).then(data=>{
            res.status(200).json({
                status : true,
                'result' : data
            })
        }).catch((err)=>{
            next(err)
        })
        
    }
    catch(e){
        next(e)
    }

}


// getOne
exports.getCreditDays = function(req, res, next){
    try{
        creditdays.find({'Active' : 1}).sort({'Order': 1}).then(data=>{
            res.status(200).json({
                status : true,
                'result' : data
            })
        }).catch((err)=>{
            next(err)
        })
    }
    catch(e){
        next(e)
    }

}


// getOne
exports.getLanguages = function(req, res, next){
    try{
        language.find({'Active' : 1}).sort({'Order': 1}).then(data=>{
            res.status(200).json({
                status : true,
                'result' : data
            })
        }).catch((err)=>{
            next(err)
        })
    }
    catch(e){
        next(e)
    }

}


// getOne
exports.getBloodGroups = function(req, res, next){
    try{
        bloodgroup.find({'Active' : 1}).sort({'Order': 1}).then(data=>{
            res.status(200).json({
                status : true,
                'result' : data
            })
        }).catch((err)=>{
            next(err)
        })
    }
    catch(e){
        next(e)
    }

}


// getOne
exports.getCompanyTypes = function(req, res, next){
    try{
        companyType.find({'Active' : 1}).sort({'Order': 1}).then(data=>{
            res.status(200).json({
                status : true,
                'result' : data
            })
        }).catch((err)=>{
            next(err)
        })
    }
    catch(e){
        next(e)
    }

}