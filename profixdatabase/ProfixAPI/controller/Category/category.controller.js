const categories = require('../../models/category.model');
const services = require('../../models/service.model');
const imgFile =require('../../global');
// create
exports.create = function (req, res, next) {
   try{
    let data = {};
    data.DateTime = new Date();
    data.Active = 1;
    data.Name = req.body.Name;
    data.Description = req.body.Description;
    data.Slug = req.body.Slug
    data.Order = req.body.Order
    const category = new categories(data);

    var imageTypeRegularExpression = /\/(.*?)$/;
    var imageBuffer = decodeBase64Image(req.body.HeaderImage);
    var headerImageTypeDetected = imageBuffer
        .type
        .match(imageTypeRegularExpression);

    var userUploadedImagePath = '/Category/Header' +
        category._id +
        '.' +
        headerImageTypeDetected[1];

    var imageIconBuffer = decodeBase64Image(req.body.IconImage);
    var iconImageTypeDetected = imageIconBuffer
        .type
        .match(imageTypeRegularExpression);

    var userIconUploadedImagePath = '/Category/Icon' +
        category._id +
        '.' +
        iconImageTypeDetected[1];

    category.HeaderImage = userUploadedImagePath;
    category.IconImage = userIconUploadedImagePath;

    categories.findOne({'Slug': category.Slug}).then((doc)=> {
        
        if(doc) {
            res.status(200).json({
                'status': false,
                'message': 'Slug Name already exist',
                'result': []
            })
        } else {
            category.save().then((data) =>{
               
                imgFile(req.body.HeaderImage , './Profix/Category/' , 'Header'+category._id)
                imgFile(req.body.IconImage , './Profix/Category/' , 'Icon'+category._id)
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    'result': category
                })
            }).catch((err)=>{
                next(err)
            })
        }
    })
   }catch(err){
    next(err)
   }
}


function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}


// update
exports.update = function(req, res, next){
    let id = req.params._id;

    imgFile(req.body.HeaderImage , './Profix/Category/' , 'Header'+id);
    imgFile(req.body.IconImage , './Profix/Category/' , 'Icon'+id);

    let data = req.body;
    data['Modified'] = new Date();

    var imageTypeRegularExpression = /\/(.*?)$/;


    var imageBuffer = decodeBase64Image(req.body.HeaderImage);
    var headerImageTypeDetected = imageBuffer
        .type
        .match(imageTypeRegularExpression);

    var userUploadedImagePath = '/Category/Header' +
        id +
        '.' +
        headerImageTypeDetected[1];

    var imageIconBuffer = decodeBase64Image(req.body.IconImage);
    var iconImageTypeDetected = imageIconBuffer
        .type
        .match(imageTypeRegularExpression);

    var userIconUploadedImagePath = '/Category/Icon' +
        id +
        '.' +
        iconImageTypeDetected[1];

    data['HeaderImage'] = userUploadedImagePath;
    data['IconImage'] = userIconUploadedImagePath;

    categories.findByIdAndUpdate(id, data, {new: true}).then( (data) => {
        
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
        
    }).catch((err)=>{
        next(err)
    })
}

// getOne
exports.getOne = function(req, res, next){
    try{
        let id = req.params._id;
        console.log(id)
        categories.findById(id).then( (data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
        .catch((exc) => {
            console.log(exc)
            next(exc);
        })
    }catch(err){
        next(err)
    }
}

// getOneCategoryWithService Data
exports.getOneCategoryWithService = function(req, res, next){
    let Slug = req.params.Slug;
    categories.findOne({'Slug': Slug}).then((data) =>{
        if(data){
            services.find({'Category':data._id , 'Active':1 })
                .populate(['Category' , 'Type','Unit'])
                .exec()
                .then(function(servicedata){
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'result': data,
                        'Table' : 'Services',
                        'Service': servicedata
                    })
                })
        }        
    })
    .catch(exc => {
        next(exc);
    })
}

// getRelatedRecordOfCategory routing
exports.getRelatedRecordOfCategory = function(req, res, next){
    let id = req.params._id;
    services.find({'Category':id , 'Active':1 }, (err, data) =>{
            res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data,
            'Table' : 'Services'
        })
    })
    .catch(exc => {
        next(exc);
    })
}

// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    categories.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec()
        .then((data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch((err)=>{
            next(err)
        })
}
// getAllFeaturedCateory
exports.getAllFeaturedCateory = function(req, res, next){
    const condition= {
        'Active': 1,
        'Isfeatured' : true
    };
    categories.find(condition)
        .sort({_id: 'desc'})
        .then((data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch((err)=>{
            next(err)
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    categories.findByIdAndUpdate(id, {Active: 0}).then((Catdata) => {
        services.updateMany({'Category' : id}, {Active: 0}).then((data) => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully deleted record',
                'result': Catdata
            })
        }).catch((err)=>{
            next(err)
        })  
    }).catch((err)=>{
        next(err)
    })
}

