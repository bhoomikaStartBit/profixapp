const teamMember = require('../../models/team-member.model');
const teamData = require('../../models/teams.model')
const User = require('../../models/user.model');
const email = require('../../routes/email-functions');

// create
exports.create = function (req, res, next) {
    let doc = req.body.Basic;
    doc.RequestTime = new Date();
    doc.ActionTime = new Date();
    doc.Active = 1;
    doc.Status = 1;

    const teammember = new teamMember(doc);
    teammember.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully send invitation',
            'result': teamcompanies
        })
    })
}

// sendTeamMemberInvitation
exports.createTeamMember = function (req, res, next) {
    
    let Teamdoc = req.body;
    
    // res.status(200).json({
    //     'status': true,
    //     'message': 'Successfully aded Team member',
    //     'result': req.body
    // })
    // /*
    for(let member of Teamdoc.TeamData.AddMembers){
        var createTeamMember = {
            TeamID : Teamdoc.TeamID,
            UserID : member._id,
            DataTime : new Date(),
            Active : 1
        }
        const teammember = new teamMember(createTeamMember);
        teammember.save(function (err) {
            if (err) {
                return next(err);
            }

            var UserID = member._id;
            var typedata = {'type': 'MSP'};

            User.findByIdAndUpdate(UserID, typedata).exec((err, result) => {
                if(err) {
                    console.log(err);
                    return err;
                }

                var replacementsValue = {
                    firstname: member.FirstName,    
                    tlname:Teamdoc.TeamData.TLName, 
                    tlemail:Teamdoc.TeamData.TLEmail,
                    tlmobile : Teamdoc.TeamData.TlPhone,
                    teamname :Teamdoc.TeamData.TeamName
                };
        
                email(member.Email,'Added in Team','team-member-template',replacementsValue);           
            })
        })
    
    } 
    setTimeout(()=>{
        res.status(200).json({
            'status': true,
            'message': 'Successfully added Team member',
            'result': []
        })
    },500)
    // */
}

// sendTeamMemberInvitation
exports.sendTeamMemberInvitation = function (req, res, next) {
    let doc = req.body;
    doc.RequestTime = new Date();
    doc.Active = 1;
    doc.Status = 1;
    doc.RequestType = 'I';
    const now = new Date();
    const condition= {
        'Active': 1,
        'ProfixID': doc.ProfixID
    };
    User.findOne(condition).exec((err, rest) => {
        if(err) {
            console.log(err);
            return err; 
        } else {
            if(!rest) {
                res.status(200).json({
                    'status': false,
                    'message': 'You are using invalid Profix ID, please use a correct Profix ID.',
                    'result': []
                })
            } else if(rest.Block){
                res.status(200).json({
                    'status': false,
                    'message': 'The account of this user is blocked, please contact to Profix Admin.',
                    'result': []
                })
            } else if(rest.type != 'SP'){
                res.status(200).json({
                    'status': false,
                    'message': 'Sorry! You are using wrong Profix ID.',
                    'result': []
                })
            } else if(now > new Date(rest.EndDate)) {
                res.status(200).json({
                    'status': false,
                    'message': "This user's account expired, please contact to Profix Admin.",
                    'result': []
                })
            } else {
                doc.UserID = rest._id;
                const teammember = new teamMember(doc);
                teamMember.findOne({'Active': 1, 'UserID': doc.UserID, 'TeamID': doc.TeamID}).exec((err, result) => {
                    if(err) {
                        console.log(err);
                        return err;
                    } else if(result && result.Status == 1) {
                        res.status(200).json({
                            'status': false,
                            'message': 'Invitation has been sent already.',
                            'result': []
                        })
                    } else if(result && result.Status == 3) {
                        res.status(200).json({
                            'status': false,
                            'message': 'This user is already in your Team.',
                            'result': []
                        })
                    } else if(result && result.Status == 2) {
                        teamMember.findByIdAndUpdate(result._id, {'Active': 1, 'Status': 1}).exec((err, myData) => {
                            if (err) {
                                console.log(err);
                                return next(err);
                            }
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully send invitation',
                                'result': doc
                            })
                        })
                    } else {
                        teammember.save(function (err) {
                            if (err) {
                                console.log(err);
                                return next(err);
                            }
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully send invitation',
                                'result': doc
                            })
                        })
                    }
                })
            }
        }
    })
}


// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    var user1  = Object.assign(data.Basic , data.Account);;
    data['Modified'] = new Date();
    teamMember.findByIdAndUpdate(id, user1, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    teamMember.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getAllByUserID
exports.getAllByUserID = function(req, res, next){
    let UserID = req.params.UserID;
    let condition = {
        Active: 1,
        RequestType: 'I'
    }
    if(UserID != '0') {
        condition.UserID = UserID;
    }
    teamMember.find(condition).populate(['TeamID', 'UserID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// getAllTeamMemberByTeamID
exports.getAllTeamMemberByTeamID = function(req, res, next){
    let TeamID = req.params.TeamID;
   
    let condition = {
        Active: 1,
        //Status: 3
    }
    if(TeamID != '0') {
        condition.TeamID = TeamID;
    }
    teamMember.find(condition).populate(['TeamID', 'UserID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}


// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    var selectedReferenceBy = req.params.selectedReferenceBy;
    if (selectedReferenceBy != 0) {
        condition.ReferenceBy = selectedReferenceBy;
    }
    teamMember.find(condition)
        .sort({_id: 'desc'})
        .populate(['State' , 'City'  , 'Owner'] )
        .exec(function(err, data){
            if(err) res.send(err);
            console.log(data)
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;
                ret.Email = row.Email;
                ret.CompanyName = row.CompanyName;
                ret.State = row.State.Name;
                ret.City = row.City.Name;
                ret.Owner = row.Owner.FirstName + " " +row.Owner.MiddleName+ " " +row.Owner.LastName ;
                return ret;
            });
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    teamMember.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': data
        })
    }
});
}

// getAllTeamMemberByTeam
exports.getAllTeamMemberByTeam = function(req, res, next){
    let team = req.params.team;
    teamMember.find({'TeamID': team, 'Active': 1, 'RequestType': 'I'}).populate(['UserID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// getAllTeamMemberForLeave
exports.getAllTeamMemberForLeave = function(req, res, next){
    let TeamID = req.params.TeamID;
    let condition = {
        Active: 1,
        RequestType: 'D'
    }
    if(TeamID != '0') {
        condition.TeamID = TeamID;
    }
    console.log(condition)
    teamMember.find(condition).populate(['TeamID', 'UserID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}


// rejectRequest
exports.rejectRequest = function(req, res, next){
    const id = req.params._id; 
    teamMember.findByIdAndUpdate(id, {'Status': 2, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            User
            res.status(200).json({
                'status': true,
                'message': 'Successfully rejected request',
                'result': data
            })
        }
    })
}

// acceptRequest
exports.acceptRequest = function(req, res, next){
    const id = req.params._id;
    const UserID = req.params.UserID;
    User.findById(UserID).exec((err, userData) => {
        if(err) {
            console.log(err);
            return err;
        } else if(userData.type == 'TL' || userData.type == 'CM') {
            res.status(200).json({
                'status': false,
                'message': "Sorry! You've already another company. Please remove your company first.",
                'result': []
            })
        }
        else if(userData.type == 'MSP') {
            res.status(200).json({
                'status': false,
                'message': "Sorry! You've already been associated with another company.",
                'result': []
            })
        } else {
            teamMember.findByIdAndUpdate(id, {'Status': 3, 'ActionTime': new Date()}).exec((err, data) =>{
                if(err) {
                    console.log(err);
                    return err;
                } else {
                    User.findByIdAndUpdate(UserID, {'type': 'MSP'}).exec((err, result) => {
                        if(err) {
                            console.log(err);
                            return err;
                        } else {
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully accepted request',
                                'result': data
                            })
                        }
                    })
                }
            })
        }
    })
}

// deleteTeamMemberRequest
exports.deleteTeamMemberRequest = function(req, res, next){
    let id = req.params._id;
    teamMember.findById(id).exec((err, doc) => {
        if (err) {
            console.log(err);
            return err;
        } else {
            let teammember = {
                TeamID: doc.TeamID,
                UserID: doc.UserID,
                RequestTime: new Date(),
                Active: 1,
                RequestType: 'D',
                Status: 1
            }
            teammember = new teamMember(teammember);
            teammember.save(function (err) {
                if (err) {
                    console.log(err);
                    return err;
                }
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully send invitation',
                    'result': doc
                })
            })
        }
    })
}

// deleteTeamMember
exports.deleteTeamMember = function(req, res, next){
    let id = req.params._id;
    let userID = req.params.userID;
    User.findByIdAndUpdate(userID, {'type': 'SP'}).exec((err, doc) => {
        if (err) {
            console.log(err);
            return err;
        }
        else{
            // teamMember.findByIdAndUpdate(id, {Status: 4}, (err, data) => {
                teamMember.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
                if (err) {
                    console.log(err);
                    return err;
                }
                else{
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully deleted record',
                        'result': data
                    })
                }
            });
        }
    })
}

// deleteTeamMember
exports.deleteTeamNTMember = function(req, res, next){
    let id = req.params._id;
    let userID = req.params.userID;
    User.findByIdAndUpdate(userID, {'type': 'SP'}).exec((err, doc) => {
        if (err) {
            console.log(err);
            return err;
        }
        else{
            // teamMember.findByIdAndUpdate(id, {Status: 4}, (err, data) => {
                teamMember.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
                if (err) {
                    console.log(err);
                    return err;
                }
                else{
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully deleted record',
                        'result': data
                    })
                }
            });
        }
    })
}

// getTeamCompanyDetailByUserD
exports.getTeamCompanyDetailByUserID = function(req, res, next){
    let userID = req.params.userID;
    teamMember.findOne({'UserID': userID, 'Active': 1, 'Status': 3}).populate('TeamID').sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// getLeaveRequestDataByUserTeam
exports.getLeaveRequestDataByUserTeam = function(req, res, next){
    let UserID = req.params.UserID;
    let TeamID = req.params.TeamID;
    let condition = {
        Active: 1,
        RequestType: 'D',
        TeamID: TeamID,
        UserID: UserID
    }
    console.log(condition);
    teamMember.findOne(condition).populate(['TeamID', 'UserID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}


// rejectLeaveRequest
exports.rejectLeaveRequest = function(req, res, next){
    const id = req.params._id; 
    teamMember.findByIdAndUpdate(id, {'Status': 2, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            User
            res.status(200).json({
                'status': true,
                'message': 'Successfully rejected request',
                'result': data
            })
        }
    })
}

// acceptLeaveRequest
exports.acceptLeaveRequest = function(req, res, next){
    const id = req.params._id;
    const UserID = req.params.UserID;
    teamMember.findByIdAndUpdate(id, {'Status': 3, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            User.findByIdAndUpdate(UserID, {'type': 'SP'}).exec((err, result) => {
                if(err) {
                    console.log(err);
                    return err;
                } else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully accepted request',
                        'result': data
                    })
                }
            })
        }
    })
}