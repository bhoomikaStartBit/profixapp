const servicetypes = require('../../models/type.model');
const services = require('../../models/service.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    console.log(req.body);
    data.DateTime = new Date();
    data.Active = 1;
    console.log(data);

    const stype = new servicetypes(data);
    stype.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': stype
        })
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    servicetypes.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    servicetypes.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}


// getRelatedRecordOfType
exports.getRelatedRecordOfType = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    services.find({'Type':id , 'Active':1 }, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data,
        'Table' : 'Services'
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    servicetypes.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    servicetypes.findByIdAndUpdate(id, {Active: 0}, (Caterr, Catdata) => {
        if (Caterr) {
            return next(Caterr);
        }
        else{
            services.updateMany({'Type' : id}, {Active: 0}, (err, data) => {
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': Catdata
        })

    })
}
});
}


