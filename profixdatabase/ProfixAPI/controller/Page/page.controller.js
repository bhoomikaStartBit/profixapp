const Page = require('../../models/page.model');
const imgFile =require('../../global')
// create
exports.create = function (req, res, next) {
    let data = {};
    data.DateTime = new Date();    
    data.Title = req.body.Title;
    data.SeoTitle = req.body.SeoTitle;
    data.SeoKeyword = req.body.SeoKeyword;
    data.SeoDescription = req.body.SeoDescription;
    data.Description = req.body.Description;
    data.Slug = req.body.Slug;
    data.Active = "P";
    const page = new Page(data);    
    if(req.body.HeaderImage != ''){
        imgFile(req.body.HeaderImage , './Page/' , 'Page'+page._id);

        var imageTypeRegularExpression = /\/(.*?)$/;
        var imageBuffer = decodeBase64Image(req.body.HeaderImage);
        var headerImageTypeDetected = imageBuffer
            .type
            .match(imageTypeRegularExpression);
    
        var userUploadedImagePath = '/Page/Page' +
            page._id +
            '.' +
            headerImageTypeDetected[1]; 
    
        page.HeaderImage = userUploadedImagePath;
    }
      

        Page.findOne({'Slug': page.Slug}).exec((err, doc)=> {
        if(err) return err;
        else if(doc) {
            res.status(200).json({
                'status': false,
                'message': 'Slug Name already exist',
                'result': []
            })
        } else {
            page.save(function (err) {
                if (err) {
                    return next(err);
                }
                imgFile(req.body.HeaderImage , './Profix/Page/' , 'Page'+page._id)                              
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    'result': page
                })
            })
        }
    })
}

exports.draft = function (req, res, next) {
    let data = {};
    data.DateTime = new Date();    
    data.Title = req.body.Title;
    data.SeoTitle = req.body.SeoTitle;
    data.SeoKeyword = req.body.SeoKeyword;
    data.SeoDescription = req.body.SeoDescription;
    data.Description = req.body.Description;
    data.Slug = req.body.Slug;
    data.Active = "D";
    const page = new Page(data);    
    

        Page.findOne({'Slug': page.Slug}).exec((err, doc)=> {
        if(err) return err;
        else if(doc) {
            res.status(200).json({
                'status': false,
                'message': 'Slug Name already exist',
                'result': []
            })
        } else {
            page.save(function (err) {
                if (err) {
                    return next(err);
                }                              
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully stored in draft',
                    'result': page
                })
            })
        }
    })
}

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}

// getRelatedRecordOfType
exports.getRelatedRecordOfPage = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    Page.find({'Type':id , 'Active':'P' }, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data,
        'Table' : 'Services'
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getAllPages
exports.getAll = function(req, res, next){
    const condition= { $or : [ {'Active': 'P'},{'Active': 'D'} ]
         
    };
    Page.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    Page.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// update
exports.update = function(req, res, next){
    let id = req.params._id; 
      
    let data = req.body;
    data.Active = "P";
    data['Modified'] = new Date(); 
    if(req.body.HeaderImage != ''){
        imgFile(req.body.HeaderImage , './Profix/Page/' , 'Page'+id); 
        var imageTypeRegularExpression = /\/(.*?)$/;


        var imageBuffer = decodeBase64Image(req.body.HeaderImage);
        var headerImageTypeDetected = imageBuffer
            .type
            .match(imageTypeRegularExpression);

        var userUploadedImagePath = '/Page/Page' +
            id +
            '.' +
            headerImageTypeDetected[1];

        data['HeaderImage'] = userUploadedImagePath;
    }
    Page.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// update
exports.updatedraft = function(req, res, next){

    let id = req.params._id;    
    let data = req.body;
    data.Active = "D";
    data['Modified'] = new Date();   

    Page.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {


            res.status(200).json({
            'status': true,
            'message': 'Successfully draft record',
            'result': data
        })
    }
});
}



// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    Page.findByIdAndUpdate(id, {Active: 'T'}, (Caterr, Catdata) => {
        if (Caterr) {
            return next(Caterr);

        }
        else{
            Page.updateMany({'Type' : id}, {Active: 'T'}, (err, data) => {            
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': Catdata
        })

    })
}
});
}