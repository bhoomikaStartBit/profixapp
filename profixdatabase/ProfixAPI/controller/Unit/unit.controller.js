const units = require('../../models/unit.model');
const services = require('../../models/service.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    console.log(req.body);
    data.DateTime = new Date();
    data.Active = 1;
    console.log(data);

    const unit = new units(data);
    unit.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': unit
        })
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    units.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    units.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}


// getRelatedRecordOfUnit
exports.getRelatedRecordOfUnit = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    services.find({'Unit':id , 'Active':1 }, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data,
        'Table' : 'Services'
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    units.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    units.findByIdAndUpdate(id, {Active: 0}, (Caterr, Catdata) => {
        if (Caterr) {
            return next(Caterr);
        }
        else{
            services.updateMany({'Unit' : id}, {Active: 0}, (err, data) => {
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': Catdata
        })

    })
}
});
}


