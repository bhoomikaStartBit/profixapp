const teamRequest = require('../../models/team-request.model');
const User = require('../../models/user.model');
const TeamCompany = require('../../models/teams.model');
const teamMember = require('../../models/team-member.model');

// requestForDisposal
exports.requestForDisposal = function (req, res, next) {
    let data = req.body;
    data.Status = 1;
    data.RequestTime = new Date();
    data.ActionBy = data.UserID;
    data.RequestType = 'D';
    data.Active = 1;
    const teamrequest = new teamRequest(data);
    teamrequest.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully send Disposal request',
            'result': data
        })
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    var user1  = Object.assign(data.Basic , data.Account);;
    data['Modified'] = new Date();
    teamMember.findByIdAndUpdate(id, user1, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data
            })
        }
    });
}

// getDisposalRequestDetail
exports.getDisposalRequestDetail = function(req, res, next){
    let UserID = req.params.UserID;
    let TeamID = req.params.TeamID;
    
    teamRequest.findOne({'Active': 1, 'RequestType': 'D', 'UserID': UserID, 'TeamID': TeamID}).sort({'_id': -1}).exec((err, data) => {
        if(err) {
            console.log(err);
            return err;
        }
        else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    });
}

// getAllPendingrequestForDisposal
exports.getAllPendingrequestForDisposal = function(req, res, next){
    teamRequest.find({'Active': 1, 'RequestType': 'D'}).populate(['UserID', 'TeamID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// rejectDisposalRequest
exports.rejectDisposalRequest = function(req, res, next){
    const id = req.params._id; 
    teamRequest.findByIdAndUpdate(id, {'Status': 2, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            User
            res.status(200).json({
                'status': true,
                'message': 'Successfully rejected disposal request',
                'result': data
            })
        }
    })
}

// acceptDisposalRequest
exports.acceptDisposalRequest = function(req, res, next){
    const id = req.params._id;
    const TeamID = req.params.TeamID;
    teamRequest.findByIdAndUpdate(id, {'Status': 3, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            TeamCompany.findByIdAndUpdate(data.TeamID, {'Active': 0}).exec((err, teamData) => {
                if(err) {
                    console.log(err);
                    return err;
                } else {
                    User.findOneAndUpdate(teamData.Owner, {'type': 'SP'}).exec();
                    teamMember.find({'Active': 1, 'RequestType': 'I', 'TeamID': data.TeamID, 'Status': 3}).exec((err, memberData) => {
                        if(err) {
                            console.log(err);
                            return err;
                        } else {
                            for(let item of memberData) {
                                User.findOneAndUpdate(item._id, {'type': 'SP', 'Status': 4}).exec();
                            }
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully accepted disposal request',
                                'result': data
                            })
                        }
                    })
                }
            })
        }
    })
}