const http = require('http');
const https = require('https');
const url = require('url');    
const qs = require('querystring');
var EventEmitter = require('events').EventEmitter;
const onEmit = new EventEmitter();
const orderPaymentmeta = require('../../models/orderPaymentMeta.model');
const SettingsData = require('../../models/settings.model');
const port = 3000;
const checksum_lib = require('./checksum.js');

exports.createServer = function (req, res,next) {
	let paytmdata = req.body;
	
	SettingsData.findOne().exec((err,data) => {
		if(err){
			console.log(err);
			return err;			
		}
		else{
			var PaytmConfig = {
				mid: data.MerchantMid,
				key: data.MerchantKey,
				website: data.Website,
				channelid: data.ChannelId,
				industrytid: data.IndustryTypeId
			}

			switch(req.url){
				
				
				case "/PayTm/createServer":			
					var params 						= {};
					params['MID'] 					= PaytmConfig.mid;
					params['WEBSITE']				= PaytmConfig.website;
					params['CHANNEL_ID']			= PaytmConfig.channelid;
					params['INDUSTRY_TYPE_ID']	    = PaytmConfig.industrytid;
					params['ORDER_ID']			    = paytmdata.ORDER_ID.toString();
					params['CUST_ID'] 			    = paytmdata.CUST_ID;
			params['TXN_AMOUNT']			= paytmdata.TXN_AMOUNT.toString();
			params['CALLBACK_URL']		    = 'http://startbitinc.com:8082/PayTm/paytm_callback';			
			params['EMAIL']				= paytmdata.EMAIL;
					params['MOBILE_NO']			= paytmdata.MOBILE_NO.toString();				
					
					
					checksum_lib.genchecksum(params, PaytmConfig.key, function (err, checksum) {
					
						var txn_url = "https://securegw-stage.paytm.in/theia/processTransaction"; // for staging
						// var txn_url = "https://securegw.paytm.in/theia/processTransaction"; // for production
						
						var form_fields = "";
						for(var x in params){
							form_fields += "<input type='hidden' name='"+x+"' value='"+params[x]+"' >";
						}
						form_fields += "<input type='hidden' name='CHECKSUMHASH' value='"+checksum+"' >";
						
						var data={params:params,checksum:checksum,txn_url:txn_url}
						
						return res.status(200).json({
							response:true,
							message:'success',
							result:data
						})
						// res.writeHead(200, {'Content-Type': 'text/html'});				
						// res.write('<html><head><title>Merchant Checkout Page</title></head><body><center><h1>Please do not refresh this page...</h1></center><form method="post" action="'+txn_url+'" name="f1">'+form_fields+'</form><script type="text/javascript">document.f1.submit();</script></body></html>');				
						// res.end();
					});
				break;
			
				case "/PayTm/paytm_callback":
					
					var body = req.body;
		
					onEmit.on('end', function () {
						var html = "";				
						var post_data = body;
		
						
						html += "<b>Callback Response</b><br>";
						// for(var x in post_data){
						// 	console.log('loop: ', post_data[x], "\n");
						// 	html += x + " => " + post_data[x] + "<br/>";
						// }
						html += 'ORDERID:' + " => " + post_data['ORDERID'] + "<br/>";
						html += 'MID:' + " => " + post_data['MID'] + "<br/>";
						html += 'TXNID:' + " => " + post_data['TXNID'] + "<br/>";
						html += 'TXNAMOUNT:' + " => " + post_data['TXNAMOUNT'] + "<br/>";
						html += 'PAYMENTMODE:' + " => " + post_data['PAYMENTMODE'] + "<br/>";
						html += 'CURRENCY:' + " => " + post_data['CURRENCY'] + "<br/>";
						html += 'TXNDATE::' + " => " + post_data['TXNDATE'] + "<br/>";
						html += 'STATUS:' + " => " + post_data['STATUS'] + "<br/>";
						html += 'RESPCODE:' + " => " + post_data['RESPCODE'] + "<br/>";
						html += 'RESPMSG:' + " => " + post_data['RESPMSG'] + "<br/>";
						html += 'GATEWAYNAME:' + " => " + post_data['GATEWAYNAME'] + "<br/>";
						html += 'BANKTXNID:' + " => " + post_data['BANKTXNID'] + "<br/>";
						html += 'BANKNAME:' + " => " + post_data['BANKNAME'] + "<br/>";
						html += 'CHECKSUMHASH:' + " => " + post_data['CHECKSUMHASH'] + "<br/>";
						html += "<br/><br/>";
		
		
						// verify the checksum
						var checksumhash = post_data.CHECKSUMHASH;
						// delete post_data.CHECKSUMHASH;
						var result = checksum_lib.verifychecksum(post_data, PaytmConfig.key, checksumhash);
						console.log("Checksum Result => ", result, "\n");
						html += "<b>Checksum Result</b> => " + (result? "True" : "False");
						html += "<br/><br/>";
		
		
		
						// Send Server-to-Server request to verify Order Status
						var params = {"MID": PaytmConfig.mid, "ORDERID": post_data.ORDERID};
		
						checksum_lib.genchecksum(params, PaytmConfig.key, function (err, checksum) {
		
							params.CHECKSUMHASH = checksum;
							post_data = 'JsonData='+JSON.stringify(params);
		
							var options = {
								hostname: 'securegw-stage.paytm.in', // for staging
								// hostname: 'securegw.paytm.in', // for production
								port: 443,
								path: '/merchant-status/getTxnStatus',
								method: 'POST',
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded',
									'Content-Length': post_data.length
								}
							};
		
		
							// Set up the request
							var response = "";
							var post_req = https.request(options, function(post_res) {
								post_res.on('data', function (chunk) {
									response += chunk;
								});
		
								post_res.on('end', function(){
									console.log('S2S Response: ', response, "\n");
		
									var _result = JSON.parse(response);
									html += "<b>Status Check Response</b><br>";
									for(var x in _result){
										html += x + " => " + _result[x] + "<br/>";								
									}												
											
									let localArr = [
										{'key': 'TXNID', 'value': _result.TXNID},
										{'key': 'BANKTXNID', 'value': _result.BANKTXNID},
										{'key': 'TXNAMOUNT', 'value': _result.TXNAMOUNT},
										{'key': 'STATUS', 'value': _result.STATUS},
										{'key': 'TXNTYPE', 'value': _result.TXNTYPE},
										{'key': 'GATEWAYNAME', 'value': _result.GATEWAYNAME},
										{'key': 'RESPCODE', 'value': _result.RESPCODE},
										{'key': 'RESPMSG', 'value': _result.RESPMSG},
										{'key': 'BANKNAME', 'value': _result.BANKNAME},
										{'key': 'PAYMENTMODE', 'value': _result.PAYMENTMODE},
										{'key': 'REFUNDAMT', 'value': _result.REFUNDAMT},
										{'key': 'TXNDATE', 'value': _result.TXNDATE},
									]
									for(let item of localArr){
										var UpdateOrder = {
											'OrderID' : _result.ORDERID,
											'payment_meta_key': item.key,
											'payment_meta_value': item.value,	
										} 
										const paymentmeta = new orderPaymentmeta(UpdateOrder);
										//console.log(paymentmeta)
										paymentmeta.save(function (err) {
											if (err) {
												return next(err);
											}
										});
									}		
									console.log("Before redirection URL");
							//res.writeHead(200, {'Content-Type': 'text/html'});				
				             res.write(`<html><head><title>Merchant Checkout Page</title></head><body><center><h1>redirecting to thank you...</h1></center><script type="text/javascript">window.location.replace('http://startbitinc.com:8082/#/paytm-thankyou/`+_result.ORDERID+`');</script></body></html>`);							
							// res.write(html);
									// return res.end();
								});
							});
		
							// post the data
							post_req.write(post_data);
							post_req.end();
		
		
						});
					});
					onEmit.emit('end');
					
					
				break;
			 }

		}
	})

	
	
}


exports.getAllPayment = function(req, res, next){ 	
	let id = req.params.id;
	orderPaymentmeta.find({'OrderID':id})   
		.sort({_id: 'asc'})     
        .populate([])
        .exec(function(err, data){
            if(err) res.send(err);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
}