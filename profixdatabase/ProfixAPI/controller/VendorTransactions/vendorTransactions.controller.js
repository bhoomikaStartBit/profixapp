const vendortransactions = require('../../models/vendorTransactions.model');



// create
exports.create = function (req, res, next) {
    let data = req.body;
    data.DateTime = new Date();

    const vtran = new vendortransactions(data);
    vtran.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': vtran
        })
    })
}


// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    vendortransactions.findById(id).populate(['VendorID', 'OrderID']).exec((err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
}

// getAll
exports.getAll = function(req, res, next){
    vendortransactions.find()
        .sort({_id: 'desc'})
        .populate( ['VendorID', 'SettledBY', 'OrderID'] )
        .exec(function(err, data){
            if(err) res.send(err);
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;
                ret.OrderID = row.OrderID.ProjectName+ "#"+ row.OrderID.OrderID;
                ret.OrderAMT = row.OrderAMT;
                ret.SettledAMT = row.SettledAMT;
                ret.SettledBY = row.SettledBY ? row.SettledBY.FirstName + ' ' +row.SettledBY.MiddleName + ' '+row.SettledBY.LastName : '';
                ret.IsSettled = row.IsSettled;
                ret.VendorID = row.VendorID ? row.VendorID.FirstName + ' ' +row.VendorID.MiddleName + ' '+row.VendorID.LastName : "";
                return ret;
            });
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        })
}


// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    vendortransactions.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data
            })
        }
    });
}