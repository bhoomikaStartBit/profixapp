const UerLog = require('../../models/userlog.model');
const customers = require('../../models/customer.model');

const imgFile =require('../../global')
// create
exports.createlog = function (req, res, next) {
    let data = {};
    data.CustomerID = req.body.CustomerID;
    data.ipaddress= req.body.ipaddress;   
    data.browser = req.body.browser;
    data.LoginDateTime = new Date();
    
    
    const uerLog = new UerLog(data);    
    
    uerLog.save(function (err) {
                if (err) {
                    return next(err);
                }                              
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    
                })
            })     
}


// getAll projects
exports.currentCustomer = function(req, res, next){
    var CurrentCustomer = req.params.id;    
    customers.find({'Email' : CurrentCustomer })
        .populate([])
        .exec((err, data) => {
        if(err) return err;
else {
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    }
})

}