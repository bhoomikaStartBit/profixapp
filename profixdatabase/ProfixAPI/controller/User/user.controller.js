const users = require('../../models/user.model');
const fileUpload = require('./../../global');
const commission = require('../../models/commission.model');
const services = require('../../models/service.model');
const configData = require('../../routes/common-config');
const email = require('../../routes/email-functions');
const SettingsData = require('../../models/settings.model');
const btoa  =require('btoa');
const relations = require('../../models/relation.model');
const membershipLog = require('../../models/membership_fee_logs.model');
const Companies = require('../../models/companies.model');
const roles = require('../../models/role.model');
const speakeasy = require('speakeasy');

var handlebars = require('handlebars');
var fs = require('fs');
const teammember = require('../../models/team-member.model');

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}

// create User
exports.create = function (req, res, next) {

    try{
        let data = req.body;
        var user1  = Object.assign(data.Basic , data.Account);
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.AdditionalDocuments = data.AdditionalDocuments;
        user1.FinancialDetail = data.Financial;
        user1.DateTime = new Date();
        user1.Active = 1;
        if(data.Account.Password == ''){
            user1.Password == 'Profix@123'
        }
        user1.Password = btoa(user1.Password);
        const user = new users(user1);

        user['UserName']  = user.Email;
        const path = './Profix/Documents/User/';
        

        if(user['Avatar']) {
            let fileName = 'Avatar_' + user._id;
            fileUpload(user['Avatar'], path, fileName);
            
            var imageTypeRegularExpression = /\/(.*?)$/;
            var imageBuffer = decodeBase64Image(user['Avatar']);
            var ImageTypeDetected = imageBuffer
                .type
                .match(imageTypeRegularExpression);

                user['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
        } else{
            user['Avatar'] = '/Documents/User/user.png'
        }

        const now = new Date();
        const random = Math.floor(Math.random() * (9999999 - 100 + 1) + 100);
        user.ProfixID = 'PFX-' + now.getFullYear() + '' + now.getMonth() + '' + random;
        
        user.Account_Blocked = false;
        user.save().then((data) => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully created record',
                'result': user
            })
        }).catch(err=>{
            next(err);
        })

    }catch(err){
        next(err);
    }
}


// create
exports.createServiceProvider = function (req, res, next) {
    try{
        let data = req.body;
        var user1  = Object.assign(data.Basic , data.Account);
        user1.Password = btoa('Profix@123');
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.AdditionalDocuments = data.AdditionalDocuments;
        user1.Services = data.Services;
        user1.FinancialDetail = data.Financial;
        user1.SecondaryDetails = data.SecondaryDetails;
        user1.FamilyDetails = data.FamilyDetails;
        const Currentuserid = data.Currentuserid;
        user1.DateTime = new Date();
        user1.Active = 1;
        const secret = speakeasy.generateSecret();
        user1.Base32secret = secret.base32;
    
        if(data.Basic.TeamID){
            user1.type = 'MSP';
        }
        else{
            user1.type = 'SP';
        }
        
        const user = new users(user1);
        user['UserName']  = user.Email;
        if(user.TimePeriod == '6') {
            user.StartDate = new Date();
            user.EndDate = new Date();
            user.EndDate.setMonth(user.EndDate.getMonth() + 6);
        } else if(user.TimePeriod == '12') {
            user.StartDate = new Date();
            user.EndDate = new Date();
            user.EndDate.setMonth(user.EndDate.getMonth() + 12);
            user.EndDate.setDate(user.EndDate.getDate() - 1);
        }

        if(data.Basic.TeamID){
            var createTeamMember = {
                TeamID : data.Basic.TeamID,
                UserID : user._id,
                Active : 1,
                DateTime : new Date()
            }
            var addteammember = new teammember(createTeamMember);
            addteammember.save().then(function (adderr) {
                    
            }).catch(err=>{
                next(err);
            }) 
        }
    
        const path = './Profix/Documents/User/';
        
    
        if(user['Avatar']) {
            let fileName = 'Avatar_' + user._id;
            fileUpload(user['Avatar'], path, fileName);
            
            var imageTypeRegularExpression = /\/(.*?)$/;
            var imageBuffer = decodeBase64Image(user['Avatar']);
            var ImageTypeDetected = imageBuffer
                .type
                .match(imageTypeRegularExpression);
    
                user['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
        } else{
            user['Avatar'] = '/Documents/User/user.png'
        }
    
        const now = new Date();
        const random = Math.floor(Math.random() * (9999999 - 100 + 1) + 100);
        user.ProfixID = 'PFX-' + now.getFullYear() + '' + now.getMonth() + '' + random;
        user.Account_Blocked = false;
        var receipt_no = '';
        for(var i = 0; i < data.MandatoryDocuments.length; i++){
            if(data.MandatoryDocuments[i].Name == 'Registration Amount Receipt'){
                receipt_no = data.MandatoryDocuments[i].Number;
            }
        }
        if(receipt_no == ''){
            SettingsData.findOne({'metaKey': 'defaultAmountforSP'}).then((data2)=>{
                            
                console.log(data2.metaValue + ' defaultAmountforSP');
                var dasp = data2.metaValue;
                        
                            var data1 = {};
                            data1.amount = dasp;
                            
                            data1.start_date = user.StartDate;
                            data1.end_date = user.EndDate;
                            data1.DateTime = new Date();
                            data1.membership_user = user._id;
                            data1.updated_by = Currentuserid;
                            data1.Active = 1;
                            data1.membership_receipt_no = receipt_no;
                            console.log(data1);
                            const membership = new membershipLog(data1);
                            console.log(membership);
                            membership.save().then((data4)=> {
                                console.log('membership save');
                                console.log(data4);
                            }).catch(err=>{
                                next(err);
                            });

            });            
        }


        user.save().then((data)=> {

                SettingsData.find().then((data5)=>{

                        var replacementsValue = {
                            firstname: user.FirstName,                           
                            email: user.Email,
                            phone: user.Phone,
                            profixid: user.ProfixID,
                            loginlink : configData.AdminLoginLink
                        };

                        email(user.Email,'Welcome to Profix','welcome-email-user',replacementsValue);
    
                        res.send(JSON.stringify({
                            'status': true,
                            'message': 'User Createdsuccessfully',
                        }))
                        
                }).catch(err=>{
                    res.send(JSON.stringify({
                        'status': false,
                        'message': 'Admin email configuration not found!',
                    }))
                })
                
        }).catch(err=>{
            next(err);
        })

    }catch(err){
        next(err);
    }
}

// create
exports.createVendor = function (req, res, next) {
    let data = req.body;
    console.log(data.VendorType)
    if(data.VendorType == 'I'){
        var user1  = Object.assign(data.Basic , data.Account , data.CreditLimit);
        user1.Password = btoa(user1.Password);
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.AdditionalDocuments = data.AdditionalDocuments;
        user1.Shop = data.Shop;
        user1.FinancialDetail = data.Financial;
        user1.VenderAddress = data.VenderAddress;
        user1.VendorType = data.VendorType;
        user1.DateTime = new Date();
        user1.Active = 1;
        const com = {};
        const user = new users(user1);
    
        user['UserName']  = user.Email;
            const condition= {
                'Active': 1
            };
            services.find(condition)
                .exec(function(err, data){
                    if(err) res.send(err);
                    console.log(data);
                    for(i in data){
                        console.log(data[i]._id + ' save');
                        let commissiondata = {};
                        commissiondata.ServiceID = data[i]._id;
                        commissiondata.VendorID = user._id;
                        commissiondata.CommissionType = data[i].CommissionType;
                        commissiondata.Commission = data[i].Commission;
                        commissiondata.Active = data[i].Active;
                        const com = new commission(commissiondata);
                        com.save(function (err) {
                            if (err) {
                                console.log(err);
                                return next(err);
                            }
    
                        });
                    }
                })
        
    
        const path = './Profix/Documents/User/';
       
    
        if(user['Avatar']) {
            let fileName = 'Avatar_' + user._id;
            fileUpload(user['Avatar'], path, fileName);
            
            var imageTypeRegularExpression = /\/(.*?)$/;
            var imageBuffer = decodeBase64Image(user['Avatar']);
            var ImageTypeDetected = imageBuffer
                .type
                .match(imageTypeRegularExpression);
    
                user['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
        }else{
            user['Avatar'] = '/Documents/User/user.png'
        }
    
        // if(user['Shop'].Image) {
        //     let fileName = 'shop_' + user._id;
        //     fileUpload(user['Shop'].Image, path, fileName);
            
        //     var imageTypeRegularExpression = /\/(.*?)$/;
        //     var imageBuffer = decodeBase64Image(user['Shop'].Image);
        //     var ImageTypeDetected = imageBuffer
        //         .type
        //         .match(imageTypeRegularExpression);
    
        //         user['Shop'].Image = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
        // }
    
        const now = new Date();
        const random = Math.floor(Math.random() * (9999999 - 100 + 1) + 100);
        user.ProfixID = 'PFX-' + now.getFullYear() + '' + now.getMonth() + '' + random;
        user.Account_Blocked = false;
        user.type = 'I';
        user.save(function (err) {
            if (err) {
                return next(err);
            }
            res.status(200).json({
                'status': true,
                'message': 'Successfully created record',
                'result': user,
                'result2': com
            })
        })
    }
    else if(data.VendorType == 'C'){
        var user1  = data.UserData
        user1.Password = btoa(user1.Password);
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.Services = data.Services;
        user1.FinancialDetail = data.Financial;
        user1.CompanyDetail = data.CompanyData;
        user1.CompanyAddress = data.VenderAddress;
        user1.VendorType = data.VendorType;
        user1.DateTime = new Date();
        user1.Active = 1;
        const user = new users(user1);
    
        user['UserName']  = user.Email;

        for(let comm of data.Commissions){
            let commissiondata = {};
            commissiondata.ServiceID = comm._id;
            commissiondata.VendorID = user._id;
            commissiondata.CommissionType = comm.CommissionType;
            commissiondata.Commission = comm.Commission;
            commissiondata.Active = 1;
            const com = new commission(commissiondata);
            com.save(function (err) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
            });
        }

        const path = './Profix/Documents/User/';
        

        const now = new Date();
        const random = Math.floor(Math.random() * (9999999 - 100 + 1) + 100);
        user.ProfixID = 'PFX-' + now.getFullYear() + '' + now.getMonth() + '' + random;
        user.Account_Blocked = false;
        user.type = 'C';

        user.save(function (err) {
            if (err) {
                return next(err);
            }
            res.status(200).json({
                'status': true,
                'message': 'Successfully created record',
                'result': user
            })
        })
    }
    else{
        var user1  = data.UserData;
        user1.Password = btoa(user1.Password);
        user1.VendorCompanyID = data.CompanyData.VendorCompanyID;
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.FinancialDetail = data.Financial;
        user1.CompanyAddress = data.VenderAddress;
        user1.VendorType = data.VendorType;
        user1.DateTime = new Date();
        user1.Active = 1;
        const user = new users(user1);
    
        user['UserName']  = user.Email;

        for(let comm of data.Commissions){
            let commissiondata = {};
            commissiondata.ServiceID = comm._id;
            commissiondata.VendorID = user._id;
            commissiondata.CommissionType = comm.CommissionType;
            commissiondata.Commission = comm.Commission;
            commissiondata.Active = 1;
            const com = new commission(commissiondata);
            com.save(function (err) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
            });
        }

        const path = './Profix/Documents/User/';
        

        const now = new Date();
        const random = Math.floor(Math.random() * (9999999 - 100 + 1) + 100);
        user.ProfixID = 'PFX-' + now.getFullYear() + '' + now.getMonth() + '' + random;
        user.Account_Blocked = false;
        user.type = 'B';

        user.save(function (err) {
            if (err) {
                return next(err);
            }
            res.status(200).json({
                'status': true,
                'message': 'Successfully created record',
                'result': user
            })
        })
    }
    
}


// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    if(data.Account.Password != ''){
        var user  = Object.assign(data.Basic , data.Account);
        user.Password = btoa(user.Password);
    }
    else{
        var user  = Object.assign(data.Basic);
    }
    user.MandatoryDocuments = data.MandatoryDocuments;
    user.AdditionalDocuments = data.AdditionalDocuments;
    
    user.FinancialDetail = data.Financial;
    user['Modified'] = new Date();

    const path = './Profix/Documents/User/';
    

    if(user['Avatar']) {
        let fileName = 'Avatar_' + id;
        fileUpload(user['Avatar'], path, fileName);
        
        var imageTypeRegularExpression = /\/(.*?)$/;
        var imageBuffer = decodeBase64Image(user['Avatar']);
        var ImageTypeDetected = imageBuffer
            .type
            .match(imageTypeRegularExpression);

            user['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
    }

    users.findByIdAndUpdate(id, user, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}


// updateServiceProvider
exports.updateServiceProvider = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    if(data.Account.Password != ''){
        var user  = Object.assign(data.Basic , data.Account);
        user.Password = btoa(user.Password);
    }
    else{
        delete data.Account.Password;
        var user  = Object.assign(data.Basic , data.Account);
    }
    if(data.Account.TimePeriod != 0){
        console.log(data.Account.TimePeriod + 'months');
        if(data.Account.TimePeriod == 6) {
            user.EndDate = new Date(data.Account.EndDate);
            user.EndDate.setMonth(user.EndDate.getMonth() + 6);
            user.EndDate.setDate(user.EndDate.getDate() - 1);
            console.log('6 months');
        } else if(data.Account.TimePeriod == 12) {
            user.EndDate = new Date(data.Account.EndDate);;
            user.EndDate.setMonth(user.EndDate.getMonth() + 12);
            user.EndDate.setDate(user.EndDate.getDate() - 1);
            console.log('1 year');
        }
    
    }
    user.Account_Block = data.Account.Account_Blocked;
    user.MandatoryDocuments = data.MandatoryDocuments;
    user.AdditionalDocuments = data.AdditionalDocuments;
    user.Services = data.Services;
    user.FinancialDetail = data.Financial;
    user.SecondaryDetails = data.SecondaryDetails;
    user.FamilyDetails = data.FamilyDetails;

    const Currentuserid = data.Currentuserid;
    user['Modified'] = new Date();
    //console.log(data.MandatoryDocuments);
    //Registration Amount Receipt
   
    const path = './Profix/Documents/User/';

    if(user['Avatar']) {
        let fileName = 'Avatar_' + id;
        fileUpload(user['Avatar'], path, fileName);
        
        var imageTypeRegularExpression = /\/(.*?)$/;
        var imageBuffer = decodeBase64Image(user['Avatar']);
        var ImageTypeDetected = imageBuffer
            .type
            .match(imageTypeRegularExpression);

            user['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
    }

    var receipt_id = '';
    for(var i = 0; i < data.MandatoryDocuments.length; i++){
        if(data.MandatoryDocuments[i].Name == 'Registration Amount Receipt'){
            receipt_id = data.MandatoryDocuments[i].Number;
        }
    }
    if(receipt_id !== '') {
        const condition = {
            'Active': 1,
            'membership_user' : id
        };
        membershipLog.find(condition)
        .exec()
        .then(function(data){           
            //console.log(data);
            if(data[0]){
                data[0].membership_receipt_no = receipt_id;
                data[0].Active = 0;
                data[0].updated_by = Currentuserid;
                membershipLog.findByIdAndUpdate(data[0]._id , data[0], {new: true}).then((dataupdate)=> {
                    console.log(dataupdate);
                }).catch((err)=>{
                    console.log(err);
                })
            }
            
            
        }).catch(err=>{
            console.log(err);
            next(err);
            // console.log("error",err);
        })
    }
    users.findById(id, (err, doc) => {
        if(err){
            console.log('data')
            return err;
        } 
        else {
            if(user.TimePeriod == '6') {
                user.StartDate = new Date();
                user.EndDate = new Date();
                user.EndDate.setMonth(user.EndDate.getMonth() + 6);
            } else if(user.TimePeriod == '12') {
                user.StartDate = new Date();
                user.EndDate = new Date();;
                user.EndDate.getMonth(user.EndDate.getMonth() + 12);
            }
            users.findByIdAndUpdate(id, user, {new: true}, (err, data) => {
                if(err) {
                    return next(err);
                }
                else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully updated record',
                        'result': data
                    })
                }
            });
        }
    })
}


// updateVendor
exports.updateVendor = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    if(data.VendorType == 'I'){
        if(data.Account.Password != ''){
            var user  = Object.assign(data.Basic , data.Account, data.CreditLimit);
            user.Password = btoa(user.Password);
        }
        else{
            var user  = Object.assign(data.Basic , data.CreditLimit);
        }
        user.MandatoryDocuments = data.MandatoryDocuments;
        user.AdditionalDocuments = data.AdditionalDocuments;
        user.Shop = data.Shop;
        user.FinancialDetail = data.Financial;
        user['Modified'] = new Date();
    
        const path = './Profix/Documents/User/';
       
    
        if(user['Avatar']) {
            let fileName = 'Avatar_' + id;
            fileUpload(user['Avatar'], path, fileName);
            
            var imageTypeRegularExpression = /\/(.*?)$/;
            var imageBuffer = decodeBase64Image(user['Avatar']);
            var ImageTypeDetected = imageBuffer
                .type
                .match(imageTypeRegularExpression);
    
                user['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
        }
    
        users.findByIdAndUpdate(id, user, {new: true}, (err, data) => {
            if (err) {
                return next(err);
            }
            else{
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully updated record',
                    'result': data
                })
            }
         });
    }
    else if(data.VendorType == 'C'){
        var user1  = data.UserData;
        if(user1.Password != ''){
            user1.Password = btoa(user1.Password);
        }
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.Services = data.Services;
        user1.FinancialDetail = data.Financial;
        user1.CompanyDetail = data.CompanyData;
        user1.CompanyAddress = data.VenderAddress;
        user1.VendorType = data.VendorType;

        
        for(let comm of data.Commissions){
            let commissiondata = {};
            commissiondata.ServiceID = comm.ServiceID._id;
            commissiondata.VendorID = comm.VendorID;
            commissiondata.CommissionType = comm.CommissionType;
            commissiondata.Commission = comm.Commission;
            commission.findByIdAndUpdate(comm._id , commissiondata , {new: true}).then((data)=> {
                
            }).catch((err)=>{
                next(err)
            })
        }

        users.findByIdAndUpdate(id, user1, {new: true}, (err, data) => {
            if(err) {
                next(err);
            }
            else {
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully updated record',
                    'result': data
                })
            }
        });
    }
    else{
        var user1  = data.UserData;
        if(user1.Password != ''){
            user1.Password = btoa(user1.Password);
        }
        user1.MandatoryDocuments = data.MandatoryDocuments;
        user1.FinancialDetail = data.Financial;
        user1.CompanyDetail = data.CompanyData;
        user1.CompanyAddress = data.VenderAddress;
        user1.VendorType = data.VendorType;
        for(let comm of data.Commissions){
            let commissiondata = {};
            commissiondata.ServiceID = comm.ServiceID._id;
            commissiondata.VendorID = comm.VendorID;
            commissiondata.CommissionType = comm.CommissionType;
            commissiondata.Commission = comm.Commission;
            commission.findByIdAndUpdate(comm._id , commissiondata , {new: true}).then((data)=> {
                
            }).catch((err)=>{
                next(err)
            })
        }

        users.findByIdAndUpdate(id, user1, {new: true}, (err, data) => {
            if(err) {
                next(err);
            }
            else {
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully updated record',
                    'result': data
                })
            }
        });
    }
    
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    users.findById(id).populate(['Role' , {path : 'VendorCompanyID' , select: {'CompanyDetail':1}}]).exec((err, data) =>{
        if(data.VendorType == 'C' || data.VendorType == 'B'){
            commission.find({'VendorID' : id}).populate([{path : 'ServiceID'}]).then((commData)=>{
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data,
                    'Commissions': commData
                })
            }).catch((err)=>{
                next(err);
            })
        }
        else{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
        
})
}

// getAll
exports.getAll = function(req, res, next){
    
    const condition= {
        'Active': 1,
        'Role' : { '$ne' : '5d0a16fdb457473128b080be'}
    };
    users.find(condition)
        .sort({_id: 'desc'})
        .populate([{path : 'State',select: { 'Name':1} }, {path : 'City',select: { 'Name':1} } , {path : 'Role',select: { 'Name':1} }] )
        .select(['FirstName' , 'LastName' ,'MiddleName','ProfixID' ,'Email','Phone','EndDate' , 'type' , 'VendorType' , 'IsAvailable' , 'Account_Block'])
        .exec()
        .then(function(data){           
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
            // console.log("error",err);
        })
}

// getAll
exports.getAllUserWithSpRole = function(req, res, next){
    const now = new Date();
    const condition= {
        'Active': 1,
        'type': 'SP',
        'EndDate': {'$gt': now},  //For not showing user whose membership date is expired.
        'Account_Block': false,
        'IsAvailable' : 'Y',
        '$and':[{
            '$or': [{
                'CompanyID': { $exists: false},
                'CompanyID':  null,
            }]
        }]
    };
    users.find(condition)
        .sort({_id: 'desc'})
        .populate(['State' , 'City'])
        .exec(function(err, data){
            if(err) res.send(err);
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;
                ret.ProfixID = row.ProfixID;
                ret.Email = row.Email;
                ret.Phone = row.Phone;
                ret.FirstName = row.FirstName;
                ret.MiddleName = row.MiddleName;
                ret.LastName = row.LastName;
                ret.State = row.State.Name;
                ret.City = row.City.Name;
                ret.Avatar = row.Avatar;
                return ret;
            });
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        })
}

// delete record
exports.delete = function(req, res, next){
    try{
        let id = req.params._id;
        users.findByIdAndUpdate(id, {Active: 0}).then((data) => {

                if(data.Role == '5cb7fe5c6d2f510b5888660f'){
                    const condition= {
                        'VendorID': id
                    };
                    commission.find(condition).then((data2)=>{

                        for(i in data2){
                            console.log(data2[i]._id + ' Delete');
                            commission.findByIdAndUpdate(data2[i]._id, {Active: 0}, (err, data3) => {
                                if (err) {
                                    return next(err);
                                }
                            });
                        }
                    }).catch(err=>{
                        next(err);
                    })
                }
                res.status(200).json({
                'status': true,
                'message': 'Successfully deleted record',
                'result': data
            });

        }).catch(err=>{
            next(err);
        });
        
    }catch(err){
        next(err);
    }
}

exports.getByType = function(req, res, next){
    try{
        users.find({})
        .sort({_id: 'desc'})
        .populate([])
        .then((data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
        })
    }catch(err){
        next(err);
    }
}



exports.checkUserName = function (req , res , next) {
    try{
        var name = req.params.name;
        users.find({'UserName': name , 'Active' : 1})
        .then((data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
        })

    }catch(err){
        next(err);
    }
}

exports.checkEmail = function (req , res , next) {
    try{
        var email = req.params.Email;
        // users.find({'Services': {'id' : "5d1071f6aa2d8b13042869b5"}, 'Active': 1}).select(['Email' , 'CompanyDetail' , 'Services'])
        users.find({'Email': email, 'Active': 1}).select(['Email'])
        .then((data)=>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
        })
    }catch(err){
        next(err);
    }
}

exports.updateSPSkills = function (req , res , next) {
    try{
        let id = req.params._id;
        let skills = req.body;
        const data = { Services : skills };
        users.findByIdAndUpdate(id, data, {new: true}).then((data1) => {

            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record 1',
                'result': data1
            })
        }).catch(err=>{

            res.status(500).json({
                'status': false,
                'message': 'Error',
                'result': err
            })
        })

    }catch(err) {
        next(err);
    }
}


exports.updateVendorCategories = function (req , res , next) {
    try{
        let id = req.params._id;
        let skills = req.body;
        const data = { Services : skills };
        users.findByIdAndUpdate(id, data, {new: true}).then((data1) => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record 1',
                'result': data1
            })
        }).catch(err=>{
            res.status(500).json({
                'status': false,
                'message': 'Error',
                'result': err
            })
        })
    }catch(err){
        next(err);
    }
}

exports.getAllCompanyVendorUser = function (req , res , next) {
    try{
        const condition= {
            'Active': 1,
            'VendorType' : 'C',
             'CompanyDetail.CompanyType' : {
                 $ne : 'Un-Registered'
             }
        };

        users.find(condition).sort({_id: 'desc'}).select(['CompanyDetail']).then((data)=>{           
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data
                })
            }).catch(err=>{
                next(err);
            })

    }catch(err){
        next(err);
    }
}

exports.getAllRelations = function(req, res, next){
    
    const condition = {
        'Active': 1
    };

    relations.find(condition)
        .sort({Order: 'asc'})
        .exec()
        .then(function(data){           
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
            // console.log("error",err);
        })
}

// getAllByRoleId
exports.getByRoleID = function(req, res, next){
    let RoleID = req.params._id;
    const condition= {
       'Active': 1,
       'Key' : { $in : ["SP", "SA", "CM", "C", "V" ,"PC" ]},
    };
    var exclude_roles = [];
    roles.find(condition)
    .select('_id')
    .exec()
    .then(function(data){           
        console.log(data);
        for(var i = 0; i < data.length; i++){
            exclude_roles.push(data[i]._id);
            console.log(data[i]._id);
        }
    
        console.log(exclude_roles + ' test');

        if(RoleID == 'other'){
            const condition= {
                'Active': 1,
                'Role': { $nin : exclude_roles},
            };
            
            //.populate([{path : 'State',select: { 'Name':1 } }, {path : 'City',select: { 'Name':1} } , {path : 'Role',select: { 'Name': 1,'Key':1}, match : { 'Key' : { $nin: ['SP','SA','V','C','CM']}}}] )
            console.log(RoleID+ ' 1');
            users.find(condition)
            .sort({_id: 'desc'})
            .populate([{path : 'State',select: { 'Name':1 } }, {path : 'City',select: { 'Name':1} } , {path : 'Role',select: { 'Name': 1}}] )
            .select(['FirstName' , 'LastName' ,'MiddleName','ProfixID' ,'Email','Phone','EndDate' , 'type' , 'VendorType' , 'IsAvailable' , 'Account_Block'])
            .exec()
            .then(function(data){           
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data
                })
            }).catch(err=>{
                next(err);
                // console.log("error",err);
            })
        }else{
            const condition= {
                'Active': 1,
                'Role': RoleID,
                '$and':[{
                    '$or': [{
                        'CompanyID': { $exists: false},
                        'CompanyID':  null,
                    }]
                }]
            };
            console.log(RoleID+ ' 2');
            users.find(condition)
            .sort({_id: 'desc'})
            .populate([{path : 'State',select: { 'Name':1} }, {path : 'City',select: { 'Name':1} } , {path : 'Role',select: { 'Name':1} }, {path : 'VendorCompanyID',select: { 'CompanyDetail':1}}] )
            .select(['FirstName','CompanyDetail' , 'LastName' ,'MiddleName','ProfixID' ,'Email','Phone','EndDate' , 'type' , 'VendorType' , 'IsAvailable' , 'Account_Block'])
            .exec()
            .then(function(data){           
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': data
                })
            }).catch(err=>{
                next(err);
                // console.log("error",err);
            })

        }
    }).catch(err=>{
        //next(err);
        console.log("error",err);
    })

    
}

exports.checkAvailable = function(req, res, next){
    let Available = req.params.Available;
    let id = req.params.id;

    users.findByIdAndUpdate(id , {'IsAvailable' : Available}).exec().then((data)=>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
        })
    }).catch((err)=>{
        next(err)
    })

}


exports.checkBlock = function(req, res, next){
    let Block = req.params.Block;
    let id = req.params.id;

    users.findByIdAndUpdate(id , {'Account_Block' : Block}).exec().then((data)=>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
        })
    }).catch((err)=>{
        next(err)
    })

}

