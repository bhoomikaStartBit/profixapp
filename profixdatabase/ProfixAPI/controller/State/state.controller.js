const states = require('../../models/state.model');
const cities = require('../../models/city.model');
const localities = require('../../models/locality.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    console.log(req.body);
    data.DateTime = new Date();
    data.Active = 1;
    console.log(data);

    const stat = new states(data);
    stat.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': stat
        })
    })
}



// update
exports.update = function(req, res, next){
    console.log(req.params._id);
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    states.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});

}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    states.findById(id, (err, data) => {
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}


// getRelatedRecordOfState
exports.getRelatedRecordOfState = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    cities.find({'State':id , 'Active':1 }, (err, data) =>{
        if(data.length > 0){
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data,
            'Table': 'Cities'
        })
    }
    else{
        localities.find({'State':id , 'Active':1 }, (err, data) =>{
            res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data,
            'Table': 'Localities'
        })
    })
    .catch(exc => {
            console.log(exc)
        return handleError(exc);
    })
    }

})
.catch(exc => {
        console.log(exc)
    return handleError(exc);
})
}

// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    states.find(condition)
        .sort({_id: 'desc'})
        .populate([])
        .exec()
        .then(function(data){           
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch(err=>{
            next(err);
            // console.log("error",err);
        })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    states.findByIdAndUpdate(id, {Active: 0}, (Stateerr, Statedata) => {
        if (Stateerr) {
            return next(Stateerr);
        }
        else{
            cities.updateMany({'State': id}, {Active: 0}, (Cityerr, Citydata) => {
                if(Cityerr) {
                    return next(Cityerr);
                }
                else {
                    localities.updateMany({'State': id}, {Active: 0}, (err, data) => {
                        if(err) {
                            return next(err);
                        }
                        else{
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully deleted record',
                                'result': Statedata
                            })
                        }
                    });
                }
            });
        }
    });
}

