const Faq = require('../../models/faq.model');
const imgFile =require('../../global')
// create
exports.create = function (req, res, next) {
    let data = {};
    data.DateTime = new Date();    
    data.Question = req.body.Question;    
    data.Answer= req.body.Answer;    
    data.Order = req.body.Order; 
    data.Active = "P";
    const faq = new Faq(data);    
    

    Faq.findOne({'Question': faq.Question}).then(( doc)=> {
        if(doc) {
            res.status(200).json({
                'status': false,
                'message': 'Question already exist',
                'result': []
            })
        } else {
            faq.save().then(function (data) {                            
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully created record',
                    'result': faq
                })
            }).catch((err)=>{
                next(err);
            })
        }
    }).catch((err)=>{
        next(err);
    })
}

exports.draft = function (req, res, next) {
    let data = {};
    data.DateTime = new Date();    
    data.Question = req.body.Question;    
    data.Answer= req.body.Answer;     
    data.Active = "D";
    const faq = new Faq(data);   

    Faq.findOne({'Question': faq.Question}).then((doc)=> {
        if(doc) {
            res.status(200).json({
                'status': false,
                'message': 'Question already exist',
                'result': []
            })
        } else {
            faq.save(function (err) {
                if (err) {
                    return next(err);
                }                              
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully stored in draft',
                    'result': faq
                })
            })
        }
    }).catch((err)=>{
        next(err);
    })
}

// function decodeBase64Image(dataString) {
//     var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
//     var response = {};

//     if (matches.length !== 3) {
//         return new Error('Invalid input string');
//     }

//     response.type = matches[1];
//     response.data = new Buffer(matches[2], 'base64');

//     return response;
// }

// getRelatedRecordOfType
exports.getRelatedRecordOfFaq = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    Faq.find({'Type':id , 'Active':'P' }).then((data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data,
        'Table' : 'Services'
    })
})
.catch(exc => {
    next(exc);
})
}

// getAllPages
exports.getAll = function(req, res, next){
    const condition= { $or : [ {'Active': 'P'},{'Active': 'D'} ]
         
    };
    Faq.find(condition)
        .sort({Order: 'asc'})
        .populate([])
        .then(function(data){
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }).catch((err)=>{
            next(err);
        })
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    Faq.findById(id).then((data) =>{
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
    .catch(exc => {
        next(exc);
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;    
    let data = req.body;
    data.Active = "P";
    data['Modified'] = new Date();   

    Faq.findByIdAndUpdate(id, data, {new: true}).then((data) => {
        
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }).catch((err)=>{
        next(err);
    })
}

// update
exports.updatedraft = function(req, res, next){

    let id = req.params._id;    
    let data = req.body;
    data.Active = "D";
    data['Modified'] = new Date();   

    Faq.findByIdAndUpdate(id, data, {new: true}).then((err, data) => {
        
        res.status(200).json({
            'status': true,
            'message': 'Successfully draft record',
            'result': data
        })
    }).catch((err)=>{
        next(err)
    })
}



// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    Faq.findByIdAndUpdate(id, {Active: 'T'}, (Caterr, Catdata) => {
        if (Caterr) {
            return next(Caterr);

        }
        else{
            Faq.updateMany({'Type' : id}, {Active: 'T'}).then((data) => {            
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully deleted record',
                    'result': Catdata
                })
            }).catch((err)=>{
                next(err)
            })
        }
    });
}