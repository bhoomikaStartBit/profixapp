const User = require('../../models/user.model');
const Customer = require('../../models/customer.model');
const fileUpload = require('../../global');
const teamRequest = require('../../models/team-request.model');
const teams = require('../../models/teams.model');
const speakeasy = require('speakeasy');
const configData = require('../../routes/common-config');
const email = require('../../routes/email-functions');
const btoa = require('btoa');


function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;

    const path = './Profix/Documents/User/';
    if(data['Avatar']) {
        let fileName = 'Avatar_' + id;
        fileUpload(data['Avatar'], path, fileName);
        console.log(data['Avatar']);
        var imageTypeRegularExpression = /\/(.*?)$/;
        var imageBuffer = decodeBase64Image(data['Avatar']);
        var ImageTypeDetected = imageBuffer
            .type
            .match(imageTypeRegularExpression);

            data['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
    }
    User.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record 1',
                'result': data,
                'Avatar': data['Avatar']
            })
        }
    });
}

// updateProfilePassword
exports.updateProfilePassword = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    const OldPassword = btoa(data.OldPassword);
    User.findOne({'_id': id, 'Password': OldPassword}).exec((err, UserData) => {
        if(err) {
            console.log(err);
            return err;
        }
        else if (!UserData){
            res.status(200).json({
                'status': false,
                'message': 'Invalid old password',
                'result': []
            })
        }
        else {
            const myData = {
                Password: btoa(data.NewPassword)
            }
            User.findByIdAndUpdate(id, myData, {new: true}, (err, data) => {
                if(err) {
                    console.log(err);
                    return err;
                }
                else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Password Updated Successfully',
                        'result': data,
                        'Avatar': data['Avatar']
                    })
                }
            });
        }
    })
}


// update Customer Profile
exports.updateCustomer = function(req, res, next){
    let id = req.params._id;
    let data = req.body;

    Customer.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }else {

            const CustEmail = data.Email;
            const CustAvatar = data.Avatar;

            const path = './Profix/Documents/User/';
            if(data['Avatar'] && CustAvatar != data.Avatar) {
                let fileName = 'Avatar_' + id;
                fileUpload(data['Avatar'], path, fileName);
                var imageTypeRegularExpression = /\/(.*?)$/;
                var imageBuffer = decodeBase64Image(data['Avatar']);
                var ImageTypeDetected = imageBuffer
                    .type
                    .match(imageTypeRegularExpression);

                    data['Avatar'] = '/Documents/User/'+ fileName +'.'+ImageTypeDetected[1];
            }

            if(CustEmail == data.Email) {

                Customer.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
                    if(err) {
                        return next(err);
                    }
                    else {
                        res.status(200).json({
                            'status': true,
                            'message': 'Profile updated successfully',
                            'result': data,
                            'Avatar': data['Avatar']
                        })
                    }
                });

            }else {

                const secret = speakeasy.generateSecret();
                data.Base32secret = secret.base32;
                data.Modified = new Date();
                data.EmailVerified = 0;

                var replacementsValue = {
                    emailverificationlink : configData.CustomerEmailVerificationLink+data.Email+'/'+data.Base32secret
                };
        
                email(data.Email,'Email Update Verification Request','email-update-verification-request',replacementsValue); 

            }      
        }
        
    }); 
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    User.findById(id).populate(['Role']).exec((err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
}

// sendRequestForTeam
exports.sendRequestForTeam = function(req, res, next){
    let id = req.params._id;
    let type = req.params.type;
    const data = {};
    
    data.UserID = id;
    data.Status = 1;
    data.RequestTime = new Date();
    data.ActionBy = id;
    data.Active = 1;
    data.RequestType = 'R';
    data.type = type;
    const temm = new teamRequest(data);
    User.findByIdAndUpdate(id, {'isTeam': true}, {new: true}, (err, result) => {
        
        if(err){
            console.log(err);
            return err;
        } else {
            temm.save(function(err, doc) {
                if(err) {console.log(err); return err;}
                else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully send request for create ' + type,
                        'result': []
                    })
                }
            })
        }
    })
}

// getAllrequest
exports.getAllPendingRequest = function(req, res, next){
    teamRequest.find({'Active': 1, 'RequestType': 'R'}).populate(['UserID']).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

// rejectRequest
exports.rejectRequest = function(req, res, next){
    const id = req.params._id; 
    teamRequest.findByIdAndUpdate(id, {'Status': 2, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            User.findOneAndUpdate({'_id': data.UserID}, {'isTeam': false}, {new: true}, (err, result) => {
        
                if(err){
                    console.log(err);
                    return err;
                } else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully rejected request',
                        'result': data
                    })
                }
            })
        }
    })
}

// acceptRequest
exports.acceptRequest = function(req, res, next){
    const id = req.params._id;
    const type = req.params.type; 
    let doc = {};
    if(type == 'Team') {
        doc = {
            type: 'TL',
        }
    } else {
        doc = {
            type: 'CM',
        } 
    }
    teamRequest.findByIdAndUpdate(id, {'Status': 3, 'ActionTime': new Date()}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            User.findOneAndUpdate({'_id': data.UserID}, doc, {new: true}, (err, result) => {
                if(err){
                    console.log(err);
                    return err;
                } else {
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully accepted request',
                        'result': data
                    })
                }
            })
        }
    })
}

// getRequestDetailByUserID
exports.getRequestDetailByUserID = function(req, res, next){
    const id = req.params._id; 
    teamRequest.findOne({'UserID': id, 'Active': 1, 'RequestType': 'R'}).sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}

exports.updateVenderAddress = function (req , res , next) {
    let id = req.params._id;
    let skills = req.body;
    const data = { VenderAddress : skills };
    User.findByIdAndUpdate(id, data, {new: true}, (err, data1) => {
        if(err) {
            //return next(err);
            res.status(500).json({
                'status': false,
                'message': 'Error',
                'result': err
            })
        }
        else {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record 1',
                'result': data1
            })
        }
    });
}

// getTeamDetailByUserID
exports.getTeamDetailByUserID = function(req, res, next){
    const id = req.params._id; 
    teams.findOne({'Owner': id, 'Active': 1})
    .populate(['State' , 'City' , 'Locality' , 'Owner'] )
    .sort({'_id': -1}).exec((err, data) =>{
        if(err) {
            console.log(err);
            return err;
        } else {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        }
    })
}


exports.checkEmail = function (req , res , next) {
    var email = req.params.Email;
    var id = req.params.Id;
    Customer.find({'Email': email, '_id': { $ne: id }})
        .exec(function(err, data){
        if(err) res.send(err);
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
}