const document = require('../../models/document.model');

// create document
exports.create = function(req, res, next){
    var Name = req.body.Name;
    console.log(Name);
    let data = new document(req.body);
    data['Modified'] = new Date();
    data['Active'] = 1;
    data.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': data
        })
    })
}

// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    document.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': data
            })
        }
      });
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    console.log(id)
    document.findById(id, (err, data) =>{     
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    })
    .catch(exc=>{
        console.log(exc)
        return handleError(exc);
    })
}

// getAll
exports.getAll = function(req, res, next){    
    document.find({'Active': 1 , 'User' : 'All'}).then(function( data){
           
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    }).catch((err)=>{
        next(err);
    })
}
// getAll
exports.getAllDocumentsByUser = function(req, res, next){
    let user = req.params.User;
    var condition = {
        'Active' : 1
    }

    if(req.params.User == 'Vendor'){
        condition.User = user;
    }
    else if(req.params.User == 'Service Provider'){
        condition.User = user;
    }
    else{
        condition.User = 'All';
    }
    document.find(condition).sort({'seq': 'ASC'}).then(function(data){
       
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data
        })
    }).catch((err)=>{
        next(err)
    })
}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    console.log(id);
    document.findByIdAndRemove(id, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
                'status': true,
                'message': 'Successfully deleted record',
                'result': data
            })
        }
      });
}