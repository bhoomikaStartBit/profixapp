const localities = require('../../models/locality.model');
const users = require('../../models/user.model');
var customers =require('../../models/customer.model')

// create
exports.create = function (req, res, next) {
    let data = req.body;
    data.DateTime = new Date();
    data.Active = 1;

    const locality = new localities(data);
    locality.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': locality
        })
    })
}



// update
exports.update = function(req, res, next){
    let id = req.params._id;
    let data = req.body;
    data['Modified'] = new Date();
    localities.findByIdAndUpdate(id, data, {new: true}, (err, data) => {
        if(err) {
            return next(err);
        }
        else {
            res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': data
        })
    }
});
}

// getOne
exports.getOne = function(req, res, next){
    let id = req.params._id;
    localities.findById(id, (err, data) =>{
        res.status(200).json({
        'status': true,
        'message': 'Success',
        'result': data
    })
})
.catch(exc => {
    next(exc);
})
}



// getRelatedRecordOfLocality
exports.getRelatedRecordOfLocality = function(req, res, next){
    let id = req.params._id;
    users.find({'Locality':id , 'Active':1 }, (err, data) =>{
        if(data.length > 0)
    {
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': data,
            'Table': 'users'
        })
    }
        else{
        customers.find({'Locality':id , 'Active':1 }, (err, data) =>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data,
                'Table': 'customers'
            })
    })
    .catch(exc => {
        next(exc);
    })

    }

})
.catch(exc => {
    next(exc);
})
}


// getAll
exports.getAll = function(req, res, next){
    const condition= {
        'Active': 1
    };
    localities.find(condition)
        .populate(['State' , 'City'])
        .then(( data) => {
            let sentData = [];
            sentData = data.map(row=>{
                let ret = {};
                ret._id = row._id;
                ret.Name = row.Name;
                ret.StateName = row.State.Name;
                ret.CityName = row.City.Name;
                return ret;
            })
            return res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': sentData
            })
        }).catch ((err)=>{
            next(err);
    })
}


// getOne
exports.getAllLocalitiesByCity = function(req, res, next){
    let id = req.params._id;
    if(id == null || id == undefined){
        res.status(200).json({
            'status': false,
            'message': 'Success',
            'result': []
        })  
    }
    else{
        localities.find({'City' : id , 'Active' : 1}, (err, data) =>{
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': data
            })
        })
        .catch(exc => {
            next(exc);
        })
    }

}

// delete
exports.delete = function(req, res, next){
    let id = req.params._id;
    localities.findByIdAndUpdate(id, {Active: 0}, (err, data) => {
        if (err) {
            return next(err);
        }
        else{
            res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': data
        })
    }
});
}

