const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FaqSchema = new Schema({
    Question: {type: String},
    Answer: {type: String},   
    Order : {type: Number},
    DateTime: {type: Date},       
    Active: {type: String, required: true},
});

// Export the model
module.exports = mongoose.model('faqs', FaqSchema);