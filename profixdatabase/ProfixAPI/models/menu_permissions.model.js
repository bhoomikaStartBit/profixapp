const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MenuPermissionSchema = new Schema({
    RoleID: {type: String },
    MenuID: {type: String },
    Permission: {type: Boolean },
    Modified: {type: Date} ,
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'users'},
    Active: {type: Number},
});

// Export the model
module.exports = mongoose.model('menu_permissions', MenuPermissionSchema);