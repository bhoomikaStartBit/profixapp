const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RelationSchema = new Schema({
    relationship: {type: String},
    value: {type: String},
    Order: {type: Number},
    Active: {type: Number, required: true}
});

// Export the model
module.exports = mongoose.model('relationships', RelationSchema);