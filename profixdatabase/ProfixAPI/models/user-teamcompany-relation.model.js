const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserTeamcompanyRelationSchema = new Schema({
    TeamID: {type: mongoose.Schema.ObjectId, ref: 'teamcompanies'},
    UserID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('userteamcompanyrelations', UserTeamcompanyRelationSchema);