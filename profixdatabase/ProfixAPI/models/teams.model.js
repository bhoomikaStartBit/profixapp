const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamSchema = new Schema({     
    CompanyID: { type: mongoose.Schema.ObjectId, ref: 'companies'}, 
    TeamName: { type: String, required: true , unique : true, index: true },      
    TLID: { type: mongoose.Schema.ObjectId, ref: 'users'},     
    DateTime: { type: Date, required: true },
    Active: { type: Number, required: true },
    Projects: { type: mongoose.Schema.ObjectId, ref: 'orders'}
});


// Export the model
    module.exports = mongoose.model('teams', TeamSchema);