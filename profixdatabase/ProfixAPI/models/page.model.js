const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PageSchema = new Schema({
    Title: {type: String},
    Description: {type: String},
    SeoTitle: {type: String},  
    SeoKeyword: {type: String},  
    SeoDescription: {type: String},
    DateTime: {type: Date},
    Slug: {type: String},
    HeaderImage: {type: String},     
    Active: {type: String, required: true},
});

// Export the model
module.exports = mongoose.model('pages', PageSchema);