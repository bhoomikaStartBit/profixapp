const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const vendorTransactionsSchema = new Schema({
    VendorID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'orders'},
    OrderAMT: {type: mongoose.Schema.Types.Double},
    SettledAMT: {type: mongoose.Schema.Types.Double},
    SettledBY: {type: mongoose.Schema.ObjectId, ref: 'users'},
    IsSettled: {type: Number},
    Notes: {type: String},
    DateTime: {type: Date},
});

// Export the model
module.exports = mongoose.model('vendortransactions', vendorTransactionsSchema);