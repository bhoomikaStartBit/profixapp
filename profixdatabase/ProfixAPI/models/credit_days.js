const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const credit_daysSchema = new Schema({
    days: {type: String},
    value: {type: String},
    Order: {type: Number},    
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('credit_days', credit_daysSchema);