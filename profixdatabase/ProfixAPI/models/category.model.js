const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    Name: {type: String},
    Description: {type: String},
    HeaderImage: {type: String},
    IconImage: {type: String},
    Isfeatured: {type: Boolean},
    DateTime: {type: Date},
    Slug: {type: String},
    Order: {type: Number},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('categories', CategorySchema);