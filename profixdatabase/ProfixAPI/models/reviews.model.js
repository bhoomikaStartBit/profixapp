const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
    OrderAssignID:{type: mongoose.Schema.ObjectId, ref: 'order-assignments'},
    UserID:{type: mongoose.Schema.ObjectId, ref: 'users'},
    CustomerID:{type: mongoose.Schema.ObjectId, ref: 'customers'},
    Rating : {type: Number , default: 0},
    Review : {type: String},
    Attachments : {type: Object},
    IsApproved : {type: Boolean, default: false },
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('reviews', ServiceSchema);