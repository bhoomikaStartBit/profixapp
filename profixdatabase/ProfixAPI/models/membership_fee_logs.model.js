const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MembershipSchema = new Schema({
    end_date: {type: Date},
    start_date: {type: Date},
    amount: {type: Number},
    membership_receipt_no: { type: String },
    membership_user: {type: mongoose.Schema.ObjectId, ref: 'users'},
    updated_by: {type: mongoose.Schema.ObjectId, ref: 'users'},
    DateTime: { type: Date, required: true },
    Active: {type: Number, required: true}
});

// Export the model
module.exports = mongoose.model('membership_fee_logs', MembershipSchema);