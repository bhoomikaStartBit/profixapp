const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DocumentSchema = new Schema({    
    Name: {type: String },
    Description: {type: String },
    Modified: {type: Date} ,
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number},  
});

// Export the model
module.exports = mongoose.model('Documents', DocumentSchema);