const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
    ProfixID: {type: String},
    UserName: { type: String },
    FirstName: { type: String},
    MiddleName: { type: String},
    LastName: { type: String},
    Password: { type: String },
    Email: { type: String },
    Phone: { type: String},
    AltPhone: { type: String},
    AltName: { type: String},
    dob: { type: Date},
    Gender: { type: String },
    Avatar: { type: String  , default : '/Documents/User/user.png'},
    Address: { type: String },
    Document: { type: Object },
    Locality: { type: mongoose.Schema.ObjectId, ref: 'localities' },
    City: {type: mongoose.Schema.ObjectId, ref: 'cities'},
    State: { type: mongoose.Schema.ObjectId, ref: 'states'},
    ReferenceBy: { type: mongoose.Schema.ObjectId, ref: 'users'},
    Pin: { type: String },
    ResetToken: { type: String },
    ResetExpires: { type: String },
    Base32secret : { type: String },
    Latitude : { type: Number },
    Longitude : { type: Number }, 
    TOTPVerified : { type: Number },
    EmailVerified : { type: Number },
    DateTime: { type: Date },
    Active: { type: Number },
});

// Export the model
module.exports = mongoose.model('customers', CustomerSchema);