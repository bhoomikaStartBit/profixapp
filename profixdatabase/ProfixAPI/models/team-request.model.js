const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    TeamID: {type: mongoose.Schema.ObjectId, ref: 'teamcompanies'},
    UserID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    RequestTime: {type: Date},
    Status: {type: Number},
    ActionTime: { type: Date},
    type: {type: String},
    RequestType: {type: String},
    ActionBy: {type: mongoose.Schema.ObjectId, ref: 'users'},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('Team_Request', CategorySchema);