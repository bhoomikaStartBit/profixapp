const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
    Title: {type: String},
    Description: {type: String},
    Width: {type: Number},
    Height: {type: Number},
    Service: {type: mongoose.Schema.ObjectId, ref: 'services'},
    UserID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    Amount: {type: mongoose.Schema.Types.Double},
    Status: {type: String},
    ScheduledDate: {type: Date},
    IsPaymentCollect: {type: Boolean},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('milestones', ServiceSchema);