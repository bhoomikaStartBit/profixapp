const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const OrderAssignSchema = new Schema({
    UserID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'orders'},
    MilestoneID: {type: mongoose.Schema.ObjectId, ref: 'milestones'},
    TaskID: {type: mongoose.Schema.ObjectId},
    Status : {type: String},
    Notes : {type: String},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('order-assignments', OrderAssignSchema);