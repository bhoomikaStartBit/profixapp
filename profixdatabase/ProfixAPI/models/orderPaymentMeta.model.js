const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderPaymentMetaSchema = new Schema({
    OrderID :{type:Number},       
    payment_meta_key: {type: String},
    payment_meta_value: {type: String}    
});

// Export the model
module.exports = mongoose.model('Order-PaymentMeta', OrderPaymentMetaSchema);