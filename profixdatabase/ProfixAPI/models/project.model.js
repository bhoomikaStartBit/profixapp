const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
    'OrderID':{type: Number},
    ProjectName: {type: String},
    Customer: {type: mongoose.Schema.ObjectId, ref: 'customers'},
    Locality: {type: mongoose.Schema.ObjectId, ref: 'localities'},
    City: {type: mongoose.Schema.ObjectId, ref: 'cities'},
    State: {type: mongoose.Schema.ObjectId, ref: 'states'},
    Address: {type: String},
    ServiceID: {type: mongoose.Schema.ObjectId, ref: 'services'},
    Services : {type : Object},
    PaymentMode: {type: String},
    Comments: {type: String},
    PaymentAmount: {type: mongoose.Schema.Types.Double},
    DiscountAmount: {type: mongoose.Schema.Types.Double},
    DiscountType: {type: String},
    ProjectImages: {type: Array},
    AssignTo: {type: mongoose.Schema.ObjectId, ref: 'users'},
    CreatedBy: {type: mongoose.Schema.ObjectId, ref: 'users'},
    Status: {type: String , default : 'New'},
    DateTime: {type: Date},
    ScheduledServiceProvider: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('orders', ServiceSchema);