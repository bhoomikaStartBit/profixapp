const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CitySchema = new Schema({
    Name: {type: String},
    State: {type: mongoose.Schema.ObjectId, ref: 'states'},
    Latitude: {type: Number},
    Longitude: {type: Number},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('cities', CitySchema);