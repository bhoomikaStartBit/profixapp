const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CompanyAddressSchema = new Schema({
    Office: {type: String},
    CompanyID: {type: mongoose.Schema.ObjectId, ref: 'companies'},
    Name : {type: String},
    Email : {type: String},
    Number : {type: String},
    State:{type: mongoose.Schema.ObjectId, ref: 'states'},
    City:{type: mongoose.Schema.ObjectId, ref: 'cities'},
    Locality:{type: mongoose.Schema.ObjectId, ref: 'localities'},
    Pin:{type: String},
    Address:{type: Object},
    DateTime : {type : Date},
    Active : {type : Number}  
});

// Export the model
module.exports = mongoose.model('companies-address', CompanyAddressSchema);