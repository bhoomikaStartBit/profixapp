const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserLogSchema = new Schema({
    CustomerID: {type: String},
    ipaddress: {type: String},
    browser:{type:String},   
    LoginDateTime: {type: Date}               
});

// Export the model
module.exports = mongoose.model('user-logs', UserLogSchema);