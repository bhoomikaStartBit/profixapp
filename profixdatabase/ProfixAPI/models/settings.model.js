const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const SettingsrSchema = new Schema({
    CompanyEmail: {type: String},
    FooterCopyrightNote: { type: String},
    CurrencySign: { type: String},
    AdminCommission: { type:  Number},
    SplitOrder: { type: Number},
    GST: { type:  Number},
    Email: { type: String},
    Host: { type: String},
    Username: { type: String},
    Password: { type: String},
    Port: { type:  Number},
    HomePageTitle: { type: String },
    HomePageMetakeywords: { type: String },
    HomePageMetadescriptions: { type: String },
    PreVisitcharges: { type: Number },
    nextDayScheduleTime: { type: Number },
    QIPCUserCommission: { type: Number },
    defaultAmountforSP: { type: Number },
    daysMilestoneWillShow: { type: Number },
    daysBeforeSPcanLeaveOrder: { type: Number },
    hoursBeforeOrderStarted: { type: Number },
    MerchantMid: { type: String },
    MerchantKey: { type:String },
    GatewayMode: { type: String },
    Website: { type: String },
    ChannelId: { type: String },
    IndustryTypeId: { type: String },
    metaKey: {type: String},
    metaValue: {type: String},
});

// Export the model
module.exports = mongoose.model('settings', SettingsrSchema);