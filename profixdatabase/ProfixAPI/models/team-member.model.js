const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamMemberSchema = new Schema({
    TeamID: {type: mongoose.Schema.ObjectId, ref: 'teams'},
    UserID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    //RequestTime: {type: Date},
    //Status: {type: Number},
    DateTime: { type: Date},
    //RequestType: {type: String},
    //ActionBy: {type: mongoose.Schema.ObjectId, ref: 'users'},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('team_members', TeamMemberSchema);