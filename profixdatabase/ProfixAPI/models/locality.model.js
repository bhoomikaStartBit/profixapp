const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LocalitySchema = new Schema({
    Name: {type: String},
    State: {type: mongoose.Schema.ObjectId, ref: 'states'},
    City: {type: mongoose.Schema.ObjectId, ref: 'cities'},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('localities', LocalitySchema);