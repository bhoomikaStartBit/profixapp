const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommissionSchema = new Schema({
    ServiceID:{type: mongoose.Schema.ObjectId, ref: 'services'},
    VendorID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    Commission:{ type: String },
    CommissionType:{ type: String, required: true},
    //DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('commission', CommissionSchema);