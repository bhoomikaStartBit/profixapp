const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const OrderActivitySchema = new Schema({
    UserID: {type: mongoose.Schema.ObjectId},
    CustomerID: {type: mongoose.Schema.ObjectId},
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'projects'},
    Status: {type: String},
    Message: {type: String},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('order-activities', OrderActivitySchema);