const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const company_typeSchema = new Schema({
    ctype: {type: String},
    value: {type: String},
    Order: {type: Number},    
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('company_types', company_typeSchema);