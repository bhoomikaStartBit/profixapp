const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CompanySchema = new Schema({
    Type: {type: String},
    Name : {type: String},
    Phone : {type: Number},
    Email : {type: String},
    Manager : {type: mongoose.Schema.ObjectId, ref: 'users'},
    Document:{type: Object},
    FinancialDetails:{type: Object},
    DateTime : {type : Date},
    Active : {type : Number}  
});

// Export the model
module.exports = mongoose.model('companies', CompanySchema);