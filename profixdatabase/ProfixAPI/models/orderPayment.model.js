const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const OrderPaymentSchema = new Schema({
    UserID: {type: mongoose.Schema.ObjectId, ref: 'users'},
    OrderAssignID: {type: mongoose.Schema.ObjectId, ref: 'order-assignments'},
    PaymentMode : {type: String},
    PaymentAmount: {type: mongoose.Schema.Types.Double},
    Notes : {type: String},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('order-payments', OrderPaymentSchema);