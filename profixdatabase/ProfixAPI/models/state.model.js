const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StateSchema = new Schema({
    Name: {type: String},
    DateTime: {type: Date},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('states', StateSchema);