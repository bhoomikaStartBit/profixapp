const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const credit_limitSchema = new Schema({
    amount: {type: String},
    value: {type: String},
    Order: {type: Number},    
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('credit_amounts', credit_limitSchema);