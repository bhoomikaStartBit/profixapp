const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MenuSchema = new Schema({
    name: { type: String },
    url: { type: String },
    icon: { type: String },
    title: { type: String },
    class: { type: String },
    ParentID: { type: String },
    sequence: { type: Number },
    Modified: { type: Date },
    ModifiedBy: { type: mongoose.Schema.ObjectId, ref: 'users' },
    Active: { type: Number },
});

// Export the model
module.exports = mongoose.model('menus', MenuSchema);