const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
    Name: {type: String},
    Description: {type: String},
    Category: {type: mongoose.Schema.ObjectId, ref: 'categories'},
    Type: {type: mongoose.Schema.ObjectId, ref: 'types'},
    Unit: {type: mongoose.Schema.ObjectId, ref: 'units'},
    Price: {type: mongoose.Schema.Types.Double},
    Commission: {type: String},
    CommissionType: {type: String},
    DateTime: {type: Date},
    Slug: {type: String},
    NoOfSP: {type: Number},
    QTYPerDay: {type: Number},
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('services', ServiceSchema);