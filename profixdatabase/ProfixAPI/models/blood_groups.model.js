const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const credit_limitSchema = new Schema({
    bgroup: {type: String},
    value: {type: String},
    Order: {type: Number},    
    Active: {type: Number, required: true},
});

// Export the model
module.exports = mongoose.model('blood_groups', credit_limitSchema);