const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    ProfixID: {type: String},
    UserName: { type: String },
    FirstName: { type: String, required: true},
    MiddleName: { type: String},
    LastName: { type: String},
    Password: { type: String},
    Email: { type: String },
    Phone: { type: String, required: true },
    SecondaryMobile: { type: String },
    AltPhone: { type: String},
    EmgPhone : {type : String},
    Gender: { type: String },
    Avatar: { type: String , default : '/Documents/User/user.png' },
    Address: { type: String },
    MandatoryDocuments: { type: Array },
    AdditionalDocuments: { type: Array },
    FinancialDetail: { type: Object },
    SecondaryDetails: { type: Object },
    FamilyDetails: { type: Object },
    Services: { type: Array },
    Role: { type: mongoose.Schema.ObjectId, ref: 'roles' },
    City: {type: mongoose.Schema.ObjectId, ref: 'cities'},
    State: { type: mongoose.Schema.ObjectId, ref: 'states'},
    Pin: { type: String },
    ResetToken: { type: String },
    ResetExpires: { type: String },
    DateTime: { type: Date, required: true },
    Active: { type: Number, required: true },
    Account_Block: { type: Boolean , default : false},
    TimePeriod: {type: Number},
    StartDate: {type: Date},
    EndDate: {type: Date},
    type: {type: String},
    DOB: {type: Date},
    Married: {type: String},
    AltName: {type: String},
    AltContactRelation: {type: String},
    TemporaryCity: {type: mongoose.Schema.ObjectId, ref: 'cities'},
    TemporaryState: { type: mongoose.Schema.ObjectId, ref: 'states'},
    TemporaryPin: { type: Number },
    TemporaryAddress: { type: String },
    TemporaryAddressType: { type: String },
    PermanentAddressType: { type: String },
    VendorCategories : {type : Array},
    IsAvailable : {type : String , default : 'Y'},
    IMEINumber : {type : String},
    Base32secret : { type: String },
    MobileDeviceID : { type: String },
    VendorType : {type : String},
    CompanyDetail : {type : Object},
    CompanyAddress : {type : Object},
    VendorCompanyID : { type: mongoose.Schema.ObjectId, ref: 'users' },
    CompanyID : { type: mongoose.Schema.ObjectId, ref: 'companies' },

});

// Export the model
module.exports = mongoose.model('users', UserSchema);