//Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
//var mongoDB = 'mongodb://profixdbuser:PdbU$2019#@134.209.146.247:27017/Profix-Dev';

var mongoDB = 'mongodb+srv://profix:Mean2019@profix-0mfub.mongodb.net/Profix-Dev?retryWrites=true';

mongoose.connect(mongoDB, { 'useNewUrlParser': true, 'useFindAndModify': false, 'useCreateIndex': true } , (err) => {
    if(err)
    console.log(err);
else
console.log('database connected');
});
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = db;
