const configData = require('./common-config');
const nodemailer = require('nodemailer');
var handlebars = require('handlebars');
var fs = require('fs');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: configData.EmailID,
        pass: configData.Password
    }
});

var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
}; 

var emailfun = function(to ,subject,templateHtml,replacementsValue){     

        readHTMLFile(__dirname + '/public/email-templates/'+templateHtml+'.html', function(err, html) {
            var template = handlebars.compile(html);
            var replacements = replacementsValue;
            var htmlToSend = template(replacements);
            var mailOptions = {
                to: to,
                from: configData.EmailID,
                subject: subject,                    
                html : htmlToSend
            };           

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

        });
}

module.exports = emailfun;

