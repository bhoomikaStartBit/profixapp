var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './Profix/Documents/uploads/');
    },
    filename: function (req, file, cb ) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
});

var upload = multer({ //multer settings
    storage: storage
}).single('file');

/** API path that will upload the files */
router.post('/upload', function(req, res) {
        
    upload(req,res,function(err){

        if(err){
            res.status(200).json({
                'status': false,
                'message': 'Successfully delete Succesfully record'
            })
        }
         res.status(200).json({
            'status': true,
            'message': 'Successfully delete Succesfully record',
            'path' : 'Documents/uploads/'+req.file.filename
        })
    });
});


router.post('/deleteFile', function(req, res) {
    var Deletepath = req.body; 
    fs.unlink('Profix/'+Deletepath.path, (err)=>{
        if(err){
            console.log(err)
             res.send({path:''});
             return;
        }
         res.send({path:'Documents/uploads/'+req.file.filename});
    });
    res.send(200);
});

module.exports = router;