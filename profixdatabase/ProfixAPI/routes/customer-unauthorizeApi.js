var express = require('express');
var custrouter = express.Router();

var categoryCtrl = require('../controller/Category/category.controller');
var stateCtrl = require('../controller/State/state.controller');
var cityCtrl = require('../controller/City/city.controller');
var localityCtrl = require('../controller/Locality/locality.controller');
var CustomerOrderCtrl = require('../controller/CustomerOrder/customerOrder.controller');
var pageCtrl = require('../controller/Page/page.controller');
var faqCtrl = require('../controller/Faq/faq.controller');
var serviceCtrl = require('../controller/Service/services.controller');
// PayTm Controller
var PayTmCtrl = require('../controller/Frontend-PayTm/PayTm.controller');
var customerCtrl = require('../controller/Customer/customer.controller');
var profileCtrl = require('../controller/Profile/profile.controller');
var settingsCtrl = require('../controller/Settings/settings.controller');
var projectCtrl = require('../controller/Project/project.controller');
var userlogCtrl = require('../controller/Log/userlog.controller');


custrouter.get('/myorders/currentOrder/:id', CustomerOrderCtrl.currentOrder);
custrouter.get('/myorders/pastOrder/:id', CustomerOrderCtrl.pastOrder);
custrouter.get('/myorders/getOneOrderDetail/:id', CustomerOrderCtrl.geyOneOrderData);
custrouter.post('/myorders/sendReview', CustomerOrderCtrl.sendReview);
custrouter.get('/myorders/getInfoReview/:OrderID/:UserID', CustomerOrderCtrl.getInfoReview);
custrouter.get('/myorders/getAllSPReviews/:OrderID/:UserID', CustomerOrderCtrl.getAllSPReviews);

custrouter.get('/project/getGSTValue/', projectCtrl.getGSTValue);
custrouter.get("/settings/getAll", settingsCtrl.getAll);
custrouter.get("/settings/getmetaValue/:metakey", settingsCtrl.getmetaValue);

custrouter.get('/pages/getAll/', pageCtrl.getAll);
custrouter.get('/state/getAll/', stateCtrl.getAll);
custrouter.get('/city/getAll/', cityCtrl.getAll);
custrouter.get('/city/getAllCitiesByState/:_id', cityCtrl.getAllCitiesByState);
custrouter.get('/locality/getAll/', localityCtrl.getAll);
custrouter.get('/locality/getAllLocalitiesByCity/:_id', localityCtrl.getAllLocalitiesByCity);
custrouter.get('/faqs/getAll/', faqCtrl.getAll);
custrouter.get('/pages/getAll/', pageCtrl.getAll);



custrouter.get('/category/getOne/:_id', categoryCtrl.getOne);
custrouter.get('/category/getOneCategoryWithService/:Slug', categoryCtrl.getOneCategoryWithService);
custrouter.get('/category/getAllFeaturedCateory/', categoryCtrl.getAllFeaturedCateory);
custrouter.get('/category/getAll/', categoryCtrl.getAll);

custrouter.get('/service/getOneServicesBySlug/:slug', serviceCtrl.getOneServicesBySlug);
custrouter.get('/service/getOneByCategory/:_id', serviceCtrl.getOneByCategory);
custrouter.get('/service/getAll/', serviceCtrl.getAll);



// PayTm Routing
custrouter.post("/PayTm/createServer", PayTmCtrl.createServer);
custrouter.post("/PayTm/paytm_callback", PayTmCtrl.createServer);
custrouter.get("/PayTm/getAllPayment/:id", PayTmCtrl.getAllPayment);


custrouter.get('/customer-profile/getOne/:_id', customerCtrl.getOne);
custrouter.put('/customer-profile/updateProfilePassword/:_id', customerCtrl.updateProfilePassword);
custrouter.post('/customer-profile/sendEmailVerification/', customerCtrl.sendEmailVerification);
custrouter.put('/customer-profile/update/:_id', profileCtrl.updateCustomer);
custrouter.get('/customer-profile/checkEmail/:Id/:Email', profileCtrl.checkEmail);
custrouter.get('/customer/checkEmail/:Email', customerCtrl.checkEmail);
custrouter.post("/user/createCustomer", customerCtrl.createCustomer);
// User log
custrouter.post("/userlogs/createlog", userlogCtrl.createlog);
custrouter.get('/userlogs/currentCustomer/:id', userlogCtrl.currentCustomer);
custrouter.get("/settings/getOrderTime/", settingsCtrl.getOrderTime);


custrouter.get('/customer/getOne/:_id', customerCtrl.getOne);
custrouter.post('/project/createProject', projectCtrl.createProject);

module.exports = custrouter;