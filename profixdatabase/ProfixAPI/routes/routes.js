var express = require('express');
var router = express.Router();

// Controller
var userCtrl = require('../controller/User/user.controller');
var profileCtrl = require('../controller/Profile/profile.controller');

var roleCtrl = require('../controller/Role/role.controller');
var categoryCtrl = require('../controller/Category/category.controller');
var typeCtrl = require('../controller/Type/type.controller');
var stateCtrl = require('../controller/State/state.controller');
var cityCtrl = require('../controller/City/city.controller');
var localityCtrl = require('../controller/Locality/locality.controller');
var unitCtrl = require('../controller/Unit/unit.controller');
var serviceCtrl = require('../controller/Service/services.controller');
var commissionCtrl = require('../controller/Commission/commission.controller');
var menuCtrl = require('../controller/Menu/menu.controller');
var customerCtrl = require('../controller/Customer/customer.controller');
var teamCompanyCtrl = require('../controller/Teams/teams.controller');
var userTeamCompanyCtrl = require('../controller/User-TeamCompany-Relation/user-team-company.controller');
var documentCtrl = require('../controller/Document/document.controller');
var projectCtrl = require('../controller/Project/project.controller');
var CustomerOrderCtrl = require('../controller/CustomerOrder/customerOrder.controller');
var teamMemberCtrl = require('../controller/TeamMember/team-member.controller');
var teamRequestCtrl = require('../controller/TeamRequest/team-request.controller');
var pageCtrl = require('../controller/Page/page.controller');
var faqCtrl = require('../controller/Faq/faq.controller');
var reviewsCtrl = require('../controller/Reviews/reviews.controller');
var userlogCtrl = require('../controller/Log/userlog.controller');
var companiesCtrl = require('../controller/Companies/companies.controller');

var commonCtrl = require('../controller/Common/common.controller')

// PayTm Controller
var PayTmCtrl = require('../controller/PayTm/PayTm.controller');


// PayTm Routing
router.post("/PayTm/createServer", PayTmCtrl.createServer);
router.post("/PayTm/paytm_callback", PayTmCtrl.createServer);
router.get("/PayTm/getAllPayment/:id", PayTmCtrl.getAllPayment);

// User log
router.post("/userlogs/createlog", userlogCtrl.createlog);
router.get('/userlogs/currentCustomer/:id', userlogCtrl.currentCustomer);


// Companies Routing
router.post("/Companies/create", companiesCtrl.create);
router.get('/Companies/getAll/', companiesCtrl.getAll);
router.get('/Companies/getOneCompanyDetail/:_id', companiesCtrl.getOneCompanyDetail);
router.get('/Companies/getCompanyUser/:_id', companiesCtrl.getCompanyUser);
router.get('/Companies/getCompanySPUser/:_id', companiesCtrl.getCompanySPUser);
router.get('/Companies/ValidateCompanyNameAndEmail/:Type/:TypeValue', companiesCtrl.ValidateCompanyNameAndEmail)
router.put('/Companies/update/:_id', companiesCtrl.update);
router.post("/Companies/addCompanyOfficeAddress/:_id", companiesCtrl.addCompanyOfficeAddress);
router.put('/Companies/updateCompanyOfficeAddress/:CompanyID/:_id', companiesCtrl.updateCompanyOfficeAddress);
// router.get('/Companies/getCompanyDetailByManager/:_id', companiesCtrl.getCompanyDetailByManager);

router.delete('/Companies/deleteCompanyOfficeAddress/:_id', companiesCtrl.deleteCompanyOfficeAddress);
router.delete('/Companies/delete/:_id', companiesCtrl.delete);
//Reviews routing

router.get('/reviews/getAllReviews', reviewsCtrl.getAllReviews);
router.get('/reviews/getOneReview/:_id', reviewsCtrl.getOneReview);
router.put('/reviews/updateReview/:_id', reviewsCtrl.updateReview);
router.get('/reviews/acceptReview/:_id', reviewsCtrl.acceptReview);


// Page Routing
router.post("/pages/create", pageCtrl.create);
router.post("/pages/draft", pageCtrl.draft);
router.get('/pages/getOne/:_id', pageCtrl.getOne);
router.put('/pages/update/:_id', pageCtrl.update);
router.put('/pages/updatedraft/:_id', pageCtrl.updatedraft);
router.get('/pages/getRelatedRecordOfPage/:_id', pageCtrl.getRelatedRecordOfPage);
router.delete('/pages/delete/:_id', pageCtrl.delete);
router.get('/pages/getAll/', pageCtrl.getAll);

// Faq Routing
router.post("/faqs/create", faqCtrl.create);
router.get('/faqs/getAll/', faqCtrl.getAll);
router.put('/faqs/update/:_id', faqCtrl.update);
router.get('/faqs/getOne/:_id', faqCtrl.getOne);
router.get('/faqs/getRelatedRecordOfFaq/:_id', faqCtrl.getRelatedRecordOfFaq);
router.delete('/faqs/delete/:_id', faqCtrl.delete);
router.post("/faqs/draft", faqCtrl.draft);
router.put('/faqs/updatedraft/:_id', faqCtrl.updatedraft);
var vendortranCtrl = require('../controller/VendorTransactions/vendorTransactions.controller');
var settingsCtrl = require('../controller/Settings/settings.controller');

// User Controller Routing
router.post("/user/create", userCtrl.create);
router.post("/user/createServiceProvider", userCtrl.createServiceProvider);
router.post("/user/createVendor", userCtrl.createVendor);
router.put('/user/update/:_id', userCtrl.update);
router.put('/user/updateServiceProvider/:_id', userCtrl.updateServiceProvider);
router.put('/user/updateVendor/:_id', userCtrl.updateVendor);
router.get('/user/getOne/:_id', userCtrl.getOne);
router.get('/user/getAll/', userCtrl.getAll);
router.get('/user/getAllUserWithSpRole/', userCtrl.getAllUserWithSpRole);
router.get('/user/getAllCompanyVendorUser/', userCtrl.getAllCompanyVendorUser);
router.delete('/user/delete/:_id', userCtrl.delete);
router.get('/users/getByType/:_id', userCtrl.getByType);
router.get('/user/getByRoleID/:_id', userCtrl.getByRoleID);
router.get('/user/checkUserName/:name', userCtrl.checkUserName);
router.get('/user/checkEmail/:Email', userCtrl.checkEmail);
router.put('/user/updateSPSkills/:_id', userCtrl.updateSPSkills);
router.put('/user/updateVendorCategories/:_id', userCtrl.updateVendorCategories);
router.get('/user/getAllRelations/', userCtrl.getAllRelations);

router.get('/user/checkAvailable/:id/:Available', userCtrl.checkAvailable);
router.get('/user/checkBlock/:id/:Block', userCtrl.checkBlock);


// Role Controller Routing
router.post("/role/create", roleCtrl.create);
router.put('/role/update/:_id', roleCtrl.update);
router.get('/role/getOne/:_id', roleCtrl.getOne);
router.get('/role/getAll/', roleCtrl.getAll);
router.delete('/role/delete/:_id', roleCtrl.delete);
router.get('/role/getRoleIDByKey/:key', roleCtrl.getRoleIDByKey);

// Service Category Controller Routing
router.post("/category/create", categoryCtrl.create);
router.put('/category/update/:_id', categoryCtrl.update);
router.get('/category/getOne/:_id', categoryCtrl.getOne);
router.get('/category/getOneCategoryWithService/:Slug', categoryCtrl.getOneCategoryWithService);
router.get('/category/getRelatedRecordOfCategory/:_id', categoryCtrl.getRelatedRecordOfCategory);
router.get('/category/getAll/', categoryCtrl.getAll);
router.get('/category/getAllFeaturedCateory/', categoryCtrl.getAllFeaturedCateory);
router.delete('/category/delete/:_id', categoryCtrl.delete);




// Sevice Type Controller Routing
router.post("/type/create", typeCtrl.create);
router.put('/type/update/:_id', typeCtrl.update);
router.get('/type/getOne/:_id', typeCtrl.getOne);
router.get('/type/getRelatedRecordOfType/:_id', typeCtrl.getRelatedRecordOfType);
router.get('/type/getAll/', typeCtrl.getAll);
router.delete('/type/delete/:_id', typeCtrl.delete);


// State Controller Routing
router.post("/state/create", stateCtrl.create);
router.put('/state/update/:_id', stateCtrl.update);
router.get('/state/getOne/:_id', stateCtrl.getOne);
router.get('/state/getRelatedRecordOfState/:_id', stateCtrl.getRelatedRecordOfState);
router.get('/state/getAll/', stateCtrl.getAll);
router.delete('/state/delete/:_id', stateCtrl.delete);


// State Controller Routing
router.post("/city/create", cityCtrl.create);
router.put('/city/update/:_id', cityCtrl.update);
router.get('/city/getOne/:_id', cityCtrl.getOne);
router.get('/city/getAllCitiesByState/:_id', cityCtrl.getAllCitiesByState);
router.get('/city/getRelatedRecordOfCity/:_id', cityCtrl.getRelatedRecordOfCity);
router.get('/city/getAll/', cityCtrl.getAll);
router.delete('/city/delete/:_id', cityCtrl.delete);


// State Controller Routing
router.post("/locality/create", localityCtrl.create);
router.put('/locality/update/:_id', localityCtrl.update);
router.get('/locality/getOne/:_id', localityCtrl.getOne);
router.get('/locality/getRelatedRecordOfLocality/:_id', localityCtrl.getRelatedRecordOfLocality);
router.get('/locality/getAllLocalitiesByCity/:_id', localityCtrl.getAllLocalitiesByCity);
router.get('/locality/getAll/', localityCtrl.getAll);
router.delete('/locality/delete/:_id', localityCtrl.delete);


// Unit Controller Routing
router.post("/unit/create", unitCtrl.create);
router.put('/unit/update/:_id', unitCtrl.update);
router.get('/unit/getOne/:_id', unitCtrl.getOne);
router.get('/unit/getRelatedRecordOfUnit/:_id', unitCtrl.getRelatedRecordOfUnit);
router.get('/unit/getAll/', unitCtrl.getAll);
router.delete('/unit/delete/:_id', unitCtrl.delete);


// Service Controller Routing
router.post("/service/create", serviceCtrl.create);
router.put('/service/update/:_id', serviceCtrl.update);
router.get('/service/getOne/:_id', serviceCtrl.getOne);
router.get('/service/getOneServicesBySlug/:slug', serviceCtrl.getOneServicesBySlug);
router.get('/service/getOneByCategory/:_id', serviceCtrl.getOneByCategory);
router.get('/service/getAll/', serviceCtrl.getAll);
router.delete('/service/delete/:_id', serviceCtrl.delete);
router.get('/service/getAllForCommission/', serviceCtrl.getAllForCommission);


// Admin Profile
router.put('/profile/update/:_id', profileCtrl.update);
router.get('/profile/getOne/:_id', profileCtrl.getOne);
router.get('/profile/sendRequestForTeam/:type/:_id', profileCtrl.sendRequestForTeam);
router.get('/profile/getAllPendingRequest', profileCtrl.getAllPendingRequest);
router.get('/profile/rejectRequest/:_id', profileCtrl.rejectRequest);
router.get('/profile/acceptRequest/:_id', profileCtrl.acceptRequest);
router.get('/profile/getRequestDetailByUserID/:_id', profileCtrl.getRequestDetailByUserID);
router.put('/profile/updateProfilePassword/:_id', profileCtrl.updateProfilePassword);
router.put('/profile/updateVenderAddress/:_id', profileCtrl.updateVenderAddress);
router.get('/profile/getTeamDetailByUserID/:_id', profileCtrl.getTeamDetailByUserID);
router.post('/commissions/create', commissionCtrl.create);
router.get('/commissions/getAll/', commissionCtrl.getAll);
router.get('/commissions/getAllVendors/', commissionCtrl.getAllVendors);
router.get('/commissions/getByID/:_id', commissionCtrl.getByVendorID);
router.get('/commissions/getOne/:_id', commissionCtrl.getOne);
router.put('/commissions/updateCommission/:_id', commissionCtrl.updateCommissionServices);
router.get('/commissions/addNewServiceCommission/:_id', commissionCtrl.addNewServiceCommission);



// Menu
router.post('/menu/create', menuCtrl.create);
router.put('/menu/update/:_id', menuCtrl.update);
router.get('/menu/getOne/:_id', menuCtrl.getOne);
router.get('/menu/getAll/:RoleID', menuCtrl.getAll);
router.get('/menu/getAllMenuByRoles/:RoleID' , menuCtrl.getAllMenuByRoles);
router.delete('/menu/delete/:_id', menuCtrl.delete);

//Customer Routing
router.post("/customer/create", customerCtrl.create);
router.post("/user/createCustomer", customerCtrl.createCustomer);
router.put('/customer/update/:_id', customerCtrl.update);
router.get('/customer/getOne/:_id', customerCtrl.getOne);
router.get('/customer/checkUserName/:name', customerCtrl.checkUserName);
router.get('/customer/checkEmail/:Email', customerCtrl.checkEmail);
router.get('/customer/getAll/:selectedReferenceBy', customerCtrl.getAll);
router.delete('/customer/delete/:_id', customerCtrl.delete);

//Team Company Routing
router.post("/team-company/createBySA", teamCompanyCtrl.createBySA);
router.post("/team-company/create", teamCompanyCtrl.create);
router.delete('/team-company/delete/:_id/:tl_id', teamCompanyCtrl.delete);
router.put('/team-company/update/:_id', teamCompanyCtrl.update);
router.get('/team-company/getOne/:_id', teamCompanyCtrl.getOne);
router.get('/team-company/getDetail/:_id', teamCompanyCtrl.getDetail);
router.get('/team-company/checkMember/:_id', teamCompanyCtrl.checkMember);
router.get('/team-company/getAll/:selectedOwner', teamCompanyCtrl.getAll);
router.get('/team-company/getOneTeamCompanyByOwner/:_id', teamCompanyCtrl.getOneTeamCompanyByOwner);
router.get('/team-company/rejectRequest/:_id', teamCompanyCtrl.rejectRequest);
router.get('/team-company/acceptRequest/:_id', teamCompanyCtrl.acceptRequest);
router.get('/team-company/requestForDisposal/:_id', teamCompanyCtrl.requestForDisposal);
router.get('/team-company/getAllServiceProvider/', teamCompanyCtrl.getAllServiceProvider);
router.get('/team-company/getOneServiceProvider/:_id', teamCompanyCtrl.getOneServiceProvider);


router.get('/team-company/DuplicateTeamNamecheck/:name', teamCompanyCtrl.DuplicateTeamNamecheck);

//User - Team Company Relation Routing
router.post("/user-team-company/create", userTeamCompanyCtrl.create);
router.put('/user-team-company/update/:_id', userTeamCompanyCtrl.update);


router.post('/document/create', documentCtrl.create);
router.put('/document/update/:_id', documentCtrl.update);
router.get('/document/getOne/:_id', documentCtrl.getOne);
router.get('/document/getAll', documentCtrl.getAll);
router.get('/document/getAllDocumentsByUser/:User', documentCtrl.getAllDocumentsByUser);
router.delete('/document/delete/:_id', documentCtrl.delete);

// project Routing
router.post('/project/create', projectCtrl.create);
router.post('/project/assignOrder', projectCtrl.assignOrder);
router.post('/project/acceptOrder', projectCtrl.acceptOrder);
router.post('/project/submitOrderBySP', projectCtrl.submitOrderBySP);
router.post('/project/paymentCollect', projectCtrl.paymentCollect);
router.post('/project/createMilestoneAndAssignOrder', projectCtrl.createMilestoneAndAssignOrder);
router.post('/project/createProject', projectCtrl.createProject);
router.get('/project/getAll/', projectCtrl.getAll);
router.get('/project/getAllOrderPriceUnder5000/:id', projectCtrl.getAllOrderPriceUnder5000);
router.get('/project/getAllOrderServiceProvicer/:SelectedAssignTo/:Role', projectCtrl.getAllOrderServiceProvicer);
router.get('/project/getOne/:_id', projectCtrl.getOne);
router.get('/project/getAllOrderActivitiesByOrderID/:OrderID', projectCtrl.getAllOrderActivitiesByOrderID);
router.get('/project/getInfoPayment/:OrderID/:UserID', projectCtrl.getInfoPayment);
router.post('/project/sendReview', projectCtrl.sendReview);
router.get('/project/getInfoReview/:OrderID/:UserID', projectCtrl.getInfoReview);
router.get('/project/deleteOrderMilestone/:_id', projectCtrl.deleteOrderMilestone);
router.get('/project/getReviews/:OrderID/:UserID', projectCtrl.getReviews);
router.get('/project/reviewApproved/:OrderAssignID', projectCtrl.reviewApproved);
router.get('/project/disapproveReview/:OrderAssignID', projectCtrl.disapproveReview);
router.get('/project/getGSTValue/', projectCtrl.getGSTValue);
router.get('/project/getAdminCommission/', projectCtrl.getAdminCommission);

router.get('/project/getForProject/', projectCtrl.getForProject);




//Team Member Routing
router.post("/team-member/create", teamMemberCtrl.create);
router.post("/team-member/createTeamMember", teamMemberCtrl.createTeamMember);
router.put('/team-member/update/:_id', teamMemberCtrl.update);
router.get('/team-member/getOne/:_id', teamMemberCtrl.getOne);
router.get('/team-member/getAll/:selectedOwner', teamMemberCtrl.getAll);
router.get('/team-member/getAllTeamMemberByTeam/:team', teamMemberCtrl.getAllTeamMemberByTeam);
router.post('/team-member/sendTeamMemberInvitation', teamMemberCtrl.sendTeamMemberInvitation);
router.get('/team-member/getAllByUserID/:UserID', teamMemberCtrl.getAllByUserID);
router.get('/team-member/rejectRequest/:_id', teamMemberCtrl.rejectRequest);
router.get('/team-member/acceptRequest/:UserID/:_id', teamMemberCtrl.acceptRequest);
router.get('/team-member/getAllTeamMemberByTeamID/:TeamID', teamMemberCtrl.getAllTeamMemberByTeamID);
router.get('/team-member/deleteTeamMember/:_id/:userID', teamMemberCtrl.deleteTeamMember);
router.get('/team-member/getTeamCompanyDetailByUserID/:userID', teamMemberCtrl.getTeamCompanyDetailByUserID);
router.get('/team-member/deleteTeamMemberRequest/:_id', teamMemberCtrl.deleteTeamMemberRequest);
router.get('/team-member/getAllTeamMemberForLeave/:TeamID', teamMemberCtrl.getAllTeamMemberForLeave);
router.get('/team-member/getLeaveRequestDataByUserTeam/:UserID/:TeamID', teamMemberCtrl.getLeaveRequestDataByUserTeam);
router.get('/team-member/rejectLeaveRequest/:_id', teamMemberCtrl.rejectLeaveRequest);
router.get('/team-member/acceptLeaveRequest/:UserID/:_id', teamMemberCtrl.acceptLeaveRequest);

//Team Member Routing
router.post("/team-request/requestForDisposal", teamRequestCtrl.requestForDisposal);
router.get("/team-request/getDisposalRequestDetail/:UserID/:TeamID", teamRequestCtrl.getDisposalRequestDetail);
router.put('/team-request/update/:_id', teamRequestCtrl.update);
router.get("/team-request/getAllPendingrequestForDisposal", teamRequestCtrl.getAllPendingrequestForDisposal);
router.get('/team-request/rejectDisposalRequest/:_id', teamRequestCtrl.rejectDisposalRequest);
router.get('/team-request/acceptDisposalRequest/:TeamID/:_id', teamRequestCtrl.acceptDisposalRequest);

//For Customer App


router.get('/customer-profile/getOne/:_id', customerCtrl.getOne);
router.put('/customer-profile/updateProfilePassword/:_id', customerCtrl.updateProfilePassword);
router.post('/customer-profile/sendEmailVerification/', customerCtrl.sendEmailVerification);
router.put('/customer-profile/update/:_id', profileCtrl.updateCustomer);
router.get('/customer-profile/checkEmail/:Id/:Email', profileCtrl.checkEmail);




// Vendor Transaction Controller Routing
router.post("/VendorTran/create", vendortranCtrl.create);
router.get("/VendorTran/getall", vendortranCtrl.getAll);
router.get("/VendorTran/getOne/:_id", vendortranCtrl.getOne);
router.put("/VendorTran/update/:_id", vendortranCtrl.update);

// Admin Settings

router.post("/settings/create", settingsCtrl.create);
router.get("/settings/getAll", settingsCtrl.getAll);
router.get("/settings/getOne/:_id", settingsCtrl.getOne);
router.put("/settings/update/:_id", settingsCtrl.update);

router.get("/settings/getDateTime/", settingsCtrl.getDateTime);
router.get("/settings/getOrderTime/", settingsCtrl.getOrderTime);

router.get('/common/getBloodGroups/' , commonCtrl.getBloodGroups)
router.get('/common/getCreditDays/' , commonCtrl.getCreditDays)
router.get('/common/getCreditLimits/' , commonCtrl.getCreditLimits)
router.get('/common/getLanguages/' , commonCtrl.getLanguages)
router.get('/common/getCompanyTypes/' , commonCtrl.getCompanyTypes)

module.exports = router;