var express = require('express');
var router = express.Router();
const CustomerUser = require('../models/customer.model');
const SettingsData = require('../models/settings.model');
var token = '';
const btoa = require('btoa');
const speakeasy = require('speakeasy');
const email = require('./email-functions');


require('crypto').randomBytes(48, function(err, buffer) {
    token = buffer.toString('hex');
});

router.post('/login',function(req, res, next){
    let UserName = req.body.username;
    let Password = btoa(req.body.password);
    console.log(UserName , Password)
    CustomerUser.findOne({'Email': UserName})
        .populate(['Role', 'City'])
        .exec(function(err, data){
            if(err || data == null){
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Account Doesn\'t Exist',
                    'result': data
                }))

            }else{

                if(Password == data.Password){

                    res.send(JSON.stringify({
                        'status': true,
                        'message': 'Succesfully Logged In',
                        'token': token,
                        'id': data._id,
                        'username': data.UserName,
                        'Phone': data.Phone,
                        'Email': data.Email,
                        'Latitude': data.City.Latitude,
                        'Longitude': data.City.Longitude,
                        'TOTPVerified' : data.TOTPVerified,
                        'Base32secret': data.Base32secret
                    }))

                }else{
                    
                    res.send(JSON.stringify({
                        'status': false,
                        'message': 'Invalid Password',
                        'result': data
                    }))
                }
            }
        })
})

router.post('/verifytotp',function(req, res, next){
    let UserName = req.body.username;
    let Password = req.body.password;
    let totptoken = req.body.totptoken;

    //console.log(UserName , Password)
    CustomerUser.findOne({'Email': UserName, 'Password': Password})
        .populate(['Role'])
        .exec(function(err, data){
            if(err || data == null){
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Invalid Username and Password',
                    'result': data
                }))
            }
            else{

                const verify = speakeasy.totp.verify({
                    secret: data.Base32secret,
                    encoding: 'base32',
                    token: totptoken,
                    step: 300
                });

                if(verify == true){
                    data['Modified'] = new Date();
                    data['TOTPVerified'] = 1;
                    data['Base32secret'] = '';
                    CustomerUser.findByIdAndUpdate(data._id, data, {new: true}, (err, data1) => {
                        if(err) {
                            return next(err);
                        }
                        else {
                            res.send(JSON.stringify({
                                'status': true,
                                'message': 'Succesfully Logged In',
                                // 'result': data,
                                'token': data1.token,
                                'id': data1._id,
                                'username': data1.UserName,
                                'Phone': data1.Phone,
                                'Email': data1.Email,
                                'TOTPVerified' : data1.TOTPVerified,
                                'Base32secret': data1.Base32secret
                            }));
                        }
                    });
                }else {
                    res.send(JSON.stringify({
                        'status': false,
                        'message': 'OTP Expired/Wrong OTP.',
                    }))
                }
            }
        })
})

router.post('/resendtotp',function(req, res, next){
    let UserName = req.body.username;
    let Password = req.body.password;
    //console.log(UserName , Password)
    CustomerUser.findOne({'Email': UserName})
        .populate(['Role'])
        .exec(function(err, data){
            if(err || data == null){
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Invalid Username',
                    'result': data
                }))
            }
            else{

                const totptoken = speakeasy.totp({
                    secret: data.Base32secret,
                    encoding: 'base32',
                    step: 300
                });               

                var replacementsValue = {
                    otp: totptoken 
                };
        
                email(data.Email,'New OTP','otp-template',replacementsValue); 

                res.status(200).json({
                    'status': true,
                    'message': 'OTP resend. Please Activate account in next 5 minutes.',
                    'result': data
                })

            }
        })
})


router.post('/forgot', function(req, res, next) {
    let useremail = req.body.useremail;
    let adminappUrl = req.body.adminappUrl;
    //console.log(useremail);
    CustomerUser.findOne({'Email': useremail})
        .exec(function(err, data){
            console.log(JSON.stringify(data));
            if(err || data == null){
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Mail ID not exists in Database.',
                    'result': data
                }))
            }
            else{

                data.ResetToken = token;
                data.ResetExpires = Date.now() + 3600000; // 1 hour
                CustomerUser.findByIdAndUpdate(data._id, data, {new: true}, (err, data) => {
                        if(err) {
                            return next(err);
                        }
                        else {
                            
                            var replacementsValue = {
                                adminappurl  : adminappUrl,
                                token : token 
                            };
                    
                            email(useremail,'Password Reset','password-reset',replacementsValue);                        
                                             
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully updated record',
                                'result': data
                            })
                        }
                })
              
            }
        })
    
});


router.post('/verify-email', function(req, res, next) {
    let CustEmail = req.body.CustEmail;
    let CustToken = req.body.CustToken;

    CustomerUser.findOne({'Base32secret': CustToken, 'Email': CustEmail})
        .exec(function(err, data){
            console.log(JSON.stringify(data));
            if(err || data == null){
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Token Expired, Please Resend verification request.',
                    'result': data
                }))
            }
            else{
                

                if(data.EmailVerified == 1){

                    res.send(JSON.stringify({
                        'status': false,
                        'message': 'Your account is already verified.',
                        'result': data
                    }))

                }else{

                    data.EmailVerified = 1;

                    CustomerUser.findByIdAndUpdate(data._id, data, {new: true}, (err, data) => {
                        if(err) {
                            return next(err);
                        }
                        else {
                            
                            res.status(200).json({
                                'status': true,
                                'message': 'Your email address successfully confirmed.',
                                'result': data
                            })
                        }
                    })
                }

            }
        })
    
    
});



router.post('/reset', function(req, res, next) {
    let password = btoa(req.body.UserPassword);
    let token = req.body.token;

    CustomerUser.findOne({'ResetToken': token})
        .exec(function(err, data){
            console.log(JSON.stringify(data));
            if(err || data == null){
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Token Expired',
                    'result': data
                }))
            }
            else{
                data.Password = password;
                var currenttime = Date.now();
                if(data.ResetExpires > currenttime) {
                    data.ResetExpires = '';
                    data.ResetToken = '';

                    CustomerUser.findByIdAndUpdate(data._id, data, {new: true}, (err, data) => {
                        if(err) {
                            return next(err);
                        }
                        else {
                            
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully Updated Password',
                                'result': data
                            })
                        }
                })
                } else {
                    res.send(JSON.stringify({
                        'status': false,
                        'message': 'Token Expired',
                        'result': data
                    }));
                }
              
            }
        })
    
});

// Contact
router.post('/contact',function(req, res, next) {

    let Subject = req.body.Subject;
    //let Name = req.body.Name;
    let Email = req.body.Email;
    let Phone = req.body.Phone;
    let Message = req.body.Message;    

    //console.log(req.body);

        SettingsData.findOne({'metaKey':'CompanyEmail'}).exec(function(err, data){

            if(err || data == null){

                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Admin email configuration not found!',
                }))

            }else {
                
                const CompanyEmail = data.metaValue; 

                var replacementsValue = {
                    subject: Subject,
                    email: Email,
                    phone: Phone,
                    message : Message
                };
                
                email(CompanyEmail,Subject,'contact-us',replacementsValue);  
                
                res.send(JSON.stringify({
                    'status': true,
                    'message': 'Email has been successfully sent!',
                }))
            }
        });

});

module.exports = router;
