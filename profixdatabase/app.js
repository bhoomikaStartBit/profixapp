var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var bodyParser = require('body-parser')
const config = require('./ProfixAPI/config');
var mainRoute = require('./ProfixAPI/routes/routes');
var signin = require('./ProfixAPI/routes/signin');
var customerSignin = require('./ProfixAPI/routes/customerLogin');
var nodemailer = require('nodemailer');
var async = require('async');

var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(morgan('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '.')));
app.use(bodyParser.json({ limit: "50mb" })); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }));

app.use((req, res, next) => {
    config;
    next();
});

app.use('/', mainRoute);
app.use('/admin', signin);
app.use('/customer', customerSignin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');

    res.status(404).render('404_error_template', {title: "Sorry, page not found"});

});

// default Heroku PORT
app.listen(process.env.PORT || 3000);