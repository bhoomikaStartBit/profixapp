import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor( private injector: Injector) { }
  intercept(req, next){
    let user = JSON.parse(localStorage.getItem('currentUser'));
    if(user){
      let tokenizedReq = req.clone({
        setHeaders : {
          Authorization: `bearer ${user.token}`
        }
      })
      return next.handle(tokenizedReq);
    }
    else{
      return next.handle(req);
    }
    
  }
}
