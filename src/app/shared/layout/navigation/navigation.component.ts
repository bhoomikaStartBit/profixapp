import {Component, OnInit} from '@angular/core';
import {LoginInfoComponent} from "../../user/login-info/login-info.component";
import {CommonService} from "@app/core/common/common.service";
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';



@Component({

  selector: 'sa-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit {
  localStorage :any;
    dynamicNavItems :any;
    checkMenu = false;
  constructor(private commonService : CommonService,
    private router: Router,
    private snackBar: MatSnackBar
    ) {
      this.localStorage = JSON.parse(localStorage.getItem('currentUser'));
      setInterval(()=>{
        if(!JSON.parse(localStorage.getItem('currentUser'))){
          this.router.navigate(["/auth/login"]);
          window.location.reload();
        }
      },5000)
  }

  ngOnInit() {
    this.getAllmenus();
  }

    getAllmenus() {
        this.commonService.getAllMenuByRoles(this.localStorage.RoleID).subscribe((data) => {
            if (data['status']) {
                this.dynamicNavItems = data['result'];
                this.checkMenu = true;
                this.dynamicNavItems.sort(function(a, b) {
                    if (a.children) {
                        a.children.sort(function(c, d) {
                            return c.sequence - d.sequence;
                        });
                    }
                    return a.sequence - b.sequence;
                });
                localStorage.setItem('VisibleMenus', JSON.stringify(this.dynamicNavItems));
            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }
    
  collapseMenu(){
    $('body').removeClass('hidden-menu-mobile-lock');
    $('body').removeClass('hidden-menu');
  }
}
