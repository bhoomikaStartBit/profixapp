import {Component, OnInit} from '@angular/core';
import { UserService } from '@app/core/services/user.service';
import { LayoutService } from '@app/core/services/layout.service';
import {Router} from "@angular/router";
import { CommonService} from "@app/core/common/common.service";

@Component({

  selector: 'sa-login-info',
  templateUrl: './login-info.component.html',
})
export class LoginInfoComponent implements OnInit {

  currentUser: any = {};
  avatarUrl: any;
  constructor(
    public us: UserService,
    private router: Router,
    private layoutService: LayoutService,
    private commonService: CommonService
    ) {
      commonService.getAvatarUrl.subscribe(name => {
        this.avatarUrl = '';
        setTimeout(()=> {
            if(name){
                this.avatarUrl = this.commonService.getApiUrl() + name;
            }
            else{
                this.avatarUrl = 'assets/img/avatars/1.png';
            }
          // this.avatarUrl = this.commonService.getApiUrl() + name;
          console.log('retrettetetertret')
        },100)
      });
  }

  ngOnInit() {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      console.log('avatars'+this.currentUser.Avatar);
      if(this.currentUser.Avatar){
          this.avatarUrl = this.commonService.getApiUrl() + this.currentUser.Avatar;
      }
      else{
          this.avatarUrl = 'assets/img/avatars/2.png';
      }

  }

  toggleShortcut() {
    this.layoutService.onShortcutToggle()
  }

  AdminProfile() {
    //alert('Hi');
   // this.router.navigate(['/profiles/createnew/1'])
  }

}
