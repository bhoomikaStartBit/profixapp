import {EventEmitter, Injectable, Output} from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable , of} from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({ providedIn : 'root'})

export class CommonService {

  @Output() getAvatarUrl: EventEmitter<any> = new EventEmitter();
  constructor(private http: HttpClient) { }
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getApiUrl() {
    //return 'http://startbitinc.com:8082';
    //return 'http://localhost:8082';
    //return 'http://192.168.225.1:8082';
    return 'https://profixdatabase.herokuapp.com';
  }
  getGSTValue() {
    
    return this.http.get(this.getApiUrl() + '/project/getGSTValue').pipe(map(this.extractData));
  }

  getAdminCommission() {
    return this.http.get(this.getApiUrl() + '/project/getAdminCommission').pipe(map(this.extractData));
  }
  

  getCreditLimits(): Observable<any> {
    return this.http.get(this.getApiUrl() + '/common/getCreditLimits/' ).pipe(
      map(this.extractData));
  }

  getCreditDays(): Observable<any> {
    return this.http.get(this.getApiUrl() + '/common/getCreditDays/'  ).pipe(
      map(this.extractData));
  }

  getLanguages(): Observable<any> {
    return this.http.get(this.getApiUrl() + '/common/getLanguages/'  ).pipe(
      map(this.extractData));
  }
  
  getBloodGroups(): Observable<any> {
    return this.http.get(this.getApiUrl() + '/common/getBloodGroups/' ).pipe(
      map(this.extractData));
  }
  
  getCompanyTypes(): Observable<any> {
    return this.http.get(this.getApiUrl() + '/common/getCompanyTypes/' ).pipe(
      map(this.extractData));
  }
 

  getAdminAppUrl() {
    //return 'http://localhost:4200';
    return window.location.origin;
  }
  playNotification() {
    const audio = new Audio();
    audio.src = '../../assets/mp3/notification.mp3';
    audio.load();
    audio.play();
  }
  getAllMenuByRoles(id): Observable<any> {
    return this.http.get(this.getApiUrl() + '/menu/getAllMenuByRoles/' + id).pipe(
      map(this.extractData));
  }
  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
