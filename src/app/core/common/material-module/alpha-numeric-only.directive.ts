import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[OnlyAlphaNumeric]'
})
export class OnlyAlphaNumeric {


  @Input() OnlyAlphaNumeric: boolean;
  constructor(private el: ElementRef) {}
  
  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let e = <KeyboardEvent> event;
       
    if (this.OnlyAlphaNumeric) {
      if(e.keyCode == 9 || e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >=35 && e.keyCode <= 40)){
        return true;
      }
      else if(e.keyCode){
        
        var keyChar = String.fromCharCode(e.keyCode);
        var re = /^[a-zA-Z ]+$/
        var num = /^[0-9]+$/
        return (re.test(keyChar) || num.test(keyChar));
      }
    }
  }
  // @HostListener('paste', ['$event'])
  // onPaste(event: ClipboardEvent) {
  //   event.preventDefault();
  //   const pastedInput: string = event.clipboardData
  //     .getData('text/plain')
  //     .replace(/\D/g, ''); // get a digit-only string
  //   document.execCommand('insertText', false, pastedInput);
  // }
  // @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
  //   e.preventDefault();
  // }
  // @HostListener('drop', ['$event'])
  // onDrop(event: DragEvent) {
  //   event.preventDefault();
  //   const textData = event.dataTransfer.getData('text').replace(/\D/g, '');
  //   document.execCommand('insertText', false, textData);
  // }
}