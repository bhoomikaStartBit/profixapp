﻿

export class User {
    id: number;
    username: string;
    password: string;
    status?: boolean;
    Name: string;
    Email: string;
    role: string;
    token?: string;
    Department: any = [];
    Branch: any = [];
}
