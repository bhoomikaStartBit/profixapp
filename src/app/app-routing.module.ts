import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MainLayoutComponent } from "./shared/layout/app-layouts/main-layout.component";
import { AuthLayoutComponent } from "./shared/layout/app-layouts/auth-layout.component";

import {AuthGuard} from "@app/core/common/_guards/auth.guard";

const routes: Routes = [
  {

        path: "",
        redirectTo: "auth/login",
        pathMatch: "full"
    },{

    path: "",
    component: MainLayoutComponent,
    data: { pageTitle: "Home" },
    children: [
      {
        path: "",
        redirectTo: "dashboard/analytics",
        pathMatch: "full"
      },

      {
        path: "app-views",
        loadChildren: "./features/Old/app-views/app-views.module#AppViewsModule",
        data: { pageTitle: "App Views" }
      },

      {
        path: "dashboard",
        loadChildren: "./features/Old/dashboard/dashboard.module#DashboardModule",
        data: { pageTitle: "Dashboard" },
          canActivate: [AuthGuard]
      },
      {
        path: "camera",
        loadChildren:
          "./features/Management/camera/camera.module#CameraModule",
        data: { pageTitle: "Camera" },
          canActivate: [AuthGuard]
      },
      {
        path: "miscellaneous",
        loadChildren:
          "./features/Old/miscellaneous/miscellaneous.module#MiscellaneousModule",
        data: { pageTitle: "Miscellaneous" }
      },
      {
        path: "users",
        loadChildren:
          "./features/Management/users/user.module#UserModule",
        data: { pageTitle: "Users" },
        canActivate: [AuthGuard]
      },

      {
        path: "customers",
        loadChildren:
          "./features/Management/customers/customers.module#CustomersModule",
        data: { pageTitle: "Customers" },
          canActivate: [AuthGuard]
      },

      {
        path: "pages",
        loadChildren:
          "./features/Management/page/page.module#PageModule",
        data: { pageTitle: "Page" },
          canActivate: [AuthGuard]
      },
      {
        path: "faqs",
        loadChildren:
          "./features/Management/faq/faq.module#FaqModule",
        data: { pageTitle: "Faq" },
          canActivate: [AuthGuard]
      },

      {
        path: "categories",
        loadChildren:
          "./features/Management/categories/categories.module#ServiceCategoriesModule",
        data: { pageTitle: "Categories" },
          canActivate: [AuthGuard]
      },
      {
        path: "commissions",
        loadChildren:
          "./features/Management/vendor-commission/commission.module#CommissionModule",
        data: { pageTitle: "Commission" },
          canActivate: [AuthGuard]
      },
      {
        path: "profiles",
        loadChildren:
          "./features/Management/profiles/profiles.module#ProfilesModule",
        data: { pageTitle: "Profile" },
          canActivate: [AuthGuard]
      },

      {
        path: "types",
        loadChildren:
          "./features/Management/types/types.module#ServiceTypesModule",
        data: { pageTitle: "Types" },
          canActivate: [AuthGuard]
      },

      {
        path: "roles",
        loadChildren:
          "./features/Management/roles/role.module#RoleModule",
        data: { pageTitle: "Roles" },
          canActivate: [AuthGuard]
      },

      {
        path: "services",
        loadChildren:
          "./features/Management/service/service.module#ServiceModule",
        data: { pageTitle: "Services" },
          canActivate: [AuthGuard]
      },

      {
        path: "settings",
        loadChildren:
          "./features/Management/settings/settings.module#SettingsModule",
        data: { pageTitle: "Settings" },
          canActivate: [AuthGuard]
      },

      {
        path: "reviews",
        loadChildren:
          "./features/Management/reviews/reviews.module#ReviewsModule",
        data: { pageTitle: "Reviews & Ratings" },
          canActivate: [AuthGuard]
      },

      {
        path: "localities",
        loadChildren:
          "./features/Management/locality/locality.module#LocalityModule",
        data: { pageTitle: "localities" },
          canActivate: [AuthGuard]
      },

      {
        path: "cities",
        loadChildren:
          "./features/Management/city/city.module#CityModule",
        data: { pageTitle: "Services" },
          canActivate: [AuthGuard]
      },

      {
        path: "states",
        loadChildren:
          "./features/Management/state/state.module#StateModule",
        data: { pageTitle: "States" },
          canActivate: [AuthGuard]
      },

      {
        path: "units",
        loadChildren:
          "./features/Management/unit/unit.module#UnitModule",
        data: { pageTitle: "Units" },
          canActivate: [AuthGuard]
      },

      {
        path: "projects",
        loadChildren:
          "./features/Management/projects/projects.module#ProjectsModule",
        data: { pageTitle: "Orders" },
          canActivate: [AuthGuard]
      },

      {
        path: "vendor-transactions",
        loadChildren:
          "./features/Management/vendor-transactions/vendor-transactions.module#VendorTransactionsModule",
        data: { pageTitle: "Vendor Transactions" },
          canActivate: [AuthGuard]
      },

      {
        path: "permissions",
        loadChildren:
          "./features/Management/permission/permission.module#PermissionModule",
        data: { pageTitle: "Permission" },
          canActivate: [AuthGuard]
      },

      {
        path: "team-company",
        loadChildren:
          "./features/Management/team-company/team-company.module#TeamCompanyModule",
        data: { pageTitle: "Team/Company" },
          canActivate: [AuthGuard]
      },

      {
        path: "my-tasks",
        loadChildren:
          "./features/Management/tasks/tasks.module#TasksModule",
        data: { pageTitle: "My Task" },
          canActivate: [AuthGuard]
      },
        {
            path: "team-request-list",
            loadChildren:
                "./features/Management/team-request/team-request.module#TeamRequestModule",
            data: { pageTitle: "Users" },
            canActivate: [AuthGuard]
        },
        {
            path: "my-team",
            loadChildren:
                "./features/Management/myteam/myteam.module#MyTeamsModule",
            data: { pageTitle: "TeamCompany" },
            canActivate: [AuthGuard]
        },
        {
          path: "companies",
          loadChildren:
            "./features/Management/Companies/companies.module#CompaniesModule",
          data: { pageTitle: "Companies" },
            canActivate: [AuthGuard]
        },
    ]
  },

  {
    path: "auth",
    component: AuthLayoutComponent,
    loadChildren: "./features/auth/auth.module#AuthModule"
  },
  { path: "**", redirectTo: "miscellaneous/error404" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
