import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import { AuthenticationService} from "@app/core/common/_services/authentication.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserIdleService } from 'angular-user-idle';
import {UserService} from "@app/core/services/user.service";

declare var device;
declare var FCMPlugin;
// declare var OTPAutoVerification;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    OTPForm: FormGroup;
    showSpinner: boolean;
    returnUrl: string;
    showLoginForm = false;
    showOTPForm = false;
    error = '';
    deviceid: string;
    phone: number;
  constructor(
    private userService: UserService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userIdle: UserIdleService
  )
  {
      this.showSpinner = false;
      if (this.authenticationService.currentUserValue) {
         this.router.navigate(['/dashboard']);
      } else {
          this.showLoginForm = true;
          this.showOTPForm = false;
      }
      FCMPlugin.getToken(token => {
        console.log('device token is ' + token);
        var deviceid = token;
        console.log('value of id is' + deviceid);
        this.deviceid = deviceid;
        console.log('value of id is' + this.deviceid);
      },
      function (err) {
        console.log('error retrieving token: ' + err);
      });

      //Start watching for user inactivity.
      this.userIdle.startWatching();
      
      // Start watching when user idle is starting.
      this.userIdle.onTimerStart().subscribe(count => console.log(count));
      
      // Start watch when time is up.
      this.userIdle.onTimeout().subscribe(() => { 
        console.log('Time is up!');
        this.userService.logout()
        this.router.navigate(['/auth/login']);
        window.location.reload();
      });      
      // var options = {
      //   delimiter : "code is",
      //   length : 6,
      //   origin : "BYTWOO"
      // };
      
      
      // OTPAutoVerification.startOTPListener(options, (otp) => {
      //   console.log("GOT OTP", otp);
      //   OTPAutoVerification.stopOTPListener();
      // },(error) => {
      //   OTPAutoVerification.stopOTPListener();
      //   console.log("Problem in listening OTP");
      // });
  }

  onResume() {
      

  }

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          LoginUserName: ['', Validators.required]      });
      this.OTPForm = this.formBuilder.group({
          LoginOTP: ['', Validators.required]      });
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';

  }

  stop() {
    this.userIdle.stopTimer();
  }
 
  stopWatching() {
    this.userIdle.stopWatching();
  }
 
  startWatching() {
    this.userIdle.startWatching();
  }
 
  restart() {
    this.userIdle.resetTimer();
  }

  resendOtp(){
    this.authenticationService.otplogin(this.phone, this.deviceid)
          .pipe()
          .subscribe(
              data => {
                
                  if (data['status']) {
                      
                    
                  } else {
                      this.error = data['message'];
                      this.showSpinner = false;
                  }
              },
              error => {
                  this.error = 'Server error';
                  this.showSpinner = false;
              });
  }

  otpsubmit(event){
    event.preventDefault();
    if(this.OTPForm.value.LoginOTP == ''){
        this.error = 'Please Enter OTP';
        return;
    }

    this.error = '';
    this.showSpinner = true;
    this.authenticationService.otpsubmit(this.OTPForm.value.LoginOTP, this.phone)
      .pipe()
      .subscribe(
              data => {
                  console.log(data);
                  if (data['status']) {
                      this.showSpinner = false;
                      this.showLoginForm = false;
                      this.showOTPForm = false;
                      this.router.navigate([this.returnUrl]);
                  } else {
                      this.error = data['message'];
                      this.showSpinner = false;
                  }
              },
              error => {
                  console.log(error);
                  this.error = 'Server error';
                  this.showSpinner = false;
              });
  }

  login(event){
    event.preventDefault();
    console.log('Login value of id is' + this.deviceid);

    // var deviceid = '';
    // FCMPlugin.getToken(token => {
    //   console.log('device token is ' + token);
    //   deviceid = token;
    //   console.log('value of id is' + deviceid);
    //   this.deviceid = deviceid;
    //   console.log('value of id is' + this.deviceid);
      this.phone = this.loginForm.value.LoginUserName;
      if(this.loginForm.value.LoginUserName == ''){
          this.error = 'Please fill phone field';
          return;
      }
      if (this.loginForm.invalid) {
          return;
      }
      this.error = '';
      this.showSpinner = true;
     
      this.authenticationService.otplogin(this.loginForm.value.LoginUserName, this.deviceid)
          .pipe()
          .subscribe(
              data => {
                // FCMPlugin.onNotification(function(data){
                //   if(data.wasTapped){
                //     //Notification was received on device tray and tapped by the user.
                //     alert( JSON.stringify(data) );
                //   }else{
                //     //Notification was received in foreground. Maybe the user needs to be notified.
                //     alert( JSON.stringify(data) );
                //   }
                // });
                  if (data['status']) {
                      this.showSpinner = false;
                      this.showLoginForm = false;
                      this.showOTPForm = true;
                      //alert(this.showSpinner);
                      //this.router.navigate([this.returnUrl]);
                  } else {
                      this.error = data['message'];
                      this.showSpinner = false;
                  }
              },
              error => {
                  this.error = 'Server error';
                  this.showSpinner = false;
              });
    // },
    // function (err) {
    //   console.log('error retrieving token: ' + err);
    // });
  }

}
