import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotRoutingModule } from './forgot-routing.module';
import { ForgotComponent } from './forgot.component';
import {FormsModule, ReactiveFormsModule } from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";

@NgModule({
  imports: [
    CommonModule,
    ForgotRoutingModule,
    FormsModule,
    ReactiveFormsModule,
      MaterialModuleModule
  ],
  exports:[
    FormsModule,
    ReactiveFormsModule
],
  declarations: [ForgotComponent]
})
export class ForgotModule { }
