import { Component, OnInit  ,ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "@app/core/services";
import {StateService} from "@app/features/Management/state/state.service";
import {CityService} from "@app/features/Management/city/city.service";
import {LocalityService} from "@app/features/Management/locality/locality.service";
import {CustomersService} from "@app/features/Management/customers/customers.service";
import {MatSnackBar, MatStepper} from "@angular/material";

import { trigger,
    state,
    style,
    transition,
    animate} from '@angular/animations'
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { AuthenticationService } from '@app/core/common/_services/authentication.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
  selector: 'customer-form',
  templateUrl: './customers-forms.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
    animations: [
        trigger('changePane', [
            state('out', style({
                height: 0,
            })),
            state('in', style({
                height: '*',
            })),
            transition('out => in', animate('250ms ease-out')),
            transition('in => out', animate('250ms 300ms ease-in'))
        ])
    ]
})
export class CustomersFormComponent implements OnInit {
    ID: any;
    States: any;
    Cities: any;
    Localities: any;
    pinlength = 6;
    currentUser: any;
    getValue = false;
    BasicInfoForm: FormGroup;
    AccountInfoForm: FormGroup;
    DocumentInfoForm: FormGroup;
    MaxDate : Date;

    @ViewChild('stepper') stepper: MatStepper;

    public validationCustomerDetailOptions:any = {};

    public validationAccountOptions:any = {};
    

  constructor(private http: HttpClient,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private location: Location,
              private notificationService: NotificationService,
              private stateService: StateService,
              private cityService: CityService,
              private localityService: LocalityService,
              private customersService: CustomersService,
              private snackBar: MatSnackBar,
              private authenticationService : AuthenticationService,
              private spinnerService : Ng4LoadingSpinnerService
  ) {
        this.spinnerService.hide();
        setTimeout(() => {
           this.authenticationService.checkSelectedComponentVisiblity('Customers');
         }, 100);
        this.validationAccountOptions = {

          // Rules for form validation
          rules: {
              Password: {
                  required: true,
                  minlength: 8,
                  pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$'
              },
              ConfirmPassword: {
                  required: true,
                  minlength: 8,
                  pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$',
                  equalTo: '#Password'
              }
          },

          // Messages for form validation
          messages: {
              Password: {
                  required: 'Please enter new password',
                  minlength: 'Password should be at least 8 characters long',
                  pattern: 'Password should contain one number,one character, one uppercase character and one special character'
              },
              ConfirmPassword: {
                  required: 'Please enter confirm password',
                  minlength: 'Password should be at least 8 characters long',
                  pattern: 'Password should contain one number,one character, one uppercase character and one special character',
                  equalTo: 'Please re-enter the same password again.'
              }
          },
          submitHandler: this.onAccountSubmit

      };
        this.validationCustomerDetailOptions = {
            // Rules for form validation
            rules: {
                FirstName: {
                    required: true
                },
                Email: {
                    required: true,
                    email: true
                },
                Phone: {
                    required: true,
                    pattern: "[0-9]+",
                    minlength:10,
                    maxlength:10
                },
                AltPhone: {
                    pattern: "[0-9]+",
                    minlength:10,
                    maxlength:10
                },
                State: {
                    required: true
                },
                City: {
                    required: true
                },
                Locality: {
                    required: true
                },
                Pin: {
                    required: true,
                    minlength:5,
                    maxlength:6
                },
                Address: {
                    required: true,
                    minlength : 10
                },
                DOB: {
                    required: true,
                }
            },

            // Messages for form validation
            messages: {
                FirstName: {
                    required: 'Please enter your first name'
                },
                Email: {
                    required: 'Please enter your email address',
                    email: '<i class="fa fa-warning"></i> &nbsp;Please enter a valid email addres'
                },
                Phone: {
                    required: 'Please enter your phone number',
                    minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                    maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
                },
                AltPhone: {
                    minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                    maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
                },
                State: {
                    required: 'Please select the state'
                },
                City: {
                    required: 'Please select the city'
                },
                Locality: {
                    required: 'Please select the locality'
                },
                Pin: {
                    required: 'Please enter pincode',
                    minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
                    maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
                },
                Address: {
                    required: 'Please enter address',
                    minlength : '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 characters.'
                },
                DOB: {
                    required: 'Please enter address',
                }
            },
            submitHandler: this.onSubmit

        };
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.ID = this.route.params['value'].id;
      if(this.ID !== '-1') {
          this.getOne(this.ID);
      }
      else{
          this.BasicInfoForm = fb.group({
              FirstName: ['', [Validators.required]],
              MiddleName: [''],
              LastName: [''],
              Email: ['', [Validators.required, Validators.email]],
              Phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
              AltPhone: ['', [ Validators.minLength(10), Validators.maxLength(10)]],
              Gender: ['Male', [Validators.required]],
              AltName: [''],
              dob: ['', [Validators.required]],
              Avatar: [''],
              Address: ['', [Validators.required]],
              Pin: ['', [Validators.required, Validators.minLength(6) ,Validators.maxLength(6) ]],
              Locality: ['', [Validators.required]],
              City: ['', [Validators.required]],
              State: ['', [Validators.required]],
          });
          this.AccountInfoForm = fb.group({
              
              Password: [ '', [Validators.minLength(8),
                  Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
              ConfirmPassword: [ '', [Validators.minLength(8),
                  Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]],
              ReferenceBy: [this.currentUser.id],
          });
          this.DocumentInfoForm = fb.group({
              IDType: ['', [Validators.required]],
              IDNo: ['', [Validators.required]],
              IDExpiryDate: ['', [Validators.required]],
              IDIssuedBy: ['', [Validators.required]],
              Image: ['', [Validators.required]],
              IsFeesPaid: ['', [Validators.required]],
          });
          this.getValue = true;
      }
      var d = new Date();
      d.setDate(d.getDate()-1)
      d.setFullYear(d.getFullYear()-18)
      this.MaxDate = d;

      this.getAllState();
  }

  ngOnInit() {
      this.ID = this.route.params['value'].id;
  }

    onSubmit(){
    }

    onAccountSubmit(){
        if(this.AccountInfoForm.invalid){
            return 0;
        }
        if(this.AccountInfoForm.controls.Password.value !== this.AccountInfoForm.controls.ConfirmPassword.value){
            return 0;
        }
        this.stepper.next();
    }


    getAllState(){
        this.spinnerService.show();
        this.stateService.getAllStates().subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.States = data['result'];
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }



    getAllCitiesByState(id){
        this.Cities = [];
        if(this.getValue){
            this.BasicInfoForm.patchValue({
                "Locality": '',
                "City": '',
            });
        }
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Cities = data['result'];
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getAllLocalitiesByCity(id){
        this.Localities = [];
        if(this.getValue) {
            this.BasicInfoForm.patchValue({
                "Locality": ''
            });
        }
        this.spinnerService.show();
        this.localityService.getAllLocalitiesByCity(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Localities = data['result'];
                this.getValue = true;
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }


    uploadImage(event, name) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png') {
                reader.onload = () => {
                    // this.allKeyPersonImages[name] = reader.result;
                    if(name == 'Document'){
                        this.DocumentInfoForm.patchValue({
                            "Image": reader.result
                        });
                    }
                    else if(name == 'Avatar'){
                        this.BasicInfoForm.patchValue({
                            "Avatar": reader.result
                        });
                    }
                };
            } else {

            }
        }
    }

    getName(id  , name){
        if(name == 'state' && id != ''){
            var sindex = this.States.findIndex(x=>x._id == id);
            if(sindex > -1){
                return this.States[sindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'city' && id != ''){
            var cindex = this.Cities.findIndex(x=>x._id == id);
            if(cindex > -1){
                return this.Cities[cindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'locality' && id != ''){
            var lindex = this.Localities.findIndex(x=>x._id == id);
            if(lindex > -1){
                return this.Localities[lindex].Name;
            }
            else{
                return '';
            }
        }
    }


    getOne(id) {
        this.spinnerService.show();
        this.customersService.getOneCustomers(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.getAllCitiesByState(data['result'].State);
                this.getAllLocalitiesByCity(data['result'].City);
                this.BasicInfoForm = this.fb.group({
                    FirstName: [ data['result'].FirstName, [Validators.required ] ],
                    MiddleName: [ data['result'].MiddleName ],
                    LastName: [ data['result'].LastName ],
                    Email: [ data['result'].Email, [Validators.required, Validators.email  ] ],
                    Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                    AltPhone: [ data['result'].AltPhone, [ Validators.minLength(10), Validators.maxLength(10) ] ],
                    Gender: [ data['result'].Gender, [Validators.required ] ],
                    Avatar: [ data['result'].Avatar ],
                    Address: [ data['result'].Address, [Validators.required  , Validators.minLength(10)] ],
                    dob: [ data['result'].dob, [Validators.required ] ],
                    AltName: [ data['result'].AltName ],
                    Pin: [ data['result'].Pin, [Validators.required , Validators.maxLength(6), Validators.max(999999)  ] ],
                    Locality: [ data['result'].Locality, [Validators.required ] ],
                    City: [ data['result'].City, [Validators.required ] ],
                    State: [ data['result'].State, [Validators.required ] ],
                });
                this.AccountInfoForm = this.fb.group({
                    
                    Password: [ '', [Validators.minLength(8),
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                    ConfirmPassword: [ '', [Validators.minLength(8),
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]],
                    ReferenceBy: [ data['result'].ReferenceBy ],
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    save(){
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    checkEmail(){
        if(this.BasicInfoForm.controls.Email.value == ''){
            return 0;
        }
        this.spinnerService.show();
        this.customersService.checkEmail(this.BasicInfoForm.controls.Email.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                if(data['result'].length > 0){
                    this.snackBar.open('This email already persent in db. Please enter diffrent email', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.BasicInfoForm.patchValue({
                        'Email': ''
                    });
                }
                else{

                }
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }

    createNew(){
        this.spinnerService.show();
        this.customersService.addCustomers(this.BasicInfoForm.value , this.AccountInfoForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.location.back();
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.customersService.updateCustomers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value ).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
            }
            if (ButtonPressed === "No") {

            }

        });
    }


    viewDetail(){
    }

    public data = {
        "Afghanistan":16.63,"Albania":11.58,"Algeria":158.97,"Angola":85.81,"Antigua and Barbuda":1.1,"Argentina":351.02,"Armenia":8.83,"Australia":1219.72,"Austria":366.26,"Azerbaijan":52.17,"Bahamas":7.54,"Bahrain":21.73,"Bangladesh":105.4,"Barbados":3.96,"Belarus":52.89,"Belgium":461.33,"Belize":1.43,"Benin":6.49,"Bhutan":1.4,"Bolivia":19.18,"Bosnia and Herzegovina":16.2,"Botswana":12.5,"Brazil":2023.53,"Brunei":11.96,"Bulgaria":44.84,"Burkina Faso":8.67,"Burundi":1.47,"Cambodia":11.36,"Cameroon":21.88,"Canada":1563.66,"Cape Verde":1.57,"Central African Republic":2.11,"Chad":7.59,"Chile":199.18,"China":5745.13,"Colombia":283.11,"Comoros":0.56,"Costa Rica":35.02,"Croatia":59.92,"Cyprus":22.75,"Czech Republic":195.23,"Democratic Republic of the Congo":12.6,"Denmark":304.56,"Djibouti":1.14,"Dominica":0.38,"Dominican Republic":50.87,"East Timor":0.62,"Ecuador":61.49,"Egypt":216.83,"El Salvador":21.8,"Equatorial Guinea":14.55,"Eritrea":2.25,"Estonia":19.22,"Ethiopia":30.94,"Fiji":3.15,"Finland":231.98,"France":2555.44,"Gabon":12.56,"Gambia":1.04,"Georgia":11.23,"Germany":3305.9,"Ghana":18.06,"Greece":305.01,"Grenada":0.65,"Guatemala":40.77,"Guinea":4.34,"Guinea-Bissau":0.83,"Guyana":2.2,"Haiti":6.5,"Honduras":15.34,"Hong Kong":226.49,"Hungary":132.28,"Iceland":12.77,"India":1430.02,"Indonesia":695.06,"Iran":337.9,"Iraq":84.14,"Ireland":204.14,"Israel":201.25,"Italy":2036.69,"Ivory Coast":22.38,"Jamaica":13.74,"Japan":5390.9,"Jordan":27.13,"Kazakhstan":129.76,"Kenya":32.42,"Kiribati":0.15,"Kuwait":117.32,"Kyrgyzstan":4.44,"Laos":6.34,"Latvia":23.39,"Lebanon":39.15,"Lesotho":1.8,"Liberia":0.98,"Libya":77.91,"Lithuania":35.73,"Luxembourg":52.43,"Macedonia":9.58,"Madagascar":8.33,"Malawi":5.04,"Malaysia":218.95,"Maldives":1.43,"Mali":9.08,"Malta":7.8,"Mauritania":3.49,"Mauritius":9.43,"Mexico":1004.04,"Moldova":5.36,"Mongolia":5.81,"Montenegro":3.88,"Morocco":91.7,"Mozambique":10.21,"Myanmar":35.65,"Namibia":11.45,"Nepal":15.11,"Netherlands":770.31,"New Zealand":138,"Nicaragua":6.38,"Niger":5.6,"Nigeria":206.66,"Norway":413.51,"Oman":53.78,"Pakistan":174.79,"Panama":27.2,"Papua New Guinea":8.81,"Paraguay":17.17,"Peru":153.55,"Philippines":189.06,"Poland":438.88,"Portugal":223.7,"Qatar":126.52,"Republic of the Congo":11.88,"Romania":158.39,"Russian Federation":3476.91,"Rwanda":5.69,"Saint Kitts and Nevis":0.56,"Saint Lucia":1,"Saint Vincent and the Grenadines":0.58,"Samoa":0.55,"Sao Tome and Principe":0.19,"Saudi Arabia":434.44,"Senegal":12.66,"Serbia":38.92,"Seychelles":0.92,"Sierra Leone":1.9,"Singapore":217.38,"Slovakia":86.26,"Slovenia":46.44,"Solomon Islands":0.67,"South Africa":354.41,"South Korea":986.26,"Spain":1374.78,"Sri Lanka":48.24,"Sudan":65.93,"Suriname":3.3,"Swaziland":3.17,"Sweden":444.59,"Switzerland":522.44,"Syria":59.63,"Taiwan":426.98,"Tajikistan":5.58,"Tanzania":22.43,"Thailand":312.61,"Togo":3.07,"Tonga":0.3,"Trinidad and Tobago":21.2,"Tunisia":43.86,"Turkey":729.05,"Turkmenistan":0,"Uganda":17.12,"Ukraine":136.56,"United Arab Emirates":239.65,"United Kingdom":2258.57,"United States":6624.18,"Uruguay":40.71,"Uzbekistan":37.72,"Vanuatu":0.72,"Venezuela":285.21,"Vietnam":101.99,"Yemen":30.02,"Zambia":15.69,"Zimbabwe":5.57, "Bolivia, Plurinational State of":121.34,"Somalia": 0.47,"Tanzania, United Republic of": 0.78,"South Sudan": 0.98,"Congo, the Democratic Republic of the": 1.45
    };


}
