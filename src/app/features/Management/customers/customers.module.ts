import {NgModule} from "@angular/core";

import {routing} from "./customers.routing";
import {CustomersComponent} from "@app/features/Management/customers/customers.component";
import {CustomersFormComponent} from "@app/features/Management/customers/customers-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminWizardsModule} from "@app/shared/forms/wizards/smartadmin-wizards.module";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {D3Module} from "@app/shared/graphs/d3/d3.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      CustomersComponent,
      CustomersFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule,
      ReactiveFormsModule,
      SmartadminInputModule,
      SmartadminWizardsModule,
      NgMultiSelectDropDownModule.forRoot(),
      D3Module,
      MaterialModuleModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [],
    exports:[
        FormsModule,
        ReactiveFormsModule
    ]
})
export class CustomersModule {

}
