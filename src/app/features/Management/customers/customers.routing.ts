
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {CustomersComponent} from "@app/features/Management/customers/customers.component";
import {CustomersFormComponent} from "@app/features/Management/customers/customers-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: CustomersComponent
  },{
    path: 'createnew/:id',
    component: CustomersFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
