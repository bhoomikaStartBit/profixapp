import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  url: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/customer/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getAllCustomers(id): Observable<any> {
    return this.http.get(this.url + 'getAll/'+id).pipe(
      map(this.extractData));
  }

  getOneCustomers(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }

    checkUserName(name): Observable<any> {
    return this.http.get(this.url + 'checkUserName/' + name).pipe(
      map(this.extractData));
  }
    checkEmail(email): Observable<any> {
    return this.http.get(this.url + 'checkEmail/' + email).pipe(
      map(this.extractData));
  }

  addCustomers (data , accountdata): Observable<any> {
    var dat = {
      'Basic':data,
        'Account':accountdata,
    }
    console.log(dat);
    return this.http.post<any>(this.url + 'create', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  updateCustomers (id, BasicData , AccountData): Observable<any> {
      var dat = {
          'Basic':BasicData,
          'Account':AccountData,
      }
    return this.http.put<any>(this.url + 'update/' + id, dat, httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${id}`)),
      catchError(this.handleError<any>('updateCity'))
    );
  }

  deleteCustomers (id): Observable<any> {
    return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
      tap(( ) => console.log(`deleted role w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
