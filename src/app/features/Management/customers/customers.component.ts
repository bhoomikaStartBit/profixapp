import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import  {CustomersService} from "@app/features/Management/customers/customers.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {AuthenticationService} from "@app/core/common/_services/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'userlist',
  templateUrl: './customers.component.html',
})
export class CustomersComponent implements OnInit {
    localStorage : any;
    Customers : any;
    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];
    displayedColumns: string[] = [
        'SN',
        'Name',
        'ReferenceBy',
        'Email',
        'Locality',
        'City',
        'State',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private customersService: CustomersService,
      private snackBar: MatSnackBar,
      private authenticationService: AuthenticationService,
      private spinnerService : Ng4LoadingSpinnerService
  ) {
      this.spinnerService.hide();
      setTimeout(() => {
          this.authenticationService.checkSelectedComponentVisiblity('Customers');
      }, 100);
      this.localStorage = JSON.parse(localStorage.getItem('currentUser'));
      if (this.localStorage.RoleName === 'Super Admin') {
          this.getAllCustomers(0);
      } else {
          this.getAllCustomers(this.localStorage.id);
      }
  }

  ngOnInit() {
  }

    getAllCustomers(selectedLeaderID) {
        this.spinnerService.show();
        this.Customers = [];
        this.customersService.getAllCustomers(selectedLeaderID).subscribe((data: {}) => {
            this.spinnerService.hide();
            this.Customers = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


    delete(id){
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: 'Are you sure delete this record?',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.customersService.deleteCustomers(id).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record delete successfully', 'Delete', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    } else {
                    }
                    if (this.localStorage.RoleName === 'Super Admin') {
                        this.getAllCustomers(0);
                    } else {
                        this.getAllCustomers(this.localStorage.id);
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

}
