
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {CameraComponent} from "@app/features/Management/camera/camera.component";


export const routes:Routes = [

  {
    path: '',
    component: CameraComponent
  }
];

export const routing = RouterModule.forChild(routes);
