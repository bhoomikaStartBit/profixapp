import { Component, OnInit , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
declare var navigator;
declare var Camera;
declare var window;
declare var MediaPicker;

@Component({
  selector: 'camera',
  templateUrl: './camera.component.html',
})
export class CameraComponent implements OnInit {
    
  constructor(
      private http: HttpClient
  ) {
     
  }

  ngOnInit() {
       
  }
  
  imagepic(){
        navigator.camera.getPicture((imageURI) => {
            //var image = document.getElementById('myImage');
            //image.src = imageURI;
            document.getElementById('myImage').setAttribute( 'src', imageURI );

        }, (message) => {
            alert('Failed because: ' + message);
        }, { quality: 50,  sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: Camera.DestinationType.FILE_URI });

  }

 imagepic2(){
      navigator.camera.getPicture((imageURI) => {
            //var image = document.getElementById('myImage');
            //image.src = imageURI;
            document.getElementById('myImage2').setAttribute( 'src', imageURI );
        }, (message) => {
            alert('Failed because: ' + message);
        }, { quality: 50, 
            destinationType: Camera.DestinationType.FILE_URI });

  }

  imagepic3(){
      navigator.device.capture.captureImage((selectedImgs) => {
          console.log(selectedImgs);
          var i, path, len;
            for (i = 0, len = selectedImgs.length; i < len; i += 1) {
                path = selectedImgs[i].fullPath;
                document.getElementById('Image'+i).setAttribute( 'src', path );

                // do something interesting with the file
            }
      }, (error) => {
          console.log(error);
      }, { limit : 5 });
       

  }

  imagepic4(){
        var resultMedias=[];
        // var imgs = document.getElementsByName('imgView');
        var args = {
            'selectMode': 100, //101=picker image and video , 100=image , 102=video
            'maxSelectCount': 40, //default 40 (Optional)
            'maxSelectSize': 188743680, //188743680=180M (Optional) only android
        };
       MediaPicker.getMedias(args, (medias) => {
        //medias [{mediaType: "image", path:'/storage/emulated/0/DCIM/Camera/2017.jpg', uri:"android retrun uri,ios retrun URL" size: 21993}]
        resultMedias = medias;
        this.getThumbnail(medias);
       }, (e) => { console.log(e) });
  }

  getThumbnail(medias){
      var imgs = document.getElementsByName('imgView');

      for (var i = 0; i < medias.length; i++) {
        //medias[i].thumbnailQuality=50; (Optional)
        //loadingUI(); //show loading ui
        MediaPicker.extractThumbnail(medias[i], function(data) {
            imgs[data.index].setAttribute('src', 'data:image/jpeg;base64,' + data.thumbnailBase64);
            imgs[data.index].setAttribute('style', 'transform:rotate(' + data.exifRotate + 'deg)');
        }, function(e) { console.log(e) });
    }
  }

}