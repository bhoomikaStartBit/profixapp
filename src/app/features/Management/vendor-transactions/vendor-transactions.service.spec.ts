import { TestBed } from '@angular/core/testing';

import { VendorTransactionsService } from './vendor-transactions.service';

describe('VendorTransactionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VendorTransactionsService = TestBed.get(VendorTransactionsService);
    expect(service).toBeTruthy();
  });
});
