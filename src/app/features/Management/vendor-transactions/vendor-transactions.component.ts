import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {VendorTransactionsService} from "@app/features/Management/vendor-transactions/vendor-transactions.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'vendor-transactions',
  templateUrl: './vendor-transactions.component.html'
})
export class VendorTransactionsComponent implements OnInit {
    Transactions : any;
    url : any;
    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];

    displayedColumns: string[] = [
        'SN',
        'OrderID',
        'VendorID',
        'OrderAMT',
        'SettledAMT',
        'SettledBY',
        'IsSettled',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private vendorTransactionsService: VendorTransactionsService,
      private snackBar: MatSnackBar,
      private commonService: CommonService,
      private spinnerService: Ng4LoadingSpinnerService

  ) {
    this.spinnerService.hide();
      this.getAllTransactions();
      this.url = this.commonService.getApiUrl();
  }

  ngOnInit() {

  }

  getAllTransactions(){
        this.spinnerService.show();
        this.vendorTransactionsService.getAllTransactions().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Transactions = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.filterPredicate = function(data, filter: string): boolean {
                return data.Name.toLowerCase().includes(filter);
            };
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            console.log(this.Transactions);
        });
    }


    applyFilter(filterValue: string) {
        console.log(filterValue)
        console.log(this.dataSource.data)
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
