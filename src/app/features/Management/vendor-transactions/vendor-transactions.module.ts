import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorTransactionsComponent } from './vendor-transactions.component';
import { EditVendorTransactionsComponent } from './edit-vendor-transactions.component';
import {routing} from "./vendor-transactions.routing";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { SmartadminWizardsModule } from '@app/shared/forms/wizards/smartadmin-wizards.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { StatsModule } from '@app/shared/stats/stats.module';
import { SmartadminWidgetsModule } from '@app/shared/widgets/smartadmin-widgets.module';
import {SmartadminLayoutModule} from "@app/shared/layout";
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';


@NgModule({
  declarations: [
    VendorTransactionsComponent,
    EditVendorTransactionsComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule,
      ReactiveFormsModule,
      SmartadminInputModule,
      SmartadminWizardsModule,
      NgMultiSelectDropDownModule.forRoot(),
      StatsModule,
      SmartadminWidgetsModule,
      SmartadminLayoutModule,
      CommonModule,
      MaterialModuleModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [],
    exports:[
        FormsModule,
        ReactiveFormsModule
    ]
})
export class VendorTransactionsModule {

}
