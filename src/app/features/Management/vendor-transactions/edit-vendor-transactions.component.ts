import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';

import { Router} from '@angular/router';
import {VendorTransactionsService} from "@app/features/Management/vendor-transactions/vendor-transactions.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "@app/core/services";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
  selector: 'edit-vendor-transactions',
  templateUrl: './edit-vendor-transactions.component.html',
})


export class EditVendorTransactionsComponent implements OnInit {
    ID : any;
    TransactionForm : FormGroup;
    
    currentUser: any;
    getData: boolean = false;
    selltedamt = false;
    OrderID: any;
    OrderAMT: any;

  constructor(private http: HttpClient ,
    private router : Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private vendorTransactionsService: VendorTransactionsService,
    private notificationService: NotificationService,
    private spinnerService : Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      
      console.log(this.currentUser.id);
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      else{
        this.TransactionForm = fb.group({
            SettledAMT: [ '', [Validators.required ] ],
            Notes: [ '', [Validators.required ] ],
            SettledBY: [ this.currentUser.id ]
        });
          this.getData = true;
      }      
    }

    ngOnInit() {
    }

    onSubmitForm() {

        if (this.TransactionForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
           // this.createNew();
        } else {

            if(this.TransactionForm.controls.SettledAMT.value > this.OrderAMT){
                this.selltedamt = true;
                return 0;
            }
            if(this.TransactionForm.controls.SettledAMT.value.toString() == this.OrderAMT.toString()){
                this.selltedamt = false;
            }

            this.selltedamt = false;
            this.editExisting();
        }
    }



    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.vendorTransactionsService.updateTransaction(this.ID, this.TransactionForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        
                        this.router.navigate(['/vendor-transactions']);
                        console.log("update success");
                    }
                }, err => {
                    this.spinnerService.hide();
                });
            // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.vendorTransactionsService.getOneTransaction(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            
            if ( data['status']) {
                this.TransactionForm = this.fb.group({
                    SettledAMT: [ data['result'].SettledAMT, [Validators.required] ],
                    Notes: [ data['result'].Notes, [Validators.required] ],
                    SettledBY: [ data['result'].SettledBY ? data['result'].SettledBY : this.currentUser.id]
                });
                this.getData = true;
                this.OrderAMT = data['result'].OrderAMT;
                this.OrderID = data['result'].OrderID.ProjectName +' #'+data['result'].OrderID.OrderID;
                console.log(this.OrderID);
            }
        }, err => {
            this.spinnerService.hide();
        });
    }



    register(){
      console.log(this.TransactionForm.value);
        this.router.navigate(['/vendor-transactions']);
    }

    cancel() {
        this.router.navigate(['/vendor-transactions']);
    }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }


}
