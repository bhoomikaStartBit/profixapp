
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {VendorTransactionsComponent} from "@app/features/Management/vendor-transactions/vendor-transactions.component";
import {EditVendorTransactionsComponent} from "@app/features/Management/vendor-transactions/edit-vendor-transactions.component";



export const routes:Routes = [

  {
    path: '',
    component: VendorTransactionsComponent
  },
  {
    path: 'edit/:id',
    component: EditVendorTransactionsComponent
  },


];

export const routing = RouterModule.forChild(routes);
