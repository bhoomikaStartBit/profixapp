import { Component, OnInit , ViewChild } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {ServicesService} from "@app/features/Management/service/services.service";
import {NotificationService} from "@app/core/services";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'userlist',
  templateUrl: './service.component.html',
})
export class ServiceComponent implements OnInit {
    cur : number = 1.3495;
    Services : any;

    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];

    displayedColumns: string[] = [
        'SN',
        'Name',
        'Category',
        'Type',
        'Commission',        
        'Price',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
      private http: HttpClient,
      private notificationService: NotificationService,
      private servicesService: ServicesService,
      private snackBar: MatSnackBar,
      private spinnerService : Ng4LoadingSpinnerService
  ) {
      this.spinnerService.hide();
      this.getAllService();
  }

  ngOnInit() {      
  }

    getAllService(){
        this.spinnerService.show();
        this.servicesService.getAllServices().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Services = data['result'];

            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            console.log(this.Services);
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


    delete(id){
        console.log(id);
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.servicesService.deleteServices(id).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record delete successfully', 'Delete', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    } else {
                    }
                    this.getAllService();
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

}
