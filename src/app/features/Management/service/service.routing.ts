
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {ServiceComponent} from "@app/features/Management/service/service.component";
import {ServiceFormComponent} from "@app/features/Management/service/service-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: ServiceComponent
  },{
    path: 'createnew/:id',
    component: ServiceFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
