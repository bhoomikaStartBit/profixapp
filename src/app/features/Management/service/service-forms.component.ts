import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {ServicesService} from "@app/features/Management/service/services.service";
import {CategoriesService} from "@app/features/Management/categories/categories.service";
import {TypesService} from "@app/features/Management/types/types.service";
import {UnitService} from "@app/features/Management/unit/unit.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from "@angular/common";
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'user-form',
  templateUrl: './service-forms.component.html',
})
export class ServiceFormComponent implements OnInit {

    ID :any;
    Categories :any;
    Services :any;
    Types :any;
    Units :any;
    ServiceForm : FormGroup;
    showSpinner: boolean = false;
  constructor(
      private http: HttpClient,
      private router : Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private servicesService: ServicesService,
      private categoriesService: CategoriesService,
      private typesService: TypesService,
      private location : Location,
      private unitService: UnitService,
      private notificationService: NotificationService,
      private snackBar: MatSnackBar,
      private spinnerService : Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.getAllCategory();
      this.getAllUnit();
      this.getAllTypes();
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      this.ServiceForm = this.fb.group({
          Name: [ '', [Validators.required ] ],
          Description: [ '', [Validators.required ] ],
          Category: [ '', [Validators.required ] ],
          Type: [ '', [Validators.required ] ],
          Unit: [ '', [Validators.required ] ],
          Price: [ '', [Validators.required ] ],
          Commission: [ '', [Validators.required ]],
          CommissionType: [ 'percentage' ],
          Slug: [ '', [Validators.required ] ],
          QTYPerDay: [ '', [Validators.required ] ],
          NoOfSP: [ '', [Validators.required ] ]
      });
  }

  ngOnInit() {
  }

    setValueNumber(val , name){
      if(name == 'price'){
          this.ServiceForm.patchValue({
              'Price': parseFloat(val).toFixed(2)
        })
      }
      else{
          this.ServiceForm.patchValue({
              'Commission': parseFloat(val).toFixed(2)
          })
      }
    }

    getAllCategory(){
        this.spinnerService.show();
        this.categoriesService.getAllCategories().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Categories = data['result'];
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getAllTypes(){
        this.spinnerService.show();
        this.typesService.getAllTypes().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Types = data['result'];
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getAllUnit(){
        this.spinnerService.show();
        this.unitService.getAllUnit().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Units = data['result'];
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }


    onSubmitForm() {
        if (this.ServiceForm.invalid) {
            this.snackBar.open('All Fields are required.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0; 
        }
        if ( this.ServiceForm.controls.Commission.value > 100 ) {
            this.snackBar.open('Please enter commissions less than 100', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        
        this.spinnerService.show();
        this.servicesService.addServices(this.ServiceForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
                console.log("Created successfully");
            } else {
            
            this.snackBar.open(data['message'], 'error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });

    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.servicesService.updateServices(this.ID, this.ServiceForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
            }
            if (ButtonPressed === "No") {

            }
        });
    }

    getOne(id) {
        this.spinnerService.show();
        this.servicesService.getOneServices(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.ServiceForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    Description: [  data['result'].Description, [Validators.required ] ],
                    Category: [  data['result'].Category._id, [Validators.required ] ],
                    Type: [  data['result'].Type._id, [Validators.required ] ],
                    Unit: [  data['result'].Unit._id, [Validators.required ] ],
                    Price: [  data['result'].Price, [Validators.required ] ],
                    Commission: [  data['result'].Commission, [Validators.required ] ],
                    CommissionType: [  data['result'].CommissionType, [Validators.required ] ],
                    Slug: [ data['result'].Slug, [Validators.required ] ],
                    QTYPerDay: [ data['result'].QTYPerDay, [Validators.required ] ],
                    NoOfSP: [ data['result'].NoOfSP, [Validators.required ] ]
                });

                console.log(this.ServiceForm);
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    register(){
            console.log(this.ServiceForm);
    }

    cancel() {
        this.location.back();
    }

    onChangeService() {
        let service = this.ServiceForm.get('Name').value;
        service = service.replace(/[^\w\s]/gi, "");
        service = service.toLowerCase();
        service = service.trim();
        service = service.split(' ').join('-');
        let str = '';
        for(let i = 0; i< service.length; i++) {
            if(service.length-1 > i) {
                let first = service.charAt(i);
                let second = service.charAt(i + 1);
                if(!(first === '-' && second === '-')) {
                    str += service.charAt(i, i+1);
                }
            } else {
                str += service.charAt(i);
            }
        }
        if (str.length > 190) {
            str = str.substring(0, 190);
        }
        this.ServiceForm.patchValue({
            Slug: str + '-service'
        });
    }

}
