import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {UnitService} from "@app/features/Management/unit/unit.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'user-form',
  templateUrl: './unit-forms.component.html',
})
export class UnitFormComponent implements OnInit {
    ID : any;
    UnitForm : FormGroup;
    
  constructor(private http: HttpClient,
              private router : Router,
              private route : ActivatedRoute,
              private fb : FormBuilder,
              private location : Location,
              private unitService : UnitService,
              private notificationService : NotificationService,
              private snackBar : MatSnackBar,
              private spinnerService : Ng4LoadingSpinnerService
  ) {
      this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      this.UnitForm = this.fb.group({
          Name: [ '', [Validators.required ] ],
          Symbol: [ '', [Validators.required ] ],
      });
  }

  ngOnInit() {

  }

    onSubmitForm() {
        if (this.UnitForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.unitService.addUnit(this.UnitForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.unitService.updateUnit(this.ID, this.UnitForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.snackBar.open('Server Error', 'Error', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    getOne(id) {
        this.spinnerService.show();
        this.unitService.getOneUnit(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.UnitForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    Symbol: [  data['result'].Symbol, [Validators.required ] ],
                });

                console.log(this.UnitForm);
            }
        }, err => {
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }


    register(){
        // this.router.navigate(['/roles']);
        this.location.back();
    }

    cancel() {
        // this.router.navigate(['/roles']);
        this.location.back();
    }

}
