import {NgModule} from "@angular/core";

import {routing} from "./unit.routing";
import {UnitComponent} from "@app/features/Management/unit/unit.component";
import {UnitFormComponent} from "@app/features/Management/unit/unit-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      UnitComponent,
      UnitFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule ,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [],
})
export class UnitModule {

}
