
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {UnitComponent} from "@app/features/Management/unit/unit.component";
import {UnitFormComponent} from "@app/features/Management/unit/unit-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: UnitComponent
  },{
    path: 'createnew/:id',
    component: UnitFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
