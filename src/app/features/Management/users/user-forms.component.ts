import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Observable ,Subject } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {NotificationService} from "@app/core/services";
import {StateService} from "@app/features/Management/state/state.service";
import {CityService} from "@app/features/Management/city/city.service";
import {RoleService} from "@app/features/Management/roles/role.service";
import {ServicesService} from "@app/features/Management/service/services.service";
import {UserService} from "@app/features/Management/users/user.service";
import {MatSnackBar, MatStepper} from "@angular/material";
import {saveAs as importedSaveAs} from 'file-saver';
import { DomSanitizer } from '@angular/platform-browser';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';

import {WebcamImage, WebcamInitError, WebcamUtil} from 'ngx-webcam';
import { NgxSmartModalService } from 'ngx-smart-modal';


export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

import { take, takeUntil } from 'rxjs/operators';

import { trigger,
    state,
    style,
    transition,
    animate} from '@angular/animations'
import { ProfilesService} from '../profiles/profiles.service';
import { AuthenticationService } from '@app/core/common/_services/authentication.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CommonService } from '@app/core/common/common.service';
import { TeamCompanyService } from '../team-company/team-company.service';
import { CompaniesService } from '../Companies/companies.service';

@Component({
  selector: 'user-form',
  templateUrl: './user-forms.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
    animations: [
        trigger('changePane', [
            state('out', style({
                height: 0,
            })),
            state('in', style({
                height: '*',
            })),
            transition('out => in', animate('250ms ease-out')),
            transition('in => out', animate('250ms 300ms ease-in'))
        ])
    ],
    styles: [`.notific-txt {
        font-size: 16px;
        color: red;
        margin-top: 11px;
    }`]
})
export class UserFormComponent implements OnInit {
    ID: any;
    States: any = [];
    Relations: any = [];
    getValue = false;
    Cities: any = [];
    ShopCities: any = [];
    TemporaryCities: any = [];
    Services: any;
    Commissions : any =[];
    
    Roles: any;
    Role: any = '';
    VendorType: any = 'I';
    getRoleData  = false;
    allDocuments  = [];
    UserServices  = [];
    pinlength = 6;
    UserForm: FormGroup;
    DocumentInfoForm: FormGroup;
    BasicInfoForm: FormGroup;
    SecondaryDetailForm: FormGroup;
    FamilyInfoForm: FormGroup;
    AccountInfoForm: FormGroup;
    ShopInfoForm: FormGroup;
    FinacInfo: FormGroup;
    FinacVendorInfo: FormGroup;
    CreditLimit: FormGroup;
    ServiceInfo: any;
    SelectRole : any;

    documentArray: Array<any> = [];
    documentUploadedArray: any = [];
    allImages: any = [];
    docMandatory = 0;


    educationDocumentArray: any = [];
    eduDocTitle = '';
    eduDocImage: any;
    educationDocType = '';

    selectedExpirtDate: Date;
    selectedStartDate : Date;

    VenderAddressCities : any = [];
    VenderAddress : any = [];
    VendorAddressForm: FormGroup;

    cameraPermission: boolean = false;
    savewebcamImage: boolean = false;
    imageCamera: boolean = false;

    userdata : any = [];
    BloodGroups : any = [];
    CreditLimits : any = [];
    CreditDays : any = [];
    Languages : any = [];

    SelectedVenderAddress = -1;
    @ViewChild('stepper') stepper: MatStepper;

    @ViewChild('avatarImage') myInputVariable: ElementRef;

    @ViewChild('password') inputPasswordField:ElementRef;
    @ViewChild('cpassword') inputCPasswordField:ElementRef;
    
    @ViewChild('Document') inputDocumentField:ElementRef;
    @ViewChild('Financial') inputFinancialField:ElementRef;
    @ViewChild('TimePeriod') inputTimePeriodField:ElementRef;
    @ViewChild('Rolefield') inputRoleField:ElementRef;

    public validationUserFormDetailOptions = {};
    public validationPasswordDetailOptions = {};
    public validationVendorAddressOptions = {};

    maxDate :Date;
    SPType : any;

    public showWebcam = true;
    public allowCameraSwitch = true;
    public multipleWebcamsAvailable = false;
    public deviceId: string;
    public videoOptions: MediaTrackConstraints = {
      // width: {ideal: 1024},
      // height: {ideal: 576}
    };
    public errors: WebcamInitError[] = [];
  
    // latest snapshot
    public webcamImage: WebcamImage = null;
  
    // webcam snapshot trigger
    private trigger: Subject<void> = new Subject<void>();
    // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();    

    CompanyForm : FormGroup;
    BranchForm : FormGroup;
    CompanyType : any = [];
    UpdateCommissions : any = [];
    CompanyVendors : any = [];
    CheckAll : boolean = false;
    DisplayCompanyName : any = '';
    imgUrl :any = '';

    CompanyManagerEmail: any;
    ServiceProviderType = 'F';
    
    Teams  = [];
    Companies  = [];
    TeamID= new FormControl();
    CompanyID = new FormControl();
    uploadurl = this.commonService.getApiUrl()+'/multipleUploads/upload';
    public CompanyFilterCtrl: FormControl = new FormControl();
    public TeamFilterCtrl: FormControl = new FormControl();
    FilterCompany : any = [];
    FilterTeam : any = [];

  constructor(private http: HttpClient,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private location: Location,
              private notificationService: NotificationService,
              private stateService: StateService,
              private cityService: CityService,
              private roleService: RoleService,
              private servicesService: ServicesService,
              private userService: UserService,
              private snackBar: MatSnackBar,
              private sanitizer: DomSanitizer,
              public ngxSmartModalService: NgxSmartModalService,
              private profilesService : ProfilesService,
              private authenticationService : AuthenticationService,
              private spinnerService : Ng4LoadingSpinnerService,
              private commonService : CommonService,
              private teamService : TeamCompanyService,
              private companyService : CompaniesService
  ) {

        var d = new Date();
        d.setDate(d.getDate()-1)
        d.setFullYear(d.getFullYear()-18)
        this.maxDate = d;

      this.imgUrl = this.commonService.getApiUrl();

    this.commonService.getCompanyTypes().subscribe((data:{})=>{
        if(data['status']){
            this.CompanyType = data['result']
        }
    }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
    this.spinnerService.hide();
    setTimeout(() => {
        this.authenticationService.checkSelectedComponentVisiblity('Users');
      }, 100);
    this.VendorAddressForm = fb.group({
        Name: [ '', [Validators.required ] ],
        Email: [ '', [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
        Number: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
        Address: [ '', [Validators.required ] ],
        Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
        
        City: [ '', [Validators.required ] ],
        State: [ '', [Validators.required ] ]
    });

  this.validationVendorAddressOptions = {
      // Rules for form validation
      rules: {
          VAName: {
              required: true
          },
          VAEmail: {
              required: true,
              email: true,
              pattern:'[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
          },
          VANumber: {
              required: true,
              pattern: "[0-9]+",
              minlength:10,
              maxlength:10
          },
          VAState: {
              required: true
          },
          VACity: {
              required: true
          },
          VAPin: {
              required: true,
              minlength:6,
              maxlength:6
          },
          VAAddress: {
              required: true
          }
      },

      // Messages for form validation
      messages: {
          VAName: {
              required: 'Please enter name'
          },
          VAEmail: {
              required: 'Please enter your email address',
              email: 'Please enter a valid email addres',
              pattern: 'Please enter a valid email pattern'
          },
          VANumber: {
              required: 'Please enter your mobile number',
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
              maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
          },
          VAState: {
              required: 'Please select the state'
          },
          VACity: {
              required: 'Please select the city'
          },
          VAPin: {
              required: 'Please enter pincode',
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
              maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
          },
          VAAddress: {
              required: 'Please enter address'
          }
      },
      submitHandler: this.onSubmit

  };
      this.validationUserFormDetailOptions = {
          // Rules for form validation
          rules: {
              FirstName: {
                  required: true
              },
              SPEmail:{
                email: true,
                pattern : '[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
              },
              Email: {
                  required: true,
                  email: true,
                  pattern : '[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
              },
              Phone: {
                  required: true,
                  pattern: "[0-9]+",
                  minlength:10,
                  maxlength:10
              },
              SecondaryPhone: {
                  pattern: "[0-9]+",
                  minlength:10,
                  maxlength:10
              },
              AltPhone: {
                 // pattern: "[0-9]+",
                //   minlength:10,
                //   maxlength:10
              },
              Avatar: {
                required: true
              },
              State: {
                  required: true
              },
              City: {
                  required: true
              },
              Pin: {
                  required: true,
                  minlength:6,
                  maxlength:6
              },
              Address: {
                  required: true
              },
              TemporaryState: {
                  required: true
              },
              TemporaryCity: {
                  required: true
              },
              TemporaryPin: {
                  required: true,
                  minlength:6,
                  maxlength:6
              },
              TemporaryAddress: {
                  required: true
              },
              DOB: {
                  required: true,
                  pattern : '^\\d{1,2}\\/\\d{1,2}\\/\\d{4}$'
              },
          },

          // Messages for form validation
          messages: {
              FirstName: {
                  required: 'Please enter your first name'
              },
              Email: {
                  required: 'Please enter your email address',
                  email: 'Please enter a valid email addres',
                  pattern: 'Please enter valid email pattern'
              },
              SPEmail:{
                email: 'Please enter a valid email addres',
                pattern: 'Please enter valid email pattern'
              },
              Phone: {
                  required: 'Please enter your mobile number',
                  minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                  maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
              },
              SecondaryPhone: {
                  minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                  maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
              },
              AltPhone: {
                //   minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                //   maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
              },
              Avatar: {
                required: 'Please Upload Only jpg, jpeg and png file.'
              },
              State: {
                  required: 'Please select the state'
              },
              City: {
                  required: 'Please select the city'
              },
              Pin: {
                  required: 'Please enter pincode',
                  minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
                  maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
              },
              Address: {
                  required: 'Please enter address'
              },
              TemporaryState: {
                  required: 'Please select the state'
              },
              TemporaryCity: {
                  required: 'Please select the city'
              },
              TemporaryPin: {
                  required: 'Please enter pincode',
                  minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
                  maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
              },
              TemporaryAddress: {
                  required: 'Please enter address'
              },
              DOB: {
                  required: 'Please enter DOB',
                  pattern: 'Please enter DOB'
              }
          },
          submitHandler: this.onSubmit

      };

      this.validationPasswordDetailOptions = {
          // Rules for form validation
          rules: {
              Password: {
                  required: true,
                  minlength: 8,
                  pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$'
              },
              ConfirmPassword: {
                  required: true,
                  minlength: 8,
                  pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$',
                  equalTo: '#Password'
              }
          },

          // Messages for form validation
          messages: {
              Password: {
                  required: 'Please enter new password',
                  minlength: 'Password should be at least 8 characters long',
                  pattern: 'Password should contain one number,one character, one uppercase character and one special character'
              },
              ConfirmPassword: {
                  required: 'Please enter confirm password',
                  minlength: 'Password should be at least 8 characters long',
                  pattern: 'Password should contain one number,one character, one uppercase character and one special character',
                  equalTo: 'Please re-enter the same password again.'
              }
          },
          submitHandler: this.onSubmitForPassword

      };
      this.ID = this.route.params['value'].id;
      if(this.ID !== '-1'){
          this.getOne(this.ID);
      }
      else{
          this.BasicInfoForm = fb.group({
              FirstName: [ '', [Validators.required ] ],
              MiddleName: [ '' ],
              LastName: [ '' ],
              Email: [ '', [Validators.required, Validators.email  ] ],
              Phone: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
              SecondaryMobile: [ '', [Validators.minLength(10), Validators.maxLength(10) ] ],
              AltPhone: [ '', [ ] ],
              EmgPhone: [ '', [ ] ],
              Gender: [ 'M', [Validators.required ] ],
              DOB: [ '', [Validators.required  ]],
              Avatar: [ '' ],
              Address: [ '', [Validators.required ] ],
              Pin: [ '', [Validators.required , Validators.maxLength(6), Validators.minLength(6) ] ],
             
              City: [ '', [Validators.required ] ],
              State: [ '', [Validators.required ] ],
          });
          this.AccountInfoForm = fb.group({
              Password: [ '', [Validators.minLength(8),
                  Validators.required,
                  Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]],
              ConfirmPassword: [ '', [Validators.minLength(8),
                  Validators.required,
                  Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]],
              Role: [ '', [Validators.required ] ],
              IsAvailable: [ 'Y', [Validators.required ] ],
              
              Account_Blocked: [ 'N', [Validators.required ] ],
              TimePeriod: [ '0']
          });
          this.CreditLimit = fb.group({
              CreditLimit: [ '', [Validators.required ] ],
          });
          
          this.FinacInfo = fb.group({
              TypeOfAccount : ['savings', [Validators.required ]],
              Name : ['', [Validators.required ]],
              AccountNumber : ['', [Validators.required ]],
              BankName : ['', [Validators.required ]],
              CreditLimits : ['', [Validators.required ]],
              IFSCCode : ['', [Validators.required ]],
              OnlineAccountType : ['paytm'],
              OnlineAccountID : [''],
              CreditDays : ['', [Validators.required ]],
          })

          this.FinacVendorInfo = fb.group({
            TypeOfAccount : ['current', [Validators.required ]],
            Name : ['', [Validators.required ]],
            AccountNumber : ['', [Validators.required ]],
            BankName : ['', [Validators.required ]],
            IFSCCode : ['', [Validators.required ]],
        })

          this.DocumentInfoForm = fb.group({
              IDType: [ '', [Validators.required ] ],
              IDNo: [ '', [Validators.required ] ],
              IDExpiryDate: [ '', [Validators.required ] ],
              IDIssuedBy: [ '', [Validators.required ] ],
              Image: [ '', [Validators.required ] ],
              IsFeesPaid: [ '', [Validators.required ] ],
          });
          this.ShopInfoForm = fb.group({
              ShopName: [ '', [Validators.required ] ],
              ShopRegistrationNo: [ '', [Validators.required ] ],
              CompanyEmail: [ '', [Validators.required , Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
          });
          this.SecondaryDetailForm = fb.group({
              EducationQualification : [''],
              Language : [''],
              Injuries : [''],
              BloodGroup : ['NA'],
              Vehicle : [''],
          })
          this.FamilyInfoForm = fb.group({
              FatherName : [''],
              MotherName : [''],
              SpouseName : [''],
          })

          this.CompanyForm = fb.group({
            Role: [''],
            CompanyType: [ '', [Validators.required ] ],
            CompanyName: [ '', [Validators.required ] ],
            CompanyEmail: [ '', [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
            CompanyPhone: [ '', [Validators.required, Validators.maxLength(11)  ] ],
            ContactPersonName: [ '', [Validators.required ] ],
            ContactPersonEmail: [ '', [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
            ContactPersonPhone: [ '', [Validators.required , Validators.minLength(10) , Validators.maxLength(10)  ] ],
            Password: [ '', [Validators.minLength(8),
                Validators.required,
                Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
            ConfirmPassword: [ '', [Validators.minLength(8),
                Validators.required,
                Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
            CreditLimit : ['', [Validators.required ]],
            CreditDays : ['', [Validators.required ]],            
            FirstName: [ '', [Validators.required ] ],
            MiddleName: [ ''],
            LastName: [ ''],
            Email: [ '', [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
            Phone: [ '', [Validators.required, Validators.minLength(10) , Validators.maxLength(10) ] ],
          })

            this.BranchForm = fb.group({
                Role: [''],
                VendorCompanyID: [''],
                FirstName: [ '', [Validators.required ] ],
                MiddleName: [ ''],
                LastName: [ ''],
                Email: [ '', [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                Phone: [ '', [Validators.required, Validators.minLength(10) , Validators.maxLength(10) ] ],
                Password: [ '', [Validators.minLength(8),
                    Validators.required,
                    Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                ConfirmPassword: [ '', [Validators.minLength(8),
                    Validators.required,
                    Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
            })
          this.selectedStartDate = new Date();
          this.getValue = true;
          this.getAllServices();

          
      }
      this.commonService.getBloodGroups().subscribe((data)=>{
        if(data['status']){
            this.BloodGroups = data['result']
        }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    
    this.commonService.getCreditDays().subscribe((data)=>{
      if(data['status']){
          this.CreditDays = data['result']
      }
    }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
  
    this.commonService.getCreditLimits().subscribe((data)=>{
        if(data['status']){
            this.CreditLimits = data['result']
        }
    }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
  
    this.commonService.getLanguages().subscribe((data)=>{
        if(data['status']){
            this.Languages = data['result']
        }
    }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });

      this.getAllState();
      this.getAllRelations();
     // this.getAllDocuments();
      this.getAllRole();
      this.userdata = JSON.parse(localStorage.getItem('currentUser'));

      this.CompanyFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterCompanySearch();
            });

        this.TeamFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterTeamSearch();
        });

  }
  private _onDestroy = new Subject<void>();

  ngOnDestroy() {
      this._onDestroy.next();
      this._onDestroy.complete();
  }
  ngOnInit() {
      this.ID = this.route.params['value'].id;
      WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });
  }

  FilterInputNumber(event: any) {
    if ((event.which >= 48 && event.which <= 57)) { 
        return true;
    }
    else {
        return false;
    }
   
};

public handleInitError(error: WebcamInitError): void {
    console.log('error',error)
    if (error.mediaStreamError && error.mediaStreamError.name === "NotAllowedError") {
      console.warn("Camera access was not allowed by user!");
    }
  }

  public triggerSnapshot(): void {
     
    this.trigger.next();
    this.BasicInfoForm.patchValue({
        'Avatar' : this.webcamImage.imageAsDataUrl
    });

  }

  onFileChange1($event) {
     this.savewebcamImage = false;
     this.webcamImage = null;
     this.uploadImage($event, 'Avatar');
  }

  saveSnapshot(){
    
    this.savewebcamImage=true;
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public showNextWebcam(directionOrDeviceId: boolean|string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    this.deviceId = deviceId;
    this.cameraPermission = true;
    console.log('deviceId')
    console.log('deviceId ' , deviceId)
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }

FilterInputText(event: any) {
    if ((event.which >= 65 && event.which <= 90) || (event.which >= 97 && event.which <= 122) || (event.which == 32)) { 
        return true;
    }
    else {
        return false;
    }
   
};

  

  



    onSubmit(){

    }
    onSubmitForPassword(){

    }


    nextAccountStep(){
        if(!this.SelectRole){
            this.inputRoleField.nativeElement.focus();
            return 0;
        }
        
        if(this.BasicInfoForm.controls.Avatar.value == ''){
            this.snackBar.open('Please provide Avatar image.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }

        if(this.SelectRole == 'Service Provider'){
            console.log(this.TeamID.value , this.CompanyID.value)
            if(this.ServiceProviderType == 'T' && this.TeamID.value == null){
                this.snackBar.open('Please select Team.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
            if(this.ServiceProviderType == 'C' && this.CompanyID.value == null){
                this.snackBar.open('Please select Company.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
            if(this.BasicInfoForm.controls.TemporaryAddressType.value == ''){
                return 0;
            }
            else if(this.BasicInfoForm.controls.TemporaryState.value == ''){
                return 0;
            }
            else if(this.BasicInfoForm.controls.TemporaryCity.value == ''){
                return 0;
            }
            
            else if(this.BasicInfoForm.controls.TemporaryPin.value == ''){
                return 0;
            }
            else if(this.BasicInfoForm.controls.TemporaryAddress.value == ''){
                return 0;
            }
        }
        var d  = new Date();
        d.setDate(d.getDate()-1);
        d.setFullYear(d.getFullYear()-18)
        if(this.ID != '-1'){
            if(d < new Date(this.BasicInfoForm.controls.DOB.value)){
                this.snackBar.open('Age should be minimum 18 years.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }
        else{
            if(d < this.BasicInfoForm.controls.DOB.value){
                this.snackBar.open('Age should be minimum 18 years.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }
        
        if(this.BasicInfoForm.invalid){
            return 0;
        }
        this.stepper.next();
    }
    gotoFinalStep(){
         if( this.AccountInfoForm.get('TimePeriod').value == '0'){
             this.snackBar.open('Please select membership duration.', '', {
                 duration: 5000,
                panelClass: ['danger-snackbar'],
                 verticalPosition: 'top'
             });
             this.inputTimePeriodField.nativeElement.focus();
             return 0;
         }
        if(this.docMandatory != 1){
            this.snackBar.open('Please upload mandantory documents.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        } 
        else{
            for(let docs of this.allDocuments){
                if(docs.value){
                    var index = this.documentArray.findIndex(x=> x.Name == docs.Name);
                    if(index == -1){
                        this.snackBar.open('Please upload '+docs.Name+' document.', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                        return 0;
                    }
                }
            }
        }
        if(!(this.route.snapshot.queryParams['ForCompany'] || this.ServiceProviderType == 'T' || this.ServiceProviderType =='C')){
            if(this.FinacInfo.invalid){
                this.snackBar.open('Please provide Financial details.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }
         

        this.stepper.next();
    }

    
    gotoServiceStep(){
        
       if(this.docMandatory != 1){
           this.snackBar.open('Please upload mandantory documents.', '', {
               duration: 5000,
               panelClass: ['danger-snackbar'],
               verticalPosition: 'top'
           });
           return 0;
       } 
       else{
           for(let docs of this.allDocuments){
               if(docs.value){
                   var index = this.documentArray.findIndex(x=> x.Name == docs.Name);
                   if(index == -1){
                       this.snackBar.open('Please upload '+docs.Name+' document.', '', {
                           duration: 5000,
                           panelClass: ['danger-snackbar'],
                           verticalPosition: 'top'
                       });
                       return 0;
                   }
               }
           }
       }
       if(!(this.route.snapshot.queryParams['ForCompany'] || this.SPType == 'MSP')){
        if(this.FinacInfo.invalid){
            this.snackBar.open('Please provide Financial details.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
    }

       this.stepper.next();
   }

    checkSameAsAddress(event , val){

        if(val){
            this.TemporaryCities = this.Cities;
            this.BasicInfoForm.patchValue({
                TemporaryAddressType : this.BasicInfoForm.controls.PermanentAddressType.value,
                TemporaryState : this.BasicInfoForm.controls.State.value,
                TemporaryCity : this.BasicInfoForm.controls.City.value,
                TemporaryPin : this.BasicInfoForm.controls.Pin.value,
                TemporaryAddress : this.BasicInfoForm.controls.Address.value,
            })
        }
    }

    getAllState(){
      this.spinnerService.show();
      this.stateService.getAllStates().subscribe((data: {}) => {
        this.spinnerService.hide();
          if(data['status']){
              this.States = data['result'];
          }
      }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
    }

    getAllRelations(){
      this.userService.getAllRelations().subscribe((data: {}) => {
          if(data['status']){
              this.Relations = data['result'];
          }
      }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
    }

    getAllDocuments(){
        this.spinnerService.show();
        this.userService.getAllDocumentsByUser(this.SelectRole).subscribe( (data :{}) =>{
            this.spinnerService.hide();
            if(data['status']){
                // this.allDocuments = data['result'];
                this.allDocuments = data['result'];
            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    addDocumentDetail(val, name) {
        // this.allImages
        let localArray = {};
        localArray['Name'] = name;
        localArray['Image'] = this.allImages[name];
        localArray['Number'] = val;
        let index = this.documentArray.findIndex(x => x.Name === name)
        if (index === -1) {
            if (name === 'Aadhar') {
                this.docMandatory++;
            }
            this.userService.addImage(this.allImages[name]).subscribe((data : {})=>{
                localArray['Image'] = data['path'];
                this.documentArray.push(localArray);
                this.documentUploadedArray[name] = name;
            }, err => {
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
            
        } else {
            this.userService.addImage(this.allImages[name]).subscribe((data : {})=>{
                this.documentArray[index].Image = this.allImages[name];
                this.documentArray[index].Number = val;
            }, err => {
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
        let deindex = this.allDocuments.findIndex(x => x.Name === name)
        console.log(deindex)
        var va = $('#'+deindex).val().split('\\');
        var filename = va[(va.length-1)];
        this.snackBar.open('File "'+filename+'" uploaded successfully.', '', {
            duration: 5000,
            panelClass: ['success-snackbar'],
            verticalPosition: 'top'
        });
        this.allDocuments[deindex].upload = false;
       
    }

    onFileChange(event, name) {
        const reader = new FileReader();
    
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            if((file.size)/1024 > 1024){
                this.snackBar.open('File size is too large , please choose file below 1MB size', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                const deindex = this.allDocuments.findIndex(x => x.Name === name);
                $('#'+deindex).val('');
                this.allDocuments[deindex].upload = false;
                return 0;
            }
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png' || extension === 'pdf' || extension === 'doc' || extension === 'docx') {
                reader.onload = () => {
                    this.allImages[name] = event.target.files;
                    const deindex = this.allDocuments.findIndex(x => x.Name === name);
                    this.allDocuments[deindex].upload = true;
                };
            } else {
                this.snackBar.open('Invalid file format. Upload Only jpg, jpeg, png , doc , docx and pdf file.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                const deindex = this.allDocuments.findIndex(x => x.Name === name);
                this.allDocuments[deindex].upload = false;
                $('#'+deindex).val('');
            }
        }
      }

    onEduDocumentFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            if((file.size)/1024 > 1024){
                this.snackBar.open('File size is too large, please choose file below 1MB size.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                $('#otherDoc').val('');
                return 0;
            }
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png' || extension === 'pdf'  || extension === 'doc' || extension === 'docx') {
                reader.onload = () => {
                    this.eduDocImage = event.target.files;
                    this.educationDocType = extension;
                };
            } else {
                this.snackBar.open('Invalid file format. Upload Only jpg, jpeg, png , doc , docx and pdf file.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                $('#otherDoc').val('');
            }
        }
    }

    addEduDocDetail() {
        if (!this.eduDocTitle || !this.eduDocImage || this.eduDocTitle === '' || this.eduDocImage === '') {
            return 0;
        }
        var va = $('#otherDoc').val().split('\\');
        var name = va[(va.length-1)];
        this.userService.addImage(this.eduDocImage).subscribe((data:{})=>{
            this.educationDocumentArray.push({
                Title: this.eduDocTitle,
                File: this.eduDocImage,
                FileType: this.educationDocType,
                FileName : name
            });
            this.eduDocImage = '';
            this.eduDocTitle = '';
            
            // this.myInputVariable.nativeElement.value = '';
            
            this.snackBar.open('File "'+name+'" uploaded successfully.', '', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
            $('#otherDoc').val('');
        })
        
    }

    deleteEduDoc(index) {
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this file?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.educationDocumentArray.splice(index, 1);
                // this.snackBar.open('Selected document is not deleted yet, please click on next button and save button to save your changes.', '', {
                //     duration: 5000,
                //     panelClass: ['danger-snackbar'],
                //     verticalPosition: 'top'
                // });
            }
        

        });

    }

    getAllRole(){
      this.roleService.getAllRoles().subscribe((data: {}) => {
        this.spinnerService.hide();
          if(data['status']){
              this.Roles = data['result'];
              if(this.route.snapshot.queryParams['ForCompany']){
                setTimeout(()=>{
                    this.Role = '5cb5b2b17d799119b01c6225';
                   this.getRole('5cb5b2b17d799119b01c6225');
                   this.getRoleData = true;
                },200)
            }
          }
          else{

          }
      }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
    }

    getAllServices(){
        this.spinnerService.show();
      this.servicesService.getAllServices().subscribe((data: {}) => {
        this.spinnerService.hide();
          if(data['status']){
              this.Services = data['result'];
              this.Commissions = data['result'];
          }
          else{

          }
      }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
    }


    getAllCitiesByState(id){
        this.Cities = [];
        if(this.getValue) {
            this.BasicInfoForm.patchValue({
               
                "City": '',
            });
        }
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Cities = data['result'];
            }
            else{

            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getAllCitiesByTemporaryState(id){
        this.TemporaryCities = [];
        if(this.getValue) {
            this.BasicInfoForm.patchValue({
               
                "TemporaryCity": '',
            });
        }
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.TemporaryCities = data['result'];
            }
            else{

            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }


    getRole(id){
        var index = this.Roles.findIndex(x=> x._id == id);
        this.SelectRole = this.Roles[index].Name;
        this.AccountInfoForm.patchValue({
            TimePeriod: '0',
            'Role': id
        })
        if(this.SelectRole == 'Vendor'){
            this.CompanyForm.patchValue({
                'Role': id
            })
            this.BranchForm.patchValue({
                'Role': id
            })
        }
        
        if(this.ID == '-1'){
            if(this.SelectRole == 'Vendor'){
                this.BasicInfoForm = this.fb.group({
                    FirstName: [ '', [Validators.required ] ],
                    MiddleName: [ '' ],
                    LastName: [ '' ],
                    Email: [ '', [Validators.required, Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                    Phone: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                    SecondaryMobile: [ '', [Validators.minLength(10), Validators.maxLength(10) ] ],
                    AltPhone: [ '' ],
                    EmgPhone: [ '' ],
                    Gender: [ 'M', [Validators.required ] ],
                    IsAvailable: [ 'Y', [Validators.required ] ],
                    Avatar: [ '' ,[Validators.required] ],
                    Address: [ '', [Validators.required ] ],
                    Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
                    
                    City: [ '', [Validators.required ] ],
                    State: [ '', [Validators.required ] ],
                    DOB: [ '' , [Validators.required  ] ],
                    Married: [ 'S'],
                    AltName: [ '' ],
                    AltContactRelation: [ '' ],
                });
                this.spinnerService.show();
                this.userService.getAllDocumentsByUser(this.SelectRole).subscribe( (data :{}) =>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.allDocuments = data['result'];
                    }
                })
                
            }
            else if(this.SelectRole == 'Service Provider'){
                this.BasicInfoForm = this.fb.group({
                    FirstName: [ '', [Validators.required ] ],
                    MiddleName: [ '' ],
                    LastName: [ ''],
                    Email: [ '', [ Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                    Phone: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                    SecondaryMobile: [ '', [Validators.minLength(10), Validators.maxLength(10) ] ],
                    AltPhone: [ '' ],
                    EmgPhone: [ '' ],
                    Gender: [ 'M', [Validators.required ] ],
                    IsAvailable: [ 'Y', [Validators.required ] ],
                    Avatar: [ '' , Validators.required],
                    SameAs: [ false ],
                    PermanentAddressType : ['R'],
                    Address: [ '', [Validators.required ] ],
                    Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6), Validators.max(999999) ] ],
                   
                    City: [ '', [Validators.required ] ],
                    State: [ '', [Validators.required ] ],
                    TemporaryAddressType : ['O'],
                    TemporaryAddress: [ null, [Validators.required ] ],
                    TemporaryPin: [ null, [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
                   
                    TemporaryCity: [ '', [Validators.required ] ],
                    TemporaryState: [ '', [Validators.required ] ],
                    DOB: [ '' , [ Validators.required  ] ],
                    Married: [ 'S' ],
                    AltName: [ '' ],
                    AltContactRelation : ['']
                });
                this.spinnerService.show();
                this.userService.getAllDocumentsByUser(this.SelectRole).subscribe( (data :{}) =>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.allDocuments = data['result'];
                    }
                })
                
            }
            else if(this.SelectRole == 'Quality Inspector' || this.SelectRole == 'Payment Collector'){
                this.BasicInfoForm = this.fb.group({
                    FirstName: [ '', [Validators.required ] ],
                    MiddleName: [ '' ],
                    LastName: [ ''],
                    Email: [ '', [ Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                    Phone: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                    SecondaryMobile: [ '', [Validators.minLength(10), Validators.maxLength(10) ] ],                    
                    AltPhone: [ '' ],
                    EmgPhone: [ '' ],
                    Gender: [ 'M', [Validators.required ] ],
                    IsAvailable: [ 'Y', [Validators.required ] ],
                    Avatar: [ '' , Validators.required],
                    SameAs: [ false ],
                    PermanentAddressType : ['R'],
                    Address: [ '', [Validators.required ] ],
                    Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6), Validators.max(999999) ] ],
                   
                    City: [ '', [Validators.required ] ],
                    State: [ '', [Validators.required ] ],
                    DOB: [ '' , [ Validators.required  ] ],
                    Married: [ 'S' ],
                    AltName: [ '' ],
                    AltContactRelation: [ '' ],
                });
                this.spinnerService.show();
                this.userService.getAllDocumentsByUser(this.SelectRole).subscribe( (data :{}) =>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.allDocuments = data['result'];
                    }
                }, err => {
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
                
            }
            else{
                this.BasicInfoForm = this.fb.group({
                    FirstName: [ '', [Validators.required ] ],
                    MiddleName: [ '' ],
                    LastName: [ '' ],
                    Email: [ '', [Validators.required, Validators.email, Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                    Phone: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                    SecondaryMobile: [ '', [Validators.minLength(10), Validators.maxLength(10) ] ],                    
                    AltPhone: [ '' ],
                    EmgPhone: [ '' ],
                    Gender: [ 'M', [Validators.required ] ],
                    IsAvailable: [ 'Y', [Validators.required ] ],
                    DOB: [ '', [Validators.required  ] ],
                    Avatar: [ '' , Validators.required ],
                    Address: [ '', [Validators.required ] ],
                    Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
                    
                    City: [ '', [Validators.required ] ],
                    State: [ '', [Validators.required ] ],
                });
                this.spinnerService.show();
                this.userService.getAllDocumentsByUser('All').subscribe( (data :{}) =>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.allDocuments = data['result'];
                    }
                })
            }
        }
        
    }

    checkEmail(){
        if(this.BasicInfoForm.controls.Email.value == ''){
            return 0;
        }
        this.userService.checkEmail(this.BasicInfoForm.controls.Email.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                if(data['result'].length > 0){
                    this.snackBar.open('Email already in-use, Please use a different email.', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.BasicInfoForm.patchValue({
                        'Email': ''
                    });
                }
            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    uploadImage(event, name) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png') {
                reader.onload = () => {
                    // this.allKeyPersonImages[name] = reader.result;
                    if(name == 'Document'){
                        this.DocumentInfoForm.patchValue({
                            "Image": reader.result
                        });
                    }
                    else if(name == 'Avatar'){
                        this.BasicInfoForm.patchValue({
                            "Avatar": reader.result
                        });
                    }
                    else{
                        
                    }
                };
            } else {
                this.snackBar.open('This file format is not supported.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                this.myInputVariable.nativeElement.value = "";
            }
        }
    }

    save(){
        if(!( this.VendorType == 'C' || this.VendorType == 'B')){
            this.AccountInfoForm.patchValue({
                "Account_Blocked": this.AccountInfoForm.controls.Account_Blocked.value == 'Y' ? true : false
            });
        }
        
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    getOne(id) {
        this.spinnerService.show();
        this.userService.getOneUsers(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            
            if ( data['status'] && !(data['result'].VendorType == 'C' || data['result'].VendorType == 'B')) {

                this.getAllCitiesByState(data['result'].State);
                
               
                if(data['result'].Role.Name == 'Service Provider'){
                    this.getAllCitiesByTemporaryState(data['result'].TemporaryState);
                    this.SPType = data['result'].type;
                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [ data['result'].Email , [ Validators.email  , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                        SecondaryMobile: [ data['result'].SecondaryMobile, [Validators.minLength(10), Validators.maxLength(10) ] ],                        
                        AltPhone: [ data['result'].AltPhone ],
                        EmgPhone: [ data['result'].Emg ],
                        Gender: [ data['result'].Gender, [Validators.required ] ],
                        IsAvailable: [ data['result'].IsAvailable, [Validators.required ] ],
                        Avatar: [ '' ],
                        SameAs: [ false ],
                        PermanentAddressType : [ data['result'].PermanentAddressType ? data['result'].PermanentAddressType :'O'],
                        Address: [ data['result'].Address, [Validators.required ] ],
                        Pin: [ data['result'].Pin, [Validators.required ,Validators.minLength(6),Validators.maxLength(6) ]],
                        
                        City: [ data['result'].City, [Validators.required ] ],
                        State: [ data['result'].State, [Validators.required ] ],
                        TemporaryAddressType : [data['result'].TemporaryAddressType ? data['result'].TemporaryAddressType :'O'],
                        TemporaryAddress: [ data['result'].TemporaryAddress ? data['result'].TemporaryAddress :'' ],
                        TemporaryPin: [ data['result'].TemporaryPin ? data['result'].TemporaryPin :'', [Validators.minLength(6), Validators.maxLength(6) ] ],
                        
                        TemporaryCity: [ data['result'].TemporaryCity ? data['result'].TemporaryCity :null ],
                        TemporaryState: [ data['result'].TemporaryState ? data['result'].TemporaryState :null ],
                        DOB: [ data['result'].DOB ? data['result'].DOB : '', [Validators.required  ] ],
                        Married: [ data['result'].Married ? data['result'].Married : '' ],
                        AltName: [ data['result'].AltName ? data['result'].AltName : '' ],
                        AltContactRelation: [ data['result'].AltContactRelation ? data['result'].AltContactRelation : ''  ],
                    });
                }
                else if( data['result'].Role.Name == 'Quality Inspector' || data['result'].Role.Name == 'Payment Collector'){
                    this.getAllCitiesByTemporaryState(data['result'].TemporaryState);
                    this.SPType = data['result'].type;
                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [ data['result'].Email , [ Validators.email  , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                        SecondaryMobile: [ data['result'].SecondaryMobile, [Validators.minLength(10), Validators.maxLength(10) ] ],                  
                        AltPhone: [ data['result'].AltPhone ],
                        EmgPhone: [ data['result'].EmgPhone ],
                        Gender: [ data['result'].Gender, [Validators.required ] ],
                        IsAvailable: [ data['result'].IsAvailable, [Validators.required ] ],
                        Avatar: [ '' ],
                        SameAs: [ false ],
                        PermanentAddressType : [ data['result'].PermanentAddressType ? data['result'].PermanentAddressType :'O'],
                        Address: [ data['result'].Address, [Validators.required ] ],
                        Pin: [ data['result'].Pin, [Validators.required ,Validators.minLength(6),Validators.maxLength(6) ]],
                        
                        City: [ data['result'].City, [Validators.required ] ],
                        State: [ data['result'].State, [Validators.required ] ],
                       
                        DOB: [ data['result'].DOB ? data['result'].DOB : '', [Validators.required  ] ],
                        Married: [ data['result'].Married ? data['result'].Married : '' ],
                        AltName: [ data['result'].AltName ? data['result'].AltName : '' ],
                        AltContactRelation: [ data['result'].AltContactRelation ? data['result'].AltContactRelation : ''  ],
                    });
                }
                else{
                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [ data['result'].Email , [Validators.required, Validators.email  , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                        SecondaryMobile: [ data['result'].SecondaryMobile, [Validators.minLength(10), Validators.maxLength(10) ] ],
                        AltPhone: [ data['result'].AltPhone ],
                        EmgPhone: [ data['result'].EmgPhone ],
                        Gender: [ data['result'].Gender, [Validators.required ] ],
                        IsAvailable: [ data['result'].IsAvailable ],
                        Avatar: [ '' ],
                        SameAs: [ false ],
                        PermanentAddressType : [ data['result'].PermanentAddressType ? data['result'].PermanentAddressType :'O'],
                        Address: [ data['result'].Address, [Validators.required ] ],
                        Pin: [ data['result'].Pin, [Validators.required ,Validators.minLength(6),Validators.maxLength(6) ]],
                        
                        City: [ data['result'].City, [Validators.required ] ],
                        State: [ data['result'].State, [Validators.required ] ],
                        TemporaryAddressType : [data['result'].TemporaryAddressType ? data['result'].TemporaryAddressType :'O'],
                        TemporaryAddress: [ data['result'].TemporaryAddress ? data['result'].TemporaryAddress :'' ],
                        TemporaryPin: [ data['result'].TemporaryPin ? data['result'].TemporaryPin :'', [Validators.minLength(6), Validators.maxLength(6) ] ],
                        
                        TemporaryCity: [ data['result'].TemporaryCity ? data['result'].TemporaryCity :null ],
                        TemporaryState: [ data['result'].TemporaryState ? data['result'].TemporaryState :null ],
                        DOB: [ data['result'].DOB ? data['result'].DOB : '' , [Validators.required  ] ],
                        Married: [ data['result'].Married ? data['result'].Married : '' ],
                        AltName: [ data['result'].AltName ? data['result'].AltName : '' ],
                        AltContactRelation: [ data['result'].AltContactRelation ? data['result'].AltContactRelation : ''  ],                        
                    });
                }
                
/*, [Validators.minLength(8),
                        Validators.required,
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]*/
                this.AccountInfoForm = this.fb.group({
                    Password: [ '', [Validators.minLength(8),
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                    ConfirmPassword: [ '', [Validators.minLength(8),
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]],
                    Role: [ data['result'].Role._id, [Validators.required] ],
                    TimePeriod: [ '0' ],
                    EndDate: [ data['result'].EndDate ? data['result'].EndDate : ''],
                    IsAvailable: [ data['result'].IsAvailable ],
                    Account_Blocked: [ data['result'].Account_Blocked ? 'Y' : 'N'  ],
                });
            
                this.selectedExpirtDate = new Date(data['result'].EndDate);
                this.selectedStartDate = new Date(data['result'].StartDate);
                this.SelectRole = data['result'].Role.Name;
                this.Role = data['result'].Role._id;
                if(this.SelectRole == 'Vendor' || this.SelectRole == 'Service Provider'){
                    this.getRoleData  = true;
                }
                else{
                    this.getRoleData  = false;
                }
                



                if(this.SelectRole == 'Vendor'){
                    this.VenderAddress = data['result'].VenderAddress;

                    this.CreditLimit = this.fb.group({
                        CreditLimit: [ data['result'].CreditLimit, [Validators.required ] ],
                    });
                }
                if(this.SelectRole == 'Service Provider'){
                    if(data['result'].SecondaryDetails){
                        this.SecondaryDetailForm = this.fb.group({
                            EducationQualification : [data['result'].SecondaryDetails.EducationQualification],
                            Language : [data['result'].SecondaryDetails.Language],
                            Injuries : [data['result'].SecondaryDetails.Injuries],
                            BloodGroup : [data['result'].SecondaryDetails.BloodGroup],
                            Vehicle : [data['result'].SecondaryDetails.Vehicle],
                        })

                    }
                    else{
                        this.SecondaryDetailForm = this.fb.group({
                            EducationQualification : [''],
                            Language : [''],
                            Injuries : [''],
                            BloodGroup : [''],
                            Vehicle : [''],
                        })
                    }
                    if(data['result'].FamilyDetails){
                        this.FamilyInfoForm = this.fb.group({
                            FatherName : [data['result'].FamilyDetails.FatherName],
                            MotherName : [data['result'].FamilyDetails.MotherName],
                            SpouseName : [data['result'].FamilyDetails.SpouseName],
                        })
                    }
                    else{
                        this.FamilyInfoForm = this.fb.group({
                            FatherName : [''],
                            MotherName : [''],
                            SpouseName : [''],
                        })
                    }

                    var SPServices = data['result'].Services;
                    this.servicesService.getAllServices().subscribe((data: {}) => {
                        this.spinnerService.hide();
                          if(data['status']){
                              this.Services = data['result'];
                              for(var i = 0; i < this.Services.length; i++){
                                this.Services[i].skill = false;
                                this.Services[i].avgCapacity = 0;
                                if(SPServices){
                                    var index = SPServices.findIndex(x=> x.id == this.Services[i]._id)  
                                    if(index != -1){
                                        this.Services[i].skill = true;
                                        this.Services[i].avgCapacity = (SPServices[index].avgCapacity ? SPServices[index].avgCapacity : 0) ;
                                    }  
                                }
                             }
                          }
                      })
                }

                if(data['result'].FinancialDetail){
                    this.FinacInfo = this.fb.group({
                        TypeOfAccount: [ data['result'].FinancialDetail.TypeOfAccount, [Validators.required ] ],
                        Name: [ data['result'].FinancialDetail.Name, [Validators.required ] ],
                        BankName: [ data['result'].FinancialDetail.BankName, [Validators.required ] ],
                        AccountNumber: [ data['result'].FinancialDetail.AccountNumber, [Validators.required ] ],
                        IFSCCode: [ data['result'].FinancialDetail.IFSCCode, [Validators.required ] ],
                        OnlineAccountType: [ data['result'].FinancialDetail.OnlineAccountType ?  data['result'].FinancialDetail.OnlineAccountType : '' ],
                        OnlineAccountID: [ data['result'].FinancialDetail.OnlineAccountID ? data['result'].FinancialDetail.OnlineAccountID : '' ],
                        CreditLimits: [ data['result'].FinancialDetail.CreditLimits, [Validators.required ] ],
                        CreditDays: [ data['result'].FinancialDetail.CreditDays, [Validators.required ] ],
                    });

                    this.CreditLimit = this.fb.group({
                        CreditLimit: [ data['result'].CreditLimit, [Validators.required ] ],
                    });
                }
                else{
                    this.FinacInfo = this.fb.group({
                        TypeOfAccount : ['', [Validators.required ]],
                        Name : ['', [Validators.required ]],
                        AccountNumber : ['', [Validators.required ]],
                        BankName : ['', [Validators.required ]],
                        CreditLimits : ['', [Validators.required ]],
                        IFSCCode : ['', [Validators.required ]],
                        OnlineAccountType : [''],
                        OnlineAccountID : [''],
                        CreditDays : ['', [Validators.required ]],
                    })
                }
                const educationTempArray = data['result'].AdditionalDocuments;
                const localArray = data['result'].MandatoryDocuments ? data['result'].MandatoryDocuments : [];
                this.getAllDocumentsOnExisting(localArray);

                for (const row of educationTempArray) {
                    // this.userService.getUserDocument(row.File).subscribe((data: {}) => {
                    //     this.createImageFromBlobForEdu(data).then(val => {
                    //         row.File = val;
                            this.educationDocumentArray.push(row);
                    //     });
                    // });
                }
                if (data['result'].Avatar) {
                    this.userService.getUserDocument(data['result'].Avatar).subscribe((data: {}) => {
                        this.createImageFromBlobForEdu(data).then(val => {
                            this.BasicInfoForm.patchValue({
                                Avatar: val
                            }); 
                        });
                    });
                }
                this.getValue = true;
            }
            else{
                if(data['result'].VendorType == 'C'){

                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [ data['result'].Email  ],
                        Phone: [ data['result'].Phone ],
                        SecondaryMobile: [ data['result'].SecondaryMobile ],
                        AltPhone: [ '' ],
                        EmgPhone: [ '' ],
                        Gender: [ '' ],
                        Address: [ '' ],
                        Pin: [''],
                        City: [ '' ],
                        State: [ '' ], 
                    });    


                    this.SelectRole = data['result'].Role.Name;
                    this.Role = data['result'].Role._id;
                    this.getCityByState(data['result'].CompanyAddress.State)
                   
                    this.VendorType = 'C';

                    this.CompanyManagerEmail = data['result'].Email;
                    this.CompanyForm = this.fb.group({
                        Role: [ data['result'].Role._id],
                        CompanyType: [ data['result'].CompanyDetail.CompanyType, [Validators.required ] ],
                        CompanyName: [ data['result'].CompanyDetail.CompanyName, [Validators.required ] ],
                        CompanyEmail: [ data['result'].CompanyDetail.CompanyEmail, [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                        CompanyPhone: [ data['result'].CompanyDetail.CompanyPhone, [Validators.required, Validators.maxLength(11) ] ],
                        ContactPersonName: [ data['result'].CompanyDetail.ContactPersonName, [Validators.required ] ],
                        ContactPersonEmail: [ data['result'].CompanyDetail.ContactPersonEmail, [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                        ContactPersonPhone: [ data['result'].CompanyDetail.ContactPersonPhone, [Validators.required , Validators.minLength(10), Validators.maxLength(10) ] ],
                        Password: ['', [Validators.minLength(8)
                            ,
                            Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                        ConfirmPassword: ['',  [Validators.minLength(8),
                            
                            Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                        CreditLimit : [data['result'].CompanyDetail.CreditLimit, [Validators.required ]],
                        CreditDays : [data['result'].CompanyDetail.CreditLimit, [Validators.required ]],            
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName],
                        LastName: [ data['result'].LastName],
                        Email: [data['result'].Email, [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10) , Validators.maxLength(10) ] ],
                    })
                    this.FinacVendorInfo = this.fb.group({
                        TypeOfAccount: [ data['result'].FinancialDetail.TypeOfAccount, [Validators.required ] ],
                        Name: [ data['result'].FinancialDetail.Name, [Validators.required ] ],
                        BankName: [ data['result'].FinancialDetail.BankName, [Validators.required ] ],
                        AccountNumber: [ data['result'].FinancialDetail.AccountNumber, [Validators.required ] ],
                        IFSCCode: [ data['result'].FinancialDetail.IFSCCode, [Validators.required ] ],
                    });

                    const localArray = data['result'].MandatoryDocuments ? data['result'].MandatoryDocuments : [];
                    this.getAllDocumentsOnExisting(localArray);

                    this.VendorAddressForm = this.fb.group({
                        State : [data['result'].CompanyAddress.State , [Validators.required]],
                        City : [data['result'].CompanyAddress.City , [Validators.required]],
                        
                        Pin : [data['result'].CompanyAddress.Pin , [Validators.required , Validators.minLength(6) , Validators.maxLength(6)]],
                        Address : [data['result'].CompanyAddress.Address , [Validators.required]],
                        Name : [data['result'].CompanyAddress.Name , [Validators.required]],
                        Email : [data['result'].CompanyAddress.Email , [Validators.required,, Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')]],
                        Number : [data['result'].CompanyAddress.Number , [Validators.required, Validators.minLength(10) , Validators.maxLength(10)]],
                    })
                    
                    var SPServices = data['result'].Services;
                    this.servicesService.getAllServices().subscribe((data: {}) => {
                        this.spinnerService.hide();
                          if(data['status']){
                              this.Services = data['result'];
                              for(var i = 0; i < this.Services.length; i++){
                                this.Services[i].skill = false;
                                if(!this.Services[i].avgCapacity){
                                    this.Services[i].avgCapacity = 0;
                                  }
                                if(SPServices){
                                    var index = SPServices.findIndex(x=> x.id == this.Services[i]._id)  
                                    if(index != -1){
                                        this.Services[i].skill = true;
                                        this.Services[i].avgCapacity = SPServices[index].avgCapacity ? SPServices[index].avgCapacity : 0;
                                    }  
                                }
                             }
                          }
                      }, err => {
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    });

                     this.UpdateCommissions = data['Commissions']
                      this.getValue = true;
                }
                else{

                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [ data['result'].Email  ],
                        Phone: [ data['result'].Phone ],
                        SecondaryMobile: [ data['result'].SecondaryMobile ],
                        AltPhone: [ '' ],
                        EmgPhone: [ '' ],
                        Gender: [ '' ],
                        Address: [ '' ],
                        Pin: [''],
                        City: [ '' ],
                        State: [ '' ], 
                    });
                    
                    this.userService.getAllCompanyVendorUser().subscribe((data:{})=>{
                        this.CompanyVendors = data['result'];
                      }, err => {
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    });
                    this.CompanyManagerEmail = data['result'].Email;
                    this.SelectRole = data['result'].Role.Name;
                    this.Role = data['result'].Role._id;
                    this.getCityByState(data['result'].CompanyAddress.State)
                    this.VendorType = 'B';
                    this.DisplayCompanyName = data['result'].VendorCompanyID.CompanyDetail.CompanyName;
                    this.BranchForm = this.fb.group({
                        Role: [ data['result'].Role._id],
                        VendorCompanyID : [ data['result'].VendorCompanyID._id],
                        Password: ['', [Validators.minLength(8),
                            Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                        ConfirmPassword: ['',  [Validators.minLength(8),
                            Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName],
                        LastName: [ data['result'].LastName],
                        Email: [data['result'].Email, [Validators.required , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')  ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10) , Validators.maxLength(10) ] ],
                    })
                    this.FinacVendorInfo = this.fb.group({
                        TypeOfAccount: [ data['result'].FinancialDetail.TypeOfAccount, [Validators.required ] ],
                        Name: [ data['result'].FinancialDetail.Name, [Validators.required ] ],
                        BankName: [ data['result'].FinancialDetail.BankName, [Validators.required ] ],
                        AccountNumber: [ data['result'].FinancialDetail.AccountNumber, [Validators.required ] ],
                        IFSCCode: [ data['result'].FinancialDetail.IFSCCode, [Validators.required ] ],
                    });

                    const localArray = data['result'].MandatoryDocuments ? data['result'].MandatoryDocuments : [];
                    this.getAllDocumentsOnExisting(localArray);

                    this.VendorAddressForm = this.fb.group({
                        State : [data['result'].CompanyAddress.State , [Validators.required]],
                        City : [data['result'].CompanyAddress.City , [Validators.required]],
                        
                        Pin : [data['result'].CompanyAddress.Pin , [Validators.required , Validators.minLength(6) , Validators.maxLength(6)]],
                        Address : [data['result'].CompanyAddress.Address , [Validators.required]],
                        Name : [data['result'].CompanyAddress.Name , [Validators.required]],
                        Email : [data['result'].CompanyAddress.Email , [Validators.required,, Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')]],
                        Number : [data['result'].CompanyAddress.Number , [Validators.required, Validators.minLength(10) , Validators.maxLength(10)]],
                    })
                      this.UpdateCommissions = data['Commissions']
                      this.getValue = true;
                }
            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    onChangeImageSource(value){
      if(value == 'C')
        this.imageCamera = true;
      else 
        this.imageCamera = false;

    }

    onChangeTimePeriod() {
    	if(this.ID != '-1'){
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure update Time period?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    const timePeriod = this.AccountInfoForm.get('TimePeriod').value;
                
                    const dt = this.ID != '-1' ?  new Date(this.AccountInfoForm.get('EndDate').value) : new Date();
                    if(timePeriod == '6') {
                        dt.setMonth(dt.getMonth() + 6);
                        dt.setDate(dt.getDate() - 1);
                    } else if(timePeriod == '12') {
                        dt.setMonth(dt.getMonth() + 12);
                        dt.setDate(dt.getDate() - 1);
                    }
                    this.selectedExpirtDate = dt;
                }
                else{
                    const timePeriod = this.AccountInfoForm.get('TimePeriod').value;
                
                    const dt = this.ID != '-1' ?  new Date(this.AccountInfoForm.get('EndDate').value) : new Date();
                    if(timePeriod == '6') {
                        dt.setMonth(dt.getMonth() + 6);
                    } else if(timePeriod == '12') {
                        dt.setMonth(dt.getMonth() + 12);
                    }
                    this.AccountInfoForm.patchValue({
                        'TimePeriod': 0
                    })
                }
            })
	}
	else{
			const timePeriod = this.AccountInfoForm.get('TimePeriod').value;
                
                    const dt = this.ID != '-1' ?  new Date(this.AccountInfoForm.get('EndDate').value) : new Date();
                    if(timePeriod == '6') {
                        dt.setMonth(dt.getMonth() + 6);
                        dt.setDate(dt.getDate() - 1);
                    } else if(timePeriod == '12') {
                        dt.setMonth(dt.getMonth() + 12);
                        dt.setDate(dt.getDate() - 1);
                    }
                    this.selectedExpirtDate = dt;
	}
}
    

    createImageFromBlobForEdu(image: any) {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                resolve(reader.result);
            }, false);
            if (image) {
                reader.readAsDataURL(image);
            }
        });
    }


    getAllDocumentsOnExisting(localArray) {
        this.spinnerService.show();
        this.userService.getAllDocumentsByUser(this.SelectRole).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.allDocuments = data['result'];
                for (const item of localArray) {
                    const index = this.allDocuments.findIndex(x => x.Name === item.Name);
                    this.allDocuments[index].value = item.Number ? item.Number : '';
                    this.allImages[item.Name] = item.Image;
                    // this.userService.getUserDocument(item.Image).subscribe((data: {}) => {
                    //     this.createImageFromBlobForEdu(data).then(val => {
                    //         this.allImages[item.Name] = val;
                            this.addDocumentDetailByPreviousSelected(item.Number, item.Name);
                     //   });
                   // });
                }
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }


    addDocumentDetailByPreviousSelected(val, name) {
        // this.allImages
        let localArray = {};
        localArray['Name'] = name;
        localArray['Image'] = this.allImages[name];
        localArray['Number'] = val;
        let index = this.documentArray.findIndex(x => x.Name === name)
        if (index === -1) {
            if (name === 'Aadhar') {
                this.docMandatory++;
            }
            this.documentArray.push(localArray);
            this.documentUploadedArray[name] = name;
        } else {
            this.documentArray[index].Image = this.allImages[name];
            this.documentArray[index].Number = val;
        }
    }


    createNew(){        
        if(this.SelectRole == 'Service Provider'){
            var Skills = [];
            for(var i = 0; i < this.Services.length; i++){
                if(this.Services[i].skill == true){
                    Skills.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
                }
            }
            this.spinnerService.show();
            var basicdetail = this.BasicInfoForm.value;
            if(this.route.snapshot.queryParams['ForCompany']){
                console.log(this.route.snapshot.queryParams['ForCompany'])
                basicdetail.CompanyID = this.route.snapshot.queryParams['ForCompany']
            }
            if(this.ServiceProviderType == 'C'){
                console.log(this.route.snapshot.queryParams['ForCompany'])
                basicdetail.CompanyID = this.CompanyID
            }
            if(this.ServiceProviderType == 'T'){
                console.log(this.route.snapshot.queryParams['ForCompany'])
                basicdetail.TeamID = this.TeamID
            }
            this.userService.addSPUsers(basicdetail, this.AccountInfoForm.value, this.documentArray, this.educationDocumentArray , Skills , this.FinacInfo.value , this.SecondaryDetailForm.value , this.FamilyInfoForm.value, this.userdata.id).subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.snackBar.open('Record created successfully', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    
                    this.location.back();
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
        else if(this.SelectRole == 'Vendor'){
            if(this.VendorType == 'I'){
                this.spinnerService.show();
                this.userService.addVendorUsers(this.BasicInfoForm.value, this.AccountInfoForm.value, this.documentArray, this.educationDocumentArray , this.ShopInfoForm.value , this.FinacInfo.value , this.VenderAddress).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record created successfully', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            else if(this.VendorType == 'C'){
                var Skills = [];
                for(var i = 0; i < this.Services.length; i++){
                    if(this.Services[i].skill == true){
                        Skills.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
                    }
                }
                this.spinnerService.show();
                this.userService.addCompanyVendorUsers(this.CompanyForm.value, this.documentArray,this.VendorAddressForm.value, this.FinacVendorInfo.value , Skills , this.Commissions).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record created successfully', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            else{
                this.spinnerService.show();
                this.userService.addCompanyBranchVendorUsers(this.BranchForm.value, this.documentArray,this.VendorAddressForm.value, this.FinacVendorInfo.value , this.Commissions).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record created successfully.', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
        }
        else {
            this.spinnerService.show();
            this.userService.addUsers(this.BasicInfoForm.value, this.AccountInfoForm.value, this.documentArray, this.educationDocumentArray , this.FinacInfo.value).subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.snackBar.open('Record created successfully.', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    
                    this.location.back();
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
    }

    editExisting(){        
        if(this.SelectRole == 'Service Provider'){
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure update this record?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    var Skills = [];
                        for(var i = 0; i < this.Services.length; i++){
                            if(this.Services[i].skill == true){
                                Skills.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
                            }
                        }
                    this.spinnerService.show();
                    this.userService.updateSPUsers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value , this.documentArray , this.educationDocumentArray , Skills , this.FinacInfo.value , this.SecondaryDetailForm.value , this.FamilyInfoForm.value, this.userdata.id).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if ( data['status']) {
                            this.snackBar.open('Record updated successfully.', '', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            
                            this.location.back();
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    });
                }
                if (ButtonPressed === "No") {
                    this.spinnerService.hide();
                }

            });
        }
        else if(this.SelectRole == 'Vendor'){
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure update this record?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    if(this.VendorType == 'I'){
                        
                        this.spinnerService.show();
                        this.userService.updateVendorUsers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value , this.documentArray , this.educationDocumentArray , this.FinacInfo.value , this.VenderAddress).subscribe((data: {}) => {
                            this.spinnerService.hide();
                            if ( data['status']) {
                                this.snackBar.open('Record updated successfully.', '', {
                                    duration: 5000,
                                    panelClass: ['success-snackbar'],
                                    verticalPosition: 'top'
                                });
                                
                                this.location.back();
                            }
                        }, err => {
                            this.spinnerService.hide();
                            this.snackBar.open('Server error', '', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                        });
                    }
                    else if(this.VendorType == 'C'){
                        this.spinnerService.show();
                        var Skills = [];
                        for(var i = 0; i < this.Services.length; i++){
                            if(this.Services[i].skill == true){
                                Skills.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
                            }
                        }
                        this.userService.updateCompanyVendorUsers(this.ID, this.CompanyForm.value, this.documentArray,this.VendorAddressForm.value, this.FinacVendorInfo.value , Skills , this.UpdateCommissions).subscribe((data: {}) => {
                            this.spinnerService.hide();
                            if ( data['status']) {
                                this.snackBar.open('Record updated successfully.', '', {
                                    duration: 5000,
                                    panelClass: ['success-snackbar'],
                                    verticalPosition: 'top'
                                });
                                
                                this.location.back();
                            }
                        }, err => {
                            this.spinnerService.hide();
                            this.snackBar.open('Server error', '', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                        });
                    }
                    else{
                        this.userService.updateCompanyBranchVendorUsers(this.ID, this.BranchForm.value, this.documentArray,this.VendorAddressForm.value, this.FinacVendorInfo.value ,  this.UpdateCommissions).subscribe((data: {}) => {
                            this.spinnerService.hide();
                            if ( data['status']) {
                                this.snackBar.open('Record updated successfully.', '', {
                                    duration: 5000,
                                    panelClass: ['success-snackbar'],
                                    verticalPosition: 'top'
                                });
                                
                                this.location.back();
                            }
                        }, err => {
                            this.spinnerService.hide();
                            this.snackBar.open('Server error', '', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                        });
                    }
                    
                }
                if (ButtonPressed === "No") {
                    this.spinnerService.hide();
                }

            });

        }
        else {
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure to update this record?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.spinnerService.show();
                    this.userService.updateUsers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value , this.documentArray , this.educationDocumentArray , this.FinacInfo.value ).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if ( data['status']) {
                            this.snackBar.open('Record updated successfully.', '', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            
                            this.location.back();
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    });
                }
                if (ButtonPressed === "No") {
                    this.spinnerService.hide();
                }

            });
        }
    }


    getName(id  , name){
        if(!id) {
            return 0;
        }
        if(name == 'state' && id != ''){
            var sindex = this.States.findIndex(x=>x._id == id);
            if(sindex > -1){
                return this.States[sindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'city' && id != ''){
            var cindex = this.Cities.findIndex(x=>x._id == id);
            if(cindex > -1){
                return this.Cities[cindex].Name;
            }
            else{
                return '';
            }
        }
        
    }

    form(){
    }
    viewDetail(){
    }
    register(){
        // this.router.navigate(['/users']);
        this.location.back();
    }

    cancel() {
        // this.router.navigate(['/users']);
        this.location.back();
    }

    role(Role){
    }

    add(){

    }


    delete(){
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure to delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {


            }
            if (ButtonPressed === "No") {

            }

        });
    }
    public data = {
        "Afghanistan":16.63,"Albania":11.58,"Algeria":158.97,"Angola":85.81,"Antigua and Barbuda":1.1,"Argentina":351.02,"Armenia":8.83,"Australia":1219.72,"Austria":366.26,"Azerbaijan":52.17,"Bahamas":7.54,"Bahrain":21.73,"Bangladesh":105.4,"Barbados":3.96,"Belarus":52.89,"Belgium":461.33,"Belize":1.43,"Benin":6.49,"Bhutan":1.4,"Bolivia":19.18,"Bosnia and Herzegovina":16.2,"Botswana":12.5,"Brazil":2023.53,"Brunei":11.96,"Bulgaria":44.84,"Burkina Faso":8.67,"Burundi":1.47,"Cambodia":11.36,"Cameroon":21.88,"Canada":1563.66,"Cape Verde":1.57,"Central African Republic":2.11,"Chad":7.59,"Chile":199.18,"China":5745.13,"Colombia":283.11,"Comoros":0.56,"Costa Rica":35.02,"Croatia":59.92,"Cyprus":22.75,"Czech Republic":195.23,"Democratic Republic of the Congo":12.6,"Denmark":304.56,"Djibouti":1.14,"Dominica":0.38,"Dominican Republic":50.87,"East Timor":0.62,"Ecuador":61.49,"Egypt":216.83,"El Salvador":21.8,"Equatorial Guinea":14.55,"Eritrea":2.25,"Estonia":19.22,"Ethiopia":30.94,"Fiji":3.15,"Finland":231.98,"France":2555.44,"Gabon":12.56,"Gambia":1.04,"Georgia":11.23,"Germany":3305.9,"Ghana":18.06,"Greece":305.01,"Grenada":0.65,"Guatemala":40.77,"Guinea":4.34,"Guinea-Bissau":0.83,"Guyana":2.2,"Haiti":6.5,"Honduras":15.34,"Hong Kong":226.49,"Hungary":132.28,"Iceland":12.77,"India":1430.02,"Indonesia":695.06,"Iran":337.9,"Iraq":84.14,"Ireland":204.14,"Israel":201.25,"Italy":2036.69,"Ivory Coast":22.38,"Jamaica":13.74,"Japan":5390.9,"Jordan":27.13,"Kazakhstan":129.76,"Kenya":32.42,"Kiribati":0.15,"Kuwait":117.32,"Kyrgyzstan":4.44,"Laos":6.34,"Latvia":23.39,"Lebanon":39.15,"Lesotho":1.8,"Liberia":0.98,"Libya":77.91,"Lithuania":35.73,"Luxembourg":52.43,"Macedonia":9.58,"Madagascar":8.33,"Malawi":5.04,"Malaysia":218.95,"Maldives":1.43,"Mali":9.08,"Malta":7.8,"Mauritania":3.49,"Mauritius":9.43,"Mexico":1004.04,"Moldova":5.36,"Mongolia":5.81,"Montenegro":3.88,"Morocco":91.7,"Mozambique":10.21,"Myanmar":35.65,"Namibia":11.45,"Nepal":15.11,"Netherlands":770.31,"New Zealand":138,"Nicaragua":6.38,"Niger":5.6,"Nigeria":206.66,"Norway":413.51,"Oman":53.78,"Pakistan":174.79,"Panama":27.2,"Papua New Guinea":8.81,"Paraguay":17.17,"Peru":153.55,"Philippines":189.06,"Poland":438.88,"Portugal":223.7,"Qatar":126.52,"Republic of the Congo":11.88,"Romania":158.39,"Russian Federation":3476.91,"Rwanda":5.69,"Saint Kitts and Nevis":0.56,"Saint Lucia":1,"Saint Vincent and the Grenadines":0.58,"Samoa":0.55,"Sao Tome and Principe":0.19,"Saudi Arabia":434.44,"Senegal":12.66,"Serbia":38.92,"Seychelles":0.92,"Sierra Leone":1.9,"Singapore":217.38,"Slovakia":86.26,"Slovenia":46.44,"Solomon Islands":0.67,"South Africa":354.41,"South Korea":986.26,"Spain":1374.78,"Sri Lanka":48.24,"Sudan":65.93,"Suriname":3.3,"Swaziland":3.17,"Sweden":444.59,"Switzerland":522.44,"Syria":59.63,"Taiwan":426.98,"Tajikistan":5.58,"Tanzania":22.43,"Thailand":312.61,"Togo":3.07,"Tonga":0.3,"Trinidad and Tobago":21.2,"Tunisia":43.86,"Turkey":729.05,"Turkmenistan":0,"Uganda":17.12,"Ukraine":136.56,"United Arab Emirates":239.65,"United Kingdom":2258.57,"United States":6624.18,"Uruguay":40.71,"Uzbekistan":37.72,"Vanuatu":0.72,"Venezuela":285.21,"Vietnam":101.99,"Yemen":30.02,"Zambia":15.69,"Zimbabwe":5.57, "Bolivia, Plurinational State of":121.34,"Somalia": 0.47,"Tanzania, United Republic of": 0.78,"South Sudan": 0.98,"Congo, the Democratic Republic of the": 1.45
    };

    downloadFile(data: any, name) {
        importedSaveAs(data, name);
    }
    getExtensionName(base64Data: any) {
        const extension = base64Data.split(';')[0].split('/')[1];
        if(extension === 'jpeg' || extension === 'jpg' || extension === 'png') {
            return 'image';
        } else if(extension === 'pdf') {
            return 'pdf';
        } else {
            return 'doc';
        }
    }

    checkPatternValidation(val, pattern) {
        if(!pattern){
            return 0;
        }
        if(!(val.match(pattern))) {
            this.snackBar.open('Invalid Pattern', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }

    deleteFile(name) {
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure to delete this file?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
               // this.userService.deleteFile(this.allImages[name]).subscribe((data : {})=>{ // functionality commented for a strong reason (Deleted file can not ne revoked) 
                    const index = this.documentArray.findIndex(x => x.Name === name);
                    if(index != -1) {
                        if(name == 'Aadhar'){
                            this.docMandatory--;
                        }
                        this.documentArray.splice(index, 1);
                    }
                    this.allImages[name] = null;
                    this.documentUploadedArray[name] = null;
                    const deindex = this.allDocuments.findIndex(x => x.Name === name);
                    
                     $('#'+deindex).val('');
               // })
            }

        });
    }

    nextSuccessStep(){
        console.clear();

        if(this.VendorType == 'C'){
            if((this.Services.filter(x=> x.skill ==  true)).length == 0){
                this.snackBar.open("Please select atleast one Service.", '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });  
                return 0;
            }        
        }
        if(this.ID != '-1'){
            if((this.UpdateCommissions.filter(x=> x.Commission > 20)).length > 0){
                this.snackBar.open("Commission can't be greater than 20%.", '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });  
                return 0;
            }
        }
        if(this.ID == '-1'){
            if((this.Commissions.filter(x=> x.Commission > 20)).length > 0){
                this.snackBar.open("Commission can't be greater than 20%.", '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });  
                return 0;
            }
        }


        
        this.stepper.next();
    }

    getVendorType(type){
        if(type == 'B'){
            this.userService.getAllCompanyVendorUser().subscribe((data:{})=>{
                this.CompanyVendors = data['result'];
              }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
        
    }

    goToAddressStep(){
        
        if(this.CompanyForm.invalid){
          return 0;
        }
        if(this.CompanyForm.value.Password != this.CompanyForm.value.ConfirmPassword){
            this.snackBar.open('Password and Confirm password should be same.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        this.stepper.next();
    }

    ServiceAndCommissionSteps(){
        if(this.VendorType =='C' && this.CompanyForm.value.CompanyType != 'Un-Registered'){
            if(this.documentArray.length != 3){
                if(this.documentArray.length >=2){
                    for(let docc of this.documentArray){
                        if(docc.Name == 'Registration Amount Receipt'){
                            if(docc.Number != ''){
                                this.snackBar.open('Please upload mandatory documents.', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                                return 0; 
                            }
                        }
                    }
                }
                else{
                    this.snackBar.open('Please upload mandatory documents.', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                    return 0; 
                }  
            }  
        }
        if(this.VendorType =='B'){
            if(this.documentArray.length != 1){
                this.snackBar.open('All Documents are mandantory.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }

        
        if(this.VendorAddressForm.invalid){
            this.snackBar.open('Please fill '+ (this.VendorType == 'C' ? 'Head' : 'Branch')+' address details.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        if(this.FinacVendorInfo.invalid){
            this.snackBar.open('Please fill '+ (this.VendorType == 'C' ? 'Head' : 'Branch')+' Office Financial details.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0; 
        }
        this.stepper.next();
    }

    checkSameAsCompanyPerson(event){
        
        if(event){
            if(this.VendorType == 'C'){
                this.VendorAddressForm.patchValue({
                    Name : this.CompanyForm.controls.ContactPersonName.value,
                    Email : this.CompanyForm.controls.ContactPersonEmail.value,
                    Number : this.CompanyForm.controls.ContactPersonPhone.value,
                })
            }
            else{
                var index = this.CompanyVendors.findIndex(x=> x._id == this.BranchForm.controls.VendorCompanyID.value);
                this.VendorAddressForm.patchValue({
                    Name : this.CompanyVendors[index].CompanyDetail.ContactPersonName,
                    Email : this.CompanyVendors[index].CompanyDetail.ContactPersonEmail,
                    Number : this.CompanyVendors[index].CompanyDetail.ContactPersonPhone,
                })
            }
        }
    }

    toConvertUppercase(event){
        this.FinacVendorInfo.patchValue({
            IFSCCode : event.toUpperCase()
        })
    }
    toConvertUppercaseFinancial(event){
        this.FinacInfo.patchValue({
            IFSCCode : event.toUpperCase()
        })
    }
    getCityByState(id){
        this.Cities = [];
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Cities = data['result'];
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    gotoAddressStepBranch(){
        if(this.BranchForm.invalid){
            return 0;
        }
        this.stepper.next();
    }

    checkAllService(event){
        console.log(event);
        if(event){
            this.Services.map(x=> x.skill=true );
        }
        else{
            this.Services.map(x=> x.skill=false );
        }
    }
    nextSPUSerSuccessStep(){
        if(this.Services.filter(x=> x.skill == true).length == 0){
            this.snackBar.open('Please choose atleast one service.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }

        if(this.Services.filter(x=> (x.skill == true && (!x.avgCapacity || x.avgCapacity == 0))).length > 0){
            this.snackBar.open('Average capacity should be greater than 0 for select Services.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        this.stepper.next();
    }

    validateEmail(type){
        if(type == 'Branch'){
            if(this.BranchForm.controls.Email.value == ''){
                return 0;
            }
            if(this.CompanyManagerEmail == this.BranchForm.controls.Email.value){
                return 0;
            }
            this.userService.checkEmail(this.BranchForm.controls.Email.value).subscribe((data: {}) => {
                this.spinnerService.hide();
                if(data['status']){
                    if(data['result'].length > 0){
                        this.snackBar.open('Email already in-use, Please use a different email ', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.BranchForm.patchValue({
                            'Email': ''
                        });
                    }
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
        else if(type == 'Company'){
            if(this.CompanyForm.controls.Email.value == ''){
                return 0;
            }
            if(this.CompanyManagerEmail == this.CompanyForm.controls.Email.value){
                return 0;
            }
            this.userService.checkEmail(this.CompanyForm.controls.Email.value).subscribe((data: {}) => {
                this.spinnerService.hide();
                if(data['status']){
                    if(data['result'].length > 0){
                        this.snackBar.open('Email already in-use, Please use a different email ', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.CompanyForm.patchValue({
                            'Email': ''
                        });
                    }
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
    }

    notGreater25(com){
        console.log(com)
        if(com > 25){
            this.snackBar.open('Commission should not be greater than 20%.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }

    goToVendorSuccessStep(){
        if(this.AccountInfoForm.invalid){
            this.snackBar.open('Please fill Password and Confirm Password.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }

        if(this.AccountInfoForm.controls.Password.value != this.AccountInfoForm.controls.ConfirmPassword.value){
            this.snackBar.open('Password and Confirm Password should be same.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }

        if(this.documentArray.length != this.allDocuments.length){
            if(this.documentArray.length >=3){
                for(let docc of this.documentArray){
                    if(docc.Name == 'Registration Amount Receipt'){
                        if(docc.Number != ''){
                            this.snackBar.open('Please upload mandatory documents.', '', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                            return 0; 
                        }
                    }
                }
            }
            else{
                this.snackBar.open('Please upload mandatory documents.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0; 
            }            
        }

        if(this.FinacInfo.invalid){
            this.snackBar.open('Please fill Financial details.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;  
        }
        this.stepper.next();
    }

    lowerCase(val){
        this.BasicInfoForm.patchValue({
            Email : val.toLowerCase()
        })
    }

    successStep(){
        if(!(this.SelectRole == 'Quality Inspector' || this.SelectRole == 'Payment Collector')){
            if(this.AccountInfoForm.invalid){
                this.snackBar.open('Please fill Password and Confirm Password.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            } 
            if(this.AccountInfoForm.controls.Password.value != this.AccountInfoForm.controls.ConfirmPassword.value){
                this.snackBar.open('Password and Confirm Password should be same.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }
        
        if(this.SelectRole == 'Quality Inspector' || this.SelectRole == 'Payment Collector'){
            if(this.FinacInfo.value.TypeOfAccount == '' || this.FinacInfo.value.Name == '' || this.FinacInfo.value.AccountNumber == '' || this.FinacInfo.value.BankName == '' || this.FinacInfo.value.IFSCCode == ''){
                this.snackBar.open('Please fill financial detail.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }
        console.log(this.documentArray)
        if(this.documentArray.length != this.allDocuments.length){
            
            this.snackBar.open('All Documents are mandantory.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        this.stepper.next();
    }

    getSPType(type){
        if(type == 'T'){
            this.teamService.getAllTeamCompany('0').subscribe((data:{})=>{
                if(data['status'])
                this.Teams = data['result']
                this.FilterTeam = data['result']
            })
        }
        else if(type == 'C'){
            this.companyService.getAll().subscribe((data:{})=>{
                if(data['status'])
                this.Companies = data['result']
                this.FilterCompany = data['result']
            })
        }
    }

    filterCompanySearch(){
        if (!this.Companies) {
            return;
        }
        // get the search keyword
        let search = this.CompanyFilterCtrl.value;
        if (!search) {
            this.FilterCompany = (this.Companies.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        // filter the banks
        var data =
            this.Companies.filter(user =>
                user.Name.toLowerCase().includes(search)
                
            );

        this.FilterCompany = data;
        
    }

    filterTeamSearch(){
        if (!this.Teams) {
            return;
        }
        // get the search keyword
        let search = this.TeamFilterCtrl.value;
        if (!search) {
            this.FilterTeam = (this.Teams.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        // filter the banks
        var data =
            this.Teams.filter(user =>
                user.TeamName.toLowerCase().includes(search)
                
            );

        this.FilterTeam = data;
        
    }

    getTeamID(id){

    }

    getCompanyID(id){
        
    }

    altNumber(number){
        var altnum = number;
        var nm = altnum.replace(/,/g,'');
        if(nm.length % 10 == 0 && nm.length > 0){
            this.BasicInfoForm.patchValue({
                'AltPhone': number + ","
            })
        }
        
    }

}
