import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from "@app/core/services";
import { StateService } from "@app/features/Management/state/state.service";
import { CityService } from "@app/features/Management/city/city.service";
import { LocalityService } from "@app/features/Management/locality/locality.service";
import { RoleService } from "@app/features/Management/roles/role.service";
import { ServicesService } from "@app/features/Management/service/services.service";
import { UserService } from "@app/features/Management/users/user.service";
import { MatSnackBar, MatStepper } from "@angular/material";
import { saveAs as importedSaveAs } from 'file-saver';
import { DomSanitizer } from '@angular/platform-browser';

import {
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/animations'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'user-sp',
    templateUrl: './user-sp.component.html',
    animations: [
        trigger('changePane', [
            state('out', style({
                height: 0,
            })),
            state('in', style({
                height: '*',
            })),
            transition('out => in', animate('250ms ease-out')),
            transition('in => out', animate('250ms 300ms ease-in'))
        ])
    ]
})
export class UserSPComponent implements OnInit {
    ID: any;
    UserServices = [];
    ServiceProvider: any = [];
    documentArray: Array<any> = [];
    documentUploadedArray: any = [];
    allImages: any = [];
    docMandatory = 0;
    Services: any;
    Skills: any = [];
    Type : any;
    

    @ViewChild('stepper') stepper: MatStepper;

    @ViewChild('avatarImage') myInputVariable: ElementRef;

    public validationUserFormDetailOptions = {};

    constructor(private http: HttpClient,
        private router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private notificationService: NotificationService,
        private servicesService: ServicesService,
        private userService: UserService,
        private snackBar: MatSnackBar,
        private sanitizer: DomSanitizer,
        private spinnerService : Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
        this.ID = this.route.params['value'].id;

        this.Type = this.route.params['value'].type;
        this.getOne(this.ID);

    }

    ngOnInit() {
        this.ID = this.route.params['value'].id;
    }

    getAllServices(SPServices) {
        if(this.Type == 'SP'){
            this.spinnerService.show();
            this.servicesService.getAllServices().subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.Services = data['result'];
                    for(var i = 0; i < this.Services.length; i++){
                          this.Services[i].skill = false;
                          if(!this.Services[i].avgCapacity){
                            this.Services[i].avgCapacity = 0;
                          }
                          if(SPServices){
                              for(var j = 0; j < SPServices.length; j++){ 
                                  if(SPServices[j].id == this.Services[i]._id){
                                      this.Services[i].skill = true;
                                      this.Services[i].avgCapacity = SPServices[j].avgCapacity ? SPServices[j].avgCapacity : 0;
                                  }
                              }
                          }
                    }
                }
                //console.log(data);
            }, err => {
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
        else{
            this.spinnerService.show();
            this.servicesService.getAllServices().subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.Services = data['result'];
                    for(var i = 0; i < this.Services.length; i++){
                          this.Services[i].skill = false;
                          if(SPServices){
                              for(var j = 0; j < SPServices.length; j++){ 
                                  if(SPServices[j].id == this.Services[i]._id){
                                      this.Services[i].skill = true;
                                      this.Services[i].avgCapacity = SPServices[j].avgCapacity;
                                  }
                              }
                          }
                    }
                }
            }, err => {
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        }
        
    }

    getOne(id) {
        this.spinnerService.show();
        this.userService.getOneUsers(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.ServiceProvider = data['result'];
                this.UserServices = this.ServiceProvider.Services;
                if(this.Type  == 'SP'){
                    this.getAllServices(this.ServiceProvider.Services);
                }
                else{
                    this.getAllServices(this.ServiceProvider.Services);
                }
                
            }
        }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    SaveSPSkills(){
        //alert(JSON.stringify(this.Services));

        var dataSkill = [];
        var conditionSkill = false;
        for(var i = 0; i < this.Services.length; i++){
            if(this.Services[i].skill == true){
                dataSkill.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
            }
        } 
        
        if(dataSkill.length != this.UserServices.length){ 
            conditionSkill = true;
        }
        else{
            for(let dt of dataSkill){
                var index = this.UserServices.findIndex(x=> x.id == dt.id);
                if(index < 0){
                    conditionSkill = true;
                }
                else{
                   if(this.UserServices[index].skill != dt.skill || (this.UserServices[index].avgCapacity != dt.avgCapacity && this.Type == 'SP')) { 
                    conditionSkill = true;
                   }
                }
            }
        } 
        if(conditionSkill){
            this.notificationService.smartMessageBox({
                title: "Update Alert!",
                content: "Are you sure to update skills?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.Skills = [];
                    if(this.Type == 'SP'){
                        for(var i = 0; i < this.Services.length; i++){
                            if(this.Services[i].skill == true){
                                this.Skills.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
                            }
                        }
                
                        this.userService.updateSPSkills(this.ID, this.Skills).subscribe((data: {}) => {
                            if ( data['status']) {
                                this.snackBar.open('Updated successfully', 'success', {
                                    duration: 5000,
                                    panelClass: ['success-snackbar'],
                                    verticalPosition: 'top'
                                });
                                this.UserServices = this.Skills;
                                console.log("Updated successfully");
                            }
                        }, err => {
                            this.spinnerService.hide();
                            this.snackBar.open('Error', 'error', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                        })
                    }
                    else{
                        for(var i = 0; i < this.Services.length; i++){
                            if(this.Services[i].skill == true){
                                this.Skills.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
                            }
                        }
                        this.spinnerService.show();
                        this.userService.updateVendorCategories(this.ID, this.Skills).subscribe((data: {}) => {
                            this.spinnerService.hide();
                            if ( data['status']) {
                                this.snackBar.open('Updated successfully', '', {
                                    duration: 5000,
                                    panelClass: ['success-snackbar'],
                                    verticalPosition: 'top'
                                });
                                this.UserServices = this.Skills;
                                console.log("Updated successfully");
                            }
                        }, err => {
                            this.spinnerService.hide();
                            this.snackBar.open('Error', '', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                        });
                    } 
                }
                else{

                }
            })
        }
        else{
            
        }

        
        
    }

    back(){
        var dataSkill = [];
        var conditionSkill = false;
        for(var i = 0; i < this.Services.length; i++){
            if(this.Services[i].skill == true){
                dataSkill.push({id: this.Services[i]._id, avgCapacity:this.Services[i].avgCapacity});
            }
        } 
        
        if(dataSkill.length != this.UserServices.length){ 
            conditionSkill = true;
        }
        else{
            for(let dt of dataSkill){
                var index = this.UserServices.findIndex(x=> x.id == dt.id);
                if(index < 0){
                    conditionSkill = true;
                }
                else{
                   if(this.UserServices[index].skill != dt.skill || (this.UserServices[index].avgCapacity != dt.avgCapacity && this.Type == 'SP')) { 
                    conditionSkill = true;
                   }
                }
            }
        } 
        if(conditionSkill){
            this.notificationService.smartMessageBox({
                title: "Back Alert!",
                content: "Oops It looks like you made some chagnes, that are not saved. Are you sure to leave this screen?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.router.navigate(['users']);
                }
                else{

                }
            })
        }
        else{
            this.router.navigate(['users']);
        }
        
        
    }



}
