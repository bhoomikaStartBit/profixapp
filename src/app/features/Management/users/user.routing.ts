
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "@app/features/Management/users/user.component";
import {UserFormComponent} from "@app/features/Management/users/user-forms.component";
import {UserSPComponent} from "@app/features/Management/users/user-sp.component";


export const routes:Routes = [

  {
    path: '',
    component: UserComponent
  },{
    path: 'createnew/:id',
    component: UserFormComponent
  },
  {
    path: 'edit/:id',
    component: UserFormComponent
  },
  {
    path: 'serviceprovider/:id/:type',
    component: UserSPComponent
  },
  {
    path: 'vender/:id/:type',
    component: UserSPComponent
  },
  {
    path: 'vender/:id/:type',
    component: UserSPComponent
  },

];

export const routing = RouterModule.forChild(routes);
