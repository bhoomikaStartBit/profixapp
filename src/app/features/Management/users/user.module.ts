import {NgModule} from "@angular/core";

import {routing} from "./user.routing";
import {UserComponent} from "@app/features/Management/users/user.component";
import {UserFormComponent} from "@app/features/Management/users/user-forms.component";
import {UserSPComponent} from "@app/features/Management/users/user-sp.component";

import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminWizardsModule} from "@app/shared/forms/wizards/smartadmin-wizards.module";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {MatStepperModule , MatIconModule} from "@angular/material";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {D3Module} from "@app/shared/graphs/d3/d3.module";
import { WebcamModule } from "ngx-webcam";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";

import { NgxSmartModalModule } from 'ngx-smart-modal';
import { MatSelectSearchModule } from "@app/core/common/material-module/mat-select-search/mat-select-search.module";

@NgModule({
    declarations: [
    UserComponent,
    UserFormComponent,
    UserSPComponent
    ],
    imports: [
    SharedModule,
    routing,
    SmartadminDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    SmartadminInputModule,
    SmartadminWizardsModule,
    NgMultiSelectDropDownModule.forRoot(),
    D3Module,
    MaterialModuleModule,
    SmartadminValidationModule,
    WebcamModule,
    Ng4LoadingSpinnerModule.forRoot(),
    NgxSmartModalModule.forRoot(),
    MatSelectSearchModule
    ],
    providers: [],
    exports:[
    FormsModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatIconModule
    ]
})
export class UserModule {

}
