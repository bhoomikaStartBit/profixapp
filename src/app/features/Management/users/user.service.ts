import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {
  url: string;
  urlProfile: string;
    DocUrl: string;
    uploadUrl : string;
    FileDeleteUrl : string;
    CompanyUrl : string;
    RoleUrl : string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/user/';
    this.urlProfile = commService.getApiUrl() + '/profile/';
    this.DocUrl = commService.getApiUrl() + '/document/';
    this.uploadUrl =  this.commService.getApiUrl()+'/multipleUploads/upload';
    this.FileDeleteUrl = this.commService.getApiUrl()+'/multipleUploads/'
    this.CompanyUrl = commService.getApiUrl() + '/Companies/';
    this.RoleUrl = commService.getApiUrl() + '/role/';

  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getAllUsers(): Observable<any> {
    return this.http.get(this.url + 'getAll/').pipe(
      map(this.extractData));
  }

  getAllRelations(): Observable<any> {
    return this.http.get(this.url + 'getAllRelations/').pipe(
      map(this.extractData));
  }


  getRoleIDByKey(key): Observable<any> {
    return this.http.get(this.RoleUrl + 'getRoleIDByKey/' + key).pipe(
      map(this.extractData));
  }

  getAllCompanyVendorUser(): Observable<any> {
    return this.http.get(this.url + 'getAllCompanyVendorUser/').pipe(
      map(this.extractData));
  }

  getAllUserWithSpRole(): Observable<any> {
    return this.http.get(this.url + 'getAllUserWithSpRole/').pipe(
      map(this.extractData));
  }

  getAllUsersByRoleID(id): Observable<any> {
    return this.http.get(this.url + 'getByRoleID/' + id).pipe(
      map(this.extractData));
  }

  getOneUsers(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }
  

  checkAvailable(id , available): Observable<any> {
    return this.http.get(this.url + 'checkAvailable/'+id+'/' + available).pipe(
      map(this.extractData));
  }
  
  checkBlock(id , block): Observable<any> {
    return this.http.get(this.url + 'checkBlock/'+id+'/' + block).pipe(
      map(this.extractData));
  }

  getAllDocuments(): Observable<any> {
    return this.http.get(this.DocUrl + 'getAll/').pipe(
      map(this.extractData));
  }

  getAllDocumentsByUser(user): Observable<any> {
    return this.http.get(this.DocUrl + 'getAllDocumentsByUser/'+user).pipe(
      map(this.extractData));
  }


  checkUserName(name): Observable<any> {
      return this.http.get(this.url + 'checkUserName/' + name).pipe(
          map(this.extractData));
  }
  checkEmail(email): Observable<any> {
      return this.http.get(this.url + 'checkEmail/' + email).pipe(
          map(this.extractData));
  }

  addUsers (data , accountdata , mainDocument , additionalDocument, Financial): Observable<any> {
    var dat = {
      'Basic':data,
        'Account':accountdata,
        'MandatoryDocuments':mainDocument,
        'AdditionalDocuments':additionalDocument,
        'Financial': Financial

    }
    console.log(dat);
    return this.http.post<any>(this.url + 'create', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  addSPUsers (data , accountdata , mainDocument , additionalDocument , service ,Financial , SecondaryDetail , FamilyDetail , currentuserid): Observable<any> {
    var dat = {
      'Basic':data,
        'Account':accountdata,
        'MandatoryDocuments':mainDocument,
        'AdditionalDocuments':additionalDocument,
        'Services': service,
        'Financial': Financial,
        'SecondaryDetails' : SecondaryDetail,
        'FamilyDetails' : FamilyDetail,
        'Currentuserid' : currentuserid
    }
    console.log(dat);
    return this.http.post<any>(this.url + 'createServiceProvider', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  addVendorUsers (data , accountdata , mainDocument , additionalDocument , shop ,  Financial , venderaddress): Observable<any> {
    var dat = {
      'Basic':data,
        'Account':accountdata,
        'MandatoryDocuments':mainDocument,
        'AdditionalDocuments':additionalDocument,
        'Shop': shop,
        'Financial': Financial , 
        'VenderAddress' : venderaddress,
        'VendorType': 'I'
    }
    console.log(dat);
    return this.http.post<any>(this.url + 'createVendor', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }
  
  addCompanyVendorUsers (data ,  mainDocument , venderaddress ,  Financial ,Services , Commissions ): Observable<any> {
    var dat = {
      'CompanyData':{
        'CompanyType' : data.CompanyType,
        'CompanyName' : data.CompanyName,
        'CompanyEmail' : data.CompanyEmail,
        'CompanyPhone' : data.CompanyPhone,
        'ContactPersonName' : data.ContactPersonName,
        'ContactPersonEmail' : data.ContactPersonEmail,
        'ContactPersonPhone' : data.ContactPersonPhone,
        'CreditLimit' : data.CreditLimit,
        'CreditDays' : data.CreditDays,
      },
      'UserData' : {
        FirstName:  data.FirstName,
        MiddleName: data.MiddleName,
        LastName: data.LastName,
        Email: data.Email,
        Phone: data.Phone,
        Password : data.Password,
        Role : data.Role
      },
      'MandatoryDocuments':mainDocument,
      'Financial': Financial , 
      'VenderAddress' : venderaddress,
      'Services':Services,
      'Commissions':Commissions,
      'VendorType' : 'C'
    }
    console.log(dat);
    return this.http.post<any>(this.url + 'createVendor', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  addCompanyBranchVendorUsers (data ,  mainDocument , venderaddress ,  Financial , Commissions ): Observable<any> {
    var dat = {
      'CompanyData':{
        'VendorCompanyID' : data.VendorCompanyID
      },
      'UserData' : {
        FirstName:  data.FirstName,
        MiddleName: data.MiddleName,
        LastName: data.LastName,
        Email: data.Email,
        Phone: data.Phone,
        Password : data.Password,
        Role : data.Role
      },
      'MandatoryDocuments':mainDocument,
      'Financial': Financial , 
      'VenderAddress' : venderaddress,
      'Commissions':Commissions,
      'VendorType' : 'B'
    }
    console.log(dat);
    return this.http.post<any>(this.url + 'createVendor', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  updateUsers (id, BasicData , AccountData  , mainDocument , additionalDocument , Financial): Observable<any> {
      var dat = {
          'Basic':BasicData,
          'Account':AccountData,
          'MandatoryDocuments':mainDocument,
          'AdditionalDocuments':additionalDocument,
          'Financial': Financial ,
      }
    return this.http.put<any>(this.url + 'update/' + id, dat, httpOptions).pipe(
      tap(( ) => console.log(`updated user w/ id=${id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

    getAllPendingRequest(): Observable<any> {
        return this.http.get(this.urlProfile + 'getAllPendingRequest').pipe(
            map(this.extractData));
    }

  updateSPUsers (id, BasicData , AccountData  , mainDocument , additionalDocument ,service  ,Financial ,SecondaryDetail , FamilyDetail , currentuserid ): Observable<any> {
      var dat = {
          'Basic':BasicData,
          'Account':AccountData,
          'MandatoryDocuments':mainDocument,
          'AdditionalDocuments':additionalDocument,
          'Services': service,
          'Financial': Financial,
          'SecondaryDetails' : SecondaryDetail,
          'FamilyDetails' : FamilyDetail,
          'Currentuserid' : currentuserid
      }
    return this.http.put<any>(this.url + 'updateServiceProvider/' + id, dat, httpOptions).pipe(
      tap(( ) => console.log(`updated user w/ id=${id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  updateVendorUsers (id, BasicData , AccountData  , mainDocument , additionalDocument ,  Financial , venderAddress): Observable<any> {
      var dat = {
          'Basic':BasicData,
          'Account':AccountData,
          'MandatoryDocuments':mainDocument,
          'AdditionalDocuments':additionalDocument,
          'Financial': Financial,
          'VenderAddress' : venderAddress,
          'VendorType' : 'I'
      }
    return this.http.put<any>(this.url + 'updateVendor/' + id, dat, httpOptions).pipe(
      tap(( ) => console.log(`updated user w/ id=${id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  updateCompanyVendorUsers (id, data ,  mainDocument , venderaddress ,  Financial ,Services , Commissions): Observable<any> {
    var dat = {
      'CompanyData':{
        'CompanyType' : data.CompanyType,
        'CompanyName' : data.CompanyName,
        'CompanyEmail' : data.CompanyEmail,
        'CompanyPhone' : data.CompanyPhone,
        'ContactPersonName' : data.ContactPersonName,
        'ContactPersonEmail' : data.ContactPersonEmail,
        'ContactPersonPhone' : data.ContactPersonPhone,
        'CreditLimit' : data.CreditLimit,
        'CreditDays' : data.CreditDays,
      },
      'UserData' : {
        FirstName:  data.FirstName,
        MiddleName: data.MiddleName,
        LastName: data.LastName,
        Email: data.Email,
        Phone: data.Phone,
        Password : data.Password,
        Role : data.Role
      },
      'MandatoryDocuments':mainDocument,
      'Financial': Financial , 
      'VenderAddress' : venderaddress,
      'Services':Services,
      'Commissions':Commissions,
      'VendorType' : 'C'
    }
  return this.http.put<any>(this.url + 'updateVendor/' + id, dat, httpOptions).pipe(
    tap(( ) => console.log(`updated uset w/ id=${id}`)),
    catchError(this.handleError<any>('updateuser'))
  );
}
updateCompanyBranchVendorUsers (id, data ,  mainDocument , venderaddress ,  Financial  , Commissions): Observable<any> {
  var dat = {
    'CompanyData':{
      'VendorCompanyID' : data.VendorCompanyID,
    },
    'UserData' : {
      FirstName:  data.FirstName,
      MiddleName: data.MiddleName,
      LastName: data.LastName,
      Email: data.Email,
      Phone: data.Phone,
      Password : data.Password,
      Role : data.Role
    },
    'MandatoryDocuments':mainDocument,
    'Financial': Financial , 
    'VenderAddress' : venderaddress,
    'Commissions':Commissions,
    'VendorType' : 'B'
  }
return this.http.put<any>(this.url + 'updateVendor/' + id, dat, httpOptions).pipe(
  tap(( ) => console.log(`updated user w/ id=${id}`)),
  catchError(this.handleError<any>('updateUser'))
);
}

  getCompanyDetailByManager(id): Observable<any> {
    return this.http.get(this.CompanyUrl + 'getCompanyDetailByManager/' + id).pipe(
      map(this.extractData));
  }

  deleteUsers (id): Observable<any> {
    return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
      tap(( ) => console.log(`deleted role w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

    getUserDocument(url): Observable<any> {
        return this.http.get(this.commService.getApiUrl() + url, { responseType: 'blob' as 'json' }).pipe(
            map(this.extractData));
    }

    rejectRequest(id): Observable<any> {
        return this.http.get(this.urlProfile + 'rejectRequest/' + id).pipe(
            map(this.extractData));
    }

    acceptRequest(id): Observable<any> {
        return this.http.get(this.urlProfile + 'acceptRequest/' + id).pipe(
            map(this.extractData));
    }

    updateSPSkills(id, data): Observable<any> {
      return this.http.put<any>(this.url + 'updateSPSkills/' + id, JSON.stringify(data), httpOptions).pipe(
        tap(( ) => { console.log('update');
        console.log(data);  }),
        catchError(this.handleError<any>('updateProfile'))
      );
    }

    
    updateVendorCategories(id, data): Observable<any> {
      return this.http.put<any>(this.url + 'updateVendorCategories/' + id, JSON.stringify(data), httpOptions).pipe(
        tap(( ) => { console.log('update');
        console.log(data);  }),
        catchError(this.handleError<any>('updateProfile'))
      );
    }

    addImage (file): Observable<any> {
      let formData = new FormData();
      formData.append('file',file[0]);
        return this.http.post<any>(this.uploadUrl ,formData).pipe(
            tap(( ) => console.log(`added image`)),
            catchError(this.handleError<any>('addImage'))
        );
      }

      deleteFile(path): Observable<any> {
        var deletePath = {
          'path' : path
        }
        return this.http.post<any>(this.FileDeleteUrl + 'deleteFile' , deletePath, httpOptions).pipe(
          tap(( ) => console.log(`deleted role w/ id=${path}`)),
          catchError(this.handleError<any>('deleteFile'))
        );
      }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
