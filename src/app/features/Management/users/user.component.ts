import { Component, OnInit , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {UserService} from "@app/features/Management/users/user.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { AuthenticationService } from '@app/core/common/_services/authentication.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'userlist',
  templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {
    Users : any;
    currentUser : any = {};
    blk = false;
    displayedColumns: string[] = [
        'SN',
        'ProfixID',
        // 'Email',
        'Name',
        'Phone',
        'type',
        //'Role',
        // 'ExpiryDate',
        'Available',
        'Block',
        'Action'
    ];

    limit:number = 50;
    pageIndex : number = 0;
    pageLimit:number[] = [10,25,50,75, 100];

    SuperAdminRoleID: any;
    CustomerManagerRoleID: any;
    ServiceProviderRoleID: any;
    VendorRoleID: any;
    PaymentCollectorRoleID: any;
    QualityInspectorRoleID: any;

    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    RoleID : any;
    
  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private userService: UserService,
      private snackBar: MatSnackBar,
      private authenticationService : AuthenticationService,
      private spinnerService : Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
    setTimeout(() => {
      this.authenticationService.checkSelectedComponentVisiblity('Users');
    }, 100);
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.userService.getRoleIDByKey('CM').subscribe( (data)=>{ 
        this.CustomerManagerRoleID = data.result[0]._id;
      });
      this.userService.getRoleIDByKey('SA').subscribe( (data)=>{ 
        this.SuperAdminRoleID = data.result[0]._id;
      });
      this.userService.getRoleIDByKey('SP').subscribe( (data)=>{ 
        this.ServiceProviderRoleID = data.result[0]._id;
        this.getAllUsers(this.ServiceProviderRoleID);
      });
      this.userService.getRoleIDByKey('V').subscribe( (data)=>{ 
        this.VendorRoleID = data.result[0]._id;
      });
      this.userService.getRoleIDByKey('PC').subscribe( (data)=>{ 
        this.PaymentCollectorRoleID = data.result[0]._id;
      });
      this.userService.getRoleIDByKey('QI').subscribe( (data)=>{ 
        this.QualityInspectorRoleID = data.result[0]._id;
      });   
     
      
  }

  ngOnInit() {

  }
  chcekAvailable(id , i , tis){
  console.log(i , tis)
    this.notificationService.smartMessageBox({
      title: "Alert!",
      content: 'Are you sure to make this change?',
      buttons: '[No][Yes]'
  }, (ButtonPressed) => {
      if (ButtonPressed === "Yes") {
        var aval = tis ? 'Y' : 'N';
          this.userService.checkAvailable(id , aval).subscribe((data:{})=>{

          })
      }
      else{
          this.dataSource.data[i].IsAvailable = !tis;
      }
  })
  }

  chcekBlock(id , i , block){
      this.notificationService.smartMessageBox({
        title: "Alert!",
        content: 'Are you sure to make this change?',
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") {
            this.userService.checkBlock(id , block).subscribe((data:{})=>{
  
            })
        }
        else{
            this.dataSource.data[i].Account_Block = !block;
        }
    })
    }

  RoleBaseUsers(RoleID){

      if(RoleID ==  this.CustomerManagerRoleID){  // CM USER
        this.displayedColumns = [
                      'SN',
                      'Name',
                      'Phone',
                     // 'type',
                     // 'Role',
                       'Available',
                      'Block'
        ];
      }else if(RoleID == this.SuperAdminRoleID){ // Super Admin
        this.displayedColumns = [
            'SN',
            'ProfixID',
            'Name',
            'Phone',
           // 'type',
           // 'Role',
           // 'Block'
        ];
      }
      else if(RoleID == this.VendorRoleID){ // Vendor
        this.displayedColumns = [
            'SN',
            'ProfixID',
            'Name',
            'CName',
            'Phone',
            'type',
           // 'Role',
            'Block',
            'Action'
        ];
      }else if(RoleID == this.ServiceProviderRoleID) { // Service Provider
          this.displayedColumns = [
              'SN',
              'ProfixID',
              'Name',
              'Phone',
              'type',
             // 'Role',
              'Available',
              'Block',
              'Action'
          ];
      }
      else {
        this.displayedColumns = [
            'SN',
            'ProfixID',
            'Name',
            'Phone',
           // 'type',
           // 'Role',
            'Available',
            'Block',
            'Action'
        ];
    }
      this.getAllUsers(RoleID);
  }

  getAllUsers(RoleID){
    this.RoleID = RoleID;
    this.spinnerService.show();
      this.userService.getAllUsersByRoleID(RoleID).subscribe( (data: {})=>{
          this.spinnerService.hide();
          if(data['status']){
              this.Users = data['result'];
	       //this.users = data['result'];
              // if(RoleID == '5d0a16fdb457473128b080be'){
              //       for(var i = 0; i < data['result'].length; i++){
              //           console.log(data['result'][i]._id);
              //           this.userService.getCompanyDetailByManager(data['result'][i]._id).subscribe( (company)=>{ 
              //              //if(company.result){
              //                   console.log(i);
              //                   console.log(data['result'][i]);
              //                   data['result'][i].Role = company.result.Name;
              //              //}
              //           });

              //       } 
              // }     
              var usedata = [];
              if( this.Users.length > 0){
                usedata = this.Users.map(row=>{
                  row.IsAvailable =  row.IsAvailable == 'Y'  ?  true : false;
                  return row
                });
              }
              console.log(usedata)
              this.dataSource = new MatTableDataSource(usedata);
              this.dataSource.filterPredicate = function(data, filter: string): boolean {
                return data.Role.Name.toLowerCase().includes(filter) || 
                      data.Email.toLowerCase().includes(filter) ||
                      data.FirstName.toLowerCase().includes(filter) ||
                      data.MiddleName.toLowerCase().includes(filter) ||
                      data.ProfixID.toLowerCase().includes(filter) ||
                      (data.Phone).toString().toLowerCase().includes(filter) ||
                      data.LastName.toLowerCase().includes(filter) 
                      ;
            };
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
          }
      }, err => {
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
    }

    getEndDate(date){
      var dat = new Date(date).getTime() - new Date().getTime();
      return Math.ceil(dat / (1000 * 3600 * 24)) <= 30;    // for showing highlighted to be expiry date of SP User membership
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


    delete(id){
       /* this.userService.getRelatedRecordOfUnit(id).subscribe((data: {}) => {
            if (data['status']) {
                var lab ='';
                if(data['result'].length){
                    lab = 'Are you sure delete this record and records related to it?';
                }
                else{
                    lab = 'Are you sure delete this record?';
                }*/
                this.notificationService.smartMessageBox({
                    title: "Delete!",
                    content: 'Are you sure delete this record?',
                    buttons: '[No][Yes]'
                }, (ButtonPressed) => {
                    if (ButtonPressed === "Yes") {
                      this.spinnerService.show();
                        this.userService.deleteUsers(id).subscribe((data: {}) => {
                          this.spinnerService.hide();
                            if (data['status']) {
                                this.snackBar.open('Record deleted successfully', 'Delete', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                            } else {
                            }
                            this.getAllUsers(this.RoleID);
                        }, err => {
                          this.snackBar.open('Server Error', '', {
                              duration: 5000,
                              panelClass: ['danger-snackbar'],
                              verticalPosition: 'top'
                          });
                      });
                    }
                    if (ButtonPressed === "No") {

                    }

                });
           /* }
        });*/
    }

    changePage(event){
      this.pageIndex = event.pageSize*event.pageIndex;
    }


}
