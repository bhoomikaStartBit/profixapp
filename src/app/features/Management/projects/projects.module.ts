import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {routing} from "./projects.routing";
import {ProjectsComponent} from "@app/features/Management/projects/projects.component";
import {ProjectsFormComponent} from "@app/features/Management/projects/projects-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { SmartadminWizardsModule } from '@app/shared/forms/wizards/smartadmin-wizards.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


import { StatsModule } from '@app/shared/stats/stats.module';
import { SmartadminWidgetsModule } from '@app/shared/widgets/smartadmin-widgets.module';
import {SmartadminLayoutModule} from "@app/shared/layout";
import {ProjectDetailComponent} from "@app/features/Management/projects/projectDetail.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      ProjectsComponent,
      ProjectsFormComponent,
      ProjectDetailComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule,
      ReactiveFormsModule,
      SmartadminInputModule,
      SmartadminWizardsModule,
      NgMultiSelectDropDownModule.forRoot(),
      StatsModule,
      SmartadminWidgetsModule,
      SmartadminLayoutModule,
      CommonModule,
      MaterialModuleModule,
      SmartadminValidationModule,
      OwlDateTimeModule,
      OwlNativeDateTimeModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [],
    exports:[
        FormsModule,
        ReactiveFormsModule
    ]
})
export class ProjectsModule {

}
