
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {ProjectsComponent} from "@app/features/Management/projects/projects.component";
import {ProjectsFormComponent} from "@app/features/Management/projects/projects-forms.component";
import {ProjectDetailComponent} from "@app/features/Management/projects/projectDetail.component";


export const routes:Routes = [

  {
    path: '',
    component: ProjectsComponent
  },{
    path: 'createnew/:id',
    component: ProjectsFormComponent
  },{
    path: 'detail/:id',
    component: ProjectDetailComponent
  },


];

export const routing = RouterModule.forChild(routes);
