import { Component, OnInit  , ViewChild ,AfterViewInit} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {CategoriesService} from "@app/features/Management/categories/categories.service";
import {MatPaginator, MatSort, MatStepper, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Router, Params, ActivatedRoute} from "@angular/router";
import {ProjectsService} from "@app/features/Management/projects/projects.service";
import {UserService} from "@app/features/Management/users/user.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'userlist',
  templateUrl: 'projectDetail.component.html',
  styleUrls: ['./projectDetail.component.scss']
})
export class ProjectDetailComponent implements OnInit,AfterViewInit {
    Categories : any;
    url : any;
    ID : any;
    selectedStatus : any;
    SelectedSP : any = {};
    OrderAssignData : any = [];
    SelectedUsers : any = [];
    OrderMilestoneData : any = [];
    PaymentInformationData : any = [];
    ReviewInformationData : any = [];
    Users : any;
    AssignUser : any = '';
    AssignTo : any = '';
    CurrentOrderStatus : any = '';
    GetProjectData = false;
    GetPaymentData = false;
    GetReviewData = false;
    GetSP = false;
    SelectedProject : any;
    CurrentLoginUser : any = {};
    workStatusArray = ['', 'New', 'in-Route', 'In Process', 'Review', 'Close'];
    @ViewChild('stepper') stepper: MatStepper;
    selectedOrderStatus: Number;
    allActivities: any = [];
    SubmissionForm  : FormGroup;
    PaymentForm  : FormGroup;
    MilestoneForm  : FormGroup;
    ReviewForm  : FormGroup;
    AllReviews : any = [];
    GetAllReviewsData = false;
    ProjectImages: any = [];
    Comments : any;
    getAdminCommission : any;
    PayAmtAfterAdCom : any;
    ServiceCharges : any; 

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private snackBar: MatSnackBar,
      private commonService: CommonService,
      private route: ActivatedRoute,
      private router: Router,
      private projectsService: ProjectsService,
      private userService : UserService,
      private fb: FormBuilder,

  ) {
      this.getAllServiceProviderUsers();
      this.CurrentLoginUser = JSON.parse(localStorage.getItem('currentUser'));
      this.ID = this.route.params['value'].id;
      this.getOne(this.ID);
      this.getAllOrderActivitiesByOrderID(this.ID);
      this.url = this.commonService.getApiUrl();
      this.SubmissionForm = fb.group({
          Status: [ '', [Validators.required ] ],
          Notes: [ '', [Validators.required ] ],
      });
      this.PaymentForm = fb.group({
          PaymentMode: [ '', [Validators.required ] ],
          Notes: [ '', [Validators.required ] ],
      });
      this.ReviewForm = fb.group({
          Rating: [ 0 ],
          Review: [ '', [Validators.required ] ],
      });
      this.MilestoneForm = fb.group({
          'Service' : ['' , [Validators.required]],
          'Title':['' , [Validators.required]],
          'Description':[''],
          'Width':['' , [Validators.required]],
          'Height':['' , [Validators.required]],
          'Amount':['' , [Validators.required]],
          'UserID':['' , [Validators.required]],
      });

      this.commonService.getAdminCommission().subscribe((data: {}) => {
        this.getAdminCommission = data['result'];
      });
  }

  ngOnInit() {

  }
  ngAfterViewInit(){
    }

    getOne(id){
        this.projectsService.getOneProjects(id).subscribe( (data : {})=>{
            if(data['status']){
                this.SelectedProject = data['result'];
                this.GetProjectData = true;
                this.selectedStatus = data['result'].Status;
                this.CurrentOrderStatus = data['result'].Status;
                this.AssignTo = data['OrderAssign'];
                this.OrderAssignData = data['OrderAssignData'];
                let index = 0;
                this.ProjectImages = data['result'].ProjectImages ? data['result'].ProjectImages : '';
                this.Comments = data['result'].Comments ? data['result'].Comments : '';

                if(data['result'].PaymentAmount){
                   this.ServiceCharges = ((this.getAdminCommission/100) * data['result'].PaymentAmount);
                   this.PayAmtAfterAdCom = data['result'].PaymentAmount - this.ServiceCharges;
                   console.log(this.PayAmtAfterAdCom);
                }

                this.workStatusArray.map(val => {
                   if(val == this.CurrentOrderStatus) {
                       this.selectedOrderStatus = index;
                   }
                    index++;
                });
                if(this.CurrentLoginUser.RoleName == 'Service Provider'){
                    this.getPayInfo();
                    this.getreviewInfo();
                }
                this.getReviewData();
                setTimeout(()=>{
                    this.AssignUser = '';
                    var user = this.SelectedUsers;
                    this.Users = user.filter(row=> row._id !== data['UserID'])
                } , 500)
            }
        })
    }

    getAllServiceProviderUsers(){
        this.userService.getAllUserWithSpRole().subscribe((data: {}) => {
            if ( data['status']) {
                this.SelectedUsers = data['result'];
                this.Users = data['result'];
            }
        });
    }


    getSPUser(id){
        console.log(id);
        var sp = this.Users.filter(row=>row._id == id);
        this.SelectedSP = sp[0];
        console.log(this.SelectedSP );
        this.GetSP = true;
    }

    assignOrder(){
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AssignOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': this.AssignUser,
            'Status': 'In-Route'
        }

        this.projectsService.assignOrder(AssignOrder).subscribe( (data : {})=>{
            if ( data['status']) {
                this.snackBar.open('Order in-route successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
        })

    }

    acceptOrder(){
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'Status': 'In-Route'
        }

        this.projectsService.acceptOrder(AcceptOrder).subscribe( (data : {})=>{
            if ( data['status']) {
                this.snackBar.open('Order accept successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{
                this.snackBar.open('Order already assigned to another person.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(['/projects']);
            }
        })

    }

    submitOrderBySP(){
        console.log("click");
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'Submit': this.SubmissionForm.value
        }

        this.projectsService.submitOrderBySP(AcceptOrder).subscribe( (data : {})=>{
            if ( data['status']) {
                this.snackBar.open('Order submit successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{

            }
        })


    }
    paymentCollect(){
        console.log("click");
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'PaymentAmount': this.SelectedProject.PaymentAmount,
            'Payment': this.PaymentForm.value
        }
        this.projectsService.paymentCollect(AcceptOrder).subscribe( (data : {})=>{
            if ( data['status']) {
                this.snackBar.open('Order Payment has been successfully paid.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{

            }
        })

    }

    changeOrderStatus(){
        // this.projectsService.changeOrderStatus(this.CurrentOrderStatus).subscribe( (data : {})=>{
        //     if ( data['status']) {
        //         this.snackBar.open('Order assigned successfully', 'success', {
        //             duration: 5000,
        //             panelClass: ['success-snackbar'],
        //             verticalPosition: 'top'
        //         });
        //         this.getOne(this.ID);
        //     }
        // })
    }

    getAllOrderActivitiesByOrderID(orderID) {
        this.projectsService.getAllOrderActivitiesByOrderID(orderID).subscribe((data) => {
            this.allActivities = data['result'];
        }, err => {
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getPayInfo(){
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        this.projectsService.orderPayInfo(this.ID , adminuser.id).subscribe( (data : {})=>{
            if(data['status']){
                console.log(data['result'].length);
                if(data['result'].length){

                    this.PaymentInformationData = data['result'];
                    this.GetPaymentData = true;
                    console.log(this.PaymentInformationData);
                }
                else{
                    this.GetPaymentData = false;
                }
            }
        })
    }

    getReviewData(){
        this.AllReviews = [];
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        this.projectsService.getAllReviews(this.ID , adminuser.id).subscribe( (data : {})=>{
            if(data['status']){
                console.log(data['result'].length);
                if(data['result'].length){

                    this.AllReviews = data['result'];
                    this.GetAllReviewsData = true;
                    console.log(this.AllReviews);
                }
                else{
                    this.GetAllReviewsData = false;
                }
            }
        })
    }

    createMilestoneAndAssignOrder(){
        console.log(this.MilestoneForm.value);
        var milestone = {
            'Milestone' : this.MilestoneForm.value,
            'OrderID' : this.ID
        }

        this.projectsService.createMilestoneAndAssignOrder(milestone).subscribe( (data : {})=>{
            if(data['status']){
                this.snackBar.open('Milestone created and assign successfully.', 'Error', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.OrderMilestoneData.push(data['result']);

                console.log(this.OrderMilestoneData);
                this.MilestoneForm.reset();
            }
        })
    }

    deleteOrderMilestone(id){
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.projectsService.deleteOrderMilestone(id).subscribe( (data : {})=>{
                    if(data['status']){
                        var index = this.OrderMilestoneData.findIndex( x=> x._id == id);
                        this.OrderMilestoneData.splice(index);

                        this.snackBar.open('Milestone delete successfully.', 'Error', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                })
            }
            else{

            }
        });

    }

    sendReview(){
        console.log("click");
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'Review': this.ReviewForm.value
        }
        this.projectsService.sendReview(AcceptOrder).subscribe( (data : {})=>{
            if ( data['status']) {
                this.snackBar.open('Order Review has been send successfully.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{

            }
        })
    }

    getreviewInfo(){
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        this.projectsService.getInfoReview(this.ID , adminuser.id).subscribe( (data : {})=>{
            if(data['status']){
                console.log(data['result'].length);
                if(data['result'].length){

                     this.ReviewInformationData = data['result'];
                    this.GetReviewData = true;
                    // console.log(this.PaymentInformationData);
                }
                else{
                    this.GetReviewData = false;
                }
            }
        })
    }

    editReview(index,id){
        console.log(index,id)
    }

    approveReview(index,id){
        console.log(index,id);

        this.projectsService.reviewApproved(id).subscribe( (data : {})=>{
            if(data['status']){
                this.snackBar.open('Order Review has been approved.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getReviewData();
            }
        })
    }

    disapproveReview(index,id){
        console.log(index,id);

        this.projectsService.disapproveReview(id).subscribe( (data : {})=>{
            if(data['status']){
                this.snackBar.open('Order Review has been disapproved.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getReviewData();
            }
        })
    }



    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
