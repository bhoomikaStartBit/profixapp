import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError, startWith } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {formatDate, Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {NotificationService} from "@app/core/services";
import {CustomersService} from "@app/features/Management/customers/customers.service";
import {StateService} from "@app/features/Management/state/state.service";
import {CityService} from "@app/features/Management/city/city.service";
import {LocalityService} from "@app/features/Management/locality/locality.service";
import {ServicesService} from "@app/features/Management/service/services.service";
import {MatStepper , MatSnackBar} from "@angular/material";
import {ProjectsService} from "@app/features/Management/projects/projects.service";
import {CommonService} from "@app/core/common/common.service";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';



export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

export const MY_CUSTOM_FORMATS = {
    parseInput: 'LL LT',
    fullPickerInput: 'DD/MM/YYYY LT',
    datePickerInput: 'DD/MM/YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
};

import { trigger,
    state,
    style,
    transition,
    animate} from '@angular/animations';
import { UserService } from '../users/user.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'projects-form',
  templateUrl: './projects-forms.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
        {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
        {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},
    ],
    animations: [
        trigger('changePane', [
            state('out', style({
                height: 0,
            })),
            state('in', style({
                height: '*',
            })),
            transition('out => in', animate('250ms ease-out')),
            transition('in => out', animate('250ms 300ms ease-in'))
        ])
    ]
})
export class ProjectsFormComponent implements OnInit {
    ID: any;
    Total: any;
    Comments = '';
    Scheduled ='';
    getValue = false;
    Customers = [];
    States = [];
    Cities = [];
    Services = [];
    Localities = [];
    dropdownList = [];
    selectedItems = [];
    ServicesArray = [];
    dropdownSettings = {};
    BasicInfoForm : FormGroup;
    ServiceInfoForm : FormGroup;
    PaymentInfoForm : FormGroup;

    DiscountType : any;
    DiscountAmount : any = 0;

    getGSTValue : any = '';

    @ViewChild('stepper') stepper: MatStepper;

    localStorage : any = {};

    public validationBasicDetailOptions:any = {};

    public validationServiceOptions:any = {};

    minDate = new Date();
    filteredStates: Observable<any[]>;
    stateCtrl = new FormControl();

  constructor(private http: HttpClient,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private location: Location,
              private notificationService: NotificationService,
              private customersService: CustomersService,
              private stateService: StateService,
              private cityService: CityService,
              private localityService: LocalityService,
              private servicesService: ServicesService,
              private snackBar: MatSnackBar,
              private projectsService: ProjectsService,
              private commonService: CommonService,
              private userService : UserService,
              private spinnerService: Ng4LoadingSpinnerService
  ) {
          this.spinnerService.hide();
          this.localStorage = JSON.parse(localStorage.getItem('currentUser'));
          if (this.localStorage.RoleName === 'Super Admin') {
              this.getAllCustomers(0);
          } else {
              this.getAllCustomers(this.localStorage.id);
          }

        this.validationBasicDetailOptions = {
          // Rules for form validation
          rules: {
              ProjectName: {
                  required: true
              },
              Customer: {
                  required: true
              },
              State: {
                  required: true
              },
              City: {
                  required: true
              },
              Locality: {
                  required: true
              },
              Address: {
                  required: true
              }
          },

          // Messages for form validation
          messages: {
              ProjectName: {
                  required: 'Please enter your project name'
              },
              Customer: {
                  required: 'Please select Customer'
              },
              State: {
                  required: 'Please select the state'
              },
              City: {
                  required: 'Please select the city'
              },
              Locality: {
                  required: 'Please select the locality'
              },
              Address: {
                  required: 'Please enter address'
              }
          },
          submitHandler: this.onSubmitProject

      };

        this.validationServiceOptions = {
          // Rules for form validation
          rules: {
              Service: {
                  required: true
              },
              Width: {
                  required: true,
              },
              Height: {
                  required: true,
              },
              ServiceSchedule: {
                required: true
              },
              ServiceComments : {
                required: true,
                minlength : 20
              }
          },

          // Messages for form validation
          messages: {
              Service: {
                  required: 'Please select the Service'
              },
              Width: {
                  required: 'Please enter Width',
              },
              Height: {
                  required: 'Please enter Height',
              },
              ServiceSchedule: {
                required: 'Please enter ScheduleDate'
              },
              ServiceComments : {
                required: 'please enter comments for service order',
                minlength : 'please enter atleast 20 charactors'
              }
          },
          submitHandler: this.onSubmitServiceForm

      };
      this.ID = this.route.params['value'].id;

      if (this.ID !== '-1') {
          this.getValue = true;
      }
      else{
          this.BasicInfoForm = fb.group({
              ProjectName: [ '', [Validators.required ] ],
              Customer: [ '', [Validators.required ] ],
              State: [ '', [Validators.required ] ],
              City: [ '', [Validators.required ] ],
              Locality: [ '', [Validators.required] ],
              Address: [ '', [Validators.required ] ],
              CreatedBy: [ this.localStorage.id ],
              CustomerEmail : [ '', [Validators.required ] ],
          });
          this.ServiceInfoForm = fb.group({
              Service: [ '', [Validators.required ] ],
              Width: [ '' , [Validators.required ]],
              Height: [ '', [Validators.required ] ],
              Comments: [ '', [Validators.required ] ],
              ScheduledServiceProvider: [ '', [Validators.required ] ],
          });
          this.PaymentInfoForm = fb.group({
              PaymentMode: [ '', [Validators.required ] ],
              PaymentAmount: [ {value : this.getTotal('') , disabled:true} , [Validators.required ]],
              DiscountType: [ '' ],
              DiscountAmount: [ '' ],
          });
          this.getValue = true;
      }


      this.dropdownList = [
          { item_id: 1, item_text: 'Wooden flooring' },
          { item_id: 2, item_text: 'Artificial Grass' },
          { item_id: 3, item_text: 'Carpet' },
          { item_id: 4, item_text: 'Liminated' },
          { item_id: 5, item_text: 'Engineered' }
      ];
      this.selectedItems = [

      ];
      this.dropdownSettings = {
          singleSelection: true,
          idField: '_id',
          textField: 'FirstName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 2,
          allowSearchFilter: true
      };
      // this.getAllCustomers();
      this.getAllState();
      this.getAllServices();
      console.log(this.Scheduled);

      this.commonService.getGSTValue().subscribe((data: {}) => {
        this.getGSTValue = data['result'];
      });
      
    
  }

  private _filterStates(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.Customers.filter(state => state.FirstName.toLowerCase().includes(filterValue) || state.LastName.toLowerCase().includes(filterValue) || state.Email.toLowerCase().includes(filterValue));
  }
  getID(id){
    this.BasicInfoForm.patchValue({
        'Customer' : id
    })  
    this.customersService.getOneCustomers(id).subscribe( (data : {})=>{
        this.spinnerService.hide();
        if(data['status']){
            this.getAllCitiesByState(data['result'].State);
            this.getAllLocalitiesByCity(data['result'].City);
            this.BasicInfoForm.patchValue({
                State : data['result'].State,
                City : data['result'].City,
                Locality : data['result'].Locality,
                Address : data['result'].Address,
                CustomerEmail : data['result'].Email,
            })
        }
    })
  }

  ngOnInit() {
      this.ID = this.route.params['value'].id;
      this.getUserInfo();
  }

  getUserInfo(){
    this.userService.getOneUsers(this.localStorage.id).subscribe( (data:{})=>{
        if(data['status']){
            console.log(data['result'])
        }
    }, err => {
        this.snackBar.open('Server Error', 'Error', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
  }

    getAllState(){
        this.spinnerService.show();
        this.stateService.getAllStates().subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.States = data['result'];
                console.log(this.States);
            }
            else{

            }
            console.log(data);
        })
    }

    getTotalSize(){
        if(this.ServiceInfoForm.controls.Width.value != '' && this.ServiceInfoForm.controls.Height.value){
            this.Total =  this.ServiceInfoForm.controls.Width.value * this.ServiceInfoForm.controls.Height.value;
        }
        else{
            this.Total = '';
        }
    }

    getAllCustomers(selectedLeaderID) {
        this.Customers = [];
        this.spinnerService.show();
        this.customersService.getAllCustomers(selectedLeaderID).subscribe((data: {}) => {
            this.spinnerService.hide();
            this.Customers = data['result'];
            this.filteredStates = this.stateCtrl.valueChanges
                .pipe( startWith(''),
                    map(state => state ? this._filterStates(state) : this.Customers.slice())
                );

        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getAllServices(){
        this.spinnerService.show();
        this.servicesService.getAllServices().subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Services = data['result'];
                console.log(this.Services);
            }
            else{

            }
            console.log(data);
        })
    }


    getAllCitiesByState(id){
        console.log(this.BasicInfoForm);
        this.Cities = [];
        this.BasicInfoForm.patchValue({
            "Locality": '',
            "City": '',
            "Address": '',
        });
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Cities = data['result'];
            }
            else{

            }
        });
    }

    getAllLocalitiesByCity(id){
        console.log(this.BasicInfoForm);
        this.Localities = [];
        this.BasicInfoForm.patchValue({
            "Locality": ''
        });
        this.spinnerService.show();
        this.localityService.getAllLocalitiesByCity(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Localities = data['result'];
            }
            else{

            }
        });
    }

    getAllSCLByCustomer(id){
        var index = this.Customers.filter(x=> x._id == id);
        this.spinnerService.show();
        this.customersService.getOneCustomers(id).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.getAllCitiesByState(data['result'].State);
                this.getAllLocalitiesByCity(data['result'].City);
                this.BasicInfoForm.patchValue({
                    State : data['result'].State,
                    City : data['result'].City,
                    Locality : data['result'].Locality,
                    Address : data['result'].Address,
                    CustomerEmail : data['result'].Email,
                })
            }
        })
    }

    addService(){

    }


    checkDiscount(){
        
        if(this.DiscountAmount>(this.getTotal('Total')*.2)){
            this.DiscountAmount = this.getTotal('Total')*.2;
        }

    }

    onSubmitProject(){
        console.log(this.BasicInfoForm);
        if(this.BasicInfoForm.invalid){
            return 0;
        }
        this.stepper.next();
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    onSubmit(){
        this.stepper.next();
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    onSubmitServiceForm(){
        console.log(this.ServiceInfoForm.value);
        if(this.ServiceInfoForm.invalid){
            return 0;
        }

        var index = this.ServicesArray.findIndex( x=> x._id == this.ServiceInfoForm.controls.Service.value);
        if(index == -1){
            this.spinnerService.show();
            this.servicesService.getOneServices(this.ServiceInfoForm.controls.Service.value).subscribe( (data : {})=>{
                this.spinnerService.hide();
                if(data['status']){
                    var service = data['result'];
                    service.Width = this.ServiceInfoForm.controls.Width.value;
                    service.Height = this.ServiceInfoForm.controls.Height.value;
                    service.Amount = service.Width * service.Height * service.Price;
                    service.GST = this.getGSTValue;
                    service.TotalAmount = service.Amount; // +((service.Amount * this.getGSTValue)/ 100);
                    service.Comments = this.ServiceInfoForm.controls.Comments.value;
                    service.ScheduledServiceProvider = this.ServiceInfoForm.controls.ScheduledServiceProvider.value;
                    this.ServicesArray.push(service);
                    this.snackBar.open('Service added successfully', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });

                    this.ServiceInfoForm.reset();
                    this.Total = '';
                    this.PaymentInfoForm.patchValue({
                        'PaymentAmount' : this.getTotal('')
                    });
                }
            })
        }
        else{
            this.snackBar.open('You already choose this service.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            this.PaymentInfoForm.patchValue({
                'PaymentAmount' : this.getTotal('')
            });
        }

    }

    getTotal(type){
        var sum = 0;
        for(let service of this.ServicesArray){
            sum = sum + (service.Width *service.Height *service.Price)
        }
        if(type == 'GST'){
            return (sum * this.getGSTValue)/100
        }
        else if(type == 'Total'){
            return sum;
        }
        else{
          // return sum + ((sum * this.getGSTValue)/100);
           return sum - this.DiscountAmount;
        }
    }

    deleteSelectesService(index){
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.ServicesArray.splice(index , 1);
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    getName(id  , name){
        if(name == 'State' && id != ''){
            var sindex = this.States.findIndex(x=>x._id == id);
            if(sindex > -1){
                return this.States[sindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'City' && id != ''){
            var cindex = this.Cities.findIndex(x=>x._id == id);
            if(cindex > -1){
                return this.Cities[cindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'Locality' && id != ''){
            var lindex = this.Localities.findIndex(x=>x._id == id);
            if(lindex > -1){
                return this.Localities[lindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'Customer' && id != ''){
            var lindex = this.Customers.findIndex(x=>x._id == id);
            if(lindex > -1){
                return this.Customers[lindex].FirstName + " " + this.Customers[lindex].MiddleName + " "+ this.Customers[lindex].LastName;
            }
            else{
                return '';
            }
        }
    }


    checkDetail(){
        console.log(this.BasicInfoForm.controls.Customer.value);
    }

    getDateValue(val){
        console.log(val);
        console.log(val);
    }

    saveRecord(){

        /*this.PaymentInfoForm.patchValue({
            'PaymentAmount': this.getTotal('Total')
        })*/
        this.PaymentInfoForm.value.PaymentAmount = this.getTotal('');
        this.PaymentInfoForm.value.DiscountType = this.DiscountType;
        this.PaymentInfoForm.value.DiscountAmount = this.DiscountAmount;

        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
    this.spinnerService.show();
    this.projectsService.addProjects(this.BasicInfoForm.value , this.ServicesArray , this.PaymentInfoForm.value , this.localStorage.RoleName).subscribe((data: {}) => {
        this.spinnerService.hide();
        if (data['status']) {
            this.snackBar.open('Record created successfully', 'success', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
            this.location.back();
        }
    }, err => {

    });

    }

    editExisting(){

    this.notificationService.smartMessageBox({
        title: "Update!",
        content: "Are you sure update this record?",
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") {
            this.spinnerService.show();
            this.projectsService.updateProjects(this.ID, this.BasicInfoForm.value , this.ServicesArray , this.PaymentInfoForm.value ).subscribe((data: {}) => {
                this.spinnerService.hide();
                if ( data['status']) {
                    this.snackBar.open('Record updated successfully', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.location.back();
                }
            }, err => {
                this.spinnerService.hide();
            });
        }
        if (ButtonPressed === "No") {

        }

    });

    }

    register(){
        // this.router.navigate(['/users']);
        this.location.back();
    }

    cancel() {
        // this.router.navigate(['/users']);
        this.location.back();
    }

    role(Role){
        // console.log(this.UserForm.value);
    }

    add(){

    }


    delete(){
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {


            }
            if (ButtonPressed === "No") {

            }

        });
    }


    onItemSelect(item: any) {
        console.log(item);
    }
    onSelectAll(items: any) {
        console.log(items);
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }


    public model = {
        email: '',
        firstname: '',
        lastname: '',
        country: '',
        city: '',
        postal: '',
        wphone: '',
        hphone: ''
    };

    public steps = [
        {
            key: 'step1',
            title: 'Basic information',
            valid: false,
            checked: false,
            submitted: false,
        },
        {
            key: 'step2',
            title: 'Service  information',
            valid: false,
            checked: false,
            submitted: false,
        },
        {
            key: 'step3',
            title: 'Payment',
            valid: true,
            checked: false,
            submitted: false,
        },
        {
            key: 'step4',
            title: 'Finish',
            valid: true,
            checked: false,
            submitted: false,
        },
    ];

    public activeStep = this.steps[0];

    setActiveStep(steo) {
        this.activeStep = steo
    }

    prevStep() {
        let idx = this.steps.indexOf(this.activeStep);
        if (idx > 0) {
            this.activeStep = this.steps[idx - 1]
        }
    }

    nextStep() {
        this.activeStep.submitted = true;
        // if(!this.activeStep.valid){
        //     return;
        // }
        this.activeStep.checked = true;
        if (this.steps.every(it=>(it.valid && it.checked))) {
            this.onWizardComplete(this.model)
        } else {
            let idx = this.steps.indexOf(this.activeStep);
            this.activeStep = null;
            while (!this.activeStep) {
                idx = idx == this.steps.length - 1 ? 0 : idx + 1;
                if (!this.steps[idx].valid || !this.steps[idx].checked ) {
                    this.activeStep = this.steps[idx]
                }
            }
        }
    }


    onWizardComplete(data) {
        console.log('basic wizard complete', data)
    }


    private lastModel;

    // custom change detection
    ngDoCheck() {
        if (!this.lastModel) {
            // backup model to compare further with
            this.lastModel = Object.assign({}, this.model)
        } else {
            if (Object.keys(this.model).some(it=>this.model[it] != this.lastModel[it])) {
                // change detected
                this.steps.find(it=>it.key == 'step1').valid = !!(this.model.email && this.model.firstname && this.model.lastname);
                this.steps.find(it=>it.key == 'step2').valid = !!(this.model.country && this.model.city && this.model.postal);
                this.lastModel = Object.assign({}, this.model)
            }
        }
    }

}
