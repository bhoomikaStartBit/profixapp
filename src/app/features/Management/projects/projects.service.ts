import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  url: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/project/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getAllProjects(): Observable<any> {
    return this.http.get(this.url + 'getAll/').pipe(
      map(this.extractData));
  }

  getForProject(): Observable<any> {
    return this.http.get(this.url + 'getForProject/').pipe(
      map(this.extractData));
  }

  getAllOrderServiceProvicer(id ,Role): Observable<any> {
    return this.http.get(this.url + 'getAllOrderServiceProvicer/'+id+'/'+Role).pipe(
      map(this.extractData));
  }

    getAllOrderPriceUnder5000(id): Observable<any> {
    return this.http.get(this.url + 'getAllOrderPriceUnder5000/'+id).pipe(
      map(this.extractData));
  }
  

  getOneProjects(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }

  addProjects (data , services , payment , userrole): Observable<any> {
      var dat = {
          'Basic':data,
          'Services':services,
          'Payment':payment,
          'UserRole': userrole
      }
      console.log(dat);
      return this.http.post<any>(this.url + 'create', dat, httpOptions).pipe(
          tap(( ) => console.log(`added projects w/ id=${data._id}`)),
          catchError(this.handleError<any>('addProject'))
      );
  }

  updateProjects (id, data ,service , payment): Observable<any> {
    return this.http.put<any>(this.url + 'update/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${data._id}`)),
      catchError(this.handleError<any>('updateCity'))
    );
  }

  deleteProjects (id): Observable<any> {
    return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
      tap(( ) => console.log(`deleted role w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

  assignOrder(data){
      return this.http.post<any>(this.url + 'assignOrder', data, httpOptions).pipe(
          tap(( ) => console.log(`added Order w/ id=${data._id}`)),
          catchError(this.handleError<any>('addOrder'))
      );
  }

    acceptOrder(data){
      return this.http.post<any>(this.url + 'acceptOrder', data, httpOptions).pipe(
          tap(( ) => console.log(`added Order w/ id=${data._id}`)),
          catchError(this.handleError<any>('addOrder'))
      );
  }

    submitOrderBySP(data){
      return this.http.post<any>(this.url + 'submitOrderBySP', data, httpOptions).pipe(
          tap(( ) => console.log(`added Order w/ id=${data._id}`)),
          catchError(this.handleError<any>('addOrder'))
      );
  }

    paymentCollect(data){
      return this.http.post<any>(this.url + 'paymentCollect', data, httpOptions).pipe(
          tap(( ) => console.log(`added Order w/ id=${data._id}`)),
          catchError(this.handleError<any>('addOrder'))
      );
  }

    createMilestoneAndAssignOrder(data){
      return this.http.post<any>(this.url + 'createMilestoneAndAssignOrder', data, httpOptions).pipe(
          tap(( ) => console.log(`added Order w/ id=${data._id}`)),
          catchError(this.handleError<any>('addOrder'))
      );
  }
  
   getAllOrderActivitiesByOrderID(id): Observable<any> {
        return this.http.get(this.url + 'getAllOrderActivitiesByOrderID/'+id).pipe(
            map(this.extractData));
    }


    orderPayInfo(OrderID , UserID): Observable<any> {
        return this.http.get(this.url + 'getInfoPayment/' + OrderID + "/"+UserID).pipe(
            map(this.extractData));
    }



    deleteOrderMilestone (id): Observable<any> {
        return this.http.get<any>(this.url + 'deleteOrderMilestone/' + id, httpOptions).pipe(
            map(this.extractData));
    }


    sendReview(data){
        return this.http.post<any>(this.url + 'sendReview', data, httpOptions).pipe(
            tap(( ) => console.log(`added sendReview w/ id=${data._id}`)),
            catchError(this.handleError<any>('sendReview'))
        );
    }


    getInfoReview(OrderID , UserID): Observable<any> {
        return this.http.get(this.url + 'getInfoReview/' + OrderID + "/"+UserID).pipe(
            map(this.extractData));
    }

    getAllReviews(OrderID , UserID): Observable<any> {
        return this.http.get(this.url + 'getReviews/' + OrderID + "/"+UserID).pipe(
            map(this.extractData));
    }

    reviewApproved(OrderAssignID ): Observable<any> {
        return this.http.get(this.url + 'reviewApproved/' + OrderAssignID ).pipe(
            map(this.extractData));
    }

    disapproveReview(OrderAssignID ): Observable<any> {
        return this.http.get(this.url + 'disapproveReview/' + OrderAssignID ).pipe(
            map(this.extractData));
    }


  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
