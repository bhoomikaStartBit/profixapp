import {Component, OnInit, ElementRef, ViewChild, AfterViewChecked  } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {ProjectsService} from "@app/features/Management/projects/projects.service";
import * as $ from 'jquery';
import {Router} from "@angular/router";
import {CommonService} from "@app/core/common/common.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { AuthenticationService } from '@app/core/common/_services/authentication.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'projectslist',
  templateUrl: './projects.component.html',
})
export class ProjectsComponent implements OnInit , AfterViewChecked {
    Projects : any = [];
    getData =false;
    called = false;
    searchControl = new FormControl();
    ProjectUrl : any = '';
    searchForm: any;
    selectedorderStatus: any;

    limit:number = 50;
    pageIndex : number = 0;
    pageLimit:number[] = [50,100,150];
    
    CurrentLoginUser = JSON.parse(localStorage.getItem('currentUser'));

    @ViewChild('button1') button1: ElementRef;


    displayedColumns: string[] = [

        'SN',
        'Name',
        'AssignTo',
        'Customer',
        'Status',
        'ScheduledDate',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    dataSourceOrder: MatTableDataSource<any>;

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private projectsService: ProjectsService,
      private router : Router,
      private commonService : CommonService,
      private fb: FormBuilder,
      private authenticationService : AuthenticationService,
      private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
    setTimeout(() => {
      this.authenticationService.checkSelectedComponentVisiblity('Orders');
    }, 100);
      this.CurrentLoginUser = JSON.parse(localStorage.getItem('currentUser'));
      if(this.CurrentLoginUser.RoleName == 'Super Admin'){
          this.getAllOrderServiceProvicer('0' , this.CurrentLoginUser.RoleName);

      }
      else{
          this.getAllOrderServiceProvicer(this.CurrentLoginUser.id , this.CurrentLoginUser.RoleName);

      }

      //this.getAllOrderPriceUnder5000();

  }

     ngOnInit() {

      }

      getOrderbyStatus(){
        // console.log(this.searchControl.value);
         this.selectedorderStatus = this.searchControl.value;
 
         if(this.selectedorderStatus){
             
             const filterValue = this.selectedorderStatus.toLowerCase();
             const filteredSet = this.Projects.filter(option => option.Status.toLowerCase().includes(filterValue));
             this.dataSource = new MatTableDataSource(filteredSet);
             this.dataSource.paginator = this.paginator;
             this.dataSource.sort = this.sort;

             this.pageIndex = 0;
         }
     }

    ngAfterViewChecked(){
        $("button.dataTableProject").on('click',function(){
            open($(this).attr('data-id'));
        })
        $("button.kc").on('mouseup',function(){
            data();
        })

        var open = (id)=> {
            if(!this.called) {
                this.open(id);
            }
        }
        var data = ()=>{
            this.called = false;
        }
    }


  getAllProjects(){
    this.spinnerService.show();
      this.projectsService.getAllProjects().subscribe( (data : {})=>{
        this.spinnerService.hide();
          if(data['status']){
              this.getData = true;
            this.Projects = data['data'];
          }
      })
  }
    getAllOrderServiceProvicer(id , Role){
      this.spinnerService.show();
      this.projectsService.getAllOrderServiceProvicer(id , Role).subscribe( (data : {})=>{
        this.spinnerService.hide();
          if(data['status']){
              this.getData = true;
              if(Role == 'Super Admin'){
                this.Projects = data['data'];
                // this.dataSource = new MatTableDataSource(data['data']);
               this.searchControl.setValue('New');
               const filterValue = 'new';
               const filteredSet = this.Projects.filter(option => option.Status.toLowerCase().includes(filterValue));
               this.dataSource = new MatTableDataSource(filteredSet);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
              }
              else{
                this.Projects = data['data'];            
               this.dataSource = new MatTableDataSource(data['data']);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
              }
              
          }
      })
  }
    getAllOrderPriceUnder5000(){
      this.spinnerService.show();
      this.projectsService.getAllOrderPriceUnder5000(this.CurrentLoginUser.id).subscribe( (data : {})=>{
        this.spinnerService.hide();
        if(data['status']){
            
                this.dataSourceOrder = new MatTableDataSource(data['data']);
                this.dataSourceOrder.paginator = this.paginator;
                this.dataSourceOrder.sort = this.sort;
          }
      })
  }

    open(id){
      console.log(id);
        this.called = true;
        this.router.navigate(['/projects/detail/'+id ])
    }

  
  delete(){
      this.notificationService.smartMessageBox({
          title: "Delete Alert!",
          content: "Are you sure delete this record?",
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") {


          }
          if (ButtonPressed === "No") {

          }

      });
  }

  changePage(event){
    this.pageIndex = event.pageSize*event.pageIndex;
  }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
/*
this.notificationService.smartMessageBox({
      title: "Smart Alert!",
      content: "This is a confirmation box. Can be programmed for button callback",
      buttons: '[No][Yes]'
    }, (ButtonPressed) => {
      if (ButtonPressed === "Yes") {

        this.notificationService.smallBox({
          title: "Callback function",
          content: "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",
          color: "#659265",
          iconSmall: "fa fa-check fa-2x fadeInRight animated",
          timeout: 4000
        });
      }
      if (ButtonPressed === "No") {
        this.notificationService.smallBox({
          title: "Callback function",
          content: "<i class='fa fa-clock-o'></i> <i>You pressed No...</i>",
          color: "#C46A69",
          iconSmall: "fa fa-times fa-2x fadeInRight animated",
          timeout: 4000
        });
      }

    });
* */