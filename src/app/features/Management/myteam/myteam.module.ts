import {NgModule} from "@angular/core";

import {routing} from "./myteam.routing";
import {MyTeamsComponent} from "@app/features/Management/myteam/myteam.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      MyTeamsComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class MyTeamsModule {

}
