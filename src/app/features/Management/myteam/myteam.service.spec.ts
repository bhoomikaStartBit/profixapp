import { TestBed } from '@angular/core/testing';

import { MyTeamsService} from "@app/features/Management/myteams/myteams.service";

describe('MyTeamsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyTeamsService = TestBed.get(MyTeamsService);
    expect(service).toBeTruthy();
  });
});
