import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class MyTeamsService {
  url: string;
    urlTeamRequest: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/profile/';
    this.urlTeamRequest = commService.getApiUrl() + '/team-request/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getRequestDetailByUserID(id): Observable<any> {
    return this.http.get(this.url + 'getRequestDetailByUserID/' + id).pipe(
      map(this.extractData));
  }

  getTeamDetailByUserID(id): Observable<any> {
    return this.http.get(this.url + 'getTeamDetailByUserID/' + id).pipe(
      map(this.extractData));
  }
  
  updateMyTeam(id, data): Observable<any> {
    return this.http.put<any>(this.url + 'update/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log('update')),
      catchError(this.handleError<any>('updateMyTeam'))
    );
  }

  sendRequestForTeam(id): Observable<any> {
      return this.http.get(this.url + 'sendRequestForTeam/' + id).pipe(
          map(this.extractData));
  }

    requestForDisposal(data): Observable<any> {
        return this.http.post<any>(this.urlTeamRequest + 'requestForDisposal', data, httpOptions).pipe(
            tap(( ) => console.log(`added`)),
            catchError(this.handleError<any>('sendTeamMemberInvitation'))
        );
    }

    getDisposalRequestDetail(UserID, TeamID): Observable<any> {
        return this.http.get(this.urlTeamRequest + 'getDisposalRequestDetail/' + UserID + '/' + TeamID).pipe(
            map(this.extractData));
    }

    getAllPendingrequestForDisposal(): Observable<any> {
        return this.http.get(this.urlTeamRequest + 'getAllPendingrequestForDisposal').pipe(
            map(this.extractData));
    }

    rejectDisposalRequest(id): Observable<any> {
        return this.http.get(this.urlTeamRequest + 'rejectDisposalRequest/' + id).pipe(
            map(this.extractData));
    }

    acceptDisposalRequest(id, TeamID): Observable<any> {
        return this.http.get(this.urlTeamRequest + 'acceptDisposalRequest/' + TeamID + '/' + id).pipe(
            map(this.extractData));
    }


  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
