import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "@app/core/services";
import {MatSnackBar, MatStepper} from "@angular/material";
import {StateService} from "@app/features/Management/state/state.service";
import {CityService} from "@app/features/Management/city/city.service";
import {LocalityService} from "@app/features/Management/locality/locality.service";
import {TeamCompanyService} from "@app/features/Management/team-company/team-company.service";
import {MyTeamsService} from "@app/features/Management/myteam/myteam.service";
import { UserService} from "@app/features/Management/users/user.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';


import { trigger,
    state,
    style,
    transition,
    animate} from '@angular/animations'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'my-team',
    templateUrl: './myteam.component.html',
    animations: [
        trigger('changePane', [
            state('out', style({
                height: 0,
            })),
            state('in', style({
                height: '*',
            })),
            transition('out => in', animate('250ms ease-out')),
            transition('in => out', animate('250ms 300ms ease-in'))
        ])
    ]
})
export class MyTeamsComponent implements OnInit {
    displayedColumns: string[] = [
        'SN',
        'UserName',
        'RequestDate',
        'ActionDate',
        'Status'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    ID: any;
    getValue = false;
    States: any;
    Cities: any;
    pinlength = 6;
    Localities: any;
    TeamCompanyBasicInfoForm: FormGroup;
    currentUser: any;
    myTeamMemberArray: any = []

    @ViewChild('stepper') stepper1: MatStepper;

    public validationOptions:any = {};
    userRequestDetail: any = {};
    userTeamDetail: any = {};
    allUsers: any = [];
    TeamMember = '';
    selectedCompany: any = {};
    isDisposalRequestData: any = {};
    constructor(private http: HttpClient,
                private router : Router,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private location: Location,
                private notificationService: NotificationService,
                private stateService: StateService,
                private cityService: CityService,
                private localityService: LocalityService,
                private snackBar: MatSnackBar,
                private teamCompanyService: TeamCompanyService,
                private myteamService: MyTeamsService,
                private userService: UserService,
                private spinnerService: Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();    
        this.validationOptions = {
            // Rules for form validation
            rules: {
                CompanyName: {
                    required: true
                },
                GSTIN: {
                    required: true
                },
                TAN: {
                    required: true
                },
                Registration: {
                    required: true
                },
                IsTeamCompany: {
                    required: true
                },
                Email: {
                    required: true,
                    email: true
                },
                Phone: {
                    required: true,
                    pattern: "[0-9]+",
                    minlength:10,
                    maxlength:10
                },
                AltPhone: {
                    pattern: "[0-9]+",
                    minlength:10,
                    maxlength:10
                },
                State: {
                    required: true
                },
                City: {
                    required: true
                },
                Locality: {
                    required: true
                },
                Pin: {
                    required: true
                },
                Address: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                CompanyName: {
                    required: 'Please enter your company name'
                },
                GSTIN: {
                    required: 'Please enter your company GSTIN No.'
                },
                TAN: {
                    required: 'Please enter your company TAN name'
                },
                Registration: {
                    required: 'Please enter your company registration name'
                },
                IsTeamCompany: {
                    required: 'Please select IsTeamCompany'
                },
                Email: {
                    required: 'Please enter your email address',
                    email: '<i class="fa fa-warning"></i> &nbsp;Please enter a valid email addres'
                },
                Phone: {
                    required: 'Please enter your phone number',
                    minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                    maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
                },
                AltPhone: {
                    minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                    maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
                },
                State: {
                    required: 'Please select the state'
                },
                City: {
                    required: 'Please select the city'
                },
                Locality: {
                    required: 'Please select the locality'
                },
                Pin: {
                    required: 'Please enter pincode'
                },
                Address: {
                    required: 'Please enter address'
                }
            },
            submitHandler: this.onSubmitTeamForm

        };
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.ID = this.route.params['value'].id;
        this.TeamCompanyBasicInfoForm = fb.group({
            CompanyName: ['', [Validators.required]],
            IsTeamCompany: ['Team', [Validators.required]],
            GSTIN: ['', [Validators.required]],
            TAN: ['', [Validators.required]],
            Registration: ['', [Validators.required]],
            Email: ['', [Validators.required, Validators.email]],
            Phone: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
            AltPhone: ['', [ Validators.min(1000000000), Validators.max(9999999999)]],
            Address: ['', [Validators.required]],
            Pin: ['', [Validators.required , Validators.maxLength(6), Validators.max(999999) ]],
            Locality: ['', [Validators.required]],
            City: ['', [Validators.required]],
            State: ['', [Validators.required]],
            Owner: [this.currentUser.id],
        });
        this.getValue = true;
        this.getAllState();
        this.ID = this.route.params['value'].id;
        this.getRequestDetailByUserID();
        this.getTeamDetailByUserID();
        this.getAllUserWithSpRole();
        this.getOne();
    }

    ngOnInit() {
        this.ID = this.route.params['value'].id;
    }

    getDisposalRequestDetail(){
        this.spinnerService.show();
        this.myteamService.getDisposalRequestDetail(this.currentUser.id, this.selectedCompany._id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.isDisposalRequestData = data['result'];
            }
        });
    }

    getOne() {
        this.spinnerService.show();
        this.teamCompanyService.getOneTeamCompanyByOwner(this.currentUser.id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.selectedCompany = data['result'];
                if(this.selectedCompany) {
                    this.getAllCitiesByState(data['result'].State);
                    this.getAllLocalitiesByCity(data['result'].City);
                    this.TeamCompanyBasicInfoForm = this.fb.group({
                        CompanyName: [ data['result'].CompanyName, [Validators.required ] ],
                        IsTeamCompany: [ data['result'].IsTeamCompany, [Validators.required ] ],
                        GSTIN: [ data['result'].GSTIN, [Validators.required ] ],
                        TAN: [ data['result'].TAN, [Validators.required ] ],
                        Registration: [ data['result'].Registration, [Validators.required ] ],
                        Email: [ data['result'].Email, [Validators.required, Validators.email  ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.min(1000000000), Validators.max(9999999999) ] ],
                        AltPhone: [ data['result'].AltPhone, [ Validators.min(1000000000), Validators.max(9999999999) ] ],
                        Address: [ data['result'].Address, [Validators.required ] ],
                        Pin: [ data['result'].Pin, [Validators.required, Validators.maxLength(6), Validators.max(999999)  ] ],
                        Locality: [ data['result'].Locality, [Validators.required ] ],
                        City: [ data['result'].City, [Validators.required ] ],
                        State: [ data['result'].State, [Validators.required ] ],
                        Owner: [ data['result'].Owner ],
                    });
                    this.getAllTeamMemberByTeam();
                    this.getDisposalRequestDetail();
                }
                this.getValue = true;
            }
        }, err => {
            this.spinnerService.hide();
        });
    }

    getAllUserWithSpRole(){
        this.spinnerService.show();
        this.userService.getAllUserWithSpRole().subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.allUsers = data['result'];
            }
        });
    }

    getRequestDetailByUserID(){
        this.spinnerService.show();
        this.myteamService.getRequestDetailByUserID(this.currentUser.id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.userRequestDetail = data['result'];
                this.TeamCompanyBasicInfoForm.patchValue({
                    'IsTeamCompany' : this.userRequestDetail ? this.userRequestDetail.type : 'Team'
                });
                this.TeamCompanyBasicInfoForm.get('IsTeamCompany').disabled;
            }
        }, err => {
            this.spinnerService.hide();
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getTeamDetailByUserID(){
        this.spinnerService.show();
        this.myteamService.getTeamDetailByUserID(this.currentUser.id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.userTeamDetail = data['result'];
                this.TeamCompanyBasicInfoForm.patchValue({
                    'IsTeamCompany' : this.userTeamDetail ? this.userTeamDetail.IsTeamCompany : 'Team'
                });
                this.TeamCompanyBasicInfoForm.get('IsTeamCompany').disabled;
            }
        }, err => {
            this.spinnerService.hide();
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllState(){
        this.spinnerService.show();
        this.stateService.getAllStates().subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.States = data['result'];
                console.log(this.States);
            }
            else{

            }
        })
    }

    getAllCitiesByState(id){
        this.Cities = [];
        this.TeamCompanyBasicInfoForm.patchValue({
            "Locality": '',
            "City": '',
        });
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Cities = data['result'];
            }
            else{

            }
        });
    }

    getAllLocalitiesByCity(id){
        this.Localities = [];
        this.TeamCompanyBasicInfoForm.patchValue({
            "Locality": ''
        });
        this.spinnerService.show();
        this.localityService.getAllLocalitiesByCity(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Localities = data['result'];
            }
            else{

            }
        });
    }

    getName(id  , name){
        if(name == 'state' && id != ''){
            var sindex = this.States.findIndex(x=>x._id == id);
            if(sindex > -1){
                return this.States[sindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'city' && id != ''){
            var cindex = this.Cities.findIndex(x=>x._id == id);
            if(cindex > -1){
                return this.Cities[cindex].Name;
            }
            else{
                return '';
            }
        }
        if(name == 'locality' && id != ''){
            var lindex = this.Localities.findIndex(x=>x._id == id);
            if(lindex > -1){
                return this.Localities[lindex].Name;
            }
            else{
                return '';
            }
        }
    }

    onSubmitTeamForm(){

    }

    save(){
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    createNew(){
        this.spinnerService.show();
        this.teamCompanyService.addTeamCompany(this.TeamCompanyBasicInfoForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Thanks, Your information is successfully submitted', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getRequestDetailByUserID();
                this.getOne()
            }
        }, err => {
            this.spinnerService.hide();
        });
    }

    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure resend approval request?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.updateTeamCompany(this.selectedCompany._id, this.TeamCompanyBasicInfoForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/team-company']);
                    }
                }, err => {
                    this.spinnerService.hide();
                });
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    register(){
        // this.router.navigate(['/users']);
        this.location.back();
    }

    cancel() {
        // this.router.navigate(['/users']);
        this.location.back();
    }

    getAllTeamMemberByTeam(){
        this.spinnerService.show();
        this.teamCompanyService.getAllTeamMemberByTeam(this.selectedCompany._id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.myTeamMemberArray = data['result'];
                this.dataSource = new MatTableDataSource(data['result']);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            }
        }, err => {
            this.spinnerService.hide();
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    sendTeamMemberInvitation(){
        this.spinnerService.show();
        this.teamCompanyService.sendTeamMemberInvitation(this.TeamMember, this.selectedCompany._id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.TeamMember = '';
                this.snackBar.open('Successfully send invitation', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getAllTeamMemberByTeam();
            } else {
                this.TeamMember = '';
                this.snackBar.open(data['message'], 'success', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }


    delete(){
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {


            }
            if (ButtonPressed === "No") {

            }

        });
    }

    requestForDisposal() {
        this.notificationService.smartMessageBox({
            title: "Delete Request!",
            content: "Are you sure to delete? This process is irrevocable. All data will be deleted.",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                const data = {
                    TeamID: this.selectedCompany._id,
                    UserID: this.currentUser.id,
                    type: this.selectedCompany.IsTeamCompany
                }
                this.spinnerService.show();
                this.myteamService.requestForDisposal(data).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.TeamMember = '';
                        this.snackBar.open('Successfully send invitation', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllTeamMemberByTeam();
                        this.getDisposalRequestDetail();
                    } else {
                        this.TeamMember = '';
                        this.snackBar.open(data['message'], 'success', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                }, err => {
                    console.log(err);
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', 'success', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
    public data = {
        "Afghanistan":16.63,"Albania":11.58,"Algeria":158.97,"Angola":85.81,"Antigua and Barbuda":1.1,"Argentina":351.02,"Armenia":8.83,"Australia":1219.72,"Austria":366.26,"Azerbaijan":52.17,"Bahamas":7.54,"Bahrain":21.73,"Bangladesh":105.4,"Barbados":3.96,"Belarus":52.89,"Belgium":461.33,"Belize":1.43,"Benin":6.49,"Bhutan":1.4,"Bolivia":19.18,"Bosnia and Herzegovina":16.2,"Botswana":12.5,"Brazil":2023.53,"Brunei":11.96,"Bulgaria":44.84,"Burkina Faso":8.67,"Burundi":1.47,"Cambodia":11.36,"Cameroon":21.88,"Canada":1563.66,"Cape Verde":1.57,"Central African Republic":2.11,"Chad":7.59,"Chile":199.18,"China":5745.13,"Colombia":283.11,"Comoros":0.56,"Costa Rica":35.02,"Croatia":59.92,"Cyprus":22.75,"Czech Republic":195.23,"Democratic Republic of the Congo":12.6,"Denmark":304.56,"Djibouti":1.14,"Dominica":0.38,"Dominican Republic":50.87,"East Timor":0.62,"Ecuador":61.49,"Egypt":216.83,"El Salvador":21.8,"Equatorial Guinea":14.55,"Eritrea":2.25,"Estonia":19.22,"Ethiopia":30.94,"Fiji":3.15,"Finland":231.98,"France":2555.44,"Gabon":12.56,"Gambia":1.04,"Georgia":11.23,"Germany":3305.9,"Ghana":18.06,"Greece":305.01,"Grenada":0.65,"Guatemala":40.77,"Guinea":4.34,"Guinea-Bissau":0.83,"Guyana":2.2,"Haiti":6.5,"Honduras":15.34,"Hong Kong":226.49,"Hungary":132.28,"Iceland":12.77,"India":1430.02,"Indonesia":695.06,"Iran":337.9,"Iraq":84.14,"Ireland":204.14,"Israel":201.25,"Italy":2036.69,"Ivory Coast":22.38,"Jamaica":13.74,"Japan":5390.9,"Jordan":27.13,"Kazakhstan":129.76,"Kenya":32.42,"Kiribati":0.15,"Kuwait":117.32,"Kyrgyzstan":4.44,"Laos":6.34,"Latvia":23.39,"Lebanon":39.15,"Lesotho":1.8,"Liberia":0.98,"Libya":77.91,"Lithuania":35.73,"Luxembourg":52.43,"Macedonia":9.58,"Madagascar":8.33,"Malawi":5.04,"Malaysia":218.95,"Maldives":1.43,"Mali":9.08,"Malta":7.8,"Mauritania":3.49,"Mauritius":9.43,"Mexico":1004.04,"Moldova":5.36,"Mongolia":5.81,"Montenegro":3.88,"Morocco":91.7,"Mozambique":10.21,"Myanmar":35.65,"Namibia":11.45,"Nepal":15.11,"Netherlands":770.31,"New Zealand":138,"Nicaragua":6.38,"Niger":5.6,"Nigeria":206.66,"Norway":413.51,"Oman":53.78,"Pakistan":174.79,"Panama":27.2,"Papua New Guinea":8.81,"Paraguay":17.17,"Peru":153.55,"Philippines":189.06,"Poland":438.88,"Portugal":223.7,"Qatar":126.52,"Republic of the Congo":11.88,"Romania":158.39,"Russian Federation":3476.91,"Rwanda":5.69,"Saint Kitts and Nevis":0.56,"Saint Lucia":1,"Saint Vincent and the Grenadines":0.58,"Samoa":0.55,"Sao Tome and Principe":0.19,"Saudi Arabia":434.44,"Senegal":12.66,"Serbia":38.92,"Seychelles":0.92,"Sierra Leone":1.9,"Singapore":217.38,"Slovakia":86.26,"Slovenia":46.44,"Solomon Islands":0.67,"South Africa":354.41,"South Korea":986.26,"Spain":1374.78,"Sri Lanka":48.24,"Sudan":65.93,"Suriname":3.3,"Swaziland":3.17,"Sweden":444.59,"Switzerland":522.44,"Syria":59.63,"Taiwan":426.98,"Tajikistan":5.58,"Tanzania":22.43,"Thailand":312.61,"Togo":3.07,"Tonga":0.3,"Trinidad and Tobago":21.2,"Tunisia":43.86,"Turkey":729.05,"Turkmenistan":0,"Uganda":17.12,"Ukraine":136.56,"United Arab Emirates":239.65,"United Kingdom":2258.57,"United States":6624.18,"Uruguay":40.71,"Uzbekistan":37.72,"Vanuatu":0.72,"Venezuela":285.21,"Vietnam":101.99,"Yemen":30.02,"Zambia":15.69,"Zimbabwe":5.57, "Bolivia, Plurinational State of":121.34,"Somalia": 0.47,"Tanzania, United Republic of": 0.78,"South Sudan": 0.98,"Congo, the Democratic Republic of the": 1.45
    };


}
