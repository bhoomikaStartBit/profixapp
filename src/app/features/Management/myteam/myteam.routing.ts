
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {MyTeamsComponent} from "@app/features/Management/myteam/myteam.component";


export const routes:Routes = [

  {
    path: '',
    component: MyTeamsComponent
  }


];

export const routing = RouterModule.forChild(routes);
