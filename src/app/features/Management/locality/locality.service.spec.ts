import { TestBed } from '@angular/core/testing';

import { LocalityService} from "@app/features/Management/locality/locality.service";

describe('LocalitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalityService = TestBed.get(LocalityService);
    expect(service).toBeTruthy();
  });
});
