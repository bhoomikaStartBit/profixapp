import { Component, OnInit , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {NotificationService} from "@app/core/services";
import {LocalityService} from "@app/features/Management/locality/locality.service";
import {StateService} from "@app/features/Management/state/state.service";
import {CityService} from "@app/features/Management/city/city.service";

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'userlist',
  templateUrl: './locality.component.html',
})
export class localityComponent implements OnInit {

    Localities :any;
    Cities :any;
    States :any;
    displayedColumns: string[] = [
        'SN',
        'Name',
        'CityName',
        'StateName',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;

    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


  constructor(private http: HttpClient,
              private notificationService: NotificationService,
              private localityService: LocalityService,
              private stateService: StateService,
              private cityService: CityService,
              private snackBar: MatSnackBar,
              private spinnerService : Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.getAllLocality();

  }

  ngOnInit() {

  }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    getAllLocality(){
        this.spinnerService.show();
        this.localityService.getAllLocalities().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Localities = data['result'];
            console.log(this.Localities);
            this.dataSource = new MatTableDataSource(data['result']);

            this.dataSource.paginator = this.paginator;
            this.dataSource.sortingDataAccessor = (obj, property) => this.getProperty(obj, property);
            this.dataSource.sort = this.sort;

            /*console.log(this.Localities);*/
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getProperty = (obj, path) => (
        path.split('.').reduce((o, p) => o && o[p], obj)
    )


    delete(id){
        this.spinnerService.show();
        this.localityService.getRelatedRecordOfLocality(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                var lab ='';
                if(data['result'].length){
                    lab = 'You can\'t delte this record because this is having many dependant '+ data['Table'] + ' records. Please delete all dependant records first.';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[Ok]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Ok") {

                        }
                    });
                }
                else{
                    lab = 'Are you sure delete this record?';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[No][Yes]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Yes") {
                            this.spinnerService.show();
                            this.localityService.deleteLocality(id).subscribe((data: {}) => {
                                this.spinnerService.hide();
                                if (data['status']) {
                                    this.snackBar.open('Record deleted successfully', 'Delete', {
                                        duration: 5000,
                                        panelClass: ['danger-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                } else {
                                }
                                this.getAllLocality();
                            }, err => {
                                this.spinnerService.hide();
                                this.snackBar.open('Server Error', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                              })
                        }
                        if (ButtonPressed === "No") {

                        }

                    });
                }

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

}
