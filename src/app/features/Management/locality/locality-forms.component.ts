import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {CityService} from "@app/features/Management/city/city.service";
import {StateService} from "@app/features/Management/state/state.service";
import {LocalityService} from "@app/features/Management/locality/locality.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'Locality-form',
  templateUrl: './locality-forms.component.html',
})
export class localityFormComponent implements OnInit {
    ID : any;
    States : any;
    Cities : any;
    LocalityForm : FormGroup;    
  constructor(private http: HttpClient,
                private router : Router ,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private cityService: CityService,
                private stateService: StateService,
                private localityService: LocalityService,
                private location: Location,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.LocalityForm = fb.group({
          Name: [ '', [Validators.required ] ],
          State: [ '', [Validators.required ] ],
          City: [ '', [Validators.required ] ],
      });

      this.ID = this.route.params['value'].id;
      console.log(this.ID);
      this.getAllState();
      if (this.ID !== '-1') {
        this.getOne(this.ID);
      }
  }

  ngOnInit() {

  }
  getAllState(){
      this.spinnerService.show();
      this.stateService.getAllStates().subscribe( (data: {}) => {
          this.spinnerService.hide();
          this.States = data['result'];
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
  }
  getAllCitiesByState(id){
      this.Cities = [];
      this.spinnerService.show();
      this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
        this.spinnerService.hide();
          this.Cities = data['result'];
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
  }

    onSubmitForm() {
        if (this.LocalityForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.localityService.addLocality(this.LocalityForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })

    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.localityService.updateLocality(this.ID, this.LocalityForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    getOne(id) {
        this.spinnerService.show();
        this.localityService.getOneLocality(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.getAllCitiesByState(data['result'].State);
                this.LocalityForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    State: [  data['result'].State, [Validators.required ] ],
                    City: [  data['result'].City, [Validators.required ] ],
                });

                console.log(this.LocalityForm);
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }

    register(){
        // this.router.navigate(['/localities']);
        this.location.back();
    }

    cancel() {
        // this.router.navigate(['/localities']);
        this.location.back();
    }

}
