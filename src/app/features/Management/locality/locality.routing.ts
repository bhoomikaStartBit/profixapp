
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {localityComponent} from "@app/features/Management/locality/locality.component";
import {localityFormComponent} from "@app/features/Management/locality/locality-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: localityComponent,
      pathMatch: 'full'
  },{
    path: 'createnew/:id',
    component: localityFormComponent,
        pathMatch: 'full'
  },


];

export const routing = RouterModule.forChild(routes);
