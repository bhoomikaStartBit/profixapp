import {NgModule} from "@angular/core";

import {routing} from "./locality.routing";
import {localityComponent} from "@app/features/Management/locality/locality.component";
import {localityFormComponent} from "@app/features/Management/locality/locality-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";

import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      localityComponent,
      localityFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class LocalityModule {

}
