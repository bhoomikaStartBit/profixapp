import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoleService} from "@app/features/Management/roles/role.service";
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "@app/core/services";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {UserService} from "@app/features/Management/users/user.service";

@Component({
  selector: 'user-form',
  templateUrl: './role-forms.component.html',
})
export class RoleFormComponent implements OnInit {
    ID : any;
    HiddenRole : any;
    RoleForm : FormGroup;

    SuperAdminRoleID: any;
    CompanyManagerRoleID: any;
    ServiceProviderRoleID: any;
    VendorRoleID: any;
    PaymentCollectorRoleID: any;
    QualityInspectorRoleID: any;
    CustomerRoleID: any;   
  constructor(private http: HttpClient,
              private router : Router,
              private route : ActivatedRoute,
              private location : Location,
              private fb: FormBuilder,
              private snackBar: MatSnackBar,
              private userService: UserService,
              private roleService: RoleService,
              private notificationService: NotificationService,
              private spinnerService : Ng4LoadingSpinnerService

  ) {
      this.userService.getRoleIDByKey('CM').subscribe( (data)=>{ 
        this.CompanyManagerRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('SA').subscribe( (data)=>{ 
        this.SuperAdminRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('SP').subscribe( (data)=>{ 
        this.ServiceProviderRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('V').subscribe( (data)=>{ 
        this.VendorRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('PC').subscribe( (data)=>{ 
        this.PaymentCollectorRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('QI').subscribe( (data)=>{ 
        this.QualityInspectorRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('C').subscribe( (data)=>{ 
        this.CustomerRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      
      this.spinnerService.hide();
      this.HiddenRole = [this.SuperAdminRoleID, this.CompanyManagerRoleID , this.QualityInspectorRoleID , this.VendorRoleID , this.CustomerRoleID , this.PaymentCollectorRoleID , this.ServiceProviderRoleID];
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          if(this.HiddenRole.indexOf(this.ID) > -1){
              this.location.back();
          }
          else{
              this.getOne(this.ID);
          }

      }
      this.RoleForm = this.fb.group({
          Name: [ '', [Validators.required ] ],
          Description: [ '', [Validators.required ] ],
      });
  }

  ngOnInit() {
  }

    onSubmitForm() {
        if (this.RoleForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.roleService.addRole(this.RoleForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                console.log("Created successfully");
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });

    }
    editExisting(){
        if(this.HiddenRole.indexOf(this.ID) > -1){
            this.snackBar.open('This record is not update.', 'Notice', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        else{

        }
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.roleService.updateRole(this.ID, this.RoleForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        console.log("update success");
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
                // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.roleService.getOneRole(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.RoleForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    Description: [  data['result'].Description, [Validators.required ] ],
                });

                console.log(this.RoleForm);
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }


    register(){
        // this.router.navigate(['/roles']);
        this.location.back();
    }

    // this.router.navigate(['/roles']);

    cancel() {
        this.location.back();
    }

}
