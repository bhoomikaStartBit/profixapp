import { Component, OnInit ,ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {NotificationService} from "@app/core/services";
import {RoleService} from "@app/features/Management/roles/role.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'userlist',
  templateUrl: './role.component.html',
})
export class RoleComponent implements OnInit {


    Roles : any;
    displayedColumns: string[] = [
        'SN',
        'Name',
        'Action'
    ];
    HiddenRole :any;
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private roleService: RoleService,
      private snackBar: MatSnackBar,
      private spinnerService : Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.getAllRole();
      this.HiddenRole = ['5cbd51bd56d3cb1e606a676e' ,'5d0a16fdb457473128b080be' , '5cb7fe746d2f510b58886610' , '5cb7fe5c6d2f510b5888660f' , '5cb7fe496d2f510b5888660e' , '5cb5b2bb7d799119b01c6226' , '5cb5b2b17d799119b01c6225'];
  }

  ngOnInit() {
  }

    getAllRole(){
        this.spinnerService.show();
        this.roleService.getAllRoles().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Roles = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            if(data['result'].length == 0){
                this.snackBar.open('No record found.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            console.log(this.Roles);
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    checkRole(id){
        if(this.HiddenRole.indexOf(id) > -1){
            return false;
        }
        else{
            return true;
        }
    }


    delete(id){
        if(this.HiddenRole.indexOf(id) > -1){
            this.snackBar.open('This record can not deleted', 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        else{

        }
        console.log(id);
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: "Are you sure delete this record and records related to it?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.roleService.deleteRole(id).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record delete successfully', 'Delete', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    } else {
                    }
                    this.getAllRole();
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

}
