
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {RoleComponent} from "@app/features/Management/roles/role.component";
import {RoleFormComponent} from "@app/features/Management/roles/role-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: RoleComponent
  },{
    path: 'createnew/:id',
    component: RoleFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
