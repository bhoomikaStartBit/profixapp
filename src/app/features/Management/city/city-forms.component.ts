import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Location} from "@angular/common";
import {CityService} from "@app/features/Management/city/city.service";
import {StateService} from "@app/features/Management/state/state.service";
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'user-form',
  templateUrl: './city-forms.component.html',
  styles: [`section.col-md-4.mmbox {
    margin: 0px 3px;
}button.btn.btn-success.getloc-btn {
    padding: 10px 6px;
    margin-top: 8px;
}`]
})
export class CityFormComponent implements OnInit {
    ID : any;
    name : any;
    States : any;
    CityForm: FormGroup;    
    isTracking: boolean = false;
    reverseGeoSub: Subscription = null
    address: any;
    lat: any;
    long: any;
    getData: boolean = false;
    geoReverseService = 'https://nominatim.openstreetmap.org/search?q={address}&format=json&polygon=1&addressdetails=1'

  constructor(private http: HttpClient,
              private router : Router ,
              private fb: FormBuilder,
              private location: Location,
              private route: ActivatedRoute,
              private cityService: CityService,
              private stateService: StateService,
              private notificationService: NotificationService,
              private snackBar: MatSnackBar,
              private httpClient: HttpClient,
              private spinnerService : Ng4LoadingSpinnerService
  ) {
     this.spinnerService.hide();
      this.CityForm = fb.group({
          Name: [ '', [Validators.required ] ],
          State: [ '', [Validators.required ] ],
          Latitude: [''],
          Longitude: ['']
      });
      this.ID = this.route.params['value'].id;
      this.getAllStates();
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }

      console.log(this.ID);
  }

  ngOnInit() {


  }
    getAllStates(){
        this.spinnerService.show();
        this.stateService.getAllStates().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.States = data['result'];
            console.log(this.States);
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    trackMe() {

        const service = (this.geoReverseService || '').replace(new RegExp('{address}', 'ig'), `${this.address}`)
        this.spinnerService.show();
        this.reverseGeoSub = this.httpClient.get(service).subscribe(data => {
            this.spinnerService.hide();
            
            const val = (data[0] || {})

            if(val){
                this.CityForm.controls.Latitude.setValue(val['lat']);
                this.CityForm.controls.Longitude.setValue(val['lon']);
                this.getData = true;
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    onSubmitForm() {
        if (this.CityForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.cityService.addCity(this.CityForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.cityService.updateCity(this.ID, this.CityForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        
                        this.location.back();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.cityService.getOneCity(id).subscribe((data: {}) => {
            if ( data['status']) {
                this.spinnerService.hide();
                this.CityForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    State: [  data['result'].State, [Validators.required ] ],
                    Latitude: [data['result'].Latitude ? data['result'].Latitude : ''],
                    Longitude: [data['result'].Longitude ? data['result'].Longitude : '']
                });

                if(data['result'].Latitude != null || data['result'].Longitude != null){
                    this.getData = true;
                }
                this.address = data['result'].Name.toLowerCase()+'+india';
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }
    register(){
        this.router.navigate(['/cities']);
    }
    cancel() {
        this.router.navigate(['/cities']);
    }


    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
