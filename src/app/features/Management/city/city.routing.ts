
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {CityComponent} from "@app/features/Management/city/city.component";
import {CityFormComponent} from "@app/features/Management/city/city-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: CityComponent
  },{
    path: 'createnew/:id',
    component: CityFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
