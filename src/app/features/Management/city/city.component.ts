import { Component, OnInit , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {NotificationService} from "@app/core/services";
import {CityService} from "@app/features/Management/city/city.service";

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'citylist',
  templateUrl: './city.component.html',
  styles: [`.width125 {
    width: 94px;
}`]
})
export class CityComponent implements OnInit {

    Cities : any;
    displayedColumns: string[] = [
        'SN',
        'Name',
        'StateName',
        'Latitude',
        'Longitude',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;

    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private cityService: CityService,
      private snackBar: MatSnackBar,
      private spinnerService: Ng4LoadingSpinnerService
  ) {
      this.spinnerService.hide();
      this.getAllCity();
  }

  ngOnInit() {
  }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    getAllCity(){
        this.spinnerService.show();
        this.cityService.getAllCities().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Cities = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.filterPredicate = function(data, filter: string): boolean {
                return data.Name.toLowerCase().includes(filter) ||  data.State.Name.toLowerCase().includes(filter) ||  data.Latitude.toString().includes(filter) ||  data.Longitude.toString().includes(filter);
            };
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            console.log(this.Cities);
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }


    delete(id){
        console.log(id);
        this.spinnerService.show();
        this.cityService.getRelatedRecordOfCity(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                var lab ='';
                if(data['result'].length){
                    lab = 'You can\'t delete this record because this is having many dependant "'+ data['Table'] + '" records. Please delete all dependant records first.';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[Ok]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Ok") {

                        }
                    });
                }
                else{
                    lab = 'Are you sure delete this record?';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[No][Yes]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Yes") {
                            this.spinnerService.show();
                            this.cityService.deleteCity(id).subscribe((data: {}) => {
                                this.spinnerService.hide();
                                if (data['status']) {
                                    this.snackBar.open('Record deleted successfully', 'Delete', {
                                        duration: 5000,
                                        panelClass: ['danger-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                } else {
                                }
                                this.getAllCity();
                            }, err => {
                                this.spinnerService.hide();
                                this.snackBar.open('Server Error', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                              })
                        }
                        if (ButtonPressed === "No") {

                        }

                    });
                }

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
