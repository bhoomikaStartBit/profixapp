import {NgModule} from "@angular/core";

import {routing} from "./city.routing";
import {CityComponent} from "@app/features/Management/city/city.component";
import {CityFormComponent} from "@app/features/Management/city/city-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";

import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      CityComponent,
      CityFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      NgMultiSelectDropDownModule.forRoot(),
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [

  ],
})
export class CityModule {

}
