import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";

import { Router} from '@angular/router';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import { CompaniesService } from './companies.service';

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotificationService } from '@app/core/services';



@Component({
  selector: 'sa-company',
  templateUrl: './companies.component.html'
})
export class CompaniesComponent implements OnInit {

  
  currentUser: any;
  ID : any;
  Companies : any = [];

  limit:number = 10;
  pageIndex : number = 0;
  pageLimit:number[] = [10,20,50];

  public validationSettingsForm : any = {};

  displayedColumns: string[] = [
    
    'Name',
    'Email',
    // 'Type',
    // 'Manager',
    'CraditDays',
    'CraditLimits',
    'Action'
];
dataSource: MatTableDataSource<any>;
isLoadingResults: any;
isRateLimitReached: any;
@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild(MatSort) sort: MatSort;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router : Router,
    private companyService : CompaniesService,
    private spinnerService : Ng4LoadingSpinnerService,
    private notificationService : NotificationService
    ) {
      this.spinnerService.hide();
     window.scrollTo(0 , 0)
    this.getAllCompanies();
    

  }

  ngOnInit() {
  }


  onSubmitForm() {

    
  }

  getAllCompanies(){
    this.spinnerService.show();
    this.companyService.getAll().subscribe((data : {})=>{
      this.spinnerService.hide();
      if(data['status']){
        this.Companies = data['result'];
        this.dataSource = new MatTableDataSource(data['result']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    }, err => {
      this.spinnerService.hide();
      this.snackBar.open('Server Error', '', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
    }
  }

  changePage(event){
    this.pageIndex = event.pageSize*event.pageIndex;
  }

  delete(id){
    this.notificationService.smartMessageBox({
      title: "Delete!",
      content: 'Are you sure delete this record?',
      buttons: '[No][Yes]'
  }, (ButtonPressed) => {
      if (ButtonPressed === "Yes") {
        this.spinnerService.show();
          this.companyService.deleteCompany(id).subscribe((data: {}) => {
            this.spinnerService.hide();
              if (data['status']) {
                  this.snackBar.open('Record deleted successfully', 'Delete', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
              } else {
              }
              this.getAllCompanies();
          }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
      }
      if (ButtonPressed === "No") {

      }

  });
  }


}
