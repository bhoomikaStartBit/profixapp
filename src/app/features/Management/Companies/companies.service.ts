import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class CompaniesService {
  url: string;
  DocUrl : string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/Companies/';
    this.DocUrl = commService.getApiUrl() + '/document/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getAllDocumentsByUser(user): Observable<any> {
    return this.http.get(this.DocUrl + 'getAllDocumentsByUser/'+user).pipe(
      map(this.extractData));
  }

  getAll(): Observable<any> {
    return this.http.get(this.url + 'getAll/').pipe(
      map(this.extractData));
  }

  getOneCompanyDetail(id): Observable<any> {
    return this.http.get(this.url + 'getOneCompanyDetail/'+id).pipe(
      map(this.extractData));
  }

  getValidateCompanyNameAndEmail(Type , TypeValue): Observable<any> {
    return this.http.get(this.url + 'ValidateCompanyNameAndEmail/'+Type+'/'+TypeValue).pipe(
      map(this.extractData));
  }

  getUserDocument(url): Observable<any> {
    return this.http.get(this.commService.getApiUrl() + url, { responseType: 'blob' as 'json' }).pipe(
        map(this.extractData));
  }
  

  getAllCompanyUser(id): Observable<any> {
    return this.http.get(this.url+'getCompanyUser/' + id).pipe(
        map(this.extractData));
  } 

  getAllCompanySPUser(id): Observable<any> {
    return this.http.get(this.url+'getCompanySPUser/' + id).pipe(
        map(this.extractData));
  }

  getSettings(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }

  addCompany (data): Observable<any> {
    return this.http.post<any>(this.url + 'create', data, httpOptions).pipe(
        tap(( ) => console.log(`added Company w/ id=${data._id}`)),
        catchError(this.handleError<any>('addCompany'))
    );
}

addCompanyOfficeAddress(id , data): Observable<any> {
  return this.http.post<any>(this.url + 'addCompanyOfficeAddress/'+id, data, httpOptions).pipe(
      tap(( ) => console.log(`added Company w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCompany'))
  );
}

updateCompany(id, data): Observable<any> {
  return this.http.put<any>(this.url + 'update/' + id, JSON.stringify(data), httpOptions).pipe(
    tap(( ) => { console.log('update');
    console.log(data);  }),
    catchError(this.handleError<any>('updateCompany'))
  );
}



  updateCompanyOfficeAddress(CompanyID , id, data): Observable<any> {
    return this.http.put<any>(this.url + 'updateCompanyOfficeAddress/'+CompanyID+'/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => { console.log('update');
      console.log(data);  }),
      catchError(this.handleError<any>('updateProfile'))
    );
  }

  

  deleteCompanyOfficeAddress (id): Observable<any> {
    return this.http.delete<any>(this.url + 'deleteCompanyOfficeAddress/' + id, httpOptions).pipe(
      tap(( ) => console.log(`deleted role w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

  deleteCompany (id): Observable<any> {
    return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
      tap(( ) => console.log(`deleted company w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

  
  
  updateSetings(id, data): Observable<any> {
    return this.http.put<any>(this.url + 'update/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log('update')),
      catchError(this.handleError<any>('updateProfile'))
    );
  }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
