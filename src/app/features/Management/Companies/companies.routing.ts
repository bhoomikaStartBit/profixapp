
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import { CompaniesComponent } from "./companies.component";
import { CompaniesFormComponent } from "./companies-form.component";
import { CompanyViewDetailsComponent } from './company-detail.component';


export const routes:Routes = [

  {
    path: '',
    component: CompaniesComponent
  },
  {
    path: 'createnew/:id',
    component: CompaniesFormComponent
  },
  {
    path: 'update/:id',
    component: CompaniesFormComponent
  },
  {
    path: 'viewDetail/:id',
    component: CompanyViewDetailsComponent
  },

];

export const routing = RouterModule.forChild(routes);
