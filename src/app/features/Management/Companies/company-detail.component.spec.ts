import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyViewDetailsComponent } from './company-detail.component';

describe('CompanyViewDetailsComponent', () => {
  let component: CompanyViewDetailsComponent;
  let fixture: ComponentFixture<CompanyViewDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyViewDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyViewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
