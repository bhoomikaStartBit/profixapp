import { Component, OnInit ,ViewChild ,AfterViewInit} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError, startWith } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';

import { Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {MatSnackBar} from "@angular/material";

import { ReplaySubject , Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { CompaniesService } from './companies.service';

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotificationService } from '@app/core/services';
import { TeamCompanyService } from '../team-company/team-company.service';
import { UserService } from '../users/user.service';


@Component({
  selector: 'user-form',
  templateUrl: './company-detail.component.html',
  styles: [
    `.mat-form-field {
    display: inline-block;
    position: relative;
    text-align: left;
    width: 100%;
    }.mat-option {
        line-height: 24px;
        height: 60px;padding: 1px;}.rwmain {
            width: 100%;
        }.rwone {
            width: 14%;
            float: left;
            margin: 0;
            padding: 0;
        }.rwtwo {
            width: 83%;
            float: left;
            text-align: left;
            margin: 0px;
            padding: 0;
            padding-left: 9px;
        }`
    ]
})
export class CompanyViewDetailsComponent implements OnInit {
    ID : any;
    getTCData : boolean = false;
    CompanyDetails : any = {};

    displayedColumns: string[] = [
        'TeamName',        
        'TLID',
        'Projects',
        'Action'
    ];
    TeamdataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    
    @ViewChild(MatSort) sort: MatSort;   

    displayedSPColumns: string[] = [
        'SN',
        'ProfixID',
        'Name',        
        'Phone',
        'Type',
        'Block',
        'Available',
        'Action'
    ];
    SPdataSource: MatTableDataSource<any>;
   
    @ViewChild(MatSort) msort: MatSort;  
    @ViewChild('paginator3') paginator3: MatPaginator;
    @ViewChild('paginator2') paginator2: MatPaginator;

  constructor(private http: HttpClient ,
              private router : Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private companiesService : CompaniesService,
              private spinnerService : Ng4LoadingSpinnerService,
              private notificationService : NotificationService,
              private teamCompanyService : TeamCompanyService,
              private userService : UserService
  ) {
    this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          this.getOne(this.ID);
          this.getAllCompanyUser(this.ID)
      }
      else{
          this.getTCData = true;
      }
      console.log(this.ID);
      
  }

  ngOnInit() {
       
  }

  getAllCompanyUser(id){
    this.companiesService.getAllCompanyUser(id) .subscribe((data:{})=>{
        console.log('data')  
        console.log(data) 
        if(data['status']){

            var usedata = [];
              if( data['result'].length > 0){
                usedata = data['result'].map(row=>{
                  row.IsAvailable =  row.IsAvailable == 'Y'  ?  true : false;
                  return row
                })
              }
              // console.log(usedata)
            this.SPdataSource = new MatTableDataSource(usedata);
            this.SPdataSource.paginator = this.paginator2;
            this.SPdataSource.sort = this.msort;
           
        } 
        else{
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }, err => {
      this.spinnerService.hide();
      this.snackBar.open('Server Error', '', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
    })
  }

  getOne(id){
      this.spinnerService.show();
      this.companiesService.getOneCompanyDetail(id).subscribe((data:{})=>{
        this.spinnerService.hide();
          if(data['status']){
            this.getTCData = true;
                console.log(data['result']);
                this.CompanyDetails = data['result'];
                this.TeamdataSource = new MatTableDataSource(data['CompanyTeams']);
                this.TeamdataSource.paginator = this.paginator3;
                console.clear()
                console.log(this.paginator3)
                this.TeamdataSource.sort = this.sort;
          }
      }, err => {
	      this.spinnerService.hide();
	      this.snackBar.open('Server Error', '', {
	          duration: 5000,
	          panelClass: ['danger-snackbar'],
	          verticalPosition: 'top'
	      });
	    })
  }

  applyFilterCompanyUser(filterValue: string) {
    this.SPdataSource.filter = filterValue.trim().toLowerCase();

    if (this.SPdataSource.paginator) {
        this.SPdataSource.paginator.firstPage();
    }
  }

  applyFilter(filterValue: string) {
    this.TeamdataSource.filter = filterValue.trim().toLowerCase();

    if (this.TeamdataSource.paginator) {
        this.TeamdataSource.paginator.firstPage();
    }
  }

  update(id){
    this.router.navigate(['/team-company/team-company-detail/'+id] , {queryParams: { ForCompany:this.ID  }})
  }

  updateUser(id){
    this.router.navigate(['/users/edit/'+id] , {queryParams: { ForCompany:this.ID  }})
  }

  deleteUser(id){      
    this.notificationService.smartMessageBox({
        title: "Delete!",
        content: 'Are you sure delete this record?',
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") { 
          this.spinnerService.show();             
            this.userService.deleteUsers(id).subscribe((data: {}) => {
              this.spinnerService.hide();
                if (data['status']) {
                  this.snackBar.open('User deleted successfully', '', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
                } else {
                }
                this.getAllCompanyUser(this.ID);
            }, err => {
              this.spinnerService.hide();
              this.snackBar.open('Server Error', '', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
            })
        }
        if (ButtonPressed === "No") {

        }

    });
}
chcekAvailable(id , i , tis){
  console.log(i , tis)
    this.notificationService.smartMessageBox({
      title: "Alert!",
      content: 'Are you sure to make this change?',
      buttons: '[No][Yes]'
  }, (ButtonPressed) => {
      if (ButtonPressed === "Yes") {
        var aval = tis ? 'Y' : 'N';
          this.userService.checkAvailable(id , aval).subscribe((data:{})=>{

          }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
      }
      else{
          this.SPdataSource.data[i].IsAvailable = !tis;
      }
  })
  }

  delete(id,TeamLeaderID){ 
    this.notificationService.smartMessageBox({
        title: "Delete!",
        content: 'Are you sure delete this record?',
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") { 
          this.spinnerService.show();             
            this.teamCompanyService.deleteTeamCompany(id,TeamLeaderID).subscribe((data: {}) => {
              this.spinnerService.hide();
                if (data['status']) {
                  this.snackBar.open('Team deleted successfully', '', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
                } else {
                }
                this.getOne(this.ID)
            }, err => {
              this.spinnerService.hide();
              this.snackBar.open('Server Error', '', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
            })
        }
        if (ButtonPressed === "No") {

        }

    });
}

goToCreateUser(){
    this.router.navigate(['/users/createnew/-1'] , {queryParams: { ForCompany:this.ID  }})
}

goToCreateTeam(){
    //routerLink="/team-company/createnew/-1"

    this.router.navigate(['/team-company/createnew/-1'] , {queryParams: { ForCompany:this.ID  }})
}
 
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
