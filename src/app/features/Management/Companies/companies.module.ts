import { NgModule } from '@angular/core';

import {routing} from "./companies.routing";
import { CompaniesComponent } from './companies.component';
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import { CompaniesFormComponent } from './companies-form.component';
import { CompanyViewDetailsComponent } from './company-detail.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';


@NgModule({
  declarations: [CompaniesComponent , CompaniesFormComponent ,CompanyViewDetailsComponent],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      SmartadminInputModule,
      MaterialModuleModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class CompaniesModule { }
