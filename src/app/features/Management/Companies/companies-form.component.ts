import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";

import { Router} from '@angular/router';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar, MatStepper} from "@angular/material";
import {saveAs as importedSaveAs} from 'file-saver';
import { CompaniesService } from './companies.service';
import { StateService } from '../state/state.service';
import { CityService } from '../city/city.service';
import { LocalityService } from '../locality/locality.service';
import { NotificationService } from '@app/core/services';
import { UserService } from '../users/user.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CommonService } from '@app/core/common/common.service';


@Component({
  selector: 'sa-company-form',
  templateUrl: './companies-form.component.html'
})
export class CompaniesFormComponent implements OnInit {

  CompanyForm : FormGroup;
  HeadOfficeForm : FormGroup;
  BranchOfficeForm : FormGroup;
  FinancialDetailsForm : FormGroup;
  CompanyOfficeAddressForm : FormGroup;
  showSpinner: boolean = false;
  currentUser: any;
  ID : any;
  public validationCompanyForm : any = {};
  public validationCompanyHeadOfficeForm : any = {};
  public validationCompanyBranchOfficeForm : any = {};
  public validationCompanyOfficeAddressOptions : any ={};
  public validationCompanyFinancialDetailsOptions : any ={};
  renderData : boolean = false;
  documentArray: Array<any> = [];
  documentUploadedArray: any = [];
  allDocuments : any =[];
  allImages: any = [];
  docMandatory = 0;
  States: any = [];
  Cities: any = [];
  Users : any =[];
  CreditLimits : any =[];
  CreditDays : any =[];
  @ViewChild('stepper') stepper: MatStepper;

  CompanyOfficeAddressCities : any = [];

  CompanyOfficeAddress : any = [];
  CompanyType : any = [];
  SelectedCompanyOfficeAddress = -1;
  ValidateCompanyEmail: boolean = false;
  ValidateCompanyName: boolean = false;
  
  ValidateCompanyManagerEmail: boolean = false;
  
  @ViewChild('Emailfield') inputEmailField:ElementRef;
  CompanyEditDetail : any ={};

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router : Router,
    private companyService : CompaniesService,
    private stateService : StateService,
    private cityService : CityService,
    private localityService : LocalityService,
    private notificationService : NotificationService,
    private route: ActivatedRoute,
    private userService : UserService,
    private spinnerService : Ng4LoadingSpinnerService,
    private commonService : CommonService
    ) {
      this.spinnerService.hide();
     this.commonService.getCompanyTypes().subscribe((data:{})=>{
      if(data['status']) {
        this.CompanyType = data['result'];
      }
     })
      this.ID = this.route.params['value'].id;
      this.stateService.getAllStates().subscribe((data:{})=>{
        if(data['status']){
          this.States = data['result']
        }
        else{
          this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
          });
        } 
      }, err => {
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })

        this.userService.getAllUserWithSpRole().subscribe((data:{})=>{
          if(data['status']){
            this.Users = data['result']
          }
        }, err => {
          this.snackBar.open('Server Error', '', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
        })
        if(this.ID == -1){
          this.CompanyForm = fb.group({
            'Type' : [ '' , [Validators.required]],
            'Name' : ['' , [Validators.required]],
            'Email' : ['' , [Validators.required, Validators.email  , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ]],
            'Phone' : ['' , [Validators.required,  Validators.minLength(10)]],
            'Password' : ['' , [Validators.required, Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$')]],
            'ConfirmPassword' : ['' , [Validators.required,Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$')]],
             'FirstName' : ['' , [Validators.required]],
             'MiddleName' : ['' , ],
             'LastName' : [''],
             'ManagerEmail' : ['' , [Validators.required , , Validators.email  , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$')]],
             'ManagerPhone' : ['' , [Validators.required , Validators.minLength(10), Validators.maxLength(10)]],
          })
        }
     

      this.CompanyOfficeAddressForm = fb.group({
        Office: [ 'H', [Validators.required ] ],
        Name: [ '', [Validators.required ] ],
        Email: [ '', [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
        Number: [ '', [Validators.required, Validators.minLength(10) ] ],
        Address: [ '', [Validators.required ] ],
        Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
        
        City: [ '', [Validators.required ] ],
        State: [ '', [Validators.required ] ]
      });
      if(this.ID == -1){
        this.FinancialDetailsForm = fb.group({
          Type: [ 'Current', [Validators.required ] ],
          Name : ['', [Validators.required ]],
          BankName: [ '', [Validators.required ] ],
          AccountNumber: [ '', [Validators.required ] ],
          IFSC: [ '', [Validators.required] ],
          CraditDays: [ '', [Validators.required ] ],
          CraditLimits: [ '', [Validators.required] ],
          
        });
      }
      this.validationCompanyForm = {
        // Rules for form validation
        rules: {              
            CompanyType:{
              required: true
            },
            CompanyName:{
              required: true
            },              
            CompanyEmail:{
              required: true,
              email: true,
              pattern : '[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
            }, 
            CompanyPhone :{
              required: true,
              minlength : 10,
            },              
            CompanyPassword: {
              required: true,
              minlength: 8,
              pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$'
            },
            CompanyConfirmPassword : {
              required: true,
              minlength: 8,
              pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$',
              equalTo: '#CompanyPassword'
            },
            FirstName : {
              required : true
            },
            ManagerEmail:{
              required : true,
              email: true,
              pattern : '[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
            },
            ManagerPhone : {
              required : true,
              minlength : 10,
              maxlength : 10
            },         
            
        },

        // Messages for form validation
        messages: {  
          CompanyType: {
              required: 'Please select company type'
            },            
            CompanyName:{
              required: 'Please enter company Name'
            },              
            CompanyEmail:{
              required: 'Please enter company email',
              email: 'Please enter a valid email addres',
              pattern: 'Please enter valid email pattern'
            },
            CompanyPhone : {
              required: 'Please enter the Company Phone',
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
              
            },
            CompanyPassword: {
              required: 'Please enter  Password',
              minlength: 'Password should be at least 8 characters long',
              pattern: 'Password should contain one number,one character, one uppercase character and one special character'
            },
            CompanyConfirmPassword: {
              required: 'Please enter confirm Password',
              minlength: 'Password should be at least 8 characters long',
              pattern: 'Password should contain one number,one character, one uppercase character and one special character',
              equalTo: 'Please re-enter the same password again.'
            },   
            FirstName : {
              required : 'Please enter First Name',
            },
            ManagerEmail:{
              required : 'Please enter Manager Email',
              email: 'Please enter a valid email addres',
              pattern: 'Please enter valid email pattern'
            },
            ManagerPhone : {
              required : 'Please enter Manager Phone',
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
              maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
            },        
            
        },
        submitHandler: this.onSubmitForm

    };
    this.validationCompanyOfficeAddressOptions = {
      // Rules for form validation
      rules: {
          VAName: {
              required: true
          },
          VAEmail: {
              required: true,
              email: true,
              pattern:'[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
          },
          VANumber: {
              required: true,
              pattern: "[0-9]+",
              minlength:10,
             
          },
          VAState: {
              required: true
          },
          VACity: {
              required: true
          },
          VAPin: {
              required: true,
              minlength:6,
              maxlength:6
          },
          VAAddress: {
              required: true
          }
      },

      // Messages for form validation
      messages: {
          VAName: {
              required: 'Please enter name'
          },
          VAEmail: {
              required: 'Please enter your email address',
              email: 'Please enter a valid email addres',
              pattern: 'Please enter a valid email pattern'
          },
          VANumber: {
              required: 'Please enter your phone number',
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
             
          },
          VAState: {
              required: 'Please select the state'
          },
          VACity: {
              required: 'Please select the city'
          },
          VAPin: {
              required: 'Please enter pincode',
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
              maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
          },
          VAAddress: {
              required: 'Please enter address'
          }
      },
      submitHandler: this.onSubmitForm

  };


  this.validationCompanyFinancialDetailsOptions = {
    // Rules for form validation
    rules: {              
        
        ACType:{
          required: true
        },              
        ACName:{
          required: true
        },              
        ACNumber:{
          required: true
        },               
        BankName: {
          required: true,
        }, 
        IFSC : {
          required : true
        }, 
        CraditDays : {
          required : true
        },
        CraditLimits : {
          required : true
        },          
        
    },

    // Messages for form validation
    messages: {  
      ACType:{
        required: 'Please select A/c Type.'
      },              
      ACName:{
        required: 'Please enter name in bank.'
      },  
      ACNumber : {
        required: 'Please enter name a/c number.'
      },        
      BankName: {
        required: 'Please enter bank name.',
      }, 
      IFSC : {
        required : 'Please enter ifsc code of your bank.'
      }, 
      CraditDays : {
        required : 'Please enter credit days.'
      },
      CraditLimits : {
        required : 'Please enter credit limits.'
      }, 
    },
    submitHandler: this.onSubmitForm

  };
  if(this.ID == '-1'){

    this.renderData = true;
  }
  else{
    this.getOne();
  }
    
  this.commonService.getCreditDays().subscribe((data)=>{
    if(data['status']){
        this.CreditDays = data['result']
    }
  })

  this.commonService.getCreditLimits().subscribe((data)=>{
      if(data['status']){
          this.CreditLimits = data['result']
      }
  })
    

}

  ngOnInit() {
    this.spinnerService.hide();
    this.getDoc();
    
  }
  getDoc(){
  	this.spinnerService.show();
	  this.companyService.getAllDocumentsByUser('Vendor').subscribe( (data :{}) =>{
	      this.spinnerService.hide();
	      if(data['status']){
	          this.allDocuments = data['result'];
	          console.log(data['result']);
	      }
	    }, err => {
	      this.spinnerService.hide();
	      this.snackBar.open('Server Error', '', {
	          duration: 5000,
	          panelClass: ['danger-snackbar'],
	          verticalPosition: 'top'
	      });
	    })
  }
  

  onSubmitForm() {

    
  }
  

  goToDocumentStep(){
    console.log(this.ValidateCompanyEmail , this.ValidateCompanyName)
    if(this.ValidateCompanyName){
      this.snackBar.open('Company Name is already exist , Please try diffrent Name', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
      });
      return 0;
    }
    if(this.ValidateCompanyEmail){ 
      this.snackBar.open('Company Email is already exist , Please try diffrent Email', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
      });
      return 0;
    }
    
    if(this.ValidateCompanyManagerEmail){ 
      this.snackBar.open('Company Manager Email is already exist , Please try diffrent Email', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
      });
      return 0;
    }
    if(this.CompanyForm.invalid){
      return 0;
    }
    this.FinancialDetailsForm.patchValue({
      'Name' : this.CompanyForm.controls.Name.value
    })
    this.stepper.next();
  }

  goToAddressStep(){
    if(this.CompanyForm.value.Type != 'Un-Registered'){
      if(this.allDocuments.length != this.documentArray.length){
        if(this.documentArray.length >=3){
            for(let docc of this.documentArray){
                if(docc.Name == 'Registration Amount Receipt'){
                    if(docc.Number != ''){
                        this.snackBar.open('Please upload mandatory documents.', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                        return 0; 
                    }
                }
            }
        }
        else{
            this.snackBar.open('Please upload mandatory documents.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0; 
        }  
      }
    }
    
    this.stepper.next();
  }

  goToFinancialStep(){
    if(this.CompanyOfficeAddress.length == 0){
      this.snackBar.open('Please fill office address form and click on "Add Address" button to add address.', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
      });
      return 0;
    }
    this.stepper.next();
  }

  onFileChange(event, name) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        if((file.size)/1024 > 1024){
            this.snackBar.open('File size is too large , please choose file below 1MB size', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            const deindex = this.allDocuments.findIndex(x => x.Name === name);
           
            $('#'+deindex).val('');
            return 0;
        }
        const fileName = event.target.files[0].name;
        const lastIndex = fileName.lastIndexOf('.');
        const extension =  fileName.substr(lastIndex + 1);
        if (extension === 'jpg' || extension === 'jpeg' || extension === 'png' || extension === 'pdf' || extension === 'doc' || extension === 'docx'  ) { //
            reader.onload = () => {
                this.allImages[name] = event.target.files;
            };
        } else {
            this.snackBar.open('Invalid file format. Upload Only jpg, jpeg, png and pdf file.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            const deindex = this.allDocuments.findIndex(x => x.Name === name);
            
            $('#'+deindex).val('');
        }
    }
  }
    
  downloadFile(data: any, name) {
      importedSaveAs(data, name);
  }

  checkPatternValidation(val, pattern) {
    console.log(pattern)
    if(!pattern){
        return 0;
    }
    if(!(val.match(pattern))) {
        this.snackBar.open('Invalid Pattern', 'Delete', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    }
}

deleteFile(name) {
  this.notificationService.smartMessageBox({
      title: "Delete Alert!",
      content: "Are you sure to delete this file?",
      buttons: '[No][Yes]'
  }, (ButtonPressed) => {
      if (ButtonPressed === "Yes") {
          const index = this.documentArray.findIndex(x => x.Name === name);
          if(index != -1) {
              if(name == 'Aadhar' || name == 'Registration Amount Receipt'){
                  this.docMandatory--;
              }
              console.log('fdsf')
              this.documentArray.splice(index, 1);
          }
          this.allImages[name] = null;
          this.documentUploadedArray[name] = null;
          
          const deindex = this.allDocuments.findIndex(x => x.Name === name);
                
          $('#'+deindex).val('');
      }
      if (ButtonPressed === "No") {

      }

  });
}

  addDocumentDetail(val, name) {
    // this.allImages
    let localArray = {};
    localArray['Name'] = name;
    localArray['Image'] = this.allImages[name];
    localArray['Number'] = val;
    let index = this.documentArray.findIndex(x => x.Name === name)
    if (index === -1) {
        if (name === 'Registration Amount Receipt' || name === 'Aadhar') {
            this.docMandatory++;
        }
        this.userService.addImage(this.allImages[name]).subscribe((data : {})=>{
          if(data['status']){
            localArray['Image'] = data['path'];
            this.documentArray.push(localArray);
            this.documentUploadedArray[name] = name;
  
            let deindex = this.allDocuments.findIndex(x => x.Name === name)
            console.log(deindex)
            var va = $('#'+deindex).val().split('\\');
            var filename = va[(va.length-1)];
            this.snackBar.open('File "'+filename+'" uploaded successfully.', '', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
            this.allDocuments[deindex].upload = false;
          }
          else{
            let deindex = this.allDocuments.findIndex(x => x.Name === name)
            var va = $('#'+deindex).val().split('\\');
            var filename = va[(va.length-1)];
            this.snackBar.open('File "'+filename+'" uploaded failed.', '', {
              duration: 5000,
              panelClass: ['success-snackbar'],
              verticalPosition: 'top'
          });
          }
          
      }, err => {
	      this.spinnerService.hide();
	      this.snackBar.open('Server Error', '', {
	          duration: 5000,
	          panelClass: ['danger-snackbar'],
	          verticalPosition: 'top'
	      });
	    })
    } else {
      this.userService.addImage(this.allImages[name]).subscribe((data : {})=>{
        if(data['status']){
          this.documentArray[index].Image = this.allImages[name];
          this.documentArray[index].Number = val;
  
  
          let deindex = this.allDocuments.findIndex(x => x.Name === name)
          console.log(deindex)
          var va = $('#'+deindex).val().split('\\');
          var filename = va[(va.length-1)];
          this.snackBar.open('File "'+filename+'" uploaded successfully.', '', {
              duration: 5000,
              panelClass: ['success-snackbar'],
              verticalPosition: 'top'
          });
          this.allDocuments[deindex].upload = false;
        }
        else{
          let deindex = this.allDocuments.findIndex(x => x.Name === name)
          console.log(deindex)
          var va = $('#'+deindex).val().split('\\');
          var filename = va[(va.length-1)];
          this.snackBar.open('File "'+filename+'" uploaded failed.', '', {
            duration: 5000,
            panelClass: ['success-snackbar'],
            verticalPosition: 'top'
        });
        }
        
    }, err => {
      this.spinnerService.hide();
      this.snackBar.open('Server Error', '', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
    })
    }
    
}

  addCompanyOfficeAddress(){
    if(this.CompanyOfficeAddressForm.invalid){
        return 0;
    }
    if(this.SelectedCompanyOfficeAddress == -1){
      for(let co of this.CompanyOfficeAddress){
        if(co.Office == 'H' && co.Office == this.CompanyOfficeAddressForm.controls.Office.value){
          this.snackBar.open('Head Office address is already exist.', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
          });
          return 0;
        }
      }
    }
    if(this.SelectedCompanyOfficeAddress == -1){
        this.CompanyOfficeAddress.push(this.CompanyOfficeAddressForm.value);

        if(this.ID !== '-1'){
            this.companyService.addCompanyOfficeAddress(this.ID , this.CompanyOfficeAddressForm.value).subscribe( (data : {})=>{
                if(data['status']){
                    this.CompanyOfficeAddress = [];
                    this.CompanyOfficeAddress = data['result'];
                    this.CompanyOfficeAddressForm.reset();
                    this.CompanyOfficeAddressForm.patchValue({
                        Office : 'B'
                    })
                    this.snackBar.open('Office address successfully added.', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            }, err => {
              this.spinnerService.hide();
              this.snackBar.open('Server Error', '', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
            })
        }
        else{
          this.SelectedCompanyOfficeAddress = -1;
          setTimeout(()=>{
            this.CompanyOfficeAddressForm.reset();
            this.CompanyOfficeAddressForm.patchValue({
                Office : 'Branch'
            })
          },300)
            this.snackBar.open('Office address successfully added.', '', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
    else{
        if(this.ID !== '-1'){
          console.log(this.CompanyOfficeAddress[this.SelectedCompanyOfficeAddress])
         // this.CompanyOfficeAddress[this.SelectedCompanyOfficeAddress] = this.CompanyOfficeAddressForm.value;
            this.companyService.updateCompanyOfficeAddress(this.ID , this.CompanyOfficeAddress[this.SelectedCompanyOfficeAddress]._id , this.CompanyOfficeAddressForm.value).subscribe( (data : {})=>{
                if(data['status']){
                    this.SelectedCompanyOfficeAddress = -1;
                    this.CompanyOfficeAddress = [];
                    this.CompanyOfficeAddress = data['CompanyAddress'];
                    this.CompanyOfficeAddressForm.reset();
                    this.CompanyOfficeAddressForm.patchValue({
                        Office : 'Branch'
                    })
                    this.snackBar.open('Office address successfully updated.', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            }, err => {
              this.spinnerService.hide();
              this.snackBar.open('Server Error', '', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
            })
        }
        else{
          this.CompanyOfficeAddress[this.SelectedCompanyOfficeAddress] = this.CompanyOfficeAddressForm.value;
          this.SelectedCompanyOfficeAddress = -1;
          setTimeout(()=>{
            this.CompanyOfficeAddressForm.reset();
            this.CompanyOfficeAddressForm.patchValue({
                Office : 'Branch'
            })
          },300)  
            this.snackBar.open('Office address successfully updated.', '', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
     
}

deleteCompanyOfficeAddress(i){
    this.notificationService.smartMessageBox({
        title: "Delete Alert!",
        content: "Are you sure to delete this record?",
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") { 
            if(this.ID !== '-1'){
                this.companyService.deleteCompanyOfficeAddress(this.CompanyOfficeAddress[i]._id).subscribe( (data : {})=>{
                    if(data['status']){
                     this.CompanyOfficeAddress.splice(i , 1);
                        this.snackBar.open('Office address successfully deleted.', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                }, err => {
                  this.spinnerService.hide();
                  this.snackBar.open('Server Error', '', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
                })
            }
            else{
              this.CompanyOfficeAddress.splice(i , 1);
            }
        }
        
    })
}

updateCompanyOfficeAddress(i){
    /*this.notificationService.smartMessageBox({
        title: "Update Alert!",
        content: "Are you sure to update this record?",
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") {
            
            
        }
        
    }) */
    this.getAllCitiesByStateVendorAddress(this.CompanyOfficeAddress[i].State);
    this.SelectedCompanyOfficeAddress = i;
    this.CompanyOfficeAddressForm = this.fb.group({
        Office: [ this.CompanyOfficeAddress[i].Office , [Validators.required ] ],
        Name: [ this.CompanyOfficeAddress[i].Name , [Validators.required ] ],
        Email: [ this.CompanyOfficeAddress[i].Email , [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
        Number: [ this.CompanyOfficeAddress[i].Number , [Validators.required, Validators.minLength(10) ] ],
        Address: [ this.CompanyOfficeAddress[i].Address , [Validators.required ] ],
        Pin: [ this.CompanyOfficeAddress[i].Pin , [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
        
        City: [ this.CompanyOfficeAddress[i].City , [Validators.required ] ],
        State: [ this.CompanyOfficeAddress[i].State , [Validators.required ] ]
    })
    this.onSubmitForm();
}



getAllCitiesByStateVendorAddress(id){
  this.spinnerService.show();
  this.CompanyOfficeAddressCities = [];
  this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
    this.spinnerService.hide();
      if(data['status']){
          this.CompanyOfficeAddressCities = data['result'];
      }
  }, err => {
    this.spinnerService.hide();
    this.snackBar.open('Server Error', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
    });
  })
}

saveCompanyData(){
  if(this.FinancialDetailsForm.invalid){
    return 0;
  }
  console.log(this.CompanyForm.value)
  console.log(this.documentArray)
  console.log(this.CompanyOfficeAddress)
  console.log(this.FinancialDetailsForm.value)

  var compdata = this.CompanyForm.value;

  compdata.Document = this.documentArray;
  compdata.Address = this.CompanyOfficeAddress;
  compdata.FinancialDetails = this.FinancialDetailsForm.value;

  console.log(compdata)

  if(this.ID == '-1'){
    this.spinnerService.show();
    this.companyService.addCompany(compdata).subscribe((data:{})=>{
      if(data['status']){
        this.spinnerService.hide();
        this.snackBar.open('Company created successfully', '', {
          duration: 5000,
          panelClass: ['success-snackbar'],
          verticalPosition: 'top'
      });
      this.router.navigate(['/companies']);
      }
    }, err => {
      this.spinnerService.hide();
      this.snackBar.open('Server Error', '', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
    })
  }
  else{
    this.notificationService.smartMessageBox({
        title: "Update!",
        content: "Are you sure update this record?",
        buttons: '[No][Yes]'
      }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") {
          this.spinnerService.show();
          this.companyService.updateCompany(this.ID ,compdata).subscribe((data:{})=>{
            if(data['status']){
              this.snackBar.open('Company updated successfully', '', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
            this.spinnerService.hide();
            this.router.navigate(['/companies']);
            }
          }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
        }
      });
  }
  
}

validateNameEmail(val , type){
  console.log(val , type);
  if(val == null || val == undefined || val == ''){
    return 0;
  }
  this.spinnerService.show();
  if(this.CompanyEditDetail){
    if(this.CompanyEditDetail.Name == val ){
      this.ValidateCompanyName = false;
      return 0;
    }
    if(this.CompanyEditDetail.Email == val ){
      this.ValidateCompanyEmail = false;
      return 0;
    }
  } 
  this.companyService.getValidateCompanyNameAndEmail(type , val).subscribe((data:{})=>{
    this.spinnerService.hide();
    if(data['status']){
      if(type == 'Name'){
        if(data['result'] == null ){
          console.log('Name not present')
          this.ValidateCompanyName = false;
          
        }
        else{
          console.log('Name already present')
          this.ValidateCompanyName = true;
          this.snackBar.open('Company Name is already exist , Please try diffrent Name', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
          });
        }
      }
      else{
        if(data['result'] == null ){
          console.log('Email not present')
          this.ValidateCompanyEmail = false;
          
        }
        else{
          console.log('Name already present')
          this.ValidateCompanyEmail = true;
          this.snackBar.open('Company Email is already exist , Please try diffrent Email', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
          });
        }
      }
    }
  }, err => {
    this.spinnerService.hide();
    this.snackBar.open('Server Error', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
    });
  })
}

getOne(){
  this.companyService.getOneCompanyDetail(this.ID).subscribe((data:{})=>{
    if(data['status']){
      this.CompanyEditDetail = data['result'];
      this.CompanyForm = this.fb.group({
        'Type' : [ data['result'].Type , [Validators.required]],
        'Name' : [data['result'].Name , [Validators.required]],
        'Email' : [data['result'].Email , [Validators.required, Validators.email  , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ]],
        'Phone' : [data['result'].Phone , [Validators.required,  Validators.maxLength(15)]],
        'Password' : ['' , [ Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$')]],
        'ConfirmPassword' : ['' , [Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$')]],
        'FirstName' : [ data['result'].Manager.FirstName , [Validators.required]],
        'MiddleName' : [ data['result'].Manager.MiddleName ],
        'LastName' : [ data['result'].Manager.LastName ],
        'ManagerEmail' : [ data['result'].Manager.Email , [Validators.required]],
        'ManagerPhone' : [ data['result'].Manager.Phone , [Validators.required]],
       
      })

      console.log(data['result'].Manager.ManagerEmail)

      this.FinancialDetailsForm = this.fb.group({
        Type: [ data['result'].FinancialDetails.Type, [Validators.required ] ],
        Name : [data['result'].FinancialDetails.Name, [Validators.required ]],
        BankName: [ data['result'].FinancialDetails.BankName, [Validators.required ] ],
        AccountNumber: [ data['result'].FinancialDetails.AccountNumber, [Validators.required ] ],
        IFSC: [ data['result'].FinancialDetails.IFSC, [Validators.required] ],
        CraditDays: [ data['result'].FinancialDetails.CraditDays, [Validators.required ] ],
        CraditLimits: [ data['result'].FinancialDetails.CraditLimits, [Validators.required] ], 
      });

      this.getAllDocumentsOnExisting(data['result'].Document);
      this.CompanyOfficeAddress = data['CompanyAddress'];
      this.renderData = true;
    }
  }, err => {
    this.spinnerService.hide();
    this.snackBar.open('Server Error', '', {
        duration: 5000,
        panelClass: ['danger-snackbar'],
        verticalPosition: 'top'
    });
  })
}
getAllDocumentsOnExisting(localArray) {
  this.userService.getAllDocumentsByUser('Vendor').subscribe((data: {}) => {
      if (data['status']) {
          this.allDocuments = data['result'];
          for (const item of localArray) {
              const index = this.allDocuments.findIndex(x => x.Name === item.Name);
              this.allDocuments[index].value = item.Number ? item.Number : '';
              this.allImages[item.Name] = item.Image;
              // this.userService.getUserDocument(item.Image).subscribe((data: {}) => {
              //     this.createImageFromBlobForEdu(data).then(val => {
              //         this.allImages[item.Name] = val;
                      this.addDocumentDetailByPreviousSelected(item.Number, item.Name);
              //     });
              // });
          }
      }
  }, err => {
      this.snackBar.open('Server Error', 'Error', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
  });
}

createImageFromBlobForEdu(image: any) {
  return new Promise(resolve => {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
          resolve(reader.result);
      }, false);
      if (image) {
          reader.readAsDataURL(image);
      }
  });
}
addDocumentDetailByPreviousSelected(val, name) {
  // this.allImages
  let localArray = {};
  localArray['Name'] = name;
  localArray['Image'] = this.allImages[name];
  localArray['Number'] = val;
  let index = this.documentArray.findIndex(x => x.Name === name)
  if (index === -1) {
      if (name === 'Registration Amount Receipt' || name === 'Aadhar') {
          this.docMandatory++;
      }
      this.documentArray.push(localArray);
      this.documentUploadedArray[name] = name;
  } else {
      this.documentArray[index].Image = this.allImages[name];
      this.documentArray[index].Number = val;
  }
}

checkEmail(email){
  if(email == ''){
    return 0;
  }
  console.log(this.CompanyEditDetail.Manager)
  if(this.CompanyEditDetail.Manager){
    if(this.CompanyEditDetail.Manager.Email == email){
      this.ValidateCompanyManagerEmail = false;
      return 0;
    }
  }

  this.userService.checkEmail(email).subscribe((data: {}) => {
      if(data['status']){
          if(data['result'].length > 0){
              this.snackBar.open('Email that you filled is already in-use, Please use a different email ', 'success', {
                  duration: 5000,
                  panelClass: ['success-snackbar'],
                  verticalPosition: 'top'
                });
                this.ValidateCompanyManagerEmail = true;
            }
            else{
              this.ValidateCompanyManagerEmail = false;
            }
          }
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      })
  }
  ifscUpperCase(event){
    this.FinancialDetailsForm.patchValue({
      IFSC : (event).toUpperCase()
    })
  }
  
  lowerCase(val){
      this.CompanyForm.patchValue({
          Email : val.toLowerCase()
      })
  }
  lowerCaseManagerEmail(val){
      this.CompanyForm.patchValue({
        ManagerEmail : val.toLowerCase()
      })
  }

}
