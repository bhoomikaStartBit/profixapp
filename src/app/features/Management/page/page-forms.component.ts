import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';

import { Router} from '@angular/router';
import { PageService } from "@app/features/Management/page/page.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "@app/core/services";
import { CommonService } from '@app/core/common/common.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


declare var $: any;

@Component({
  selector: 'page-form',
  templateUrl: './page-forms.component.html',
})


export class PageFormComponent implements OnInit {
    ID : any;
    headerImageUrl : any;
    PageForm : FormGroup;
    getDescription = false;

    getData = false;    
    public validationPageOptions:any = {};
  constructor(private http: HttpClient ,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private pageService: PageService,
              private notificationService: NotificationService,
              private commonService : CommonService,
              private spinnerService : Ng4LoadingSpinnerService
  ) {
      this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      //console.log(this.ID);
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      this.PageForm = fb.group({
          Title: [ '', [Validators.required ] ],                
          Slug : [ '', [Validators.required ] ] ,
          HeaderImage : [ '' ],
          Description: [ '' ],
          SeoTitle: [ ''],
          SeoKeyword: [ ''],
          SeoDescription: [ ''],     
      });

      this.validationPageOptions = {
        // Rules for form validation
        rules: {
            Title: {
                required: true
            },                           
        },

        // Messages for form validation
        messages: {
            Title: {
                required: 'Please enter Page Title'
            },                     
        },
        submitHandler: this.onSubmitForm        

    };
    this.getData = true;

  }

    ngOnInit() {
    }

    uploadImage(event, name) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png') {
                reader.onload = () => {
                    // this.allKeyPersonImages[name] = reader.result;
                    if(name == 'Header'){
                        // this.CategoryForm.controls.HeaderImage.value = reader.result;
                        this.PageForm.patchValue({
                            "HeaderImage": reader.result
                        });
                    }
                    else{
                        // this.CategoryForm.controls.IconImage.value = reader.result;
                        this.PageForm.patchValue({
                            "IconImage": reader.result
                        });
                    }
                };
            } else {

            }
        }
    }
    
  descriptionss(){
      this.getDescription = true;
    }

    onSubmitForm() {
        this.PageForm.patchValue({
            'Description' : $('#summernote').summernote('code')
        })
        if (this.PageForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNewpage();
            this.onChangePageTitle();
        } else {
            this.editExisting();
        }        
    }

    onSubmitDraft() {
        this.PageForm.patchValue({
            'Description' : $('#summernote').summernote('code')
        })
        if (this.PageForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNewpagedraft();
            this.onChangePageTitle();
        } else {
            this.draftExisting();
        }        
    }


    createNewpage(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.pageService.addPage(this.PageForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(['/pages']);
                console.log("Created successfully");
            } else {
                
                this.snackBar.open(data['message'], 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });

    }

    createNewpagedraft(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.pageService.draftPage(this.PageForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record saved in Draft', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(['/pages']);
                console.log("Created successfully");
            } else {                
                this.snackBar.open(data['message'], 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });

    }
    
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.pageService.updatePage(this.ID, this.PageForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/pages']);
                        console.log("update success");
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
        // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    draftExisting(){
        this.notificationService.smartMessageBox({
            title: "Draft!",
            content: "Are you sure to draft this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.pageService.draftExistPage(this.ID, this.PageForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record stored in draft successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/pages']);
                        console.log("draft success");
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
        // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.pageService.getOnePage(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.PageForm = this.fb.group({
                    Title: [  data['result'].Title, [Validators.required ] ],
                    HeaderImage: [  '' ],
                    Description: [  data['result'].Description ],
                    SeoTitle: [ data['result'].SeoTitle ],                    
                    SeoKeyword: [ data['result'].SeoKeyword ],                    
                    SeoDescription: [ data['result'].SeoDescription ], 
                });
                this.headerImageUrl = this.commonService.getApiUrl()+data['result'].HeaderImage;
                $('#summernote').summernote('code', data['result'].Description);              
                this.pageService.getUserDocument(data['result'].HeaderImage).subscribe((data: {}) => {
                    this.createImageFromBlobForEdu(data).then(val => {
                        this.PageForm.patchValue({
                            'HeaderImage' : val
                        })
                    });
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
                
                // console.log(this.PageForm);
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }
    
    createImageFromBlobForEdu(image: any) {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                resolve(reader.result);
            }, false);
            if (image) {
                reader.readAsDataURL(image);
            }
        });

    }

    register(){
      console.log(this.PageForm.value);
        this.router.navigate(['/pages']);
    }

    cancel() {
        this.router.navigate(['/pages']);
    }

    onChangePageTitle() {
        let page = this.PageForm.get('Title').value
        page = page.replace(/[^\w\s]/gi, "");
        page = page.trim().toLowerCase();
        page = page.split(' ').join('-');
        let str = '';
        for(let i = 0; i< page.length; i++) {
          if(page.length-1 > i) {
              let first = page.charAt(i);
              let second = page.charAt(i + 1);
              if(!(first === '-' && second === '-')) {
                  str += page.charAt(i, i+1);
              }
          } else {
              str += page.charAt(i);
          }
        }
        if (str.length > 190) {
            str = str.substring(0, 190);
        }
        this.PageForm.patchValue({
            Slug: str
        });
    }
    

}
