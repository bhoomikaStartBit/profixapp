import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import { PageComponent } from "@app/features/Management/page/page.component";
import { PageFormComponent } from "@app/features/Management/page/page-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: PageComponent
  },
  {
    path: 'createnewpage/:id',
    component: PageFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
