import {NgModule} from "@angular/core";

import {routing} from "./page.routing";
import { PageComponent } from "@app/features/Management/page/page.component";
import { PageFormComponent } from "@app/features/Management/page/page-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import{SmartadminEditorsModule} from "@app/shared/forms/editors/smartadmin-editors.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";

@NgModule({
  declarations: [
    PageComponent,
     PageFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      SmartadminEditorsModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class PageModule {

}
