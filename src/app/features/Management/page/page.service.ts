import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class PageService {
  url: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/pages/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getAllPages(): Observable<any> {
    return this.http.get(this.url + 'getAll/').pipe(
      map(this.extractData));
  }

  getOnePage(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }

  getRelatedRecordOfPage(id): Observable<any> {
    return this.http.get(this.url + 'getRelatedRecordOfPage/' + id).pipe(
      map(this.extractData));
  }

  addPage (data): Observable<any> {
    return this.http.post<any>(this.url + 'create', JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }
  
  draftPage (data): Observable<any> {
    return this.http.post<any>(this.url + 'draft', JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  updatePage (id, data): Observable<any> {
    return this.http.put<any>(this.url + 'update/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${data._id}`)),
      catchError(this.handleError<any>('updateCity'))
    );
  }
  
  draftExistPage (id, data): Observable<any> {
    return this.http.put<any>(this.url + 'updatedraft/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${data._id}`)),
      catchError(this.handleError<any>('updateCity'))
    );
  }

  deletePage (id): Observable<any> {
    return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
      tap(( ) => console.log(`deleted role w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

    getUserDocument(url): Observable<any> {
        return this.http.get(this.commService.getApiUrl() + url, { responseType: 'blob' as 'json' }).pipe(
            map(this.extractData));
    }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
