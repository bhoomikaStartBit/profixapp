import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import { PageService } from "@app/features/Management/page/page.service";
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatSnackBar } from "@angular/material";
import { CommonService } from "@app/core/common/common.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'pagelist',
  templateUrl: './page.component.html',
})
export class PageComponent implements OnInit {
    Pages : any;
    url : any;
    displayedColumns: string[] = [        
        'Title',
        'Slug',
        'DateTime',
        'Active',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private pageService: PageService,
      private snackBar: MatSnackBar,
      private commonService: CommonService,
      private spinnerService: Ng4LoadingSpinnerService

  ) {
      this.spinnerService.hide();
      this.getAllPages();
      this.url = this.commonService.getApiUrl();
  }

  ngOnInit() {

  }

  getAllPages(){
        this.spinnerService.show();
        this.pageService.getAllPages().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Pages = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.filterPredicate = function(data, filter: string): boolean {
                return data.Title.toLowerCase().includes(filter);
            };
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            console.log(this.Pages);
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    applyFilter(filterValue: string) {
        console.log(filterValue)
        console.log(this.dataSource.data)
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    delete(id){
        console.log(id);
        this.spinnerService.show();
        this.pageService.getRelatedRecordOfPage(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                var lab ='';
                if(data['result'].length){
                    lab = 'You can\'t delete this record because this is having many dependant "'+ data['Table']+ '" records. Please delete all dependant records first.';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[Ok]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Ok") {

                        }

                    });
                }
                else{
                    lab = 'Are you sure delete this record?';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[No][Yes]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Yes") {
                            this.spinnerService.show();
                            this.pageService.deletePage(id).subscribe((data: {}) => {
                                this.spinnerService.hide();
                                if (data['status']) {
                                    this.getAllPages();
                                    this.snackBar.open('Record deleted successfully', 'Delete', {
                                        duration: 5000,
                                        panelClass: ['danger-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                } else {
                                }

                            }, err => {
                                this.spinnerService.hide();
                                this.snackBar.open('Server Error', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                              })
                        }
                        if (ButtonPressed === "No") {

                        }

                    });
                }

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }
}
