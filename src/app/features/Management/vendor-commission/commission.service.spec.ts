import { TestBed } from '@angular/core/testing';

import { CommissionService} from "@app/features/Management/vendor-commission/Commission.service";

describe('RoleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommissionService = TestBed.get(CommissionService);
    expect(service).toBeTruthy();
  });
});
