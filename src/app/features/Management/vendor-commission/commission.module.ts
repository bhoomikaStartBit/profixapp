import {NgModule} from "@angular/core";

import {routing} from "./commission.routing";
import {CommissionComponent} from "@app/features/Management/vendor-commission/commission.component";
import {CommissionFormComponent} from "@app/features/Management/vendor-commission/commission-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
    CommissionComponent,
    CommissionFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [],
})
export class CommissionModule {

}
