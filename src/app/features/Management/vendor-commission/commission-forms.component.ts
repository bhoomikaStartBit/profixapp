import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommissionService} from "@app/features/Management/vendor-commission/commission.service";

@Component({
  selector: 'user-form',
  templateUrl: './commission-forms.component.html',
})
export class CommissionFormComponent implements OnInit {
    ID : any;
    RoleForm : FormGroup;
  constructor(private http: HttpClient,
              private router : Router,
              private route : ActivatedRoute,
              private location : Location,
              private fb: FormBuilder,
              private commissionService: CommissionService,
  ) {
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      this.RoleForm = this.fb.group({
          Name: [ '', [Validators.required ] ],
          Description: [ '', [Validators.required ] ],
      });
  }

  ngOnInit() {

  }

    onSubmitForm() {
        if (this.RoleForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        
    }
    editExisting(){
       
    }

    getOne(id) {
        
    }


    register(){
        // this.router.navigate(['/roles']);
        this.location.back();
    }

    // this.router.navigate(['/roles']);

    cancel() {
        this.location.back();
    }
}
