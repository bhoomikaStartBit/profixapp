import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class CommissionService {
  url: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/commissions/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getAllVendors(): Observable<any> {
    return this.http.get(this.url + 'getAllVendors/').pipe(
      map(this.extractData));
  }

  getAll(): Observable<any> {
    return this.http.get(this.url + 'getAll/').pipe(
      map(this.extractData));
  }

  getByID(id): Observable<any> {
    return this.http.get(this.url + 'getByID/' + id).pipe(
      map(this.extractData));
  }

  getOne(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }

  

  addNewServiceCommission(id): Observable<any> {
    return this.http.get(this.url + 'addNewServiceCommission/' + id).pipe(
      map(this.extractData));
  }

  addCommissionServices (data): Observable<any> {
    return this.http.post<any>(this.url + 'create', JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  updateCommissionServices(id, data): Observable<any> {
    return this.http.put<any>(this.url + 'updateCommission/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => { console.log('update');
      console.log(data);  }),
      catchError(this.handleError<any>('updateProfile'))
    );
  }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
