
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {CommissionComponent} from "@app/features/Management/vendor-commission/commission.component";
import {CommissionFormComponent} from "@app/features/Management/vendor-commission/commission-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: CommissionComponent
  },
  {
    path: ':id',
    component: CommissionComponent
  },{
    path: 'createnew/:id',
    component: CommissionFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
