import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError, startWith } from 'rxjs/operators';
import {NotificationService} from "@app/core/services";
import {CommissionService} from "@app/features/Management/vendor-commission/commission.service";
import {ServicesService} from "@app/features/Management/service/services.service";
import { forEach } from '@angular/router/src/utils/collection';
import {MatSnackBar} from "@angular/material";
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { UnitService } from "@app/features/Management/unit/unit.service";
import {CommonService} from "@app/core/common/common.service";

import {FormControl} from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'userlist',
  templateUrl: './commission.component.html',
  styles: [
    `.mat-form-field {
  display: inline-block;
  position: relative;
  text-align: left;
  width: 100%;
}.mat-option {
    line-height: 24px;
    height: 60px;padding: 1px;}.rwmain {
        width: 100%;
    }.rwone {
        width: 17%;
        float: left;
        margin: 0;
        padding: 0;
    }.rwtwo {
        width: 83%;
        float: left;
        text-align: left;
        margin: 0px;
        padding: 0;
        padding-left: 9px;
    }`
]
})
export class CommissionComponent implements OnInit {

  Vendors : any;
  Vendor : any;
  Services : any;
  Commissions : any;
  selectedVendor: string;
  cvendor : Array<{VendorID: string, ServiceID: string, CommissionType : string, Commission : string, Active : number }> = [];
  ButtonText : any;
  Units : any;
  searchControl = new FormControl();
  filteredOptions: Observable<string[]>;
  url: any;
  vndName = false;
  defalutimageurl: any;
  ID :any ;
  

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private commissionService: CommissionService,
      private servicesService: ServicesService,
      private snackBar: MatSnackBar,
      private unitService: UnitService,
      private commonService: CommonService,
      private spinnerService : Ng4LoadingSpinnerService,
      private route : ActivatedRoute,
      private router: Router,

  ) {
    this.spinnerService.hide();
        this.getAllVendors();
        this.ButtonText = 'None';
        this.url = this.commonService.getApiUrl();
        this.defalutimageurl = this.commonService.getApiUrl()+'/Documents/Avatar.png';
        this.unitService.getAllUnit().subscribe( (data: {}) => {
        this.Units = data['result'];
      });
      this.ID = this.route.params['value'].id;
      if(this.ID ){
          this.changeCommission(this.ID)
      }
  }

  ngOnInit() {
    this.filteredOptions = this.searchControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

    private _filter(value: string): string[] {
        if(value.length>=3) {
            const filterValue = value.toLowerCase();
            const filteredSet = this.Vendors.filter(option => option.FirstName.toLowerCase().includes(filterValue));
            return filteredSet;
        }else {
            return []; 
        }
    }

    getCommission(value){
        //console.log(value);
        this.changeCommission(value);
        this.searchControl.setValue('');
    }

    getAll(){
        this.spinnerService.show();
        this.commissionService.getAll().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Services = data['result'];
            console.log(this.Services);
        });
        //this.Vendors = [{ '_id' : 1, 'UserName' : 'Ravi' },{ '_id' : 2, 'UserName' : 'Rohit' } ]
    }

    getVendorByID(id){
        if(id){
            this.spinnerService.show();
            this.commissionService.getOne(id).subscribe( (data: {}) => {
                this.spinnerService.hide();
                this.Vendor = data['result'];
                this.searchControl.setValue(this.Vendor.FirstName+' '+ (this.Vendor.MiddleName ? this.Vendor.MiddleName : '')+' '+this.Vendor.LastName);
            });
        }
    }

    getAllVendors(){
        this.spinnerService.show();
        this.commissionService.getAllVendors().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Vendors = data['result'];
            console.log(this.Vendors);
        });
        //this.Vendors = [{ '_id' : 1, 'UserName' : 'Ravi' },{ '_id' : 2, 'UserName' : 'Rohit' } ]
    }

    getAllServices(){
        this.spinnerService.show();
        this.servicesService.getAllForCommission().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Services = data['result'];
            console.log(this.Services);
        });
    }

    changeCommission(value){
        this.selectedVendor = value;
        
        if(this.selectedVendor){
            this.getVendorByID(this.selectedVendor);
            this.spinnerService.show();
            this.commissionService.getByID(this.selectedVendor).subscribe( (data: {}) => {
                this.spinnerService.hide();
                this.Commissions = data['result'];
                console.log(this.Commissions.length);
                if(this.Commissions.length > 0){
                    this.Services = [];
                    this.ButtonText = 'Update';
                }else {
                    this.getAllServices();
                    this.ButtonText = 'Save';
                }
            });
        }
    }
    SaveVendorCommission(){
        var commCondition =  false;
        console.clear();

        this.commissionService.getByID(this.selectedVendor).subscribe( (data: {}) => {
  
            for(let comm of data['result']){
                
            var index = this.Commissions.findIndex(x=> x.ServiceID._id == comm.ServiceID._id);
                if(this.Commissions[index].Commission != comm.Commission ){
                    commCondition =  true;                
                }
                if(this.Commissions[index].Commission > 20){
                    //Commission can't be greater than 20%.
                    this.snackBar.open("Commission can't be greater than 20%.", '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                    return 0;
                }
            
            }
            if(commCondition){
                this.notificationService.smartMessageBox({
                    title: "Update Alert!",
                    content: "Are you sure to update Record?",
                    buttons: '[No][Yes]'
                }, (ButtonPressed) => {
                    if (ButtonPressed === "Yes") {
                        if(this.Commissions.length > 0){

                            for(var i=0; i < this.Commissions.length; i++){
                                this.cvendor = [];
                                //alert(JSON.stringify(this.Services[i]));
                                this.cvendor.push({'ServiceID' : this.Commissions[i].ServiceID._id, 'VendorID': this.selectedVendor, 'CommissionType': this.Commissions[i].CommissionType, 'Commission': this.Commissions[i].Commission, 'Active' : 1 })
                                this.spinnerService.show();
                                this.commissionService.updateCommissionServices(this.Commissions[i]._id, this.cvendor).subscribe((data: {}) => {
                                    this.spinnerService.hide();
                                    if ( data['status']) {
                                        this.snackBar.open('Updated successfully', 'success', {
                                            duration: 5000,
                                            panelClass: ['success-snackbar'],
                                            verticalPosition: 'top'
                                        });
                                        console.log("Updated successfully");
                                    }
                                }, err => {
                                    this.spinnerService.hide();
                                    this.snackBar.open('Error', 'error', {
                                        duration: 5000,
                                        panelClass: ['danger-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                });
                            }
                            
                            
                        }else {
                        
                            for(var i=0; i < this.Services.length; i++){
                                //alert(JSON.stringify(this.Services[i]));
                                this.cvendor.push({'ServiceID' : this.Services[i]._id, 'VendorID': this.selectedVendor, 'CommissionType': this.Services[i].CommissionType, 'Commission': this.Services[i].Commission, 'Active' : 1 })
                            
                            }
                            this.spinnerService.show();
                            this.commissionService.addCommissionServices(this.cvendor).subscribe((data: {}) => {
                                this.spinnerService.hide();
                                if ( data['status']) {
                                    console.log("Created successfully");
                                    this.snackBar.open('Created successfully', 'success', {
                                        duration: 5000,
                                        panelClass: ['success-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                    this.changeCommission(this.selectedVendor);
                                }
                            }, err => {
                                this.snackBar.open('Server Error', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                            });
                        }  
                    }
                })
            }
            else{
                
            }
    })

       

        
        //alert(JSON.stringify(this.cvendor));
    }

    back(){
        var commCondition =  false;
        console.clear();

        this.commissionService.getByID(this.selectedVendor).subscribe( (data: {}) => {
  
            for(let comm of data['result']){
                
            var index = this.Commissions.findIndex(x=> x.ServiceID._id == comm.ServiceID._id);
                if(this.Commissions[index].Commission != comm.Commission ){
                    commCondition =  true;                
                }
            
            }
            if(commCondition){
                this.notificationService.smartMessageBox({
                    title: "Back Alert!",
                    content: "Oops It looks like you made some chagnes, that are not saved. Are you sure to leave this screen?",
                    buttons: '[No][Yes]'
                }, (ButtonPressed) => {
                    if (ButtonPressed === "Yes") {
                        this.router.navigate(['users']);
                    }
                    else{
    
                    }
                })
            }
            else{
                this.router.navigate(['users']);
            }
    })

        
    }
}
