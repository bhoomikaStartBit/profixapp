import {NgModule} from "@angular/core";

import {routing} from "./categories.routing";
import {ServiceCategoriesComponent} from "@app/features/Management/categories/categories.component";
import {ServiceCategoriesFormComponent} from "@app/features/Management/categories/categories-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import{SmartadminEditorsModule} from "@app/shared/forms/editors/smartadmin-editors.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      ServiceCategoriesComponent,
      ServiceCategoriesFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      SmartadminEditorsModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class ServiceCategoriesModule {

}
