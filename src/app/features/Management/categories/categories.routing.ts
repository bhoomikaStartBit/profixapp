
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {ServiceCategoriesComponent} from "@app/features/Management/categories/categories.component";
import {ServiceCategoriesFormComponent} from "@app/features/Management/categories/categories-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: ServiceCategoriesComponent
  },{
    path: 'createnew/:id',
    component: ServiceCategoriesFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
