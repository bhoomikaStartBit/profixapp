import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';

import { Router} from '@angular/router';
import {CategoriesService} from "@app/features/Management/categories/categories.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "@app/core/services";
import { CommonService } from '@app/core/common/common.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


declare var $: any;

@Component({
  selector: 'user-form',
  templateUrl: './categories-forms.component.html',
})


export class ServiceCategoriesFormComponent implements OnInit {
    ID : any;
    headerImageUrl : any;
    iconImageUrl: any;
    CategoryForm : FormGroup;
    getDescription = false;    
  constructor(private http: HttpClient ,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private categoriesService: CategoriesService,
              private notificationService: NotificationService,
              private commonService : CommonService,
              private spinnerService : Ng4LoadingSpinnerService
              
  ) {
      this.spinnerService.hide();      
      this.ID = this.route.params['value'].id;
      console.log(this.ID);
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      this.CategoryForm = fb.group({
          Name: [ '', [Validators.required ] ],
          Description: [ 'Demo', [Validators.required ] ],
          HeaderImage : [ '', [Validators.required ] ],
          IconImage : [ '', [Validators.required ] ],
          Isfeatured : [ false ],
          Slug : [ '', [Validators.required ] ],
          Order : [ 0, [Validators.required ] ]
      });
  }

  ngOnInit() {
  }



    uploadImage(event, name) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            if((file.size)/1024 > 1024){
                this.snackBar.open('File size is too large , please choose file below 1MB size', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png') {
                reader.onload = () => {
                    // this.allKeyPersonImages[name] = reader.result;
                    if(name == 'Header'){
                        // this.CategoryForm.controls.HeaderImage.value = reader.result;
                        this.CategoryForm.patchValue({
                            "HeaderImage": reader.result
                        });
                    }
                    else{
                        // this.CategoryForm.controls.IconImage.value = reader.result;
                        this.CategoryForm.patchValue({
                            "IconImage": reader.result
                        });
                    }
                };
            } else {
                this.snackBar.open('File format is not support , please choose diffrent file', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            }
        }
    }
    descriptionss(){
      this.getDescription = true;
    }

    onSubmitForm() {
        this.CategoryForm.patchValue({
            'Description' : $('#summernote').summernote('code')
        })
        if (this.CategoryForm.invalid || !this.getDescription) {
            this.snackBar.open('All fields are required.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        
        this.spinnerService.show();
        this.categoriesService.addCategories(this.CategoryForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                
                this.router.navigate(['/categories']);
                console.log("Created successfully");
            } else {
                
                this.snackBar.open(data['message'], 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.categoriesService.updateCategories(this.ID, this.CategoryForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        
                        this.router.navigate(['/categories']);
                        console.log("update success");
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
        // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.categoriesService.getOneCategories(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {                
                this.CategoryForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    Description: [  data['result'].Description, [Validators.required ] ],
                    HeaderImage: [  '', [Validators.required ] ],
                    IconImage: [  '', [Validators.required ] ],
                    Slug: [ data['result'].Slug, [Validators.required ] ],
                    Order: [ data['result'].Order, [Validators.required ] ],
                    Isfeatured: [  data['result'].Isfeatured ]
                });
                this.headerImageUrl = this.commonService.getApiUrl()+data['result'].HeaderImage;
                this.iconImageUrl = this.commonService.getApiUrl()+data['result'].IconImage;
                $('#summernote').summernote('code', data['result'].Description);
                this.categoriesService.getUserDocument(data['result'].HeaderImage).subscribe((data: {}) => {
                    this.createImageFromBlobForEdu(data).then(val => {
                        this.CategoryForm.patchValue({
                            'HeaderImage' : val
                        })
                    });
                });

                this.categoriesService.getUserDocument(data['result'].IconImage).subscribe((data: {}) => {
                    this.createImageFromBlobForEdu(data).then(val => {
                        this.CategoryForm.patchValue({
                            'IconImage' : val
                        })
                    });
                });
                console.log(this.CategoryForm);
            }
        }, err => {
            this.spinnerService.hide();
        });
    }

    createImageFromBlobForEdu(image: any) {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                resolve(reader.result);
            }, false);
            if (image) {
                reader.readAsDataURL(image);
            }
        });

    }

    register(){
      console.log(this.CategoryForm.value);
        this.router.navigate(['/categories']);
    }

    cancel() {
        this.router.navigate(['/categories']);
    }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    onChangeCategory() {
        let category = this.CategoryForm.get('Name').value
        category = category.replace(/[^\w\s]/gi, "");
        category = category.trim().toLowerCase();
        category = category.split(' ').join('-');
        let str = '';
        for(let i = 0; i< category.length; i++) {
          if(category.length-1 > i) {
              let first = category.charAt(i);
              let second = category.charAt(i + 1);
              if(!(first === '-' && second === '-')) {
                  str += category.charAt(i, i+1);
              }
          } else {
              str += category.charAt(i);
          }
        }
        if (str.length > 190) {
            str = str.substring(0, 190);
        }
        this.CategoryForm.patchValue({
            Slug: str + '-category'
        });
    }

}
