import {NgModule} from "@angular/core";

import {routing} from "./types.routing";
import {ServiceTypesComponent} from "@app/features/Management/types/types.component";
import {ServiceTypesFormComponent} from "@app/features/Management/types/types-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";

import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      ServiceTypesComponent,
      ServiceTypesFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      MaterialModuleModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
  ],
})
export class ServiceTypesModule {

}
