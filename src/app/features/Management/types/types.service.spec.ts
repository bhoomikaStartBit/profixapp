import { TestBed } from '@angular/core/testing';

import { CategoriesService} from "@app/features/Management/categories/categories.service";

describe('TypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypesService = TestBed.get(TypesService);
    expect(service).toBeTruthy();
  });
});
