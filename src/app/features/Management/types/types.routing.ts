
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {ServiceTypesComponent} from "@app/features/Management/types/types.component";
import {ServiceTypesFormComponent} from "@app/features/Management/types/types-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: ServiceTypesComponent
  },{
    path: 'createnew/:id',
    component: ServiceTypesFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
