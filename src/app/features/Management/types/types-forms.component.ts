import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import {Location} from "@angular/common";
import {TypesService} from "@app/features/Management/types/types.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Params} from '@angular/router';
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'user-form',
  templateUrl: './types-forms.component.html',
})
export class ServiceTypesFormComponent implements OnInit {
    ID : any;
    TypeForm : FormGroup;
 
    constructor(private http: HttpClient,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private location : Location,
              private typesService : TypesService,
              private notificationService : NotificationService,
              private snackBar : MatSnackBar,
              private spinnerService : Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
        this.TypeForm = this.fb.group({
            Name: [ '', [Validators.required ] ],
            Description: [ '', [Validators.required ] ],
        });
    }

    ngOnInit() {


    }


    onSubmitForm() {
        if (this.TypeForm.invalid) {
            this.snackBar.open('All Fields are required.', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.typesService.addTypes(this.TypeForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.typesService.updateTypes(this.ID, this.TypeForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.snackBar.open('Server Error', 'Error', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }
        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.typesService.getOneTypes(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.TypeForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                    Description: [  data['result'].Description, [Validators.required ] ],
                });

                console.log(this.TypeForm);
            }
        }, err => {
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }
    register(){
        this.location.back();
    }
    cancel() {
        // this.router.navigate(['/types']);
        this.location.back();
    }

}
