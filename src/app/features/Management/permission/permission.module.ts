import {NgModule} from "@angular/core";

import {routing} from "./permission.routing";
import {PermissionComponent} from "@app/features/Management/permission/permission.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import { TreeviewModule } from 'ngx-treeview';
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
    declarations: [
        PermissionComponent,
    ],
    imports: [
        SharedModule,
        routing,
        SmartadminDatatableModule,
        FormsModule , ReactiveFormsModule,
        MaterialModuleModule , TreeviewModule.forRoot(),
        Ng4LoadingSpinnerModule.forRoot()
    ],
    providers: [
        FormsModule , ReactiveFormsModule
    ],
})
export class PermissionModule {

}
