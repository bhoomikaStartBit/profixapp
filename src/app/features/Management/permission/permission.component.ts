import {Component, OnInit, ViewChild, Injectable} from '@angular/core';
import { PermissionService} from './permission.service';
import { Router} from '@angular/router';

import { MatDialog, MatDialogRef  } from '@angular/material';
import {MatSnackBar} from '@angular/material';
import {TreeviewI18n, TreeviewItem, TreeviewConfig, TreeviewHelper, TreeviewComponent,
    TreeviewEventParser, OrderDownlineTreeviewEventParser, DownlineTreeviewItem} from 'ngx-treeview';
import { RoleService} from "@app/features/Management/roles/role.service";
import { AuthenticationService} from "@app/core/common/_services/authentication.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {UserService} from "@app/features/Management/users/user.service";


export class ProductTreeviewConfig extends TreeviewConfig {
    hasAllCheckBox = false;
    hasFilter = false;
    hasCollapseExpand = false;
    maxHeight = 400;
}

@Component({
    selector: 'app-permission',
    templateUrl: './permission.component.html',
    styleUrls: ['./permission.component.scss'],
    providers: [
        { provide: TreeviewEventParser, useClass: OrderDownlineTreeviewEventParser },
        { provide: TreeviewConfig, useClass: ProductTreeviewConfig }],
})
export class PermissionComponent implements OnInit {
    allMenus: any = [];
    
    @ViewChild(TreeviewComponent) treeviewComponent: TreeviewComponent;
    items: TreeviewItem[];
    rows: string[];
    allRoles: any = [];
    HiddenRole: any;
    RoleID: string;

    SuperAdminRoleID: any;
    CustomerManagerRoleID: any;
    ServiceProviderRoleID: any;
    VendorRoleID: any;
    PaymentCollectorRoleID: any;
    QualityInspectorRoleID: any;
    CustomerRoleID: any;
    constructor(
        private permissionService: PermissionService,
        private router: Router,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        private userService: UserService,
        private roleService: RoleService,
        private authenticationService: AuthenticationService,
        private spinnerService : Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
        /* setTimeout(() => {
           this.authenticationService.checkSelectedComponentVisiblity('Permission');
         }, 100);*/
      this.userService.getRoleIDByKey('CM').subscribe( (data)=>{ 
        this.CustomerManagerRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('SA').subscribe( (data)=>{ 
        this.SuperAdminRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('SP').subscribe( (data)=>{ 
        this.ServiceProviderRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('V').subscribe( (data)=>{ 
        this.VendorRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('PC').subscribe( (data)=>{ 
        this.PaymentCollectorRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('QI').subscribe( (data)=>{ 
        this.QualityInspectorRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      this.userService.getRoleIDByKey('C').subscribe( (data)=>{ 
        this.CustomerRoleID = data.result[0]._id;
      }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
      
      this.HiddenRole = [ this.QualityInspectorRoleID , this.VendorRoleID , this.CustomerRoleID , this.PaymentCollectorRoleID , this.ServiceProviderRoleID];
    }

    ngOnInit() {
        this.getAllRoles();
    }
    onSelectedChange(downlineItems: DownlineTreeviewItem[]) {
        // console.log(this.items);
    }
    getAllMenus() {
        this.spinnerService.show();
        this.permissionService.getAll(this.RoleID).subscribe((data) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.allMenus = data['result'];
                this.allMenus.sort(function(a, b) {
                    if (a.children) {
                        a.children.sort(function(c, d) {
                            return c.sequence - d.sequence;
                        });
                    }
                    return a.sequence - b.sequence;
                });
                const tempArray = [];
                for (const arr of this.allMenus) {
                    const val = new TreeviewItem(arr);
                    tempArray.push(val);
                }
                this.items = tempArray;
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }
    getAllRoles() {
        this.spinnerService.show();
        this.roleService.getAllRoles().subscribe((data) => {
            this.spinnerService.hide();
            if (data['status'])  {
                var role = data['result'];
                var selectedRole = role.filter((row)=>{
                    if(this.HiddenRole.indexOf(row._id) == -1){
                       return row;
                    }
                })
                this.allRoles = selectedRole;
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }
    onSubmit() {
        this.spinnerService.show();
        this.permissionService.updatePermission(this.RoleID, this.items).subscribe((data) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.snackBar.open(data['message'], 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }
}
