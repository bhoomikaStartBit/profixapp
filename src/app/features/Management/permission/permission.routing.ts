
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {PermissionComponent} from "@app/features/Management/permission/permission.component";


export const routes:Routes = [

    {
        path: '',
        component: PermissionComponent,
        pathMatch: 'full'
    }


];

export const routing = RouterModule.forChild(routes);
