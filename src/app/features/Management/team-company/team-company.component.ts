import { Component, OnInit ,ViewChild } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {TeamCompanyService} from "@app/features/Management/team-company/team-company.service";
import {MatSnackBar} from "@angular/material";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'userlist',
  templateUrl: './team-company.component.html',
})
export class TeamCompanyComponent implements OnInit {
    localStorage : any;
    TeamCompanies : any;
    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];

    displayedColumns: string[] = [
        'SN',
        'Name',        
        'TLID',
        'Projects',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
        private http: HttpClient ,
        private notificationService: NotificationService,
        private snackBar: MatSnackBar,
        private teamCompanyService: TeamCompanyService,
        private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.localStorage = JSON.parse(localStorage.getItem('currentUser'));
      if (this.localStorage.RoleName === 'Super Admin') {
          this.getAllTeamCompanies(0);
      } else {
          this.getAllTeamCompanies(this.localStorage.id);
      }
  }

  ngOnInit() {
  }

    getAllTeamCompanies(selectedLeaderID) {
        this.spinnerService.show();
        this.TeamCompanies = [];
        this.teamCompanyService.getAllTeamCompany(selectedLeaderID).subscribe((data: {}) => {
            this.spinnerService.hide();
            this.TeamCompanies = data['result'];   
            console.log(this.TeamCompanies);         
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            
        }, err => {
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

  delete(id,TeamLeaderID){      
      this.notificationService.smartMessageBox({
          title: "Delete!",
          content: 'Are you sure delete this record?',
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") { 
            this.spinnerService.show();             
              this.teamCompanyService.deleteTeamCompany(id,TeamLeaderID).subscribe((data: {}) => {
                this.spinnerService.hide();
                  if (data['status']) {
                    this.snackBar.open('Record deleted successfully', 'Delete', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  } else {
                  }
                  if (this.localStorage.RoleName === 'Super Admin') {
                    this.getAllTeamCompanies(0);
                } else {
                    this.getAllTeamCompanies(this.localStorage.id);
                }
              }, err => {
                this.snackBar.open('Server Error', 'Error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
          }
          if (ButtonPressed === "No") {

          }

      });
  }

    rejectRequest(id){
        this.notificationService.smartMessageBox({
            title: "Are you sure?",
            content: 'On this event all entries will be deleted and respected user will be notified about this event.',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.rejectRequest(id).subscribe( (data: {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open(data['message'], 'Delete', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        if (this.localStorage.RoleName === 'Super Admin') {
                            this.getAllTeamCompanies(0);
                        } else {
                            this.getAllTeamCompanies(this.localStorage.id);
                        }
                    }
                }, err => {
                    console.log(err);
                    this.snackBar.open('Server Error', 'Delete', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    acceptRequest(id){
        this.spinnerService.show();
        this.teamCompanyService.acceptRequest(id).subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open(data['message'], 'Delete', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                if (this.localStorage.RoleName === 'Super Admin') {
                    this.getAllTeamCompanies(0);
                } else {
                    this.getAllTeamCompanies(this.localStorage.id);
                }
            }
        }, err => {
            this.spinnerService.hide();
            console.log(err);
            this.snackBar.open('Server Error', 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }

}
