import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NotificationService } from "@app/core/services";
import { MatSnackBar, MatStepper } from "@angular/material";
import { TeamCompanyService } from "@app/features/Management/team-company/team-company.service";
import { CommonService } from "@app/core/common/common.service";
import { ProjectsService } from "@app/features/Management/projects/projects.service";
import { CompaniesService } from "@app/features/Management/Companies/companies.service";

import {
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/animations'
import { UserService } from '../users/user.service';


import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'team-form',
    templateUrl: './team-company-forms.component.html',
    animations: [
        trigger('changePane', [
            state('out', style({
                height: 0,
            })),
            state('in', style({
                height: '*',
            })),
            transition('out => in', animate('250ms ease-out')),
            transition('in => out', animate('250ms 300ms ease-in'))
        ])
    ],
    styles: [
        `.mat-form-field {
      display: inline-block;
      position: relative;
      text-align: left;
    }.mat-option {
        line-height: 24px;
        height: 60px;padding: 1px;}.rwmain {
            width: 100%;
        }.rwone {
            width: 14%;
            float: left;
            margin: 0;
            padding: 0;
        }.rwtwo {
            width: 83%;
            float: left;
            text-align: left;
            margin-top: 0.6em;
            padding: 0;
            padding-left: 9px;
        }`
    ]
})
export class TeamCompanyFormComponent implements OnInit {
    ID: any;
    Projects: any = [];
    getValue = false;
    TeamMember = '';
    TeamCompanyMembers: any = [];
    TeamCompanyBasicInfoForm: FormGroup;
    currentUser: any;
    filteredOptions: Observable<string[]>;
    UserfilteredOptions: Observable<string[]>;
    spControl = new FormControl();
    userspControl = new FormControl();
    ServiceProviders: any = [];
    ServiceProvider: any = [];
    defalutimageurl: any;
    imageurl: any;
    ServiceProviderFullName: any;
    ServiceProviderEmail: any;
    getData = false;
    checkteamtype: any;
    Companies: any;
    AddTeamMemebers = new FormControl();

    @ViewChild('stepper') stepper1: MatStepper;
    ServiceProvidersList: any = []

    public validationOptions: any = {};

    /** control for the selected bank for multi-selection */
    public bankMultiCtrl: FormControl = new FormControl();
    /** control for the MatSelect filter keyword multi-selection */
    public bankMultiFilterCtrl: FormControl = new FormControl();

    public filteredBanksMulti: any = [];

    constructor(private http: HttpClient,
        private router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private location: Location,
        private notificationService: NotificationService,
        private snackBar: MatSnackBar,
        private userService: UserService,
        private teamCompanyService: TeamCompanyService,
        private commonService: CommonService,
        private projectsService: ProjectsService,
        private companiesService: CompaniesService,
        private spinnerService: Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
        this.validationOptions = {
            // Rules for form validation
            rules: {
                TeamType: {
                    required: true
                },
                TeamName: {
                    required: true
                },
                Projects: {
                    required: true
                },
                spControl: {
                    required: true
                },

            },

            // Messages for form validation
            messages: {
                TeamType: {
                    required: 'Please select TeamType'
                },
                TeamName: {
                    required: 'Please enter Team Name'
                },
                Projects: {
                    required: 'Please select Projects'
                },
                spControl: {
                    required: 'Please select the Service Provider'
                },

            },
            submitHandler: this.onSubmitTeamForm

        };
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.ID = this.route.params['value'].id;
        if (this.ID !== '-1') {
            this.getOne(this.ID);
        }

        this.TeamCompanyBasicInfoForm = fb.group({
            Projects: [''],
            TeamName: ['', [Validators.required]],
            TeamType: ['Permanent', [Validators.required]],
            TLID: ['', [Validators.required]],
            FullName: [''],
            Email: [''],
            TeamMembers: ['']

        });
        this.getValue = true;

        this.ID = this.route.params['value'].id;

        this.getAllServiceProvider();


        if (this.ID !== '-1') {
            this.getAllTeamMemberByTeamID(this.ID);
        }
        this.imageurl = this.commonService.getApiUrl();
        this.defalutimageurl = this.commonService.getApiUrl() + '/Documents/Avatar.png';

    }

    ngOnInit() {
        this.ID = this.route.params['value'].id;

        this.filteredOptions = this.spControl.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );

        this.UserfilteredOptions = this.userspControl.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filterval(value))
            );


        this.bankMultiFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterBanksMulti();
            });

    }

    private _onDestroy = new Subject<void>();

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    private filterBanksMulti() {
        if (!this.ServiceProviders) {
            return;
        }
        // get the search keyword
        let search = this.bankMultiFilterCtrl.value;
        if (!search) {
            this.filteredBanksMulti = (this.ServiceProviders.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        // filter the banks
        var data =
            this.ServiceProviders.filter(user =>
                user.FirstName.toLowerCase().includes(search) ||
                user.MiddleName.toLowerCase().includes(search) ||
                user.LastName.toLowerCase().includes(search) ||
                (user.Phone).toString().toLowerCase().includes(search) ||
                user.ProfixID.toLowerCase().includes(search)
            )
            ;

        this.filteredBanksMulti = data;
    }

    private _filter(value: string): string[] {
        if (value.length >= 3) {
            const filterValue = value.toLowerCase();
            const filteredSet = this.ServiceProviders.filter(
                option => (
                    option.FirstName.toLowerCase().includes(filterValue) ||
                    option.MiddleName.toLowerCase().includes(filterValue) ||
                    option.LastName.toLowerCase().includes(filterValue) ||
                    option.ProfixID.toLowerCase().includes(filterValue) ||
                    (option.Phone).toString().includes(filterValue)));

            return filteredSet;
        } else {
            return [];
        }
    }

    private _filterval(value: string): string[] {
        if (value.length >= 3) {
            const filterValue = value.toLowerCase();
            const filteredSet2 = this.ServiceProviders.filter(option => (option.FirstName.toLowerCase().includes(filterValue) || option.ProfixID.toLowerCase().includes(filterValue) || (option.Phone).toString().includes(filterValue)));
            return filteredSet2;
        } else {
            return [];
        }
    }

    getServiceProviderId(value) {
        this.TeamMember = value;
        this.TeamCompanyBasicInfoForm.controls.TLID.setValue(value);
        this.spControl.setValue('');
        this.getOneServiceProvider(value);
    }
    getUserServiceProviderId(value) {
        var data = this.ServiceProviders.filter(x => x._id == value);
        this.TeamCompanyMembers.push(data[0]);
        this.TeamCompanyBasicInfoForm.controls.TeamMember.setValue(value);
        this.userspControl.setValue('');
    }

    getAllServiceProvider() {
        this.spinnerService.show();
        if (this.route.snapshot.queryParams['ForCompany']) {
            this.companiesService.getAllCompanySPUser(this.route.snapshot.queryParams['ForCompany']).subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.ServiceProvidersList = data['result'];
                    this.ServiceProviders = data['result'];
                    // if(data['result'].length < 1){
                    //     this.snackBar.open('No sp present , please first create sp user', 'success', {
                    //         duration: 5000,
                    //         panelClass: ['danger-snackbar'],
                    //         verticalPosition: 'top'
                    //     });
                    // }
                }
                else {

                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            })
        }
        else {
            this.userService.getAllUserWithSpRole().subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.ServiceProvidersList = data['result'];
                    this.ServiceProviders = data['result'];
                }
                else {

                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', 'success', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            })
        }
    }

    getOneServiceProvider(id) {
        this.spinnerService.show();
        this.teamCompanyService.getOneServiceProvider(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.ServiceProvider = data['result'];
                this.spControl.setValue(this.ServiceProvider.FirstName + ' ' + this.ServiceProvider.MiddleName + ' ' + this.ServiceProvider.LastName);
                this.ServiceProviderFullName = this.ServiceProvider.FirstName + ' ' + this.ServiceProvider.MiddleName + ' ' + this.ServiceProvider.LastName;
                this.ServiceProviderEmail = this.ServiceProvider.Email;
            }
            else {

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', 'error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllTeamMemberByTeamID(TID) {
        this.spinnerService.show();
        this.teamCompanyService.getAllTeamMemberByTeamID(TID).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.TeamCompanyMembers = data['result'];
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    DuplicateTeamNamecheck(name) {
        this.spinnerService.show();
        this.teamCompanyService.DuplicateTeamNamecheck(this.TeamCompanyBasicInfoForm.controls.TeamName.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                if (data['result']) {
                    this.snackBar.open('This tean name is already exist , please try with a diffrent name.', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                }

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllProjects() {

        this.projectsService.getForProject().subscribe((data: {}) => {
            if (data['status']) {
                this.Projects = data['data'];
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }



    getTeamType(id) {
        this.checkteamtype = id;
        if (id == 'Permanent') {
            this.TeamCompanyBasicInfoForm.patchValue({
                Projects: ''
            })
        }
        if (id == 'Project') {
            this.getAllProjects();
        }
    }

    backTL() {
        this.ServiceProviders = this.ServiceProvidersList;
        if (this.bankMultiCtrl.value) {
            this.ServiceProviders = this.ServiceProviders.filter(x => {
                var index = this.bankMultiCtrl.value.findIndex(y => y._id == x._id);
                if (index == '-1') {
                    return x;
                }
            })
        }
        this.stepper1.previous();
    }

    nextStep() {
        if(this.spControl.value == ''){
            return 0;
        }
        this.TeamCompanyMembers = [];
        this.ServiceProviders = this.ServiceProvidersList;
        if (this.bankMultiCtrl.value) {
            this.ServiceProviders = this.ServiceProviders.filter(x => x._id != this.TeamCompanyBasicInfoForm.controls.TLID.value);
            var dat = this.ServiceProviders.filter(x => {
                var index = this.bankMultiCtrl.value.findIndex(y => (y._id == x._id));
                if (index != '-1') {
                    this.TeamCompanyMembers.push({
                        _id: x._id,
                        Email: x.Email,
                        ProfixID: x.ProfixID,
                        FirstName: x.FirstName,
                        MiddleName: x.MiddleName,
                        LastName: x.LastName,
                        Phone: x.Phone,
                    });
                }
            })
        }
        else {
            this.ServiceProviders = this.ServiceProviders.filter(x => x._id != this.TeamCompanyBasicInfoForm.controls.TLID.value);
        }
        if (this.checkteamtype == 'Project' && this.TeamCompanyBasicInfoForm.controls.Projects.value == '') {
            return 0;
        }
        if (this.TeamCompanyBasicInfoForm.invalid) {
            return 0;
        }
        this.filteredBanksMulti = this.ServiceProviders;
        this.stepper1.next();
    }

    onSubmitTeamForm() {
    }

    getOne(id) {
        this.spinnerService.show();
        this.teamCompanyService.getOneTeamCompany(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.TeamCompanyBasicInfoForm = this.fb.group({
                    // CompanyName: [ data['result'].CompanyName, [Validators.required ] ],
                    TeamName: [data['result'].TeamName, [Validators.required]],
                    TeamType: [data['result'].Projects ? 'Project' : 'Permanent', [Validators.required]],
                    TLID: [data['result'].TLID, [Validators.required]],
                    Projects: [data['result'].Projects],
                    FullName: [''],
                    Email: [''],
                    TeamMembers: ['']
                });

                this.checkteamtype = data['result'].Projects ? 'Project' : 'Permanent';


                this.getOneServiceProvider(data['result'].TLID);

                this.getValue = true;
            }
        }, err => {
            this.spinnerService.hide();
        });
    }

    save() {
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    createNew() {
        if (this.route.snapshot.queryParams['ForCompany']) {
            this.TeamCompanyBasicInfoForm.patchValue({
                'FullName': this.ServiceProviderFullName,
                'Email': this.ServiceProviderEmail,
                'TeamMembers': this.TeamCompanyMembers
            })

            var teamdata = this.TeamCompanyBasicInfoForm.value;

            teamdata.CompanyID = this.route.snapshot.queryParams['ForCompany'];
            this.spinnerService.show();
            this.teamCompanyService.addTeamCompany(teamdata).subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    this.snackBar.open('Team created successfully', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.router.navigate(['/companies/viewDetail/' + this.route.snapshot.queryParams['ForCompany']]);
                }
                else {
                    this.snackBar.open('Team Name already exist', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            }, err => {
                this.spinnerService.hide();
            });
        }
        else {
            this.TeamCompanyBasicInfoForm.patchValue({
                'FullName': this.ServiceProviderFullName,
                'Email': this.ServiceProviderEmail,
                'TeamMembers': this.TeamCompanyMembers
            })
            this.spinnerService.show();
            this.teamCompanyService.addTeamCompany(this.TeamCompanyBasicInfoForm.value).subscribe((data: {}) => {
                this.spinnerService.hide();
                if (data['status']) {
                    if (data['TeamMemberExisting'].length > 0) {
                        this.snackBar.open('Team created successfully but some member (' + data['TeamMemberExisting'].toString() + ') not joined team because these members associated with another team.', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                    else {
                        this.snackBar.open('Team created successfully', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                    }

                    this.router.navigate(['/team-company']);
                }
                else {

                    if (data['createStatus']) {
                        this.snackBar.open(data['message'] + ". Your Team data isn't saved.", '', {
                            duration: 10000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/team-company']);
                    }
                    else {
                        this.snackBar.open(data['message'], '', {
                            duration: 10000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                }
            }, err => {
                this.spinnerService.hide();
            });
        }

    }

    editExisting() {
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.updateTeamCompany(this.ID, this.TeamCompanyBasicInfoForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if (data['status']) {
                        this.snackBar.open('Record updated successfully', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/team-company']);
                    }
                }, err => {
                    this.spinnerService.hide();
                });
            }

        });
    }


    getProjectName(id) {
        if (this.Projects) {
            var index = this.Projects.findIndex(x => x._id == id);
            if (index != -1) {
                return this.Projects[index].ProjectName;
            }

        }

    }

    register() {
        // this.router.navigate(['/users']);
        this.location.back();
    }

    cancel() {
        // this.router.navigate(['/users']);
        this.location.back();
    }


    add() {

    }


    delete() {
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {


            }
            if (ButtonPressed === "No") {

            }

        });
    }

    createProject() {
        this.router.navigate(['/projects/createnew/-1'], { queryParams: { creteProject: 'Team' } })
    }


    public data = {
        "Afghanistan": 16.63, "Albania": 11.58, "Algeria": 158.97, "Angola": 85.81, "Antigua and Barbuda": 1.1, "Argentina": 351.02, "Armenia": 8.83, "Australia": 1219.72, "Austria": 366.26, "Azerbaijan": 52.17, "Bahamas": 7.54, "Bahrain": 21.73, "Bangladesh": 105.4, "Barbados": 3.96, "Belarus": 52.89, "Belgium": 461.33, "Belize": 1.43, "Benin": 6.49, "Bhutan": 1.4, "Bolivia": 19.18, "Bosnia and Herzegovina": 16.2, "Botswana": 12.5, "Brazil": 2023.53, "Brunei": 11.96, "Bulgaria": 44.84, "Burkina Faso": 8.67, "Burundi": 1.47, "Cambodia": 11.36, "Cameroon": 21.88, "Canada": 1563.66, "Cape Verde": 1.57, "Central African Republic": 2.11, "Chad": 7.59, "Chile": 199.18, "China": 5745.13, "Colombia": 283.11, "Comoros": 0.56, "Costa Rica": 35.02, "Croatia": 59.92, "Cyprus": 22.75, "Czech Republic": 195.23, "Democratic Republic of the Congo": 12.6, "Denmark": 304.56, "Djibouti": 1.14, "Dominica": 0.38, "Dominican Republic": 50.87, "East Timor": 0.62, "Ecuador": 61.49, "Egypt": 216.83, "El Salvador": 21.8, "Equatorial Guinea": 14.55, "Eritrea": 2.25, "Estonia": 19.22, "Ethiopia": 30.94, "Fiji": 3.15, "Finland": 231.98, "France": 2555.44, "Gabon": 12.56, "Gambia": 1.04, "Georgia": 11.23, "Germany": 3305.9, "Ghana": 18.06, "Greece": 305.01, "Grenada": 0.65, "Guatemala": 40.77, "Guinea": 4.34, "Guinea-Bissau": 0.83, "Guyana": 2.2, "Haiti": 6.5, "Honduras": 15.34, "Hong Kong": 226.49, "Hungary": 132.28, "Iceland": 12.77, "India": 1430.02, "Indonesia": 695.06, "Iran": 337.9, "Iraq": 84.14, "Ireland": 204.14, "Israel": 201.25, "Italy": 2036.69, "Ivory Coast": 22.38, "Jamaica": 13.74, "Japan": 5390.9, "Jordan": 27.13, "Kazakhstan": 129.76, "Kenya": 32.42, "Kiribati": 0.15, "Kuwait": 117.32, "Kyrgyzstan": 4.44, "Laos": 6.34, "Latvia": 23.39, "Lebanon": 39.15, "Lesotho": 1.8, "Liberia": 0.98, "Libya": 77.91, "Lithuania": 35.73, "Luxembourg": 52.43, "Macedonia": 9.58, "Madagascar": 8.33, "Malawi": 5.04, "Malaysia": 218.95, "Maldives": 1.43, "Mali": 9.08, "Malta": 7.8, "Mauritania": 3.49, "Mauritius": 9.43, "Mexico": 1004.04, "Moldova": 5.36, "Mongolia": 5.81, "Montenegro": 3.88, "Morocco": 91.7, "Mozambique": 10.21, "Myanmar": 35.65, "Namibia": 11.45, "Nepal": 15.11, "Netherlands": 770.31, "New Zealand": 138, "Nicaragua": 6.38, "Niger": 5.6, "Nigeria": 206.66, "Norway": 413.51, "Oman": 53.78, "Pakistan": 174.79, "Panama": 27.2, "Papua New Guinea": 8.81, "Paraguay": 17.17, "Peru": 153.55, "Philippines": 189.06, "Poland": 438.88, "Portugal": 223.7, "Qatar": 126.52, "Republic of the Congo": 11.88, "Romania": 158.39, "Russian Federation": 3476.91, "Rwanda": 5.69, "Saint Kitts and Nevis": 0.56, "Saint Lucia": 1, "Saint Vincent and the Grenadines": 0.58, "Samoa": 0.55, "Sao Tome and Principe": 0.19, "Saudi Arabia": 434.44, "Senegal": 12.66, "Serbia": 38.92, "Seychelles": 0.92, "Sierra Leone": 1.9, "Singapore": 217.38, "Slovakia": 86.26, "Slovenia": 46.44, "Solomon Islands": 0.67, "South Africa": 354.41, "South Korea": 986.26, "Spain": 1374.78, "Sri Lanka": 48.24, "Sudan": 65.93, "Suriname": 3.3, "Swaziland": 3.17, "Sweden": 444.59, "Switzerland": 522.44, "Syria": 59.63, "Taiwan": 426.98, "Tajikistan": 5.58, "Tanzania": 22.43, "Thailand": 312.61, "Togo": 3.07, "Tonga": 0.3, "Trinidad and Tobago": 21.2, "Tunisia": 43.86, "Turkey": 729.05, "Turkmenistan": 0, "Uganda": 17.12, "Ukraine": 136.56, "United Arab Emirates": 239.65, "United Kingdom": 2258.57, "United States": 6624.18, "Uruguay": 40.71, "Uzbekistan": 37.72, "Vanuatu": 0.72, "Venezuela": 285.21, "Vietnam": 101.99, "Yemen": 30.02, "Zambia": 15.69, "Zimbabwe": 5.57, "Bolivia, Plurinational State of": 121.34, "Somalia": 0.47, "Tanzania, United Republic of": 0.78, "South Sudan": 0.98, "Congo, the Democratic Republic of the": 1.45
    };


}
