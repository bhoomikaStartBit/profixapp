import {NgModule} from "@angular/core";

import {routing} from "./team-company.routing";
import {TeamCompanyComponent} from "@app/features/Management/team-company/team-company.component";
import {TeamCompanyFormComponent} from "@app/features/Management/team-company/team-company-forms.component";
import {TeamCompanyDetailComponent} from "@app/features/Management/team-company/team-company-detail.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminWizardsModule} from "@app/shared/forms/wizards/smartadmin-wizards.module";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {D3Module} from "@app/shared/graphs/d3/d3.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { MatSelectSearchModule } from "@app/core/common/material-module/mat-select-search/mat-select-search.module";


@NgModule({
  declarations: [
      TeamCompanyComponent,
      TeamCompanyFormComponent,
      TeamCompanyDetailComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule,
      ReactiveFormsModule,
      SmartadminInputModule,
      SmartadminWizardsModule,
      NgMultiSelectDropDownModule.forRoot(),
      D3Module,
      MaterialModuleModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot(),
      MatSelectSearchModule
  ],
  providers: [],
    exports:[
        FormsModule,
        ReactiveFormsModule
    ]
})
export class TeamCompanyModule {

}
