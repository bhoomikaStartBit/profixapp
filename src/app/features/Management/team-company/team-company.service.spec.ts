import { TestBed } from '@angular/core/testing';

import { TeamCompanyService} from "@app/features/Management/team-company/team-company.service";

describe('TeamCompanyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeamCompanyService = TestBed.get(TeamCompanyService);
    expect(service).toBeTruthy();
  });
});
