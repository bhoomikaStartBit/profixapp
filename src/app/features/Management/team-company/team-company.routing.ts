
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {TeamCompanyComponent} from "@app/features/Management/team-company/team-company.component";
import {TeamCompanyFormComponent} from "@app/features/Management/team-company/team-company-forms.component";
import {TeamCompanyDetailComponent} from "@app/features/Management/team-company/team-company-detail.component";


export const routes:Routes = [

  {
    path: '',
    component: TeamCompanyComponent
  },{
    path: 'createnew/:id',
    component: TeamCompanyFormComponent
  },{
    path: 'edit/:id',
    component: TeamCompanyFormComponent
  }, {
    path: 'team-company-detail/:id',
    component: TeamCompanyDetailComponent
  },


];

export const routing = RouterModule.forChild(routes);
