import { Component, OnInit ,ViewChild ,AfterViewInit} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError, startWith } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';

import { Router} from '@angular/router';
import {TeamCompanyService} from "@app/features/Management/team-company/team-company.service";
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "@app/core/services";
import {UserService} from "@app/features/Management/users/user.service";
import {CommonService} from "@app/core/common/common.service";

import { ReplaySubject , Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {CompaniesService} from "@app/features/Management/Companies/companies.service";



@Component({
  selector: 'user-form',
  templateUrl: './team-company-detail.component.html',
  styles: [
    `.mat-form-field {
    display: inline-block;
    position: relative;
    text-align: left;
    width: 100%;
    }.mat-option {
        line-height: 24px;
        height: 60px;padding: 1px;}.rwmain {
            width: 100%;
        }.rwone {
            width: 14%;
            float: left;
            margin: 0;
            padding: 0;
        }.rwtwo {
            width: 83%;
            float: left;
            text-align: left;
            margin: 0px;
            padding: 0;
            padding-left: 9px;
        }`
    ]
})
export class TeamCompanyDetailComponent implements OnInit {
    ID : any;
    TeamDetail : any;
    TeamMember = '';
    TeamCompanyMembers : any;
    Users : any;
    getTCData = false;
    CategoryForm : FormGroup;
    filteredOptions: Observable<string[]>;
    mspControl = new FormControl();
    defalutimageurl: any;
    imageurl: any;
    ServiceProvider: any;
    ServiceProviderFullName: any;

    /** control for the selected bank for multi-selection */
    public bankMultiCtrl: FormControl = new FormControl();
    /** control for the MatSelect filter keyword multi-selection */
    public bankMultiFilterCtrl: FormControl = new FormControl();

    public filteredBanksMulti: any = [];

  constructor(private http: HttpClient ,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private teamCompanyService: TeamCompanyService,
              private notificationService: NotificationService,
              private userService: UserService,
              private commonService: CommonService,
              private spinnerService : Ng4LoadingSpinnerService,
              
            private companiesService: CompaniesService,
  ) {
    this.spinnerService.hide();
      this.getAllUserWithSpRole();
      this.ID = this.route.params['value'].id;
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      console.log(this.ID);
      this.getAllTeamMemberByTeamID(this.ID);
      this.imageurl = this.commonService.getApiUrl();
      this.defalutimageurl = this.commonService.getApiUrl()+'/Documents/Avatar.png';

      console.log(this.route.snapshot.queryParams['ForCompany'])
  }

  ngOnInit() {
    this.filteredOptions = this.mspControl.valueChanges
        .pipe(
        startWith(''),
        map(value => this._filter(value))
        );

        this.bankMultiFilterCtrl.valueChanges
              .pipe(takeUntil(this._onDestroy))
              .subscribe(() => {
                this.filterBanksMulti();
              });
       
  }
  private _onDestroy = new Subject<void>();

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

    private _filter(value: string): string[] {
        if(value.length>=3) {
            const filterValue = value.toLowerCase();
            const filteredSet = this.Users.filter(option => option.FirstName.toLowerCase().includes(filterValue));
            return filteredSet;
        }else {
            return []; 
        }
    }

    private filterBanksMulti() {
        if (!this.Users) {
          return;
        }
        // get the search keyword
        let search = this.bankMultiFilterCtrl.value;
        if (!search) {
          this.filteredBanksMulti = (this.Users.slice());
          return;
        } else {
          search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBanksMulti = 
          this.Users.filter(user =>
            user.FirstName.toLowerCase().indexOf(search) > -1
             || user.LastName.toLowerCase().indexOf(search) > -1
             || user.MiddleName.toLowerCase().indexOf(search) > -1
             || (user.Phone).toString().toLowerCase().indexOf(search) > -1
             || user.ProfixID.toLowerCase().indexOf(search) > -1
             )
        ;
      }

    getServiceProviderId(value){
        //console.log(value);
        this.TeamMember = value;
        this.mspControl.setValue('');
        this.getOneServiceProvider(value);
        
    }

    getOneServiceProvider(id){
        this.spinnerService.show();
        this.teamCompanyService.getOneServiceProvider(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.ServiceProvider = data['result'];
                console.log(this.ServiceProvider);
                this.mspControl.setValue(this.ServiceProvider.FirstName+' '+this.ServiceProvider.MiddleName+' '+this.ServiceProvider.LastName);
                this.ServiceProviderFullName = this.ServiceProvider.FirstName+' '+this.ServiceProvider.MiddleName+' '+this.ServiceProvider.LastName;
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getAllTeamMemberByTeamID(TID) {
        this.spinnerService.show();
        this.teamCompanyService.getAllTeamMemberByTeamID(TID).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.TeamCompanyMembers = data['result'];
            }
        }, err => {
            this.spinnerService.hide();
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllUserWithSpRole(){
        this.filteredBanksMulti = [];
        // this.spinnerService.show();
        if(this.route.snapshot.queryParams['ForCompany']){
            this.companiesService.getAllCompanySPUser(this.route.snapshot.queryParams['ForCompany']).subscribe((data: {}) => {
                this.spinnerService.hide();
                if ( data['status']) {
                    this.Users = data['result'];
                    this.filteredBanksMulti = data['result'];
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
              });
        }
        else{  
            this.userService.getAllUserWithSpRole().subscribe((data: {}) => {
                this.spinnerService.hide();
                if ( data['status']) {
                    this.Users = data['result'];
                    this.filteredBanksMulti = data['result'];
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
              });
        }
    }

    getOne(id) {
        this.spinnerService.show();
        this.teamCompanyService.getDetail(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.getTCData = true;
                this.TeamDetail = data['result'];
                console.log(data['result'])
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    checkMember(){
        if(this.bankMultiCtrl.value == null){
            this.snackBar.open('Please select user', 'Notice', {
                duration: 5000,
                panelClass: ['warning-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        var createMember = {
            'AddMembers' : this.bankMultiCtrl.value,
            'TLID' : this.TeamDetail.TLID._id,
            'TLName': this.TeamDetail.TLID.FirstName+' '+this.TeamDetail.TLID.LastName,
            'TLEmail' : this.TeamDetail.TLID.Email,
            'TlPhone' : this.TeamDetail.TLID.Phone,
            'TeamName' : this.TeamDetail.TeamName
        }
        this.spinnerService.show();
            this.teamCompanyService.createTeamMember(this.ID , createMember).subscribe((data: {}) => {
                this.spinnerService.hide();
                if(data['status']) {
                    //var cd = this.Users.filter((row) => row._id == this.TeamMember);
                    //this.TeamCompanyMembers.push(cd[0]);
                    this.getAllTeamMemberByTeamID(this.ID);
                    this.TeamMember = '';
                    this.mspControl.setValue('');
                    this.bankMultiCtrl.reset();
                    this.getAllUserWithSpRole();
                    this.snackBar.open('User Added', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
              });

    }

    deleteMember(id, userID){    
        if(this.TeamCompanyMembers.length == 1){
            this.notificationService.smartMessageBox({
                title: "Delete!",
                content: 'Do you want to delete the last member of team? and then the entire team will be delete.' ,
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.spinnerService.show();
                    this.teamCompanyService.deleteTeamCompany(this.ID, this.TeamDetail.TLID._id).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if(data['status']){
                            this.snackBar.open('Successfully deleted team member from team', 'success', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            if(this.route.snapshot.queryParams['ForCompany']){
                                this.router.navigate(['/companies/viewDetail/'+this.route.snapshot.queryParams['ForCompany']])
                            }
                            else{
                                this.router.navigate(['/team-company'])
                            }
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                      });
                }
            })
        } 
        else{
            this.notificationService.smartMessageBox({
                title: "Delete!",
                content: 'Are you sure to remove this member from the Team?' ,
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.spinnerService.show();
                    this.teamCompanyService.deleteTeamMember(id, userID).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if(data['status']){
                            this.snackBar.open('Successfully deleted team member from team', 'success', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            this.getAllUserWithSpRole();
                            this.getAllTeamMemberByTeamID(this.ID);
                            
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                      });
                }
            })
        }   
        
    }
}
