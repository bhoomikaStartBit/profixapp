import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class TeamCompanyService {
  url: string;
  urlTeamMember: string;
    UserTeamCompanyUrl: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/team-company/';
    this.urlTeamMember = commService.getApiUrl() + '/team-member/';
    this.UserTeamCompanyUrl = commService.getApiUrl() + '/user-team-company/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getAllTeamCompany(id): Observable<any> {
    return this.http.get(this.url + 'getAll/'+id).pipe(
      map(this.extractData));
  }
  getDetail(id): Observable<any> {
    return this.http.get(this.url + 'getDetail/'+id).pipe(
      map(this.extractData));
  }

  
  DuplicateTeamNamecheck(name): Observable<any> {
    return this.http.get(this.url + 'DuplicateTeamNamecheck/'+name).pipe(
      map(this.extractData));
  }

  getOneTeamCompany(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }

  getOneTeamCompanyByOwner(id): Observable<any> {
      return this.http.get(this.url + 'getOneTeamCompanyByOwner/' + id).pipe(
          map(this.extractData));
  }

  sendTeamMemberInvitation(UserID, TeamID): Observable<any> {
      var data = {
          TeamID : TeamID,
          ProfixID : UserID
      }

      return this.http.post<any>(this.urlTeamMember + 'sendTeamMemberInvitation', data, httpOptions).pipe(
          tap(( ) => console.log(`added`)),
          catchError(this.handleError<any>('sendTeamMemberInvitation'))
      );
  }

    createTeamMember(TeamID, member): Observable<any> {
      var data = {
          TeamID : TeamID,
          TeamData : member
      }

      return this.http.post<any>(this.urlTeamMember + 'createTeamMember', data, httpOptions).pipe(
          tap(( ) => console.log(`added msp`)),
          catchError(this.handleError<any>('createTeamMember'))
      );
    }

    getAllTeamMemberByTeam(id): Observable<any> {
        return this.http.get(this.urlTeamMember + 'getAllTeamMemberByTeam/' + id).pipe(
            map(this.extractData));
    }

    getOneServiceProvider(id): Observable<any> {
      return this.http.get(this.url + 'getOneServiceProvider/' + id).pipe(
          map(this.extractData));
    }

    getAllServiceProvider(): Observable<any> {
      return this.http.get(this.url + 'getAllServiceProvider/').pipe(
        map(this.extractData));
    }

    getAllByUserID(id): Observable<any> {
        return this.http.get(this.urlTeamMember + 'getAllByUserID/' + id).pipe(
            map(this.extractData));
    }

    getAllTeamMemberForLeave(id): Observable<any> {
        return this.http.get(this.urlTeamMember + 'getAllTeamMemberForLeave/' + id).pipe(
            map(this.extractData));
    }

    getAllTeamMemberByTeamID(id): Observable<any> {
        return this.http.get(this.urlTeamMember + 'getAllTeamMemberByTeamID/' + id).pipe(
            map(this.extractData));
    }

    rejectRequest(id): Observable<any> {
        return this.http.get(this.url + 'rejectRequest/' + id).pipe(
            map(this.extractData));
    }

    acceptRequest(id): Observable<any> {
        return this.http.get(this.url + 'acceptRequest/' + id).pipe(
            map(this.extractData));
    }

    rejectInvitationRequest(id): Observable<any> {
        return this.http.get(this.urlTeamMember + 'rejectRequest/' + id).pipe(
            map(this.extractData));
    }

    acceptInvitationRequest(id, UserID): Observable<any> {
        return this.http.get(this.urlTeamMember + 'acceptRequest/' + UserID + '/' + id).pipe(
            map(this.extractData));
    }

    checkMember(id): Observable<any> {
      return this.http.get(this.url + 'checkMember/' + id).pipe(
        map(this.extractData));
    }

  addUserTeamCompany (TeamID , UserID): Observable<any> {
    var data = {
      TeamID : TeamID,
      UserID : UserID
    }

    return this.http.post<any>(this.UserTeamCompanyUrl + 'create', data, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data.TeamID}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  addTeamCompany (data): Observable<any> {
    var dat = {
      'Basic':data,
    }
    console.log(dat);
    return this.http.post<any>(this.url + 'createBySA', dat, httpOptions).pipe(
      tap(( ) => console.log(`added role w/ id=${data._id}`)),
      catchError(this.handleError<any>('addCity'))
    );
  }

  updateTeamCompany (id, BasicData): Observable<any> {
      var dat = {
          'Basic':BasicData,
      }
    return this.http.put<any>(this.url + 'update/' + id, dat, httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${id}`)),
      catchError(this.handleError<any>('updateCity'))
    );
  }

  updateUserTeamCompany (id, UserID): Observable<any> {
      var dat = {
          'UserID':UserID,
      }
    return this.http.put<any>(this.UserTeamCompanyUrl + 'update/' + id, dat, httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${id}`)),
      catchError(this.handleError<any>('updateUserTeamCompany'))
    );
  }

  deleteTeamCompany (id, TeamLeaderID): Observable<any> {        
    return this.http.delete<any>(this.url + 'delete/' + id + '/' + TeamLeaderID, httpOptions).pipe(
      tap(( ) => console.log(`deleted role w/ id=${id}`)),
      catchError(this.handleError<any>('deleteCity'))
    );
  }

    deleteTeamMember (TeamMemberID, userID): Observable<any> {
        return this.http.get(this.urlTeamMember + 'deleteTeamMember/' + TeamMemberID + '/' + userID).pipe(
            map(this.extractData));
    }
    
    deleteTeamNTMember (TeamMemberID, userID): Observable<any> {
      return this.http.get(this.urlTeamMember + 'deleteTeamNTMember/' + TeamMemberID + '/' + userID).pipe(
          map(this.extractData));
  }

    deleteTeamMemberRequest (TeamMemberID): Observable<any> {
        return this.http.get(this.urlTeamMember + 'deleteTeamMemberRequest/' + TeamMemberID).pipe(
            map(this.extractData));
    }

    getTeamCompanyDetailByUserID (userID): Observable<any> {
        return this.http.get(this.urlTeamMember + 'getTeamCompanyDetailByUserID/' + userID).pipe(
            map(this.extractData));
    }

    getLeaveRequestDataByUserTeam (UserID, TeamID): Observable<any> {
        return this.http.get(this.urlTeamMember + 'getLeaveRequestDataByUserTeam/' + UserID + '/' + TeamID).pipe(
            map(this.extractData));
    }

    rejectLeaveRequest(id): Observable<any> {
        return this.http.get(this.urlTeamMember + 'rejectLeaveRequest/' + id).pipe(
            map(this.extractData));
    }

    acceptLeaveRequest(id, UserID): Observable<any> {
        return this.http.get(this.urlTeamMember + 'acceptLeaveRequest/' + UserID + '/' + id).pipe(
            map(this.extractData));
    }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
