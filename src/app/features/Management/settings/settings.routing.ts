
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {SettingsComponent} from "@app/features/Management/settings/settings.component";



export const routes:Routes = [

  {
    path: '',
    component: SettingsComponent
  },

];

export const routing = RouterModule.forChild(routes);
