import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";

import { Router} from '@angular/router';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "@app/core/services";
import {SettingsService} from "@app/features/Management/settings/settings.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



@Component({
  selector: 'sa-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {

  SettingsForm : FormGroup;
 
  currentUser: any;
  ID : any;
  SettingData: any =[];
  public validationSettingsForm : any = {};

  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private settingsService: SettingsService,
    private router : Router,
    private spinnerService : Ng4LoadingSpinnerService
    ) {
      this.spinnerService.hide();
      this.validationSettingsForm = {
        // Rules for form validation
        rules: {
            CompanyEmail: {
              required: true,
              email: true,
              pattern:'[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
            },
            FooterCopyrightNote: {
              required: true
            },
            AdminCommission: {
              number: true
            },
            SplitOrder: {
              number: true
            },
            GST: {
              number: true
            },
            PreVisitcharges: {
              number: true
            },
            QIPCUserCommission: {
              number: true
            },
            defaultAmountforSP: {
              number: true
            },
            MerchantMid: {
              minlength:20,
              maxlength:20
            },
            Website: {
              required: true
            },
            ChannelId: {
              required: true
            },
            IndustryTypeId: {
              required: true
            },
        },

        // Messages for form validation
        messages: {
            CompanyEmail: {
              required: 'Please enter your email address',
              email: 'Please enter a valid email addres',
              pattern: 'Please enter a valid email pattern'
            },
            FooterCopyrightNote: {
                required: 'Please enter Footer Copy right Note'
            },
            AdminCommission: {
                number: 'Please enter a valid number'
            },
            SplitOrder: {
                number: 'Please enter a valid number'
            },
            GST: {
                number: 'Please enter a valid number'
            },
            PreVisitcharges: {
                number: 'Please enter a valid number'
            },
            QIPCUserCommission: {
              number: 'Please enter a valid number'
            },
            defaultAmountforSP: {
              number: 'Please enter a valid number'
            },
            MerchantMid: {
              minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 20 digits',
              maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 20 digits.',
            },
            Website: {
              required: 'Please enter Website for staging please type DEFAULT'
            },
            ChannelId: {
              required: 'Please enter Channed Id for staging please type WEB'
            },
            IndustryTypeId: {
              required: 'Please enter Industry Type Id for staging please type Retail'
            }
        },
        submitHandler: this.onSubmitForm

      };
    
    this.ID = "5ced1407e70e0c36bcc171e1";
    this.getOne();
    this.SettingsForm = fb.group({
      CompanyEmail: [ '', [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
      FooterCopyrightNote: [ '' ],
      AdminCommission: [ '' ],
      SplitOrder: [ '' ],
      GST: [ '' ],
      Host: [ '' ],
      Username: [ '' ],
      Password: [ '' ],
      Port: [ '' ],
      HomePageTitle: [ '' ],
      HomePageMetakeywords: [ '' ],
      HomePageMetadescriptions: [ '' ],
      PreVisitcharges: [ '' ],
      nextDayScheduleTime: [ '' ],
      QIPCUserCommission: [ '' ],
      defaultAmountforSP: [ '' ],
      daysMilestoneWillShow: [ '' ],
      daysBeforeSPcanLeaveOrder: [ '' ],
      hoursBeforeOrderStarted: [ '' ],
      MerchantMid: [ '' ],
      MerchantKey: [ '' ],
      GatewayMode: [ '' ],
      Website: [ '' ],
      ChannelId: [ '' ],
      IndustryTypeId: [ '' ],
    });

  }

  ngOnInit() {
  }


  onSubmitForm() {

    if (this.SettingsForm.invalid) {
        return 0;
    }

    this.editExisting();
  }


  editExisting(){
      this.notificationService.smartMessageBox({
          title: "Update!",
          content: "Are you sure update this record?",
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") {
            this.spinnerService.show();
              this.ID = "5ced1407e70e0c36bcc171e1";
              this.settingsService.updateSetings(this.ID, this.SettingsForm.value).subscribe((data: {}) => {
                this.spinnerService.hide();
                  if ( data['status']) {
                      this.snackBar.open('Record updated successfully', 'success', {
                          duration: 5000,
                          panelClass: ['success-snackbar'],
                          verticalPosition: 'top'
                      });
                      this.router.navigate(['/settings']);
                      console.log("update success");
                  }
              }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
              });
          }
          if (ButtonPressed === "No") {
          }
      });
  }

  getOne() {
    this.spinnerService.show();
    this.settingsService.getSettings().subscribe((data: {}) => {
      this.spinnerService.hide();
      this.SettingData = data['result']
        if ( data['status']) {
            this.SettingsForm = this.fb.group({
              CompanyEmail: [ this.getName('CompanyEmail'), [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
              FooterCopyrightNote: [ this.getName('FooterCopyrightNote') ],
              AdminCommission: [ this.getName('AdminCommission') ],
              SplitOrder: [ this.getName('SplitOrder')],
              GST: [  this.getName('GST') ],
              Host: [ this.getName('Host') ],
              Username: [ this.getName('Username') ],
              Password: [ this.getName('Password') ],
              Port: [ this.getName('Port') ],
              HomePageTitle: [ this.getName('HomePageTitle') ],
              HomePageMetakeywords: [ this.getName('HomePageMetakeywords') ],
              HomePageMetadescriptions: [ this.getName('HomePageMetadescriptions') ],
              PreVisitcharges:  [ this.getName('PreVisitcharges') ],
              nextDayScheduleTime:  [ this.getName('nextDayScheduleTime') ],
              QIPCUserCommission: [ this.getName('QIPCUserCommission') ],
              defaultAmountforSP: [ this.getName('defaultAmountforSP') ],
              daysMilestoneWillShow: [ this.getName('daysMilestoneWillShow')  ],
              daysBeforeSPcanLeaveOrder: [ this.getName('daysBeforeSPcanLeaveOrder') ],
              hoursBeforeOrderStarted: [ this.getName('hoursBeforeOrderStarted') ],
              MerchantMid: [ this.getName('MerchantMid') ],
              MerchantKey: [ this.getName('MerchantKey') ],
              GatewayMode: [ this.getName('GatewayMode') ],
              Website: [ this.getName('Website') ],
              ChannelId: [ this.getName('ChannelId') ],
              IndustryTypeId: [ this.getName('IndustryTypeId') ],
            });

            console.log(this.SettingsForm);
        }
    }, err => {
      this.spinnerService.hide();
      this.snackBar.open('Server Error', '', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
    });
  }

  getName(name){
	  if(this.SettingData){
	    return this.SettingData[(this.SettingData.findIndex(x=>x.metaKey == name))].metaValue;
	  }
	  else{
	  	return null;
	  }
  }

}
