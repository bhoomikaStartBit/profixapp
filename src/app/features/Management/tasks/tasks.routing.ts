import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {TasksComponent} from "@app/features/Management/tasks/tasks.component";
import {TasksDetailComponent} from "@app/features/Management/tasks/tasksDetail.component";


export const routes:Routes = [

  {
    path: '',
    component: TasksComponent
  },{
    path: 'detail/:id',
    component: TasksDetailComponent
  },


];

export const routing = RouterModule.forChild(routes);
