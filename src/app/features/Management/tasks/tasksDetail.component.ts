import { Component, OnInit  , ViewChild ,AfterViewInit} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {CategoriesService} from "@app/features/Management/categories/categories.service";
import {MatPaginator, MatSort, MatStepper, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Router, Params, ActivatedRoute} from "@angular/router";
import {ProjectsService} from "@app/features/Management/projects/projects.service";
import {UserService} from "@app/features/Management/users/user.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'userlist',
    templateUrl: 'tasksDetail.component.html',
    styleUrls: ['./tasksDetail.component.scss']
})
export class TasksDetailComponent implements OnInit,AfterViewInit {
    Categories : any;
    url : any;
    ID : any;
    selectedStatus : any;
    SelectedSP : any = {};
    SelectedUsers : any = [];
    OrderMilestoneData : any = [];
    Users : any;
    AssignUser : any = '';
    AssignTo : any = '';
    CurrentOrderStatus : any = '';
    GetProjectData = false;
    GetSP = false;
    SelectedProject : any;
    CurrentLoginUser : any = {};
    workStatusArray = ['', 'New', 'Assigned', 'In Process', 'Review', 'Completed'];
    @ViewChild('stepper') stepper: MatStepper;
    selectedOrderStatus: Number;
    allActivities: any = [];
    SubmissionForm  : FormGroup;
    PaymentForm  : FormGroup;
    MilestoneForm  : FormGroup;
    constructor(
        private http: HttpClient ,
        private notificationService: NotificationService,
        private snackBar: MatSnackBar,
        private commonService: CommonService,
        private route: ActivatedRoute,
        private router: Router,
        private projectsService: ProjectsService,
        private userService : UserService,
        private fb: FormBuilder,
        private spinnerService: Ng4LoadingSpinnerService

    ) {
        this.spinnerService.hide();
        this.getAllServiceProviderUsers();
        this.CurrentLoginUser = JSON.parse(localStorage.getItem('currentUser'));
        this.ID = this.route.params['value'].id;
        this.getOne(this.ID);
        this.getAllOrderActivitiesByOrderID(this.ID);
        this.url = this.commonService.getApiUrl();
        this.SubmissionForm = fb.group({
            Status: [ '', [Validators.required ] ],
            Notes: [ '', [Validators.required ] ],
        });
        this.PaymentForm = fb.group({
            PaymentMode: [ '', [Validators.required ] ],
            Notes: [ '', [Validators.required ] ],
        });
        this.MilestoneForm = fb.group({
            'Service' : ['' , [Validators.required]],
            'Title':['' , [Validators.required]],
            'Description':[''],
            'Width':['' , [Validators.required]],
            'Height':['' , [Validators.required]],
            'Amount':['' , [Validators.required]],
            'UserID':['' , [Validators.required]],
        })
    }

    ngOnInit() {

    }
    ngAfterViewInit(){
        //this.stepper.selectedIndex = 1;
    }

    getOne(id){
        this.spinnerService.show();
        this.projectsService.getOneProjects(id).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.SelectedProject = data['result'];
                this.GetProjectData = true;
                this.selectedStatus = data['result'].Status;
                this.CurrentOrderStatus = data['result'].Status;
                this.AssignTo = data['OrderAssign'];
                let index = 0;
                this.workStatusArray.map(val => {
                    if(val == this.CurrentOrderStatus) {
                        this.selectedOrderStatus = index;
                    }
                    index++;
                });
                setTimeout(()=>{
                    this.AssignUser = '';
                    var user = this.SelectedUsers;
                    this.Users = user.filter(row=> row._id !== data['UserID'])
                } , 500)
            }
        })
    }

    getAllServiceProviderUsers(){
        this.spinnerService.show();
        this.userService.getAllUserWithSpRole().subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.SelectedUsers = data['result'];
                this.Users = data['result'];
            }
        });
    }


    getSPUser(id){
        console.log(id);
        var sp = this.Users.filter(row=>row._id == id);
        this.SelectedSP = sp[0];
        console.log(this.SelectedSP );
        this.GetSP = true;
    }

    assignOrder(){
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AssignOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': this.AssignUser,
            'Status': 'Assigned'
        }
        this.spinnerService.show();
        this.projectsService.assignOrder(AssignOrder).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Order assigned successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
        })

    }

    acceptOrder(){
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'Status': 'Assigned'
        }
        this.spinnerService.show();
        this.projectsService.acceptOrder(AcceptOrder).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Order accept successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{
                this.snackBar.open('Order already assigned to another person.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(['/projects']);
            }
        })

    }

    submitOrderBySP(){
        console.log("click");
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'Submit': this.SubmissionForm.value
        }
        this.spinnerService.show();
        this.projectsService.submitOrderBySP(AcceptOrder).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Order submit successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{

            }
        })


    }
    paymentCollect(){
        console.log("click");
        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        var AcceptOrder = {
            "OrderID": this.ID,
            "UserID": adminuser.id,
            'AssignUser': adminuser.id,
            'PaymentAmount': this.SelectedProject.PaymentAmount,
            'Payment': this.PaymentForm.value
        }
        this.spinnerService.show();
        this.projectsService.paymentCollect(AcceptOrder).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Order Payment has been successfully paid.', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne(this.ID);
                this.GetSP = false;
            }
            else{

            }
        })

    }

    changeOrderStatus(){
        // this.projectsService.changeOrderStatus(this.CurrentOrderStatus).subscribe( (data : {})=>{
        //     if ( data['status']) {
        //         this.snackBar.open('Order assigned successfully', 'success', {
        //             duration: 5000,
        //             panelClass: ['success-snackbar'],
        //             verticalPosition: 'top'
        //         });
        //         this.getOne(this.ID);
        //     }
        // })
    }

    getAllOrderActivitiesByOrderID(orderID) {
        this.spinnerService.show();
        this.projectsService.getAllOrderActivitiesByOrderID(orderID).subscribe((data) => {
            this.spinnerService.hide();
            this.allActivities = data['result'];
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getPayInfo(){

        var adminuser = JSON.parse(localStorage.getItem('currentUser'));
        this.spinnerService.show();
        this.projectsService.orderPayInfo(this.ID , adminuser.id).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if(data['status']){
                if(data['result'].length){
                    return false;
                }
                else{
                    return true;
                }
            }
        })
    }

    createMilestoneAndAssignOrder(){
        console.log(this.MilestoneForm.value);
        var milestone = {
            'Milestone' : this.MilestoneForm.value,
            'OrderID' : this.ID
        }
        this.spinnerService.show();
        this.projectsService.createMilestoneAndAssignOrder(milestone).subscribe( (data : {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open('Milestone created and assign successfully.', 'Error', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.OrderMilestoneData.push(data['result']);

                console.log(this.OrderMilestoneData);
                this.MilestoneForm.reset();
            }
        })
    }

    deleteOrderMilestone(id){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.projectsService.deleteOrderMilestone(id).subscribe( (data : {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        var index = this.OrderMilestoneData.findIndex( x=> x._id == id);
                        this.OrderMilestoneData.splice(index);

                        this.snackBar.open('Milestone delete successfully.', 'Error', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                })
            }
            else{

            }
        });

    }



    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
