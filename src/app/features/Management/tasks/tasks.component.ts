import {Component, OnInit, ElementRef, ViewChild, AfterViewChecked  } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {ProjectsService} from "@app/features/Management/projects/projects.service";
import * as $ from 'jquery';
import {Router} from "@angular/router";
import {CommonService} from "@app/core/common/common.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'taskslist',
    templateUrl: './tasks.component.html',
})
export class TasksComponent implements OnInit , AfterViewChecked {
    Projects : any = [];
    getData =false;
    called = false;
    CurrentLoginUser = JSON.parse(localStorage.getItem('currentUser'));

    ProjectUrl : any = '';
    @ViewChild('button1') button1: ElementRef;

    constructor(
        private http: HttpClient ,
        private notificationService: NotificationService,
        private projectsService: ProjectsService,
        private router : Router,
        private commonService : CommonService,
        private spinnerService : Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
        this.CurrentLoginUser = JSON.parse(localStorage.getItem('currentUser'));
        if(this.CurrentLoginUser.RoleName == 'Super Admin'){
            this.getAllProjects();
            this.ProjectUrl = this.commonService.getApiUrl()+'/project/getAllOrderServiceProvicer/0';
        }
        else{
            this.getAllProjects();
            this.ProjectUrl = this.commonService.getApiUrl()+'/project/getAllOrderServiceProvicer/'+this.CurrentLoginUser.id;
        }

    }

    ngOnInit() {

    }
    ngAfterViewChecked(){
        $("button.dataTableProject").on('click',function(){
            open($(this).attr('data-id'));
        })
        $("button.kc").on('mouseup',function(){
            data();
        })

        var open = (id)=> {
            if(!this.called) {
                this.open(id);
            }
        }
        var data = ()=>{
            this.called = false;
        }
    }


    getAllProjects(){
        this.spinnerService.show();
        this.projectsService.getAllProjects().subscribe( (data : {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.getData = true;
                this.Projects = data['data'];
            }
        })
    }
    getAllOrderServiceProvicer(id){
        /*this.projectsService.getAllOrderServiceProvicer(id).subscribe( (data : {})=>{
            if(data['status']){
                this.getData = true;
                this.Projects = data['data'];
            }
        })*/
    }

    open(id){
        console.log(id);
        this.called = true;
        this.router.navigate(['/projects/detail/'+id ])
    }

    public options = {
        "ajax": this.commonService.getApiUrl()+'/project/getAllOrderServiceProvicer/' + (this.CurrentLoginUser.RoleName === 'Super Admin' ? '0' : this.CurrentLoginUser.id),
        "iDisplayLength": 10,
        "columns": [
            {
                "class": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "name"},
            /*{"data": "est"},
            {"data": "AssignTo"},*/
            {"data": "customer"},
            {"data": "status"},
            // {"data": "starts"},
            // {"data": "tracker"}
        ],
        "order": [[1, 'asc']]
    }

    public detailsFormat(d) {

        return `<table cell-padding="5" cell-spacing="0" border="0" class="table table-hover table-condensed">
            <tbody><tr>
                <td style="width:100px">Project Title:</td>
                <td>${d.name}</td>
            </tr>
            <tr>
                <td>Customer:</td>
                <td>${d.customer}</td>
            </tr>
            <tr>
                <td>Comments:</td>
                <td>${d.comments}</td>
            </tr>
            <tr>
                <td>Action:</td>
                <td><button class='btn btn-xs dataTableProject' data-id="${d._id}">Open Detail</button> </td>
            </tr></tbody>
        </table>`
    }


    public OrderOptions = {
        "ajax": this.commonService.getApiUrl()+'/project/getAllOrderPriceUnder5000/',
        "iDisplayLength": 10,
        "columns": [
            {
                "class": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "name"},
            /*{"data": "est"},
            {"data": "AssignTo"},*/
            {"data": "customer"},
            {"data": "status"},
            {"data": "starts"},
            // {"data": "tracker"}
        ],
        "order": [[1, 'asc']]
    }

    public OrderdetailsFormat(d) {

        return `<table cell-padding="5" cell-spacing="0" border="0" class="table table-hover table-condensed">
            <tbody><tr>
                <td style="width:100px">Project Title:</td>
                <td>${d.name}</td>
            </tr>
            <tr>
                <td>Customer:</td>
                <td>${d.customer}</td>
            </tr>
            <tr>
                <td>Comments:</td>
                <td>${d.comments}</td>
            </tr>
            <tr>
                <td>Action:</td>
                <td><button class='btn btn-xs dataTableProject' data-id="${d._id}">Open Detail</button> </td>
            </tr></tbody>
        </table>`
    }

    delete(){
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {


            }
            if (ButtonPressed === "No") {

            }

        });
    }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
/*
this.notificationService.smartMessageBox({
      title: "Smart Alert!",
      content: "This is a confirmation box. Can be programmed for button callback",
      buttons: '[No][Yes]'
    }, (ButtonPressed) => {
      if (ButtonPressed === "Yes") {

        this.notificationService.smallBox({
          title: "Callback function",
          content: "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",
          color: "#659265",
          iconSmall: "fa fa-check fa-2x fadeInRight animated",
          timeout: 4000
        });
      }
      if (ButtonPressed === "No") {
        this.notificationService.smallBox({
          title: "Callback function",
          content: "<i class='fa fa-clock-o'></i> <i>You pressed No...</i>",
          color: "#C46A69",
          iconSmall: "fa fa-times fa-2x fadeInRight animated",
          timeout: 4000
        });
      }

    });
* */