import { TestBed } from '@angular/core/testing';

import {ProjectsService} from "@app/features/Management/projects/projects.service";

describe('TasksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectsService = TestBed.get(ProjectsService);
    expect(service).toBeTruthy();
  });
});
