import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
  url: string;
  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/profile/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getProfile(id): Observable<any> {
    return this.http.get(this.url + 'getOne/' + id).pipe(
      map(this.extractData));
  }
  
  updateProfile(id, data): Observable<any> {
    return this.http.put<any>(this.url + 'update/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log('update')),
      catchError(this.handleError<any>('updateProfile'))
    );
  }

    updateProfilePassword(id, data): Observable<any> {
        return this.http.put<any>(this.url + 'updateProfilePassword/' + id, JSON.stringify(data), httpOptions).pipe(
            tap(( ) => console.log('update')),
            catchError(this.handleError<any>('updateProfilePassword'))
        );
    }

    updateVenderAddress(id, data): Observable<any> {
      return this.http.put<any>(this.url + 'updateVenderAddress/' + id, JSON.stringify(data), httpOptions).pipe(
        tap(( ) => { console.log('update');
        console.log(data);  }),
        catchError(this.handleError<any>('updateProfile'))
      );
    }

  sendRequestForTeam(id, type): Observable<any> {
      return this.http.get(this.url + 'sendRequestForTeam/' + type + '/' + id).pipe(
          map(this.extractData));
  }

  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
