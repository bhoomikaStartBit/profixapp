import {NgModule} from "@angular/core";

import {routing} from "./profiles.routing";
import {ProfilesComponent} from "@app/features/Management/profiles/profiles.component";
import {ProfilesFormComponent} from "@app/features/Management/profiles/profiles-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";


@NgModule({
  declarations: [
      ProfilesComponent,
      ProfilesFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      SmartadminInputModule,
      MaterialModuleModule,
      SmartadminValidationModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class ProfilesModule {

}
