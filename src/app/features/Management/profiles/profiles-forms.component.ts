import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";

import { Router} from '@angular/router';
import {ProfilesService} from "@app/features/Management/profiles/profiles.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";

import {StateService} from "@app/features/Management/state/state.service";
import {CityService} from "@app/features/Management/city/city.service";
import {LocalityService} from "@app/features/Management/locality/locality.service";
import {RoleService} from "@app/features/Management/roles/role.service";
import {ServicesService} from "@app/features/Management/service/services.service";
import {UserService} from "@app/features/Management/users/user.service";
import { CommonService} from '@app/core/common/common.service';
import { MyTeamsService} from "@app/features/Management/myteam/myteam.service";
import {TeamCompanyService} from "@app/features/Management/team-company/team-company.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { NotificationService} from "@app/core/services";
import {saveAs as importedSaveAs} from 'file-saver';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};


@Component({
  selector: 'user-form',
  templateUrl: './profiles-forms.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
})
export class ProfilesFormComponent implements OnInit {
    displayedColumns: string[] = [
        'SN',
        'CompanyTeamName',
        'RequestDate',
        'ActionDate',
        'Status'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;



    ID : any;
    Role : any = '';
    SelectedUser : any ;
    GetValue = false ;
    currentUser : any;
    BasicInfoForm : FormGroup;
    AccountInfoForm : FormGroup;
    ShopInfoForm : FormGroup;
    PasswordChangeForm : FormGroup;
    FinacInfo : FormGroup;

    SecondaryDetailForm: FormGroup;
    FamilyInfoForm: FormGroup;
    

    States: any = [];
    Cities: any = [];
    Localities: any = [];
    getValue = false;
    ShopCities: any = [];
    ShopLocalities: any = [];
    UserServices  = [];
    Services: any;
    pinlength = 6;
    RequestType = 'Team';
    UserEmail = '';
    UserProfileFullDetail: any;
    isTeamRequestData: any = {};
    isInvitationRequestData: any = [];
    public validationUserFormDetailOptions : any = {};
    public validationPasswordFormDetailOptions : any = {};
    public validationVendorAddressOptions: any = {};
    selectedTeamCompany: any = {};
    isLeaveRequestData: any = {};

    allDocuments  = [];
    documentArray: Array<any> = [];
    documentUploadedArray: any = [];
    allImages: any = [];
    docMandatory = 0;

    TemporaryCities: any = [];
    TemporaryLocalities: any = [];

    educationDocumentArray: any = [];
    eduDocTitle = '';
    eduDocImage: any;
    educationDocType = '';

    
    VenderAddressCities: any = [];
    VenderAddressLocalities: any = [];
    VenderAddress: any = [];
    VendorAddressForm: FormGroup;
    userdata : any = {};

    SelectedVenderAddress = -1;

    @ViewChild('avatarImage') myInputVariable: ElementRef;
  constructor(private http: HttpClient ,
              private router : Router,
              private fb: FormBuilder,
              private location : Location,
              private route: ActivatedRoute,
              private profilesService: ProfilesService,
              private snackBar: MatSnackBar,
              private stateService: StateService,
              private cityService: CityService,
              private localityService: LocalityService,
              private roleService: RoleService,
              private servicesService: ServicesService,
              private userService: UserService,
              private commonService: CommonService,
              private myteamService: MyTeamsService,
              private teamCompanyService: TeamCompanyService,
              private notificationService: NotificationService,
              private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.userdata = JSON.parse(localStorage.getItem('currentUser'));
        this.spinnerService.hide();
        this.VendorAddressForm = fb.group({
            Office: [ 'Head', [Validators.required ] ],
            Name: [ '', [Validators.required ] ],
            Email: [ '', [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
            Number: [ '', [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
            Address: [ '', [Validators.required ] ],
            Pin: [ '', [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
           
            City: [ '', [Validators.required ] ],
            State: [ '', [Validators.required ] ]
        });

      this.validationVendorAddressOptions = {
          // Rules for form validation
          rules: {
              VAName: {
                  required: true
              },
              VAEmail: {
                  required: true,
                  email: true,
                  pattern:'[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
              },
              VANumber: {
                  required: true,
                  pattern: "[0-9]+",
                  minlength:10,
                  maxlength:10
              },
              VAState: {
                  required: true
              },
              VACity: {
                  required: true
              },
              VAPin: {
                  required: true,
                  minlength:6,
                  maxlength:6
              },
              VAAddress: {
                  required: true
              }
          },

          // Messages for form validation
          messages: {
              VAName: {
                  required: 'Please enter name'
              },
              VAEmail: {
                  required: 'Please enter your email address',
                  email: 'Please enter a valid email addres',
                  pattern: 'Please enter a valid email pattern'
              },
              VANumber: {
                  required: 'Please enter your phone number',
                  minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                  maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
              },
              VAState: {
                  required: 'Please select the state'
              },
              VACity: {
                  required: 'Please select the city'
              },
              VAPin: {
                  required: 'Please enter pincode',
                  minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
                  maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
              },
              VAAddress: {
                  required: 'Please enter address'
              }
          },
          submitHandler: this.onSubmit

      };


      this.validationUserFormDetailOptions = {
        // Rules for form validation
        rules: {
            FirstName: {
                required: true
            },
            Email: {
                required: true,
                email: true,
                pattern:'[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$'
            },
            Phone: {
                required: true,
                pattern: "[0-9]+",
                minlength:10,
                maxlength:10
            },
            AltPhone: {
                pattern: "[0-9]+",
                minlength:10,
                
            },
            State: {
                required: true
            },
            City: {
                required: true
            },
            Pin: {
                required: true,
                minlength:6,
                maxlength:6
            },
            Address: {
                required: true
            }
        },

        // Messages for form validation
        messages: {
            FirstName: {
                required: 'Please enter your first name'
            },
            Email: {
                required: 'Please enter your email address',
                email: 'Please enter a valid email addres',
                pattern: 'Please enter a valid email pattern'
            },
            Phone: {
                required: 'Please enter your phone number',
                minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
            },
            AltPhone: {
                minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                
            },
            State: {
                required: 'Please select the state'
            },
            City: {
                required: 'Please select the city'
            },
            Pin: {
                required: 'Please enter pincode',
                minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 6 digits',
                maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 6 digits.',
            },
            Address: {
                required: 'Please enter address'
            }
        },
        submitHandler: this.onSubmit

    };

      this.validationPasswordFormDetailOptions = {
          // Rules for form validation
          rules: {
              OldPassword:{
                required: true,
              },
              MyNewPassword: {
                  required: true,
                  minlength: 8,
                  pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$'
              },
              MyConfirmPassword: {
                  required: true,
                  minlength: 8,
                  pattern: '^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$',
                  equalTo: '#MyNewPassword'
              }
          },

          // Messages for form validation
          messages: {
              OldPassword:{
                required: 'Please enter old password',
              },
              MyNewPassword: {
                  required: 'Please enter new password',
                  minlength: 'Password should be at least 8 characters long',
                  pattern: 'Password should contain one number,one character, one uppercase character and one special character'
              },
              MyConfirmPassword: {
                  required: 'Please enter confirm password',
                  minlength: 'Password should be at least 8 characters long',
                  pattern: 'Password should contain one number,one character, one uppercase character and one special character'
                  // equalTo: 'Please enter same as new password'
              }
          },
          submitHandler: this.onSubmitForPassword

      };

      this.getAllState();
      this.PasswordChangeForm = fb.group({
          OldPassword: ['', [Validators.required]],
          NewPassword: ['', [Validators.minLength(8),
              Validators.required,
              Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]],
          ConfirmPassword: ['', [Validators.minLength(8),
              Validators.required,
              Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ]]
      });
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.ID = this.currentUser.id;
      this.getOne();
      //this.getTeamRequestData();
      //this.getInvitationRequestData();
  }

  ngOnInit() {

  }

  getTeamRequestData() {
    this.spinnerService.show();
      this.myteamService.getRequestDetailByUserID(this.currentUser.id).subscribe((data: {}) => {
        this.spinnerService.hide();
          if(data['status']){
              this.isTeamRequestData = data['result'];
          }
      }, err => {
          this.spinnerService.hide();
          this.snackBar.open('Server Error', '', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      })
  }

    getInvitationRequestData() {
        this.spinnerService.show();
        this.teamCompanyService.getAllByUserID(this.currentUser.id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.isInvitationRequestData = data['result'];
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onSubmit(){
    }

    onSubmitForPassword(){
    }

    onSubmitForm() {
        if (this.BasicInfoForm.invalid) {
            return 0;
        }
        this.spinnerService.show();
        this.profilesService.updateProfile(this.ID, this.BasicInfoForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Profile updated successfully', '', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.commonService.getAvatarUrl.emit(data['Avatar']);
                
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
        
    }

    onPasswordSubmitForm(){
        if (this.PasswordChangeForm.invalid) {
            return 0;
        }
        const oldPass = this.PasswordChangeForm.get('OldPassword').value;
        const newPass = this.PasswordChangeForm.get('NewPassword').value;
        const confirmPass = this.PasswordChangeForm.get('ConfirmPassword').value;

        if(newPass !== confirmPass) {
            return 0;
        }
        this.spinnerService.show();
        this.profilesService.updateProfilePassword(this.ID, this.PasswordChangeForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.PasswordChangeForm.reset();
                this.snackBar.open('Password updated successfully', '', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.commonService.getAvatarUrl.emit(data['Avatar']);
            } else {
                this.snackBar.open(data['message'], 'Error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getAllCitiesByTemporaryState(id){
        this.TemporaryCities = [];
        if(this.getValue) {
            this.BasicInfoForm.patchValue({
                "TemporaryCity": '',
            });
        }
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.TemporaryCities = data['result'];
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

   

    getOne() {
        this.spinnerService.show();
        this.profilesService.getProfile(this.ID).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                if(data['result'].State){
                    this.getAllCitiesByState(data['result'].State);
                }
                if(data['result'].Role.Name == 'Service Provider'){
                    this.getAllCitiesByTemporaryState(data['result'].TemporaryState);
                }
                
                
                this.UserProfileFullDetail = data['result'];
                if(this.UserProfileFullDetail.type === 'MSP') {
                    this.getTeamCompanyDetailByUserID();
                }
                this.UserEmail = data['result'].Email;
                if(data['result'].Role.Name == 'Service Provider'){
                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [  data['result'].Email , [Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                        AltPhone: [ data['result'].AltPhone, [ Validators.minLength(10)] ],
                        Gender: [ data['result'].Gender, [Validators.required ] ],
                        IsAvailable: [ data['result'].IsAvailable ],
                        Avatar: [ '' ],SameAs: [ false ],
                        PermanentAddressType : [ data['result'].PermanentAddressType ? data['result'].PermanentAddressType :'Own'],
                        Address: [ data['result'].Address, [Validators.required ] ],
                        Pin: [ data['result'].Pin, [Validators.required , Validators.minLength(6) , Validators.maxLength(6) ] ],
                        
                        City: [ data['result'].City, [Validators.required ] ],
                        State: [ data['result'].State, [Validators.required ] ],
                        DOB: [ data['result'].DOB ? data['result'].DOB : '' ],
                        Married: [ data['result'].Married ? data['result'].Married : '' ],
                        AltName: [ data['result'].AltName ? data['result'].AltName : '' ],
                        TemporaryAddressType : [data['result'].TemporaryAddressType ? data['result'].TemporaryAddressType :'Own'],
                        TemporaryAddress: [ data['result'].TemporaryAddress ? data['result'].TemporaryAddress :'' ],
                        TemporaryPin: [ data['result'].TemporaryPin ? data['result'].TemporaryPin :'', [Validators.minLength(6), Validators.maxLength(6) ] ],
                    
                        TemporaryCity: [ data['result'].TemporaryCity ? data['result'].TemporaryCity :null ],
                        TemporaryState: [ data['result'].TemporaryState ? data['result'].TemporaryState :null ],
                    });
                }
                else{
                    this.BasicInfoForm = this.fb.group({
                        FirstName: [ data['result'].FirstName, [Validators.required ] ],
                        MiddleName: [ data['result'].MiddleName ],
                        LastName: [ data['result'].LastName ],
                        Email: [  data['result'].Email , [Validators.required, Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
                        Phone: [ data['result'].Phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
                        AltPhone: [ data['result'].AltPhone, [ Validators.minLength(10)] ],
                        Gender: [ data['result'].Gender, [Validators.required ] ],
                        IsAvailable: [ data['result'].IsAvailable ],
                        Avatar: [ '' ],SameAs: [ false ],
                        PermanentAddressType : [ data['result'].PermanentAddressType ? data['result'].PermanentAddressType :'Own'],
                        Address: [ data['result'].Address, [Validators.required ] ],
                        Pin: [ data['result'].Pin, [Validators.required , Validators.minLength(6) , Validators.maxLength(6) ] ],
                        
                        City: [ data['result'].City, [Validators.required ] ],
                        State: [ data['result'].State, [Validators.required ] ],
                        DOB: [ data['result'].DOB ? data['result'].DOB : '' ],
                        Married: [ data['result'].Married ? data['result'].Married : '' ],
                        AltName: [ data['result'].AltName ? data['result'].AltName : '' ],
                        TemporaryAddressType : [data['result'].TemporaryAddressType ? data['result'].TemporaryAddressType :'Own'],
                        TemporaryAddress: [ data['result'].TemporaryAddress ? data['result'].TemporaryAddress :'' ],
                        TemporaryPin: [ data['result'].TemporaryPin ? data['result'].TemporaryPin :'', [Validators.minLength(6), Validators.maxLength(6) ] ],
                    
                        TemporaryCity: [ data['result'].TemporaryCity ? data['result'].TemporaryCity :null ],
                        TemporaryState: [ data['result'].TemporaryState ? data['result'].TemporaryState :null ],
                    });
                }
                this.Role = data['result'].Role.Name;
                this.SelectedUser = data['result'].Shop;
                if (data['result'].Avatar) {
                    this.userService.getUserDocument(data['result'].Avatar).subscribe((data: {}) => {
                        this.createImageFromBlobForEdu(data).then(val => {
                            this.BasicInfoForm.patchValue({
                                Avatar: val
                            });
                        });
                    });
                }
                this.AccountInfoForm = this.fb.group({
                    UserName: [{ value :data['result'].UserName , disabled : true}, [Validators.minLength(4),Validators.maxLength(8),Validators.required ] ],
                    Password: [ '', [Validators.minLength(8),
                        Validators.required,
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                    ConfirmPassword: [ '', [Validators.minLength(8),
                        Validators.required,
                        Validators.pattern('^(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,20}$') ] ],
                    Role: [ data['result'].Role._id, [Validators.required] ],
                    
                });
                if(this.Role == 'Service Provider'){
                    if(data['result'].SecondaryDetails){
                        this.SecondaryDetailForm = this.fb.group({
                            EducationQualification : [data['result'].SecondaryDetails.EducationQualification],
                            Language : [data['result'].SecondaryDetails.Language],
                            Injuries : [data['result'].SecondaryDetails.Injuries],
                            BloodGroup : [data['result'].SecondaryDetails.BloodGroup],
                            Vehicle : [data['result'].SecondaryDetails.Vehicle],
                        })

                    }
                    else{
                        this.SecondaryDetailForm = this.fb.group({
                            EducationQualification : [''],
                            Language : [''],
                            Injuries : [''],
                            BloodGroup : [''],
                            Vehicle : [''],
                        })
                    }
                    if(data['result'].FamilyDetails){
                        this.FamilyInfoForm = this.fb.group({
                            FatherName : [data['result'].FamilyDetails.FatherName],
                            MotherName : [data['result'].FamilyDetails.MotherName],
                            SpouseName : [data['result'].FamilyDetails.SpouseName],
                        })
                    }
                    else{
                        this.FamilyInfoForm = this.fb.group({
                            FatherName : [''],
                            MotherName : [''],
                            SpouseName : [''],
                        })
                    }
                }

                if(this.Role == 'Vendor'){

                    this.VenderAddress = data['result'].VenderAddress;

                   // this.getAllCitiesByShopState(data['result'].Shop.State);
                    
                    this.ShopInfoForm = this.fb.group({
                        ShopName: [ data['result'].Shop.ShopName, [Validators.required ] ],
                        ShopRegistrationNo: [ data['result'].Shop.ShopRegistrationNo, [Validators.required ] ],
                        CompanyEmail: [ data['result'].Shop.CompanyEmail ? data['result'].Shop.CompanyEmail : '', [Validators.required , Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
                        Image: [ data['result'].Shop.Image, [Validators.required ] ],
                        Address: [ data['result'].Shop.Address, [Validators.required ] ],
                        
                        City: [ data['result'].Shop.City, [Validators.required ] ],
                        State: [ data['result'].Shop.State, [Validators.required ] ],
                        Pin: [ data['result'].Shop.Pin, [Validators.required ] ],
                        ContactNo: [ data['result'].Shop.ContactNo, [Validators.required ] ],
                    });
                }

                if(data['result'].FinancialDetail){
                    this.FinacInfo = this.fb.group({
                        TypeOfAccount: [ data['result'].FinancialDetail.TypeOfAccount, [Validators.required ] ],
                        Name: [ data['result'].FinancialDetail.Name, [Validators.required ] ],
                        BankName: [ data['result'].FinancialDetail.BankName, [Validators.required ] ],
                        AccountNumber: [ data['result'].FinancialDetail.AccountNumber, [Validators.required ] ],
                        IFSCCode: [ data['result'].FinancialDetail.IFSCCode, [Validators.required ] ],
                        OnlineAccountType: [ data['result'].FinancialDetail.OnlineAccountType ?  data['result'].FinancialDetail.OnlineAccountType : '' ],
                        OnlineAccountID: [ data['result'].FinancialDetail.OnlineAccountID ? data['result'].FinancialDetail.OnlineAccountID : '' ],
                        CreditLimits: [ data['result'].FinancialDetail.CreditLimits, [Validators.required ] ],
                        CreditDays: [ data['result'].FinancialDetail.CreditDays, [Validators.required ] ],
                    });

                }
                else{
                    if(data['result'].FinancialDetail){
                        this.FinacInfo = this.fb.group({
                            TypeOfAccount : [data['result'].FinancialDetail.TypeOfAccount, [Validators.required ]],
                            Name : [data['result'].FinancialDetail.Name, [Validators.required ]],
                            AccountNumber : [data['result'].FinancialDetail.AccountNumber, [Validators.required ]],
                            BankName : [data['result'].FinancialDetail.BankName, [Validators.required ]],
                            CreditLimits : [data['result'].FinancialDetail.CreditLimits, [Validators.required ]],
                            OnlineAccountType: [data['result'].FinancialDetail.OnlineAccountType],
                            OnlineAccountID: [ data['result'].FinancialDetail.OnlineAccountID],
                            IFSCCode : [data['result'].FinancialDetail.IFSCCode, [Validators.required ]],
                            CreditDays : [data['result'].FinancialDetail.CreditDays, [Validators.required ]],
                        })
                    }
                    else{
                        this.FinacInfo = this.fb.group({
                            TypeOfAccount : ['', [Validators.required ]],
                            Name : ['', [Validators.required ]],
                            AccountNumber : ['', [Validators.required ]],
                            BankName : ['', [Validators.required ]],
                            CreditLimits : ['', [Validators.required ]],
                            OnlineAccountType: [ ''],
                            OnlineAccountID: [ ''],
                            IFSCCode : ['', [Validators.required ]],
                            CreditDays : ['', [Validators.required ]],
                        })
                    }
                }

                const educationTempArray = data['result'].AdditionalDocuments;
                const localArray = data['result'].MandatoryDocuments ? data['result'].MandatoryDocuments : [];
                this.getAllDocumentsOnExisting(localArray);
                this.educationDocumentArray = [];
                for (const row of educationTempArray) {
                    this.userService.getUserDocument(row.File).subscribe((data: {}) => {
                        this.createImageFromBlobForEdu(data).then(val => {
                            row.File = val;
                            this.educationDocumentArray.push(row);
                        });
                    });
                }

                this.GetValue = true;
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    cancel() {
        this.location.back();
    }

    checkSameAsAddress(event , val){

        if(val){
            this.TemporaryCities = this.Cities;

            this.BasicInfoForm.patchValue({
                TemporaryAddressType : this.BasicInfoForm.controls.PermanentAddressType.value,
                TemporaryState : this.BasicInfoForm.controls.State.value,
                TemporaryCity : this.BasicInfoForm.controls.City.value,
                
                TemporaryPin : this.BasicInfoForm.controls.Pin.value,
                TemporaryAddress : this.BasicInfoForm.controls.Address.value,
            })
        }
    }

    onFileChange(event, name) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            if((file.size)/1024 > 1024){
                this.snackBar.open('File size is too large, please choose file below 1MB size.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                var index = this.allDocuments.findIndex(x=> x.Name == name);
                $('#'+index).val('');
                return 0;
            }
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png' || extension === 'pdf') {
                reader.onload = () => {
                    this.allImages[name] = reader.result;
                   
                };
            } else {
                this.snackBar.open('Invalid file format. Upload Only jpg, jpeg, png and pdf file.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                var index = this.allDocuments.findIndex(x=> x.Name == name);
                $('#'+index).val('');
            }
        }
    }

    checkPatternValidation(val, pattern) {
        if(!(val.match(pattern))) {
            this.snackBar.open('Invalid Pattern', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }


    downloadFile(data: any, name) {
        importedSaveAs(data, name);
    }

    getExtensionName(base64Data: any) {
        const extension = base64Data.split(';')[0].split('/')[1];
        if(extension === 'jpeg' || extension === 'jpg' || extension === 'png') {
            return 'image';
        } else if(extension === 'pdf') {
            return 'pdf';
        } else {
            return 'doc';
        }
    }

    deleteFile(name) {
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure to delete this file?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                const index = this.documentArray.findIndex(x => x.Name === name);
                if(index != -1) {
                    if(name == 'Aadhar' || name == 'Registration Amount Receipt'){
                        this.docMandatory--;
                    }
                    this.documentArray.splice(index, 1);
                }
                this.allImages[name] = null;
                this.documentUploadedArray[name] = null;

                const deindex = this.allDocuments.findIndex(x => x.Name === name);
                
                 $('#'+deindex).val('');
            }

        });
    }

    
    addDocumentDetail(val, name) {
        // this.allImages
        let localArray = {};
        localArray['Name'] = name;
        localArray['Image'] = this.allImages[name];
        localArray['Number'] = val;
        let index = this.documentArray.findIndex(x => x.Name === name)
        if (index === -1) {
            if (name === 'Registration Amount Receipt' || name === 'Aadhar') {
                this.docMandatory++;
            }
            this.documentArray.push(localArray);
            this.documentUploadedArray[name] = name;
        } else {
            this.documentArray[index].Image = this.allImages[name];
            this.documentArray[index].Number = val;
        }
        let deindex = this.allDocuments.findIndex(x => x.Name === name)
        console.log(deindex)
        var va = $('#'+deindex).val().split('\\');
        var filename = va[(va.length-1)];
        this.snackBar.open('File "'+filename+'" uploaded successfully.', '', {
            duration: 5000,
            panelClass: ['success-snackbar'],
            verticalPosition: 'top'
        });
        this.allDocuments[deindex].upload = false;
       
    }

    onEduDocumentFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png' || extension === 'pdf') {
                reader.onload = () => {
                    this.eduDocImage = reader.result;
                    this.educationDocType = extension;
                };
            } else {
                this.snackBar.open('Invalid file format. Upload Only jpg, jpeg, png and pdf file.', 'warning', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    addEduDocDetail() {
        if (!this.eduDocTitle || !this.eduDocImage || this.eduDocTitle === '' || this.eduDocImage === '') {
            return 0;
        }
        this.educationDocumentArray.push({
            Title: this.eduDocTitle,
            File: this.eduDocImage,
            FileType: this.educationDocType
        });
        this.eduDocImage = '';
        this.eduDocTitle = '';
        var va = $('#otherDoc').val().split('\\');
        var name = va[(va.length-1)];
        // this.myInputVariable.nativeElement.value = '';
        this.snackBar.open('File "'+name+'" uploaded successfully.', '', {
            duration: 5000,
            panelClass: ['success-snackbar'],
            verticalPosition: 'top'
        });
        $('#otherDoc').val('');
    }

    deleteEduDoc(index) {
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this file?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.educationDocumentArray.splice(index, 1);
                this.snackBar.open('Selected document is not deleted yet, please click on Update button to save your changes.', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getAllDocumentsOnExisting(localArray) {
        this.spinnerService.show();
        this.userService.getAllDocumentsByUser(this.Role).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                this.allDocuments = data['result'];
                for (const item of localArray) {
                    const index = this.allDocuments.findIndex(x => x.Name === item.Name);
                    this.allDocuments[index].value = item.Number;
                    this.allImages[item.Name] = item.Image;
                    this.userService.getUserDocument(item.Image).subscribe((data: {}) => {
                        this.createImageFromBlobForEdu(data).then(val => {
                            this.allImages[item.Name] = val;
                            this.addDocumentDetailByPreviousSelected(item.Number, item.Name);
                        });
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                      });
                }
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    addDocumentDetailByPreviousSelected(val, name) {
        // this.allImages
        let localArray = {};
        localArray['Name'] = name;
        localArray['Image'] = this.allImages[name];
        localArray['Number'] = val;
        let index = this.documentArray.findIndex(x => x.Name === name)
        if (index === -1) {
            if (name === 'PAN' || name === 'Aadhar') {
                this.docMandatory++;
            }
            this.documentArray.push(localArray);
            this.documentUploadedArray[name] = name;
        } else {
            this.documentArray[index].Image = this.allImages[name];
            this.documentArray[index].Number = val;
        }
    }

    createImageFromBlobForEdu(image: any) {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                resolve(reader.result);
            }, false);
            if (image) {
                reader.readAsDataURL(image);
            }
        });
    }

    getAllState(){
        this.spinnerService.show();
        this.stateService.getAllStates().subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.States = data['result'];
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }

    getAllCitiesByState(id){
        this.Cities = [];
        if(this.getValue) {
            this.BasicInfoForm.patchValue({
                "City": '',
            });
        }
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.Cities = data['result'];
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getAllCitiesByShopState(id){
        this.ShopCities = [];
        if(this.getValue) {
            this.ShopInfoForm.patchValue({
                "City": '',
            });
        }
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.ShopCities = data['result'];
            }
            else{

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

   

    checkEmail(){

        if(this.BasicInfoForm.controls.Email.value == ''){
            return 0;
        }
        if(this.UserEmail == this.BasicInfoForm.controls.Email.value){
            return 0;
        }
        this.spinnerService.show();
        this.userService.checkEmail(this.BasicInfoForm.controls.Email.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                if(data['result'].length > 0){
                    this.snackBar.open('Email that you filled is already in-use, Please use a different email ', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.BasicInfoForm.patchValue({
                        'Email': ''
                    });
                }
                else{

                }
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }

    uploadImage(event, name) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension =  fileName.substr(lastIndex + 1);
            if (extension === 'jpg' || extension === 'jpeg' || extension === 'png') {
                reader.onload = () => {
                    // this.allKeyPersonImages[name] = reader.result;
                    if(name == 'Avatar'){
                        this.BasicInfoForm.patchValue({
                            "Avatar": reader.result
                        });
                    }
                };
            } else {
                this.snackBar.open('This format is not supported.', 'Error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                this.myInputVariable.nativeElement.value = "";
            }
        }
    }

    sendRequestForTeam(){
        this.spinnerService.show();
        this.profilesService.sendRequestForTeam(this.currentUser.id, this.RequestType).subscribe((data) => {
            this.spinnerService.hide();
            this.snackBar.open(data['message'], '', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
            this.getOne();
            this.getTeamRequestData();
        }, err => {
            this.spinnerService.hide();

            this.snackBar.open('Server Error', 'Error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getTeamCompanyDetailByUserID(){
        this.spinnerService.show();
        this.teamCompanyService.getTeamCompanyDetailByUserID(this.currentUser.id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.selectedTeamCompany = data['result'];
                if (this.selectedTeamCompany) {
                    this.getLeaveRequestDataByUserTeam(this.currentUser.id, this.selectedTeamCompany.TeamID._id);
                }
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    getLeaveRequestDataByUserTeam(UserID, TeamID) {
        this.spinnerService.show();
        this.teamCompanyService.getLeaveRequestDataByUserTeam(UserID, TeamID).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.isLeaveRequestData = data['result'];
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    sendRequestForLeaveTeam(){
        this.notificationService.smartMessageBox({
            title: "Oops!",
            content: 'Do you really want to leave?' ,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.deleteTeamMemberRequest(this.selectedTeamCompany._id).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open('Successfully deleted team member from team', '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getOne();
                        this.getTeamRequestData();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
            }
            else{

            }
        })
    }

    rejectRequest(id){
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: 'Are you sure reject user team request?',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.rejectInvitationRequest(id).subscribe( (data: {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open(data['message'], '', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getOne();
                        this.getTeamRequestData();
                        this.getInvitationRequestData();
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    acceptRequest(id){
        this.spinnerService.show();
        this.teamCompanyService.acceptInvitationRequest(id, this.currentUser.id).subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open(data['message'], '', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getOne();
                this.getTeamRequestData();
                this.getInvitationRequestData();
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }


    // update

    updateRecord(){
        if(this.Role == 'Service Provider'){
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure update this record?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.spinnerService.show();
                    this.userService.updateSPUsers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value , this.documentArray , this.educationDocumentArray ,this.UserServices , this.FinacInfo.value , this.SecondaryDetailForm.value , this.FamilyInfoForm.value, this.userdata.id).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if ( data['status']) {
                            this.snackBar.open('Record updated successfully', '', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            this.getOne();
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                      });
                }
                if (ButtonPressed === "No") {

                }

            });
        }
        else if(this.Role == 'Vendor'){
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure update this record?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.spinnerService.show();
                    this.userService.updateVendorUsers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value , this.documentArray , this.educationDocumentArray , this.FinacInfo.value , this.VenderAddress).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if ( data['status']) {
                            this.snackBar.open('Record updated successfully', '', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            this.getOne();
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                      });
                }
                if (ButtonPressed === "No") {

                }

            });

        }
        else {
            this.notificationService.smartMessageBox({
                title: "Update!",
                content: "Are you sure update this record?",
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.spinnerService.show();
                    this.userService.updateUsers(this.ID, this.BasicInfoForm.value , this.AccountInfoForm.value , this.documentArray , this.educationDocumentArray , this.FinacInfo.value ).subscribe((data: {}) => {
                        this.spinnerService.hide();
                        if ( data['status']) {
                            this.snackBar.open('Record updated successfully', '', {
                                duration: 5000,
                                panelClass: ['success-snackbar'],
                                verticalPosition: 'top'
                            });
                            this.getOne();
                        }
                    }, err => {
                        this.spinnerService.hide();
                        this.snackBar.open('Server Error', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                      });
                }
                if (ButtonPressed === "No") {

                }

            });
        }
    }

    
    
    
    getAllCitiesByStateVendorAddress(id){
        this.VenderAddressCities = [];
        this.VendorAddressForm.patchValue({
            'City' : id,
        })
        this.spinnerService.show();
        this.cityService.getAllCitiesByState(id).subscribe( (data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.VenderAddressCities = data['result'];
            }
            else{
    
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    addVenderAddress(){
        if(this.VendorAddressForm.invalid){
            return 0;
        }
        if(this.SelectedVenderAddress == -1){
            this.VenderAddress.push(this.VendorAddressForm.value);
            this.spinnerService.show();
            this.profilesService.updateVenderAddress(this.currentUser.id , this.VenderAddress).subscribe( (data : {})=>{
                this.spinnerService.hide();
                if(data['status']){
                    this.VendorAddressForm.reset();
                    this.VendorAddressForm.patchValue({
                        Office : 'Branch'
                    })
                    this.snackBar.open('Office address successfully added.', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            })
        }
        else{
            this.VenderAddress[this.SelectedVenderAddress] = this.VendorAddressForm.value;
            this.spinnerService.show();
            this.profilesService.updateVenderAddress(this.currentUser.id , this.VenderAddress).subscribe( (data : {})=>{
                this.spinnerService.hide();
                if(data['status']){
                    this.SelectedVenderAddress = -1;
                    this.VendorAddressForm.reset();
                    this.VendorAddressForm.patchValue({
                        Office : 'Branch'
                    })
                    this.snackBar.open('Office address successfully updated.', '', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
              })
        }
        
        
    }

    deleteVenderAddress(i){
        this.notificationService.smartMessageBox({
            title: "Delete Alert!",
            content: "Are you sure delete this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.VenderAddress.splice(i , 1);
                this.spinnerService.show();
                this.profilesService.updateVenderAddress(this.currentUser.id , this.VenderAddress).subscribe( (data : {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.VendorAddressForm.reset();
                        this.VendorAddressForm.patchValue({
                            Office : 'Branch'
                        })

                        this.snackBar.open('Office address successfully deleted.', '', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
            }
            
        })
    }

    updateVenderAddress(i){
        /*this.notificationService.smartMessageBox({
            title: "Update Alert!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                
                
            }
            
        }) */
        this.getAllCitiesByStateVendorAddress(this.VenderAddress[i].State);
       
        this.SelectedVenderAddress = i;
        this.VendorAddressForm = this.fb.group({
            Office: [ this.VenderAddress[i].Office , [Validators.required ] ],
            Name: [ this.VenderAddress[i].Name , [Validators.required ] ],
            Email: [ this.VenderAddress[i].Email , [Validators.required, Validators.email ,  Validators.email , Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$') ] ],
            Number: [ this.VenderAddress[i].Number , [Validators.required, Validators.minLength(10), Validators.maxLength(10) ] ],
            Address: [ this.VenderAddress[i].Address , [Validators.required ] ],
            Pin: [ this.VenderAddress[i].Pin , [Validators.required ,Validators.minLength(6), Validators.maxLength(6) ] ],
            
            City: [ this.VenderAddress[i].City , [Validators.required ] ],
            State: [ this.VenderAddress[i].State , [Validators.required ] ]
        })
    }
}
