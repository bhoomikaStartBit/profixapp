import { TestBed } from '@angular/core/testing';

import { ProfilesService} from "@app/features/Management/profiles/profiles.service";

describe('ProfilesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfilesService = TestBed.get(ProfilesService);
    expect(service).toBeTruthy();
  });
});
