import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';

import {ProfilesService} from "@app/features/Management/profiles/profiles.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'userlist',
  templateUrl: './profiles.component.html',
})
export class ProfilesComponent implements OnInit {

    Profiles : any;
  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private profilesService: ProfilesService,
      private spinnerService : Ng4LoadingSpinnerService

  ) {
    this.spinnerService.hide();
      this.getAllCategories();
  }

  ngOnInit() {
  }

  getAllCategories(){
        // this.ProfilesService.getAllCategories().subscribe( (data: {}) => {
        //     this.Categories = data['result'];
        //     console.log(this.Categories);
        // });
  }

  private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // log to console instead
        return Observable.throw(errMsg);
  }

}
