
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {ProfilesComponent} from "@app/features/Management/profiles/profiles.component";
import {ProfilesFormComponent} from "@app/features/Management/profiles/profiles-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: ProfilesComponent
  },{
    path: 'createnew/:id',
    component: ProfilesFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
