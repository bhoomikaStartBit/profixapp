import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router} from '@angular/router';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import {StateService} from "@app/features/Management/state/state.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'user-form',
  templateUrl: './state-forms.component.html',
})
export class StateFormComponent implements OnInit {
    ID: any;
    StateForm: FormGroup;
    
    constructor(private http: HttpClient,
              private router : Router ,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private location: Location,
              private stateService: StateService,
              private notificationService: NotificationService,
              private snackBar: MatSnackBar,
              private spinnerService : Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
      this.StateForm = fb.group({
          Name: [ '', [Validators.required ] ],
      });
      this.ID = this.route.params['value'].id;
      console.log(this.ID);
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
    }

    ngOnInit() {

    }

    getOne(id) {
        this.spinnerService.show();
        this.stateService.getOneState(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.StateForm = this.fb.group({
                    Name: [  data['result'].Name, [Validators.required ] ],
                });

                console.log(this.StateForm);
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }

    onSubmitForm() {
        if (this.StateForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    createNew(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.stateService.addState(this.StateForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          });
    }
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
            this.stateService.updateState(this.ID, this.StateForm.value).subscribe((data: {}) => {
                this.spinnerService.hide();
                if ( data['status']) {
                    this.snackBar.open('Record updated successfully', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.location.back();
                }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  });
            }
            if (ButtonPressed === "No") {

            }

        });
    }
    cancel(){
        // this.router.navigate(['/states']);
        this.location.back();
    }
}
