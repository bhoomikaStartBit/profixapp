import {NgModule} from "@angular/core";

import {routing} from "./state.routing";
import {StateComponent} from "@app/features/Management/state/state.component";
import {StateFormComponent} from "@app/features/Management/state/state-forms.component";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";

@NgModule({
  declarations: [
      StateComponent,
      StateFormComponent
  ],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule ,
      ReactiveFormsModule,
      MaterialModuleModule,
      Ng4LoadingSpinnerModule.forRoot()

  ],
  providers: [FormsModule , ReactiveFormsModule],
})
export class StateModule {

}
