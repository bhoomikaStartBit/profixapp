import { Component, OnInit , ViewChild } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import {NotificationService} from "@app/core/services";

import {StateService} from "@app/features/Management/state/state.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'statelist',
  templateUrl: './state.component.html',
})
export class StateComponent implements OnInit {

  States : any;
    displayedColumns: string[] = [
        'SN',
        'Name',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;

    limit:number = 10;
    pageIndex : number = 0;
    pageLimit:number[] = [10,20,50];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private stateService: StateService,
      private snackBar: MatSnackBar,
      private spinnerService: Ng4LoadingSpinnerService

  ) {
    this.spinnerService.hide();
      this.getAllState();
  }

  ngOnInit() {

  }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

  getAllState(){
       this.spinnerService.show();
       this.stateService.getAllStates().subscribe( (data: {}) => {
        this.spinnerService.hide();
           this.States = data['result'];
           this.dataSource = new MatTableDataSource(data['result']);
           this.dataSource.paginator = this.paginator;
           this.dataSource.sort = this.sort;
       }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
  }


    delete(id){
      console.log(id);
      this.spinnerService.show();
        this.stateService.getRelatedRecordOfState(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                var lab ='';
                if(data['result'].length){
                    lab = 'You can\'t delete this record because this is having many dependant "'+ data['Table']+'" records. Please delete all dependant records first.';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[Ok]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Ok") {


                        }

                    });
                }
                else{
                    lab = 'Are you sure delete this record?';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[No][Yes]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Yes") {
                            this.spinnerService.show();
                            this.stateService.deleteState(id).subscribe((data: {}) => {
                                this.spinnerService.hide();
                                if (data['status']) {
                                    this.snackBar.open('Record deleted successfully', 'Delete', {
                                        duration: 5000,
                                        panelClass: ['danger-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                } else {
                                }
                                this.getAllState();
                            }, err => {
                                this.spinnerService.hide();
                                this.snackBar.open('Server Error', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                              });
                        }
                        if (ButtonPressed === "No") {

                        }

                    });
                }

            }
        });
    }

    changePage(event){
        this.pageIndex = event.pageSize*event.pageIndex;
    }
    
}
