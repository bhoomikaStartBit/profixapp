
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {StateComponent} from "@app/features/Management/state/state.component";
import {StateFormComponent} from "@app/features/Management/state/state-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: StateComponent
  },{
    path: 'createnew/:id',
    component: StateFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
