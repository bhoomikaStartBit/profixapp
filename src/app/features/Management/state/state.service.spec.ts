import { TestBed } from '@angular/core/testing';

import { StateService} from "@app/features/Management/state/state.service";

describe('RoleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StateService = TestBed.get(StateService);
    expect(service).toBeTruthy();
  });
});
