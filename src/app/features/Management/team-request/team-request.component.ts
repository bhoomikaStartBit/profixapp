import { Component, OnInit , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {UserService} from "@app/features/Management/users/user.service";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import { TeamCompanyService} from "@app/features/Management/team-company/team-company.service";
import {ProfilesService} from "@app/features/Management/profiles/profiles.service";
import {MyTeamsService} from "@app/features/Management/myteam/myteam.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'team-request',
  templateUrl: './team-request.component.html',
})
export class TeamRequestComponent implements OnInit {
    Users : any;
    blk = false;
    displayedColumns: string[] = [
        'SN',
        'Name',
        'Request Date',
        'AdminAction',
        'type',
        'Status'
    ];
    dataSource: MatTableDataSource<any>;

    displayedColumnsForDisposal: string[] = [
        'SN',
        'TeamName',
        'Name',
        'Request Date',
        'AdminAction',
        'type',
        'Status'
    ];
    dataSourceForDisposal: MatTableDataSource<any>;

    displayedColumnsForInviteRequest: string[]
    dataSourceForInviteRequest: MatTableDataSource<any>;

    displayedColumnsForLeaveRequest: string[] = [
        'SN',
        'Team Name',
        'InvitedBy',
        'Name',
        'Request Date',
        'AdminAction',
        'Status'
    ];
    dataSourceForLeaveRequest: MatTableDataSource<any>;

    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    isInvitationRequestData: any = [];
    isLeaveRequestData: any = [];
    currentUser: any;
    selectedTeamCompany: any = {};
    DisposalData: any = [];
  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private userService: UserService,
      private snackBar: MatSnackBar,
      private teamCompanyService: TeamCompanyService,
      private profilesService: ProfilesService,
      private myTeamsService: MyTeamsService,
      private spinnerService : Ng4LoadingSpinnerService
  ) {
     this.spinnerService.hide(); 
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(this.currentUser.RoleName === 'Super Admin') {
          this.displayedColumnsForInviteRequest = [
              'SN',
              'Team Name',
              'InvitedBy',
              'Name',
              'Request Date',
              'AdminAction',
              'Status'
          ];
      } else {
          this.displayedColumnsForInviteRequest = [
              'SN',
              'Team Name',
              'InvitedBy',
              'Request Date',
              'AdminAction',
              'Status'
          ];
      }
      this.getAllPendingRequest();
      this.getAllPendingrequestForDisposal();
      this.getTeamDetail();
      this.getAllUserRequestForInvite();
  }

  ngOnInit() {

  }
    chcekBlock(i , tis){
      console.log(i , tis);
    }

    getTeamDetail() {
        this.spinnerService.show();
        this.teamCompanyService.getOneTeamCompanyByOwner(this.currentUser.id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.selectedTeamCompany = data['result'];
                if(this.selectedTeamCompany) {
                    this.getAllUserRequestForLeave();
                }
            }
        }, err => {
            this.spinnerService.hide(); 
        });
    }

    getAllUserRequestForLeave() {
        let TID = '0';
        if(this.currentUser.RoleName !== 'Super Admin') {
            TID = this.selectedTeamCompany._id;
        }
        this.spinnerService.show();
        this.teamCompanyService.getAllTeamMemberForLeave(TID).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.isLeaveRequestData = data['result'];
                this.dataSourceForLeaveRequest = new MatTableDataSource(data['result']);
                this.dataSourceForLeaveRequest.paginator = this.paginator;
                this.dataSourceForLeaveRequest.sort = this.sort;
            }
        }, err => {
            this.spinnerService.hide(); 
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }


    getAllUserRequestForInvite() {
        let UID = '0';
        if(this.currentUser.RoleName !== 'Super Admin') {
            UID = this.currentUser.id;
        }
        this.spinnerService.show();
        this.teamCompanyService.getAllByUserID(UID).subscribe((data: {}) => {
            this.spinnerService.hide();
            if(data['status']){
                this.isInvitationRequestData = data['result'];
                this.dataSourceForInviteRequest = new MatTableDataSource(data['result']);
                this.dataSourceForInviteRequest.paginator = this.paginator;
                this.dataSourceForInviteRequest.sort = this.sort;
            }
        }, err => {
            this.spinnerService.hide(); 
            console.log(err);
            this.snackBar.open('Server Error', 'success', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllPendingRequest(){
        this.spinnerService.show();
      this.userService.getAllPendingRequest().subscribe( (data: {})=>{
        this.spinnerService.hide();
          if(data['status']){
              this.Users = data['result'];
              this.dataSource = new MatTableDataSource(data['result']);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              console.log(this.Users);
          }
      })
    }

    getAllPendingrequestForDisposal(){
        this.spinnerService.show();
        this.myTeamsService.getAllPendingrequestForDisposal().subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.DisposalData = data['result'];
                this.dataSourceForDisposal = new MatTableDataSource(this.DisposalData);
                this.dataSourceForDisposal.paginator = this.paginator;
                this.dataSourceForDisposal.sort = this.sort;
                console.log(this.DisposalData)
            }
        })
    }

    rejectRequest(id){
        this.notificationService.smartMessageBox({
            title: "Oops!",
            content: 'Are you sure that you want to reject this request?',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.userService.rejectRequest(id).subscribe( (data: {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open(data['message'], 'Delete', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllPendingRequest();
                    }
                }, err => {
                    this.spinnerService.hide(); 
                    console.log(err);
                    this.snackBar.open('Server Error', 'Delete', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    acceptRequest(id){
        this.spinnerService.show();
        this.userService.acceptRequest(id).subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open(data['message'], 'Delete', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getAllPendingRequest();
            }
        }, err => {
            this.spinnerService.hide(); 
            console.log(err);
            this.snackBar.open('Server Error', 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    rejectInvitationRequest(id){
        this.notificationService.smartMessageBox({
            title: "Oops!",
            content: 'Are you sure that you want to reject this request?',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.rejectInvitationRequest(id).subscribe( (data: {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open(data['message'], 'Delete', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllUserRequestForInvite();
                    } else {
                        this.snackBar.open(data['message'], 'Delete', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                }, err => {
                    this.spinnerService.hide(); 
                    console.log(err);
                    this.snackBar.open('Server Error', 'Delete', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    acceptInvitationRequest(id){
        this.spinnerService.show();
        this.teamCompanyService.acceptInvitationRequest(id, this.currentUser.id).subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open(data['message'], 'Delete', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getAllUserRequestForInvite();
            } else {
                this.snackBar.open(data['message'], 'Delete', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide(); 
            console.log(err);
            this.snackBar.open('Server Error', 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    rejectLeaveRequest(id){
        this.notificationService.smartMessageBox({
            title: "Oops!",
            content: 'Are you sure that you want to reject this request?',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.teamCompanyService.rejectLeaveRequest(id).subscribe( (data: {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open(data['message'], 'Delete', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllUserRequestForLeave();
                    }
                }, err => {
                    this.spinnerService.hide(); 
                    console.log(err);
                    this.snackBar.open('Server Error', 'Delete', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    acceptLeaveRequest(id, user){
        this.spinnerService.show();
        this.teamCompanyService.acceptLeaveRequest(id, user).subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open(data['message'], 'Delete', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getAllUserRequestForLeave();
            }
        }, err => {
            this.spinnerService.hide(); 
            console.log(err);
            this.snackBar.open('Server Error', 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }


    rejectDisposalRequest(id){
        this.notificationService.smartMessageBox({
            title: "Oops!",
            content: 'Are you sure that you want to reject this request?',
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.myTeamsService.rejectDisposalRequest(id).subscribe( (data: {})=>{
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open(data['message'], 'Delete', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllPendingrequestForDisposal();
                    }
                }, err => {
                    this.spinnerService.hide(); 
                    console.log(err);
                    this.snackBar.open('Server Error', 'Delete', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {

            }

        });
    }

    acceptDisposalRequest(id, team){
        this.spinnerService.show();
        this.myTeamsService.acceptDisposalRequest(id, team).subscribe( (data: {})=>{
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open(data['message'], 'Delete', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.getAllPendingrequestForDisposal();
            }
        }, err => {
            this.spinnerService.hide(); 
            console.log(err);
            this.snackBar.open('Server Error', 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}