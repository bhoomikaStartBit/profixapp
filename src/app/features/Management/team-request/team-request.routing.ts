
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {TeamRequestComponent} from "@app/features/Management/team-request/team-request.component";


export const routes:Routes = [

  {
    path: '',
    component: TeamRequestComponent
  }
];

export const routing = RouterModule.forChild(routes);
