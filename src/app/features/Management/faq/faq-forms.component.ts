import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ActivatedRoute, Params} from '@angular/router';
import {Location} from "@angular/common";
import { Router} from '@angular/router';
import { FaqService } from "@app/features/Management/faq/faq.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from "@angular/material";
import { NotificationService } from "@app/core/services";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
  selector: 'faq-form',
  templateUrl: './faq-forms.component.html',
})


export class FaqFormComponent implements OnInit {
    ID : any;
    FaqForm : FormGroup;
    getDescription = false;
    getData = false;    
    public validationFaqOptions:any = {};
  constructor(private http: HttpClient ,
              private router : Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private faqService: FaqService,
              private notificationService: NotificationService,
              private spinnerService : Ng4LoadingSpinnerService,
              private location: Location,
  ) {
      this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      console.log(this.ID);
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      this.FaqForm = fb.group({
        Question: [ '', [Validators.required ] ],                
        Answer : [ '', [Validators.required ] ] ,   
        Order : [ 0, [Validators.required ] ]         
      });

      this.validationFaqOptions = {
        // Rules for form validation
        rules: {
            Question: {
                required: true
            },  
            Answer: {
                required: true
            }, 
            Order: {
                required: true
            },                         
        },

        // Messages for form validation
        messages: {
            Question: {
                required: 'Please enter Question'
            }, 
            Answer: {
                required: 'Please enter Answer'
            },    
            Order: {
                required: 'Please enter Order number of FAQ'
            },                    
        },
        submitHandler: this.onSubmitForm        

    };
    this.getData = true;
  }

    ngOnInit() {
    }
    
  descriptionss(){
      this.getDescription = true;
    }

    onSubmitForm() {        
        if (this.FaqForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNewfaq();            
        } else {
            this.editExisting();
        }        
    }

    onSubmitDraft() {        
        if (this.FaqForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNewfaqdraft();            
        } else {
            this.draftExisting();
        }        
    }


    createNewfaq(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.faqService.addFaq(this.FaqForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(['/faqs']);
                console.log("Created successfully");
            } else {
                
                this.snackBar.open(data['message'], 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })

    }

    createNewfaqdraft(){
        // this.router.navigate(['states']);
        this.spinnerService.show();
        this.faqService.draftFaq(this.FaqForm.value).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.snackBar.open('Record saved in Draft', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(['/faqs']);
                console.log("Draft Created successfully");
            } else {                
                this.snackBar.open(data['message'], 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })

    }
    
    editExisting(){
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.faqService.updateFaq(this.ID, this.FaqForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/faqs']);
                        console.log("update success");
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
        // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    draftExisting(){
        this.notificationService.smartMessageBox({
            title: "Draft!",
            content: "Are you sure to draft this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.faqService.draftExistFaq(this.ID, this.FaqForm.value).subscribe((data: {}) => {
                    this.spinnerService.hide();
                    if ( data['status']) {
                        this.snackBar.open('Record stored in draft successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.router.navigate(['/faqs']);
                        console.log("draft success");
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('Server Error', '', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                  })
        // this.router.navigate(['states']);
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.faqService.getOneFaq(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if ( data['status']) {
                this.FaqForm = this.fb.group({
                    Question: [  data['result'].Question, [Validators.required ] ],
                    Answer: [  data['result'].Answer,[Validators.required ] ],      
                    Order: [ data['result'].Order, [Validators.required ] ],             
                });                      
            //    console.log(this.FaqForm);
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }    

    register(){
     // console.log(this.FaqForm.value);
        this.router.navigate(['/faqs']);
    }

    cancel() {
        this.location.back();
    }

    onChangeFaqTitle() {
        let faq = this.FaqForm.get('Title').value
        faq = faq.replace(/[^\w\s]/gi, "");
        faq = faq.trim().toLowerCase();
        faq = faq.split(' ').join('-');
        let str = '';
        for(let i = 0; i< faq.length; i++) {
          if(faq.length-1 > i) {
              let first = faq.charAt(i);
              let second = faq.charAt(i + 1);
              if(!(first === '-' && second === '-')) {
                  str += faq.charAt(i, i+1);
              }
          } else {
              str += faq.charAt(i);
          }
        }
        if (str.length > 190) {
            str = str.substring(0, 190);
        }
        this.FaqForm.patchValue({
            Slug: str
        });
    }
    

}
