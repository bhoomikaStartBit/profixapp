import { ModuleWithProviders } from "@angular/core"
import { RouterModule, Routes } from "@angular/router";
import { FaqComponent } from "@app/features/Management/faq/faq.component";
import { FaqFormComponent } from "@app/features/Management/faq/faq-forms.component";


export const routes:Routes = [

  {
    path: '',
    component: FaqComponent
  },
  {
    path: 'createnewfaq/:id',
    component: FaqFormComponent
  },


];

export const routing = RouterModule.forChild(routes);
