import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import { FaqService } from "@app/features/Management/faq/faq.service";
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatSnackBar } from "@angular/material";
import { CommonService } from "@app/core/common/common.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'faqlist',
  templateUrl: './faq.component.html',
})
export class FaqComponent implements OnInit {
    Faqs : any;
    url : any;
    displayedColumns: string[] = [        
        'Question',
        'Order',
        'DateTime',
        'Active',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
      private http: HttpClient ,
      private notificationService: NotificationService,
      private faqService: FaqService,
      private snackBar: MatSnackBar,
      private commonService: CommonService,
      private spinnerService: Ng4LoadingSpinnerService

  ) {
      this.spinnerService.hide();
      this.getAllFaqs();
      this.url = this.commonService.getApiUrl();
  }

  ngOnInit() {

  }

  getAllFaqs(){
        this.spinnerService.show();
        this.faqService.getAllFaqs().subscribe( (data: {}) => {
            this.spinnerService.hide();
            this.Faqs = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.filterPredicate = function(data, filter: string): boolean {
                return data.Title.toLowerCase().includes(filter);
            };
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            console.log(this.Faqs);
        });
    }


    applyFilter(filterValue: string) {
        console.log(filterValue)
        console.log(this.dataSource.data)
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


    delete(id){      
        this.spinnerService.show();
        this.faqService.getRelatedRecordOfFaq(id).subscribe((data: {}) => {
            this.spinnerService.hide();
            if (data['status']) {
                var lab ='';
                if(data['result'].length){
                    lab = 'You can\'t delete this record because this is having many dependant "'+ data['Table']+ '" records. Please delete all dependant records first.';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[Ok]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Ok") {

                        }

                    });
                }
                else{
                    lab = 'Are you sure delete this record?';
                    this.notificationService.smartMessageBox({
                        title: "Delete!",
                        content: lab,
                        buttons: '[No][Yes]'
                    }, (ButtonPressed) => {
                        if (ButtonPressed === "Yes") {
                            this.spinnerService.show();
                            this.faqService.deleteFaq(id).subscribe((data: {}) => {
                                this.spinnerService.hide();
                                if (data['status']) {
                                    this.getAllFaqs();
                                    this.snackBar.open('Record deleted successfully', 'Delete', {
                                        duration: 5000,
                                        panelClass: ['danger-snackbar'],
                                        verticalPosition: 'top'
                                    });
                                } else {
                                }

                            }, err => {
                                this.spinnerService.hide();
                                this.snackBar.open('Server Error', '', {
                                    duration: 5000,
                                    panelClass: ['danger-snackbar'],
                                    verticalPosition: 'top'
                                });
                              })
                        }
                        if (ButtonPressed === "No") {

                        }

                    });
                }

            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('Server Error', '', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
          })
    }
}
