import { Component, OnInit  , ViewChild} from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationService } from '@app/core/services';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {ReviewsService} from "@app/features/Management/reviews/reviews.service";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'sa-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
  Reviews: any;
  url : any;
  defalutimageurl: any;
  displayedColumns: string[] = [
      'SN',
      'UserID',
      'ReviewDate',
      'Rating',
      'Review',
      'Action'
      
  ];
  dataSource: MatTableDataSource<any>;
  isLoadingResults: any;
  isRateLimitReached: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient ,
      private notificationService: NotificationService,
      private snackBar: MatSnackBar,
      private commonService: CommonService,
      private reviewsService: ReviewsService,
      private spinnerService: Ng4LoadingSpinnerService
    ) {
        this.spinnerService.hide();
        this.getAllReviews();
        this.url = this.commonService.getApiUrl();
        this.defalutimageurl = this.commonService.getApiUrl()+'/Documents/Avatar.png';
  }

  ngOnInit() {
  }

  getAllReviews(){
    this.spinnerService.show();
    this.reviewsService.getAllReviews().subscribe( (data: {}) => {
        this.spinnerService.hide();
        this.Reviews = data['result'];
        this.dataSource = new MatTableDataSource(data['result']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

    }, err => {
      this.spinnerService.hide();
      this.snackBar.open('Server Error', '', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
      });
    });
  }

  acceptReview(id){
    this.spinnerService.show();
    this.reviewsService.acceptReview(id).subscribe( (data: {})=>{
      this.spinnerService.hide();
        if(data['status']){
            this.snackBar.open(data['message'], 'Delete', {
                duration: 5000,
                panelClass: ['success-snackbar'],
                verticalPosition: 'top'
            });
            this.getAllReviews();
        } else {
            this.snackBar.open(data['message'], 'Delete', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }, err => {
      this.spinnerService.hide();
        console.log(err);
        this.snackBar.open('Server Error', 'Delete', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    })
  }

}
