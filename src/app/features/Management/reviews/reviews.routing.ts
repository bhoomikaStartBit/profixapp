
import {ModuleWithProviders} from "@angular/core"
import {RouterModule, Routes} from "@angular/router";
import {ReviewsComponent} from "@app/features/Management/reviews/reviews.component";
import {ReviewEditComponent} from "@app/features/Management/reviews/review-edit.component";



export const routes:Routes = [

  {
    path: '',
    component: ReviewsComponent
  },

  {
    path: 'edit/:id',
    component: ReviewEditComponent
  },

];

export const routing = RouterModule.forChild(routes);
