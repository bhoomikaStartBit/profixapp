import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { CommonService} from "@app/core/common/common.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  url: string;

  constructor(private http: HttpClient, private commService: CommonService) {
    this.url = commService.getApiUrl() + '/reviews/';
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  
  getAllReviews(): Observable<any> {
    return this.http.get(this.url + 'getAllReviews/').pipe(
      map(this.extractData));
  }

  getOneReview(id): Observable<any> {
    return this.http.get(this.url + 'getOneReview/' + id).pipe(
      map(this.extractData));
  }


  updateReview (id, data): Observable<any> {
    return this.http.put<any>(this.url + 'updateReview/' + id, JSON.stringify(data), httpOptions).pipe(
      tap(( ) => console.log(`updated role w/ id=${data._id}`)),
      catchError(this.handleError<any>('updateCity'))
    );
  }

  acceptReview(id): Observable<any> {
    return this.http.get(this.url + 'acceptReview/' + id).pipe(
        map(this.extractData));
  }


  private handleError<T> (operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      const errorData = {
        status: false,
        message: 'Server Error'
      };
      // Let the app keep running by returning an empty result.
      return of(errorData);
    };
  }
}
