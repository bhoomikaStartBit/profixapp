import {NgModule} from '@angular/core';
import {routing} from "./reviews.routing";
import {ReviewsComponent} from './reviews.component';
import {ReviewEditComponent} from './review-edit.component';
import {SharedModule} from "@app/shared/shared.module";
import {SmartadminInputModule} from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminDatatableModule} from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';


@NgModule({
  declarations: [ReviewsComponent, ReviewEditComponent],
  imports: [
    SharedModule,
    routing,
      SmartadminDatatableModule,
      FormsModule , ReactiveFormsModule,
      SmartadminInputModule,
      MaterialModuleModule,
      SmartadminValidationModule,
      OwlDateTimeModule,
      OwlNativeDateTimeModule,
      Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
      FormsModule , ReactiveFormsModule
  ],
})
export class ReviewsModule { }
