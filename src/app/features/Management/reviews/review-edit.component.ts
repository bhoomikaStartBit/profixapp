import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {ReviewsService} from "@app/features/Management/reviews/reviews.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Params} from '@angular/router';
import { Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'sa-reviews',
  templateUrl: './review-edit.compontent.html',
  
})
export class ReviewEditComponent implements OnInit {
    ID : any;
    ReviewsForm : FormGroup;
    showSpinner: boolean = false;
    currentUser: any;
    getData: boolean = false;
    selltedamt = false;
    ReviewedBy: any;
    RatingIs: Number;
    OrderAssignIDIs: any;

  constructor(private http: HttpClient ,
    private router : Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private reviewsService: ReviewsService,
    private notificationService: NotificationService,
    private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.spinnerService.hide();
      this.ID = this.route.params['value'].id;
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      
      console.log(this.currentUser.id);
      if (this.ID !== '-1') {
          this.getOne(this.ID);
      }
      else{
        this.ReviewsForm = fb.group({
            Review: [ '', [Validators.required ] ],
            Rating: [ '', [Validators.required ] ]
            
        });
          this.getData = true;
      }      
    }

  ngOnInit() {
  }

  onSubmitForm() {

    if (this.ReviewsForm.invalid) {
        return 0;
    }
    if (this.ID === '-1') {
       // this.createNew();
    } else {
        this.editExisting();
    }
}

editExisting(){
    this.notificationService.smartMessageBox({
        title: "Update!",
        content: "Are you sure update this record?",
        buttons: '[No][Yes]'
    }, (ButtonPressed) => {
        if (ButtonPressed === "Yes") {
            this.showSpinner = true;
            this.reviewsService.updateReview(this.ID, this.ReviewsForm.value).subscribe((data: {}) => {
                if ( data['status']) {
                    this.snackBar.open('Record updated successfully', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.showSpinner = false;
                    this.router.navigate(['/reviews']);
                    console.log("update success");
                }
            }, err => {
                this.spinnerService.hide();
                this.snackBar.open('Server Error', '', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
              });
        // this.router.navigate(['states']);
        }
        if (ButtonPressed === "No") {

        }

    });

}

getOne(id) {
    this.spinnerService.show();
    this.reviewsService.getOneReview(id).subscribe((data: {}) => {
        this.spinnerService.hide();
        if ( data['status']) {
            this.ReviewsForm = this.fb.group({
                Review: [ data['result'].Review, [Validators.required ] ],
                Rating: [ data['result'].Rating, [Validators.required ] ],
                IsApproved: [ data['result'].IsApproved ]
            });
            this.getData = true;
            this.ReviewedBy = data['result'].UserID ? data['result'].UserID.FirstName+' '+data['result'].UserID.MiddleName+' '+data['result'].UserID.LastName : "";
            this.RatingIs = data['result'].Rating;
            this.OrderAssignIDIs = data['result'].OrderAssignID ?  data['result'].OrderAssignID.ProjectName + " #"+data['result'].OrderAssignID.OrderID : 'Not Available';
            console.log(this.ReviewsForm);
        }
    }, err => {
        this.spinnerService.hide();
        this.snackBar.open('Server Error', '', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
      });
}



register(){
  console.log(this.ReviewsForm.value);
    this.router.navigate(['/reviews']);
}

cancel() {
    this.router.navigate(['/reviews']);
}

}
