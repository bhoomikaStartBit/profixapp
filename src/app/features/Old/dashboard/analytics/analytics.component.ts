import {Component, OnInit, ViewChild} from '@angular/core';
import { Store } from '@ngrx/store';
import {Router} from '@angular/router';

import * as fromCalendar from "@app/core/store/calendar";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {FormControl} from "@angular/forms";
import {Angular5Csv} from "angular5-csv/dist/Angular5-csv";
import { ProjectsService } from '@app/features/Management/projects/projects.service';

@Component({
  selector: 'sa-analytics',
  templateUrl: './analytics.component.html',
})
export class AnalyticsComponent implements OnInit {

    displayedOrderColumns: string[] = [
        'Name',
        'AssignTo',
        'Customer',
        'Status',
        'ScheduledDate',
        'Action'
    ];
    dataSourceOrder: MatTableDataSource<any>;

  public calendar$
  currentUser: any;
    statusData: any[] = [{
        step: 'New',
        status: 'Completed'
    }, {
        step: 'Assignment',
        status: 'Completed'
    }, {
        step: 'Progress',
        status: 'Completed'
    }, {
        step: 'Review',
        status: 'In progress'
    }, {
        step: 'Completed',
        status: 'Not Started'
    }];

    WorkStatusArray = ['Assign', 'New', 'Assignment', 'In Progress', 'Review', 'Completed'];
    displayedColumns: string[] = ['OrderName', 'CustomerName', 'Start', 'Status', 'Cost', 'Location'];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    allOrders: any = [];
    selectedWorkArray: any = [];
    ShowExpiryAlert : boolean =  false;
    allStatus: any = [
        {StatusID: 2, Name: 'Assignment'},
        {StatusID: 3, Name: 'In Progress'},
        {StatusID: 4, Name: 'Review'},
        {StatusID: 5, Name: 'Completed'}
        ];
    localStorage: any;
    viewActive = 'MTD';
    WorkStatus = new FormControl([2, 3, 4, 5]);
  constructor(
    private store: Store<any>,
    private router: Router,
    private projectsService : ProjectsService
  ) {
    this.calendar$ = this.store.select(fromCalendar.getCalendarState);
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.RoleName == 'Service Provider'){
        this.getAllOrderPriceUnder5000();
        setInterval(()=>{
            this.getAllOrderPriceUnder5000();
        } , 120000);
    }
    if(this.currentUser && this.currentUser.ExpiryDate){
        console.log(new Date(this.currentUser.ExpiryDate))
        if(new Date(this.currentUser.ExpiryDate) < new Date()){
            this.ShowExpiryAlert = true;
        }
    }
   
    
  }

  ngOnInit() {
      this.selectedWorkArray = [
          {
              OrderName: 'Medical Help',
              CustomerName: 'Teja Ram',
              StartDate: new Date(),
              StatusID: 2,
              Cost: 500,
              PaidAmount: 300,
              DueAmount: 200,
              Location: 'Vaishali Nagar Jaipur'
          },
          {
              OrderName: 'Create filters and directives',
              CustomerName: 'Bhagirath',
              StartDate: new Date(),
              StatusID: 2,
              Cost: 200,
              PaidAmount: 180,
              DueAmount: 20,
              Location: 'D4/130 Chitrakoot Stadium'
          },
          {
              OrderName: 'Management contracts',
              CustomerName: 'Harish Choudhary',
              StartDate: new Date(),
              StatusID: 3,
              Cost: 500,
              PaidAmount: 300,
              DueAmount: 200,
              Location: 'Street-1 VT road Mansrovar'
          },
          {
              OrderName: 'Prepare property management contracts',
              CustomerName: 'Tapan sain',
              StartDate: new Date(),
              StatusID: 4,
              Cost: 500,
              PaidAmount: 300,
              DueAmount: 200,
              Location: 'Vaishali Nagar Jaipur'
          },
          {
              OrderName: 'Medical Help',
              CustomerName: 'Prakash Pradhan',
              StartDate: new Date(),
              StatusID: 5,
              Cost: 500,
              PaidAmount: 300,
              DueAmount: 200,
              Location: 'Sanganer'
          }
      ];
      this.allOrders = this.selectedWorkArray;
      this.dataSource = new MatTableDataSource(this.selectedWorkArray);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  }
    onClickProfile(){
      this.router.navigate(['profiles/createnew/1'])
    }
    getStatusName(status) {
        if (status && this.allStatus.length > 0) {
            const index = this.allStatus.findIndex(x => x.StatusID === status);
            return this.allStatus[index].Name;
        } else {
            return '';
        }
    }
    exportToExcel() {
        const localArray = this.dataSource.data.map((row) => {
            return {
                OrderName: row.TaskID ? row.TaskID.Name : row.Title,
                CustomerName: row.ClientID ? row.ClientID.Name : row.OtherClient,
                StartDate: row.StartDate,
                StatusID: this.WorkStatusArray[row.StatusID],
            };
        });
        const options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: true,
            title: 'Order List',
            useBom: true,
            noDownload: false,
            headers: ['Order Name', 'Customer Name', 'Start Date', 'Status']  /*, 'Total Cost', 'Paid Amount', 'Due Amount'*/
        };
        new Angular5Csv(localArray, 'Work List', options);
    }
    onClickOrder(){
        this.router.navigate(['projects/detail/5cc7d649a1789708dc1b20c4']);
    }
    getAllOrderByFilter(){

    }



    getAllOrderPriceUnder5000(){
        this.projectsService.getAllOrderPriceUnder5000(this.currentUser.id).subscribe( (data : {})=>{
            if(data['status']){
                this.dataSourceOrder = new MatTableDataSource(data['data']);
                this.dataSourceOrder.paginator = this.paginator;
                this.dataSourceOrder.sort = this.sort;
            }
        })
    }
}
