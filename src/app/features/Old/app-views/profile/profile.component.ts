import { Component, OnInit } from '@angular/core';
import {CommonService} from "@app/core/common/common.service";
import {UserService} from "@app/core/services";

@Component({
  selector: 'sa-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {
  localstorage : any;
  constructor(
      private commonService :CommonService,
      private userService :UserService,
  )
  {
    this.localstorage =  JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.localstorage);
    this.getOneUser(this.localstorage.id);
  }

  ngOnInit() {
  }
  getOneUser(id){
      this.userService.getOneUser(id).subscribe((data: {}) => {
          if (data['status']) {
            console.log(data['result']);
          }
      });
  }
}
